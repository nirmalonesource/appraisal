//
//  stripeviewcon.swift
//  Appraisal
//
//  Created by Hiral Mac on 28/09/18.
//  Copyright © 2018 empiereTech. All rights reserved.
//

import UIKit
import Stripe
import Alamofire
import SwiftyJSON


class stripeviewcontroller: UIViewController,UIPickerViewDelegate,UIPickerViewDataSource,UITextFieldDelegate {
    

    

    
    let Month = ["01","02","03","04","05","06","07","08","09","10","11","12"]
 
    
    
    
    @IBOutlet weak var otlpayment: UIButton!
    
    @IBOutlet weak var myview: UIView!
    
    @IBOutlet weak var DescView1: UIView!
    
    @IBOutlet weak var DescLabel: UILabel!
    
    @IBOutlet weak var txtcardnumber: UITextField!
    
    @IBOutlet weak var txtcardholdername: UITextField!
    
    @IBOutlet weak var txtmonth: UITextField!
    
    @IBOutlet weak var txtyear: UITextField!
    
    @IBOutlet weak var txtcvv: UITextField!
    
    @IBOutlet weak var txtzipcode: UITextField!
    
    var picker : UIPickerView!
    var activeTextField = 0
    var activeTF : UITextField!
    var activeValue = ""
    
    var Certificateno  = String()
    var AppraisalCosts  = String()
    var ShippingCosts  = String()
    var orderid  = String()
    var amount  = Decimal()
    var Year :[String] = []
    override func viewDidLoad()
    {
        super.viewDidLoad()
        
        
        myview.frame = CGRect.init(x: myview.frame.origin.x, y: myview.frame.origin.y, width: myview.frame.size.width, height: myview.frame.size.height + 40)
        print("************************ myview.frame.origin.y \(myview.frame.origin.y)***********************")
        print("************************ myview.frame.size.height \(myview.frame.size.height)***********************")
        
//        DescView1.frame.origin.y = myview.frame.size.height + 10
//        otlpayment.frame.origin.y = DescView1.frame.size.height + 10
//        DescLabel.frame.origin.y = otlpayment.frame.size.height + 10
       
        
        
        //otlpayment.layer.cornerRadius = otlpayment.frame.size.height/2
        
        self.title = "Check Out"
        
        for Years in 2018...2099{
            Year.append(String(Years))
        }
        //NavigationBar
        navigationController?.navigationBar.barTintColor = UIColor.white
        navigationController?.navigationBar.titleTextAttributes = [NSAttributedStringKey.foregroundColor: UIColor(red: 18/255, green: 64/255, blue: 118/255, alpha: 1),NSAttributedStringKey.font: UIFont(name: "Montserrat-Medium", size: 17)!]
        
        txtmonth.delegate = self
        txtyear.delegate = self
        txtcardnumber.bottomlinetext()
        txtcardholdername.bottomlinetext()
        txtmonth.bottomlinetext()
        txtyear.bottomlinetext()
        txtcvv.bottomlinetext()
        txtzipcode.bottomlinetext()
    }
    func allalert(title:String,message:String)
    {
        let alert = UIAlertController(title:title, message: message, preferredStyle: .alert)
        let ok = UIAlertAction(title: "ok", style: .default, handler: nil)
        alert.addAction(ok)
        self.present(alert, animated: true, completion: nil)
    }
  
    @IBAction func btnconfirmpayment(_ sender: Any)
    {
       
        if (txtcardnumber.text?.isEmpty)!
        {
            allalert(title: "Alert", message:"Please Enter Card Number")
        }
        else if (txtcardholdername.text?.isEmpty)!
        {
            allalert(title: "Alert", message: "Please Enter CardHolder Name")
        }else if (txtmonth.text?.isEmpty)!
        {
            allalert(title: "Alert", message: "Please Select Month")
        }else if (txtyear.text?.isEmpty)!
        {
            allalert(title: "Alert", message: "Please Select Year")
        }else if (txtcvv.text?.isEmpty)!
        {
            allalert(title: "Alert", message: "Please Enter CVV")
        }else if (txtcvv.text?.characters.count != 3)
        {
            allalert(title: "Alert", message: "Please Enter 3 character Number")
        }else if (txtzipcode.text?.isEmpty)!
        {
            allalert(title: "Alert", message: "Please Enter Zipcode")
        }
        else
        {
       let cardnumber = txtcardnumber.text!
       let cardholdername = txtcardholdername.text!
       let expirmonth = txtmonth.text!
       let expiryear  = txtyear.text!
       let cvc = txtcvv.text!
       let zipcode = txtzipcode.text!
            
        let cardparameters = STPCardParams()
        cardparameters.number = cardnumber
        cardparameters.name = cardholdername
        cardparameters.expMonth = UInt(expirmonth)!
        cardparameters.expYear =  UInt(String(expiryear.suffix(2)))!
        cardparameters.cvc = cvc
        cardparameters.address.postalCode = zipcode
        
        AppDelegate.shared().ShowHUD()
        STPAPIClient.shared().createToken(withCard: cardparameters, completion: { (token, error) -> Void in
            
            if error != nil {
                AppDelegate.shared().HideHUD()
                self.handleError(error: error! as NSError)
                return
            } else {
                self.CallMyMethod(token: token!)
              //  self.completeCharge(with: token!, amount: self.amount)
            }
            
            //   self.postStripeToken(token!)
        })
    }
}
    
    func   CallMyMethod(token:STPToken)
    {
       // UserDefaults.standard.set(jsonResponse.value(forKeyPath: "data.user.email"), forKey: "Email")
        let email = UserDefaults.standard.object(forKey: "Email")

        let params : Parameters = ["card_holder_name":txtcardholdername.text!,
                                   "email":email!,
                                   "tokenId": token
                                   ]
        print("param is \(params)")
        
        let url = URL(string: "\(Constants.API)\(Constants.AddUserCard)")!
        print("Url printed \(url)")
        
        let manager = Alamofire.SessionManager.default
        manager.session.configuration.timeoutIntervalForRequest = 120
        
        let login = UserDefaults.standard.string(forKey: "LoginToken")
        let UserID = UserDefaults.standard.string(forKey: "UserID")
        
        let headers = ["Authorization": "Basic YWRtaW46QVBJQEZFREVSQUwhIyRXRUIk",
                       "X-FEDERAL-API-KEY": "kkcoswggwgwkkc8w4ok808o48kswc40c0www4wss",
                       "X-FEDERAL-LOGIN-TOKEN": login!,
                       "USER-ID": UserID!]
        
        manager.request(url, method: .post, parameters: params, headers: headers).responseJSON() { response in
            switch (response.result) {
            case .success:
                //do json stuff
                
                let jsonResponse = response.result.value as! NSDictionary
                print("JsonResponse printed \(jsonResponse)")
                
                let mydic:NSDictionary = jsonResponse.value(forKey: "data") as! NSDictionary
              //  self.completeCharge(with: (mydic.value(forKey: "Customer_id") as! String), amount: self.amount)
             //   self.completeCharge(with:  (mydic.value(forKey: "Customer_id") as! String), CustCardId:  (mydic.value(forKey: "Card_id") as! String), amount: self.amount)
                self.completeCharge(with:  (mydic.value(forKey: "Customer_id") as! String), CustCardId: (mydic.value(forKey: "Card_id") as! String), amount: self.amount, UserStripeid: (mydic.value(forKey: "UserStripeID") as! Int))
                break
                
            case .failure(let error):
                if error._code == NSURLErrorTimedOut {
                    //HANDLE TIMEOUT HERE
                }
                 AppDelegate.shared().HideHUD()
                print("\n\nAuth request failed with error:\n \(error)")
                break
            }
        }
    }
    
    func completeCharge(with Custid: String,CustCardId:String, amount: Decimal,UserStripeid:Int) {
        // 1
        
        // 2
//        let parameters: Parameters=[
//            "source":token,
//            "amount":amount*100,
//            "currency":Constants.DefultCurrency,
//            "description":self.orderid
//            ]
        let mydic:NSDictionary = UserDefaults.standard.object(forKey: "storedata") as! NSDictionary

        let parameters: Parameters=[
            "description":mydic.value(forKey: "orderid") as! String,
            "amount": Int(truncating: mydic.value(forKey: "amount") as! NSNumber)*100,
            "currency":Constants.DefultCurrency,
            "source":CustCardId,
            "customer":Custid
        ]
        print("Parameters : ",parameters)
        
        let headers = [
         //   "authorization": Constants.KEY_TEST,
            "authorization": Constants.KEY_LIVE,
            "content-type": "application/x-www-form-urlencoded",
            "cache-control": "no-cache",
            "postman-token": "fc7c5030-35b5-4974-64a3-54a48f0ded96"
        ]

        print("headers : ",headers)
        
        let manager = Alamofire.SessionManager.default
        let url = URL(string:  Constants.StripeChargeURL)!
        
        manager.session.configuration.timeoutIntervalForRequest = 120
        manager.request(url, method: .post, parameters: parameters, encoding: URLEncoding.default, headers : headers).responseJSON() { response in
            switch (response.result) {
            case .success:
                //do json stuff
              
                print("JsonResponse printed \(response)")
                let jsonResponse = response.result.value as! NSDictionary
                print("JsonResponse printed \(jsonResponse)")
                
            //    let datadic:NSDictionary = jsonResponse.value(forKey: "data") as! NSDictionary
                
                //  self.ShipmentyMethod(OrderID:mydic.value(forKey: "orderid") as! String, Certificate_No : mydic.value(forKey: "Certificateno") as! String, TransactionNo: jsonResponse["id"]! as! String)
                self.ShipmentyMethod(OrderID: mydic.value(forKey: "orderid") as! String, Certificate_No: mydic.value(forKey: "Certificateno") as! String, TransactionNo: jsonResponse["id"]! as! String, UserStripeid: UserStripeid)
                break
                
            case .failure(let error):
                AppDelegate.shared().HideHUD()
                if error._code == NSURLErrorTimedOut {
                    //HANDLE TIMEOUT HERE
                }
                AppDelegate.shared().HideHUD()
                print("\n\nAuth request failed with error:\n \(error)")
                break
            }
        }
        
        
    }
    
    func ShipmentyMethod(OrderID : String, Certificate_No : String, TransactionNo : String, UserStripeid:Int)
    {
        AppDelegate.shared().ShowHUD()
        let url = URL(string: "\(Constants.API)\(Constants.AddOrderPayment)")!
        print("Url printed \(url)")
        
        var params : Parameters = [String: String]()
        
        let mydic:NSDictionary = UserDefaults.standard.object(forKey: "storedata") as! NSDictionary
        
        let AppraisalCost = mydic.value(forKey: "AppraisalCosts") as! String
        let Final = Int(truncating: mydic.value(forKey: "amount") as! NSNumber)
        let Shipping = mydic.value(forKey: "ShippingCosts") as! String
        
        params = ["Status":"apporved","OrderID":OrderID,"TransactionNo":TransactionNo,"FeeAmount":AppraisalCost,"Discount":"0","Total":Final,"Note":"abvc","ShippingAmount":Shipping,"UserStripreID":UserStripeid]
        print("Get Param \(params)")
        
        let manager = Alamofire.SessionManager.default
        manager.session.configuration.timeoutIntervalForRequest = 120
        
        let login = UserDefaults.standard.string(forKey: "LoginToken")
        let UserID = UserDefaults.standard.string(forKey: "UserID")
        
        let headers = ["Authorization": "Basic YWRtaW46QVBJQEZFREVSQUwhIyRXRUIk",
                       "X-FEDERAL-API-KEY": "kkcoswggwgwkkc8w4ok808o48kswc40c0www4wss",
                       "X-FEDERAL-LOGIN-TOKEN": login!,
                       "USER-ID": UserID!]
        
        manager.request(url, method: .post, parameters: params, headers: headers).responseJSON() { response in
            switch (response.result) {
            case .success:
                //do json stuff
                let jsonResponse = response.result.value as! NSDictionary
                print("JsonResponse printed \(jsonResponse)")
                
                AppDelegate.shared().HideHUD()
                let myInt: Int = jsonResponse["status"]! as! Int
                
                if(myInt == 1)
                {
                    let storyboard = UIStoryboard(name:"Main", bundle:nil)
                    let home = storyboard.instantiateViewController(withIdentifier: "OrderSuccessViewController")as! OrderSuccessViewController
                    UserDefaults.standard.set(myInt, forKey: "status")
                    
                    let Paid : String = "\("Paid : $ ")\(jsonResponse.value(forKeyPath: "data.paid") as! String)"
                    UserDefaults.standard.set(Paid, forKey: "Paid")
                    
                    let Shipping : String = jsonResponse.value(forKeyPath: "data.shipping_method") as! String
                    UserDefaults.standard.set(Shipping, forKey: "Order")
                    
                    let Cert : String = "\("Certificate # ") \(jsonResponse.value(forKeyPath: "data.certificate_no") as! String)"
                    UserDefaults.standard.set(Cert, forKey: "Certificate")
                    
                    
                    self.navigationController?.pushViewController(home, animated: true)
                }
                
                break
                
            case .failure(let error):
                AppDelegate.shared().HideHUD()
                if error._code == NSURLErrorTimedOut {
                    //HANDLE TIMEOUT HERE
                    AppDelegate.shared().ShowAlert(title: Constants.ERROR_HEADER, msg: error.localizedDescription)
                }
                print("\n\nAuth request failed with error:\n \(error)")
                break
            }
        }
    }
    func handleError(error: NSError) {
        UIAlertView(title: "Please Try Again",
                    message: error.localizedDescription,
                    delegate: nil,
                    cancelButtonTitle: "OK").show()
        
    }
    func numberOfComponents(in pickerView: UIPickerView) -> Int
    {
        return 1
    }
    
    func pickerView(_ pickerView: UIPickerView, numberOfRowsInComponent component: Int) -> Int
    {
        switch activeTextField {
        case 1:
            return Month.count
        case 2:
            return Year.count
        
            
        default:
            return 0
        }
    }
   
    
    func pickerView(_ pickerView: UIPickerView, titleForRow row: Int, forComponent component: Int) -> String?
    {
        switch activeTextField {
        case 1:
            return Month[row]
        case 2:
            return Year[row]
        
            
        default:
            return ""
        }
    }
    
    // get currect value for picker view
    func pickerView(_ pickerView: UIPickerView, didSelectRow row: Int, inComponent component: Int)
    {
        switch activeTextField {
        case 1:
            activeValue = Month[row]
            print("ACTIVE VALUE >> ",activeValue)
        case 2:
            activeValue = Year[row]
       
            
        default:
            activeValue = ""
        }
    }
    
    func textFieldDidBeginEditing(_ textField: UITextField)
    {
        switch textField {
        case txtmonth:
            activeTextField = 1
            self.pickUpValue(textField: textField)
        case  txtyear:
            activeTextField = 2
            self.pickUpValue(textField: textField)
        default:
            activeTextField = 0
        }
        activeTF = textField
        
        //self.pickUpValue(textField: textField)
        
    }
    func pickUpValue(textField: UITextField)
    {
        
        picker = UIPickerView(frame:CGRect(origin: CGPoint(x: 0, y: 0), size: CGSize(width: self.view.frame.size.width, height: 216)))
        
        picker.delegate = self
        picker.dataSource = self
        
        if let currentValue = textField.text
        {
            
            var row : Int?
            
            switch activeTextField {
            case 1:
                row = Month.index(of: currentValue)
            case 2:
                row = Year.index(of: currentValue)
           
            default:
                row = nil
            }
            
            if row != nil
            {
                picker.selectRow(row!, inComponent: 0, animated: true)
            }
        }
        
        picker.backgroundColor = UIColor.white
        textField.inputView = self.picker
        
        let toolBar = UIToolbar()
        toolBar.barStyle = .default
        toolBar.isTranslucent = true
        toolBar.barTintColor = UIColor.darkGray
        toolBar.sizeToFit()
        
        
        let doneButton = UIBarButtonItem(title: "Done", style: .plain, target: self, action: #selector(doneClick))
        let spaceButton = UIBarButtonItem(barButtonSystemItem: .flexibleSpace, target: nil, action: nil)
        let cancelButton = UIBarButtonItem(title: "Cancel", style: .plain, target: self, action: #selector(cancelClick))
        toolBar.setItems([cancelButton, spaceButton, doneButton], animated: false)
        toolBar.isUserInteractionEnabled = true
        textField.inputAccessoryView = toolBar
        
    }
    @objc func doneClick()
    {        print("ACTIVE TF >>",activeTF,"ACTIVE value",activeValue)

        activeTF.text = String(activeValue)
        activeTF.resignFirstResponder()
    }
    @objc func cancelClick()
    {
        activeTF.resignFirstResponder()
    }

    
}
