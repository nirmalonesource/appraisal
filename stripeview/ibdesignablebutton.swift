//
//  ibdesignablebutton.swift
//  Appraisal
//
//  Created by Hiral Mac on 02/10/18.
//  Copyright © 2018 empiereTech. All rights reserved.
//

import UIKit

class ibdesignablebutton: UIButton {

    @IBInspectable
    public var cornerradius:CGFloat = 2.0
    {
        didSet{
            self.layer.cornerRadius = self.cornerradius
        }
    }
}
