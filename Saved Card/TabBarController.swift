//
//  TabBarController.swift
//  Appraisal
//
//  Created by My Mac on 06/03/19.
//  Copyright © 2019 Blenzabi. All rights reserved.
//

import UIKit

class TabBarController: UITabBarController {
    var Certificateno  = String()
    var AppraisalCosts  = String()
    var ShippingCosts  = String()
    var orderid  = String()
    var amount  = Decimal()
    @IBOutlet weak var tabbar: UITabBar!
    override func viewDidLoad() {
        super.viewDidLoad()

        // Do any additional setup after loading the view.
        
        let appearance = UITabBarItem.appearance(whenContainedInInstancesOf: [TabBarController.self])
        appearance.setTitleTextAttributes([NSAttributedStringKey.foregroundColor: UIColor.gray], for: .normal)
        appearance.setTitleTextAttributes([NSAttributedStringKey.foregroundColor: UIColor.white], for: .selected)
        
   
    }
    override func viewDidLayoutSubviews()
    {
       // tabBar.frame = CGRect(x: 0, y: yNavBar! + yStatusBar + tabbar.frame.size.height, width: tabbar.frame.size.width, height: tabbar.frame.size.height)
        let yNavBar = self.navigationController?.navigationBar.frame.size.height
        // yStatusBar indicates the height of the status bar
      //  let yStatusBar = UIApplication.shared.statusBarFrame.size.height
        // Set the size and the position in the screen of the tab bar
       // tabbar.frame = CGRect(x: 0, y: yNavBar! + tabbar.frame.size.height, width: tabbar.frame.size.width, height: tabbar.frame.size.height)
         tabbar.frame = CGRect(x: 0, y: yNavBar!+20, width: tabbar.frame.size.width, height: tabbar.frame.size.height)
    }
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    

    /*
    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        // Get the new view controller using segue.destinationViewController.
        // Pass the selected object to the new view controller.
    }
    */

}
