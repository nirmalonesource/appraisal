//
//  SavedCardCell.swift
//  Appraisal
//
//  Created by My Mac on 06/03/19.
//  Copyright © 2019 Blenzabi. All rights reserved.
//

import UIKit

class SavedCardCell: UITableViewCell {

    @IBOutlet weak var lblcardno: UILabel!
    @IBOutlet weak var lblcardname: UILabel!
    @IBOutlet weak var lbldate: UILabel!
    @IBOutlet weak var lblcardtype: UILabel!
    @IBOutlet weak var BGView: UIView!
    var btnpay : (() -> Void)? = nil
    var btnremove : (() -> Void)? = nil

    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }

    @IBAction func pay_click(_ sender: UIButton) {
        
        if let pay_click = self.btnpay
        {
            pay_click()
        }
    }
    @IBAction func remove_click(_ sender: UIButton) {
        if let rmv_click = self.btnremove
        {
            rmv_click()
        }
        
    }
    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }

}
