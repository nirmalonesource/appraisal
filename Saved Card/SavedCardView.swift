//
//  SavedCardView.swift
//  Appraisal
//
//  Created by My Mac on 06/03/19.
//  Copyright © 2019 Blenzabi. All rights reserved.
//

import UIKit
import Alamofire
import SwiftyJSON
import Stripe

class SavedCardView: UIViewController,UITableViewDelegate,UITableViewDataSource
{

    @IBOutlet weak var scrollvview: UIScrollView!
    @IBOutlet weak var tblsavedcard: UITableView!
    var cardarray = NSMutableArray()

    override func viewDidLoad() {
        super.viewDidLoad()
        // Do any additional setup after loading the view.
      //   let yNavBar = self.navigationController?.navigationBar.frame.size.height
        scrollvview.contentSize = CGSize(width: self.view.frame.size.width, height: 690)
//        scrollvview.frame = CGRect(x: self.view.frame.origin.x, y: yNavBar!+50, width: self.view.frame.size.width, height: 700)
        CallMyMethod()
    }
    
    //MARK: - Delegates✋🏻😇
    
    //tableView numberOfRowsInSection
    public func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int
    {
        return(cardarray.count)
    }
    
    //tableView cellForRowAt
    public func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell
    {
        let cell:SavedCardCell = tableView.dequeueReusableCell(withIdentifier: "cell") as! SavedCardCell
        
        let mydic:NSDictionary = cardarray.object(at: indexPath.row) as! NSDictionary
        
        cell.lblcardno?.text = "XXXXXXXXXXXX" + String(mydic.value(forKey: "lastfourdigit") as! String)
        cell.lblcardname?.text = mydic.value(forKey: "CardHolderName") as? String
        
        let exp_month = mydic.value(forKey: "exp_month") as? String
        let exp_year = mydic.value(forKey: "exp_year") as? String
        let datestring = String(format: "%@/%@", exp_month!,exp_year!)
        cell.lbldate?.text = datestring
        
        cell.lblcardtype?.text = mydic.value(forKey: "brand") as? String
        
        cell.BGView.layer.cornerRadius = 10.0
        cell.BGView.isUserInteractionEnabled = true
        cell.BGView.layer.masksToBounds = false
        cell.BGView.layer.shadowOffset = CGSize.zero
        cell.BGView.layer.shadowRadius = 5
        cell.BGView.layer.shadowOpacity = 0.2
        
        cell.btnpay =
        {
            let fvc = self.storyboard?.instantiateViewController(withIdentifier: "TabBarController") as! TabBarController

            print("MYAMT:",fvc.amount)
         //   self.completeCharge(custid: (mydic.value(forKey: "CustomerID") as? String)!)
          //  self.completeCharge(with: token!, amount: self.amount)
            
           self.completeCharge(with:  (mydic.value(forKey: "CustomerID") as? String)!, custcardid:  (mydic.value(forKey: "CustomerCardID") as? String)!, amount: fvc.amount, UserStripeid: (mydic.value(forKey: "UserStripreID") as! String))
        }
        cell.btnremove =
            {
                let alertController = UIAlertController(title: "Alert", message: "Are you sure you want to remove this card?", preferredStyle: .alert)
                
                let cancelAction = UIAlertAction(title: "Cancel", style: .cancel) { (action) in
                    // ...
                }
                alertController.addAction(cancelAction)
                
                let OKAction = UIAlertAction(title: "OK", style: .default) { (action) in
                    // ...
                    self.cardarray.removeObject(at: indexPath.row)
                    let stripeid = mydic.value(forKey: "UserStripreID") as? String
                    self.CallREMOVECARDAPI(Userstripeid: stripeid!)
                }
                alertController.addAction(OKAction)
                self.present(alertController, animated: true, completion: nil)
             
                
            }
        
        return cell
    }
    
    //tableView didSelectRowAt
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        // do with place whatever you want
        
        tableView.deselectRow(at: indexPath as IndexPath, animated: true)
        

        
    }
    
    //MARK: - API🤞🏻🙄
    
    func  CallREMOVECARDAPI(Userstripeid:String)
    {
        let params : Parameters = ["UserStripreID":Userstripeid]
        print("param is \(params)")
        let url = URL(string: "\(Constants.API)\(Constants.RemoveCard)")!
        print("Url printed \(url)")
        
        let manager = Alamofire.SessionManager.default
        manager.session.configuration.timeoutIntervalForRequest = 120
        
        let login = UserDefaults.standard.string(forKey: "LoginToken")
        let UserID = UserDefaults.standard.string(forKey: "UserID")
        
        let headers = ["Authorization": "Basic YWRtaW46QVBJQEZFREVSQUwhIyRXRUIk",
                       "X-FEDERAL-API-KEY": "kkcoswggwgwkkc8w4ok808o48kswc40c0www4wss",
                       "X-FEDERAL-LOGIN-TOKEN": login!,
                       "USER-ID": UserID!]
        
        manager.request(url, method: .post, parameters: params, headers: headers).responseJSON() { response in
            switch (response.result) {
            case .success:
                //do json stuff
                
                let jsonResponse = response.result.value as! NSDictionary
                print("JsonResponse printed \(jsonResponse)")
              //  self.cardarray = (jsonResponse.value(forKey: "data") as! NSArray).mutableCopy() as! NSMutableArray
                self.tblsavedcard.reloadData()
                
                //                let revealController: SWRevealViewController = self.revealViewController()
                //                let storyboard = UIStoryboard(name:"Main", bundle:nil)
                //                let home = storyboard.instantiateViewController(withIdentifier: "LoginViewController")as! LoginViewController
                //                self.newFrontController = home
                //                let navigationController = UINavigationController(rootViewController: self.newFrontController)
                //                self.revealViewController()?.pushFrontViewController(navigationController, animated: true)
                
                break
                
            case .failure(let error):
                if error._code == NSURLErrorTimedOut {
                    //HANDLE TIMEOUT HERE
                }
                print("\n\nAuth request failed with error:\n \(error)")
                break
            }
        }
    }
    
    func   CallMyMethod()
    {
        let params : Parameters = ["device_id":"121"]
        print("param is \(params)")
        let url = URL(string: "\(Constants.API)\(Constants.UserCardList)")!
        print("Url printed \(url)")
        
        let manager = Alamofire.SessionManager.default
        manager.session.configuration.timeoutIntervalForRequest = 120
        
        let login = UserDefaults.standard.string(forKey: "LoginToken")
        let UserID = UserDefaults.standard.string(forKey: "UserID")
        
        let headers = ["Authorization": "Basic YWRtaW46QVBJQEZFREVSQUwhIyRXRUIk",
                       "X-FEDERAL-API-KEY": "kkcoswggwgwkkc8w4ok808o48kswc40c0www4wss",
                       "X-FEDERAL-LOGIN-TOKEN": login!,
                       "USER-ID": UserID!]
        
        manager.request(url, method: .get, parameters: params, headers: headers).responseJSON() { response in
            switch (response.result) {
            case .success:
                //do json stuff
                
                let jsonResponse = response.result.value as! NSDictionary
                print("JsonResponse printed \(jsonResponse)")
                if ((jsonResponse.value(forKey: "data") as? NSArray) != nil)
                {
                     self.cardarray = (jsonResponse.value(forKey: "data") as? NSArray)?.mutableCopy() as? NSMutableArray ?? []
                }
               
                self.tblsavedcard.reloadData()
                
                
//                let revealController: SWRevealViewController = self.revealViewController()
//                let storyboard = UIStoryboard(name:"Main", bundle:nil)
//                let home = storyboard.instantiateViewController(withIdentifier: "LoginViewController")as! LoginViewController
//                self.newFrontController = home
//                let navigationController = UINavigationController(rootViewController: self.newFrontController)
//                self.revealViewController()?.pushFrontViewController(navigationController, animated: true)
                
                break
                
            case .failure(let error):
                if error._code == NSURLErrorTimedOut {
                    //HANDLE TIMEOUT HERE
                }
                print("\n\nAuth request failed with error:\n \(error)")
                break
            }
        }
    }
    
    func completeCharge(with custid: String,custcardid:String, amount: Decimal,UserStripeid:String) {
        // 1
       // let fvc = self.storyboard?.instantiateViewController(withIdentifier: "TabBarController") as! TabBarController
        AppDelegate.shared().ShowHUD()
        let mydic:NSDictionary = UserDefaults.standard.object(forKey: "storedata") as! NSDictionary
        // 2
        let parameters: Parameters=[
            "description":mydic.value(forKey: "orderid") as Any,
            "amount":Int(truncating: mydic.value(forKey: "amount") as! NSNumber)*100,
            "currency":Constants.DefultCurrency,
            "source":custcardid,
            "customer":custid
        ]
        print("Parameters : ",parameters)
//        "amount" => $amount_cents,
//        "currency" => "usd",
//        "customer" => $customer,
//        "source" => $card_id
        let headers = [
            
         //    "authorization": Constants.KEY_TEST,
            "authorization": Constants.KEY_LIVE,
            "content-type": "application/x-www-form-urlencoded",
            "cache-control": "no-cache",
            "postman-token": "fc7c5030-35b5-4974-64a3-54a48f0ded96"
        ]
        
        print("headers : ",headers)
        
        let manager = Alamofire.SessionManager.default
        let url = URL(string:  Constants.StripeChargeURL)!
        
        manager.session.configuration.timeoutIntervalForRequest = 120
        manager.request(url, method: .post, parameters: parameters, encoding: URLEncoding.default, headers : headers).responseJSON() { response in
            switch (response.result) {
            case .success:
                //do json stuff
                
                print("JsonResponse printed \(response)")
                let jsonResponse = response.result.value as! NSDictionary
                print("JsonResponse printed \(jsonResponse)")
//                AppDelegate.shared().HideHUD()
//                let myInt: Int = jsonResponse["status"]! as! Int
//
//                if(myInt == 1)
//                {
                    self.ShipmentyMethod(OrderID:mydic.value(forKey: "orderid") as! String, Certificate_No : mydic.value(forKey: "Certificateno") as! String, TransactionNo: jsonResponse["id"] as? String ?? "", UserStripeid: UserStripeid)

//                }else{
//                     AppDelegate.shared().ShowAlert(title: Constants.ERROR_HEADER, msg:"Payment Failed!")
//                }
                break
                
            case .failure(let error):
                AppDelegate.shared().HideHUD()
                if error._code == NSURLErrorTimedOut {
                    //HANDLE TIMEOUT HERE
                }
                print("\n\nAuth request failed with error:\n \(error)")
                break
            }
        }
        
    }
    
    func ShipmentyMethod(OrderID : String, Certificate_No : String, TransactionNo : String, UserStripeid:String)
    {
        AppDelegate.shared().ShowHUD()
        let url = URL(string: "\(Constants.API)\(Constants.AddOrderPayment)")!
        print("Url printed \(url)")
        
        var params : Parameters = [String: String]()
        
        let mydic:NSDictionary = UserDefaults.standard.object(forKey: "storedata") as! NSDictionary
        
        let AppraisalCost = mydic.value(forKey: "AppraisalCosts") as! String
        let Final = Int(truncating: mydic.value(forKey: "amount") as! NSNumber)
        let Shipping = mydic.value(forKey: "ShippingCosts") as! String
        
        params = ["Status":"apporved","OrderID":OrderID,"TransactionNo":TransactionNo,"FeeAmount":AppraisalCost,"Discount":"0","Total":Final,"Note":"abvc","ShippingAmount":Shipping,"UserStripreID":UserStripeid]
        print("Get Param \(params)")
        
        let manager = Alamofire.SessionManager.default
        manager.session.configuration.timeoutIntervalForRequest = 120
        
        let login = UserDefaults.standard.string(forKey: "LoginToken")
        let UserID = UserDefaults.standard.string(forKey: "UserID")
        
        let headers = ["Authorization": "Basic YWRtaW46QVBJQEZFREVSQUwhIyRXRUIk",
                       "X-FEDERAL-API-KEY": "kkcoswggwgwkkc8w4ok808o48kswc40c0www4wss",
                       "X-FEDERAL-LOGIN-TOKEN": login!,
                       "USER-ID": UserID!]
        
        manager.request(url, method: .post, parameters: params, headers: headers).responseJSON() { response in
            switch (response.result) {
            case .success:
                //do json stuff
                let jsonResponse = response.result.value as! NSDictionary
                print("JsonResponse printed \(jsonResponse)")
                
                AppDelegate.shared().HideHUD()
                let myInt: Int = jsonResponse["status"]! as! Int
                
                if(myInt == 1)
                {
                    let storyboard = UIStoryboard(name:"Main", bundle:nil)
                    let home = storyboard.instantiateViewController(withIdentifier: "OrderSuccessViewController")as! OrderSuccessViewController
                    UserDefaults.standard.set(myInt, forKey: "status")
                    
                    let Paid : String = "\("Paid : $ ")\(jsonResponse.value(forKeyPath: "data.paid") as! String)"
                    UserDefaults.standard.set(Paid, forKey: "Paid")
                    
                    let Shipping : String = jsonResponse.value(forKeyPath: "data.shipping_method") as! String
                    UserDefaults.standard.set(Shipping, forKey: "Order")
                    
                    let Cert : String = "\("Certificate # ") \(jsonResponse.value(forKeyPath: "data.certificate_no") as! String)"
                    UserDefaults.standard.set(Cert, forKey: "Certificate")
                    
                    
                    self.navigationController?.pushViewController(home, animated: true)
                }else{
                    AppDelegate.shared().ShowAlert(title: "Payment Failed!", msg:"")
                  }
                
                break
                
            case .failure(let error):
                AppDelegate.shared().HideHUD()
                if error._code == NSURLErrorTimedOut {
                    //HANDLE TIMEOUT HERE
                    AppDelegate.shared().ShowAlert(title: Constants.ERROR_HEADER, msg: error.localizedDescription)
                }
                print("\n\nAuth request failed with error:\n \(error)")
                break
            }
        }
    }
   
}
