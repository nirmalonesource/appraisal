//
//  ChatDetailViewController.swift
//  Appraisal
//
//  Created by My Mac on 24/04/20.
//  Copyright © 2020 Blenzabi. All rights reserved.
//


import UIKit
import Alamofire
import SDWebImage

class sendercell: UITableViewCell {
    @IBOutlet var imgsender: UIImageView!
    @IBOutlet var lblsender: UILabel!
    @IBOutlet var lblsendertime: UILabel!
    @IBOutlet var imgheight: NSLayoutConstraint!
    
}

class receivercell: UITableViewCell {
    
    @IBOutlet var imgreceiver: UIImageView!
    @IBOutlet var lblreceiver: UILabel!
    @IBOutlet var lblreceivertime: UILabel!
    @IBOutlet var imgheight: NSLayoutConstraint!
    
}

class ChatDetailViewController: UIViewController, UITextFieldDelegate,UIImagePickerControllerDelegate,MyProtocol,UINavigationControllerDelegate {
    func setResultOfBusinessLogic(valueSent: UIImage) {
        print("Editediimage:",valueSent)
        ImageItem.removeAll()
         //     print("valueSent \(valueSent)")
            //  let photoItem = JSQPhotoMediaItem(image: valueSent)
        
              ImageItem.append(valueSent)
        
         CallMyMethod(OrderID: OrderID as String, Comment: "", CommentType: "I")
    }
    
    
    @IBOutlet var tblchat: UITableView!
    @IBOutlet var lblusername: UILabel!
    @IBOutlet weak var img_Profile: UIImageView!
    @IBOutlet weak var btn_sendmsg: UIButton!
    @IBOutlet var lbltyping: UILabel!
    @IBOutlet weak var txt_message: UITextField!
    @IBOutlet var img_bottomprofile: UIImageView!
    @IBOutlet var vwpopuptop: UIView!
    
    @IBAction func btnBackAction(_ sender: UIButton) {
        self.navigationController?.popViewController(animated: true)
        }
    
    var dataDict : NSDictionary! = nil
    var chatBubblesArray1:NSArray = []
    var chatHistory:[[String:Any]] = []
    var MessageArray:[[String:Any]] = []
    var dic_DataDetail : [String: Any]?
    
    var getbookingid:String? = ""
    var getridename:String? = ""
    var getstatus:String? = ""
    
    var ReceiverID:String = ""
    var messagesArray : NSMutableArray = []
    var SenderID : String = ""
    var currentUserName : String = ""
    var currentUserimg : String = ""
    var ReciverType : String = ""
    var DATAS: String = ""
    var UserimgURL: String = ""
     var OrderID: String = ""
    
    let picker = UIImagePickerController()
    var ImageItem = [UIImage]()

    override func viewWillAppear(_ animated: Bool) {
         self.navigationController?.navigationBar.isHidden = true
    }
    override func viewDidLoad()
    {
        super.viewDidLoad()

               txt_message.setLeftPaddingPoints(30, strImage: "")
               txt_message.layer.masksToBounds = true
               txt_message.layer.cornerRadius = 20.0
               txt_message.setRadiusBorder(color: UIColor.white)
        
        if dic_DataDetail != nil
        {
            print("dic_DataDetail BB ========>>>>>> ",dic_DataDetail as Any)
            print("dic_DataDetail AA ========>>>>>> ",dic_DataDetail!["BookingStatus"] as Any)
        }
        else
        {
        }
       

        
    }

    
    @IBAction func attachment_click(_ sender: UIButton) {
        self.view.endEditing(true)
        picker.delegate = self
               picker.allowsEditing = false
               picker.sourceType = .photoLibrary
             //  picker.mediaTypes = UIImagePickerController.availableMediaTypes(for: .photoLibrary)!
               present(picker, animated: true, completion: nil)
    }
    
    func imagePickerControllerDidCancel(_ picker: UIImagePickerController) {
           dismiss(animated: true, completion: nil)
       }
       
       func imagePickerController(_ picker: UIImagePickerController, didFinishPickingMediaWithInfo info: [String : Any]) {
           NSLog("\(info)")
           let image = info[UIImagePickerControllerOriginalImage] as? UIImage
           imagePickerController(picker, pickedImage: image, info: info)
       }
       
       @objc func imagePickerController(_ picker: UIImagePickerController, pickedImage: UIImage?, info: [String : Any]) {
           let chosenImage = info[UIImagePickerControllerOriginalImage] as! UIImage //2
           
           let storyboard = UIStoryboard(name:"Main", bundle:nil)
           let home = storyboard.instantiateViewController(withIdentifier: "ReportIssueViewController")as! ReportIssueViewController
           home.GetChat = true
           home.GetChatImage = chosenImage
           home.delegate = self
           self.navigationController?.pushViewController(home, animated: true)
           dismiss(animated:true, completion: nil) //5
       }
    
    @IBAction func btn_SENDMSG_A(_ sender: Any) {
        ImageItem.removeAll()
        if txt_message.text?.trimmingCharacters(in: .whitespaces) != ""
        {
              
            CallMyMethod(OrderID: OrderID as String, Comment: txt_message.text ?? "", CommentType: "T")
             self.txt_message.text = ""
        }
        else
        {
            AppDelegate.shared().ShowAlert(title: "", msg: "Please Enter Message")
        }
    }
   
    func scrollToBottom(animated: Bool) {
        let section = self.tblchat.numberOfSections
        let row = self.tblchat.numberOfRows(inSection: self.tblchat.numberOfSections - 1) - 1;
        guard (section > 0) && (row > 0) else{ // check bounds
            return
        }
        let indexPath = IndexPath(row: row-1, section: section-1)
        self.tblchat.scrollToRow(at: indexPath, at: .top, animated: true)
    }
    
    func CallMyMethod(OrderID : String, Comment : String, CommentType: String)
      {
          AppDelegate.shared().ShowHUD()
          let params : Parameters = ["OrderID":OrderID,"Comment":Comment,"CommentType":CommentType]
          print("param is \(params)")
          
          let url = URL(string: "\(Constants.API)\(Constants.AddOrderComment)")!
          print("Url printed \(url)")
          
          let manager = Alamofire.SessionManager.default
          manager.session.configuration.timeoutIntervalForRequest = 120
          
          let login = UserDefaults.standard.string(forKey: "LoginToken")
          let UserID = UserDefaults.standard.string(forKey: "UserID")
          
          let headers = ["Authorization": "Basic YWRtaW46QVBJQEZFREVSQUwhIyRXRUIk",
                         "X-FEDERAL-API-KEY": "kkcoswggwgwkkc8w4ok808o48kswc40c0www4wss",
                         "X-FEDERAL-LOGIN-TOKEN": login!,
                         "USER-ID": UserID!]
          print("param headers is \(headers)")
          
          
          if CommentType == "T"
          {
              manager.request(url, method: .post, parameters: params, headers: headers).responseJSON() { response in
                  switch (response.result) {
                  case .success:
                      //do json stuff
                      let jsonResponse = response.result.value as! NSDictionary
                      print("JsonResponse printed \(jsonResponse)")
                      
                      AppDelegate.shared().HideHUD()
                      
                      let myInt: Int = jsonResponse["status"]! as! Int
                      
                      if(myInt == 1)
                      {
                        self.GetCommentsMethod(OrderID: OrderID)
                      }
                      
                      break
                      
                  case .failure(let error):
                      AppDelegate.shared().HideHUD()
                      if error._code == NSURLErrorTimedOut {
                          //HANDLE TIMEOUT HERE
                          AppDelegate.shared().ShowAlert(title: Constants.ERROR_HEADER, msg: error.localizedDescription)
                      }
                      print("\n\nAuth request failed with error:\n \(error)")
                      break
                  }
              }
              return
          }
          else if CommentType == "I"
          {
              //Upload Image
              Alamofire.upload(multipartFormData: { (multipartFormData) in
                  
                  for var i in 0..<self.ImageItem.count
                  {
                      let imageData: NSData = UIImagePNGRepresentation(self.ImageItem[i])! as NSData
                      let image: UIImage = UIImage(data:imageData as Data,scale:1.0)!
                      
                      multipartFormData.append(UIImageJPEGRepresentation(image, 1)!, withName: "image", fileName: "photos.jpg", mimeType: "image/jpeg")
                      for (key, value) in params {
                          multipartFormData.append((value as AnyObject).data(using: String.Encoding.utf8.rawValue)!, withName: key)
                      }
                  }
                  
              }, to:url, method:.post,headers:headers)
              { (result) in
                  
                  switch result {
                  case .success(let upload, _, _):
                      
                      upload.uploadProgress(closure: { (progress) in
                          //Print progress
                          print(progress)
                      })
                      
                      upload.responseJSON { response in
                          //print response.result
                          
                          AppDelegate.shared().HideHUD()
                          let jsonResponse = response.result.value as! NSDictionary
                          print("JsonResponse printed \(jsonResponse)")
                          
                          AppDelegate.shared().HideHUD()
                          let myInt: Bool = jsonResponse["status"]! as! Bool
                          
                          if(myInt == true)
                          {
                              print("Success")
                            self.GetCommentsMethod(OrderID: OrderID)
                          }
                      }
                      break
                  case .failure(let encodingError):
                      //print encodingError.description
                      AppDelegate.shared().HideHUD()
                      //HUD Hide With Error
                      
                      break
                  }
              }
          }
   
      }
    
    
     func GetCommentsMethod(OrderID : String)
        {
            AppDelegate.shared().ShowHUD()
            let url = URL(string: "\(Constants.API)\(Constants.GetrderComment)\(OrderID)")!
            print("Url printed \(url)")
            
            let manager = Alamofire.SessionManager.default
            manager.session.configuration.timeoutIntervalForRequest = 120
            
            let login = UserDefaults.standard.string(forKey: "LoginToken")
            let UserID = UserDefaults.standard.string(forKey: "UserID")
            
            let headers = ["Authorization": "Basic YWRtaW46QVBJQEZFREVSQUwhIyRXRUIk",
                           "X-FEDERAL-API-KEY": "kkcoswggwgwkkc8w4ok808o48kswc40c0www4wss",
                           "X-FEDERAL-LOGIN-TOKEN": login!,
                           "USER-ID": UserID!]
            
            manager.request(url, method: .get, parameters: nil, headers: headers).responseJSON() { response in
                switch (response.result) {
                case .success:
                    //do json stuff
                    let jsonResponse = response.result.value as! NSDictionary
                    print("JsonResponse printed \(jsonResponse)")
                    
                    AppDelegate.shared().HideHUD()
                    let myInt: Bool = jsonResponse["status"]! as! Bool
                    
                    if(myInt == true)
                    {
                        
                        var Chatarray = [[String:AnyObject]]() //Array of dictionary
                        let dict = jsonResponse
                        Chatarray = dict["data"]! as! [[String:AnyObject]]
                        self.messagesArray = (dict["data"] as! NSArray).mutableCopy() as! NSMutableArray
                        self.tblchat.reloadData()
                         self.scrollToBottom(animated: true)

                    }
                    
                    break
                    
                case .failure(let error):
                    AppDelegate.shared().HideHUD()
                    if error._code == NSURLErrorTimedOut {
                        //HANDLE TIMEOUT HERE
                    }
                    print("\n\nAuth request failed with error:\n \(error)")
                    break
                }
            }
        }
}



extension ChatDetailViewController : UITableViewDataSource,UITableViewDelegate {
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return messagesArray.count
    }
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        
        let msgDic : NSDictionary = messagesArray.object(at: indexPath.row) as! NSDictionary
        let senderid = msgDic.value(forKey: "CreatedBy") as! String
        let msgText = msgDic.value(forKey: "Comment") as! String
         let UpdatedAt = msgDic.value(forKey: "CommentedDate") as! String
         let username = msgDic.value(forKey: "username") as! String
        
        let inputFormatter = DateFormatter()
        inputFormatter.dateFormat = "yyyy-MM-dd HH:mm:ss"
        let showDate = inputFormatter.date(from: UpdatedAt)
        inputFormatter.dateFormat = "yyyy-MM-dd hh:mm a"
        let resultString = inputFormatter.string(from: showDate!)
        print(resultString)
        
        if senderid != "admin"  {
            let cell:receivercell = tableView.dequeueReusableCell(withIdentifier: "cell2") as! receivercell
              let imgURL = msgDic.value(forKey: "ImageURL") as? String ?? ""
            if imgURL != "" {
                cell.imgheight.constant = 200
                 cell.imgreceiver.sd_setImage(with: URL(string : imgURL), placeholderImage :UIImage(named: "Logo") , options: .progressiveDownload, completed: nil)
            }
            else
            {
               cell.imgheight.constant = 0
            }

            cell.lblreceiver.text = msgText
            cell.lblreceivertime.text = "\(username) | \(resultString)"
            return cell
            
        } else {
            let cell:sendercell = tableView.dequeueReusableCell(withIdentifier: "cell1") as! sendercell
              let imgURL = msgDic.value(forKey: "ImageURL") as? String ?? ""
            if imgURL != "" {
                           cell.imgheight.constant = 200
                 cell.imgsender.sd_setImage(with: URL(string : imgURL), placeholderImage :UIImage(named: "Logo") , options: .progressiveDownload, completed: nil)
                       }
                       else
                       {
                          cell.imgheight.constant = 0
                       }

            cell.lblsender.text = msgText
            cell.lblsendertime.text = "\(username) | \(resultString)"
            return cell
        }
        
    }
    
}

