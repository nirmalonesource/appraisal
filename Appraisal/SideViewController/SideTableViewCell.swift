//
//  SideTableViewCell.swift
//  SKGJST
//
//  Created by ADMIN on 3/21/18.
//  Copyright © 2018 empiereTech. All rights reserved.
//

import UIKit

class SideTableViewCell: UITableViewCell {

    @IBOutlet var Lbl_Name: UILabel!
    @IBOutlet var Img_Vw: UIImageView!
    
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }

}
