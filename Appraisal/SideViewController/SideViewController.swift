//
//  SideViewController.swift
//  SKGJST
//
//  Created by ADMIN on 3/20/18.
//  Copyright © 2018 empiereTech. All rights reserved.
//

import UIKit
import SWRevealViewController
import Alamofire
import SwiftyJSON

class SideViewController: UIViewController,UITableViewDelegate,UITableViewDataSource {
    
//MARK: - Outlets🤔
    
    //View
    @IBOutlet var Tbl_Main: UITableView!
    @IBOutlet var Vw_Profile: UIView!
    @IBOutlet var Lbl_Username: UILabel!
    @IBOutlet var Lbl_UserEmai: UILabel!
    @IBOutlet var lblversion: UILabel!
    
    
//MARK: - Declare Variables👇🏻😬
    var newFrontController: UIViewController!
    let arrRes = ["Home","My Certificates","Support"]
    
    
//MARK: - Happy Coding😊
    override func viewDidLoad() {
        super.viewDidLoad()
        // Do any additional setup after loading the view.
        
        if let version = Bundle.main.infoDictionary?["CFBundleShortVersionString"] as? String {
            lblversion.text = "Version \(version)"
        }
        
        let Weblinkstap = UITapGestureRecognizer(target: self, action: #selector(ProfileAction))
        Weblinkstap.numberOfTapsRequired = 1
        Vw_Profile.addGestureRecognizer(Weblinkstap)
    }
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(true)
        
        let FirstName = UserDefaults.standard.string(forKey: "FirstName")
        let LastName = UserDefaults.standard.string(forKey: "LastName")
        let Email = UserDefaults.standard.string(forKey: "Email")
        
        Lbl_Username.text = "\(FirstName!) \(LastName!)"
        Lbl_UserEmai.text = "\(Email!)"
        
    }
    
    @IBAction func LogotButtonPressed(_ sender: Any) {

        
        let alertController = UIAlertController(title: "Logout", message: "Are you sure you want to logout?", preferredStyle: .alert)
        
        let cancelAction = UIAlertAction(title: "Cancel", style: .cancel) { (action) in
            // ...
        }
        alertController.addAction(cancelAction)
        
        let OKAction = UIAlertAction(title: "OK", style: .default) { (action) in
            // ...
            if NetworkReachabilityManager()!.isReachable {
                self.CallMyMethod()
            } else {
             AppDelegate.shared().HideHUD()
            AppDelegate.shared().ShowAlert(title: Constants.ERROR_NO_INTERNET, msg: "Unable to connect Internet, Please try after some time.")
            }
        }
        alertController.addAction(OKAction)
        self.present(alertController, animated: true, completion: nil)
        
    }
    
//MARK: - Methods(Functions)☝🏻😳
    @objc func ProfileAction(){
        // Do what you want
        
        newFrontController = nil
        let storyboard = UIStoryboard(name:"Main", bundle:nil)
        
        let home = storyboard.instantiateViewController(withIdentifier: "ProfileViewController")as! ProfileViewController
        newFrontController = home
        let navigationController = UINavigationController(rootViewController: newFrontController)
        revealViewController()?.pushFrontViewController(navigationController, animated: true)
    }
    
//MARK: - Delegates✋🏻😇
    
    //tableView numberOfRowsInSection
    public func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int
    {
        return(arrRes.count)
    }
    
    //tableView cellForRowAt
    public func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell
    {
        let cell:SideTableViewCell = tableView.dequeueReusableCell(withIdentifier: "cell") as! SideTableViewCell
        cell.Lbl_Name?.text = arrRes[indexPath.row]
        cell.Img_Vw.image = UIImage(named: arrRes[indexPath.row])!
        return cell
    }
    
    //tableView didSelectRowAt
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        // do with place whatever you want
        
        tableView.deselectRow(at: indexPath as IndexPath, animated: true)
        
        let row : NSInteger = indexPath.row
        
        let revealController: SWRevealViewController = revealViewController()
        
        newFrontController = nil
        let storyboard = UIStoryboard(name:"Main", bundle:nil)
        
        switch row {
        case 0:

            let home = storyboard.instantiateViewController(withIdentifier: "HomeViewController")as! HomeViewController
            newFrontController = home
            
            break
        case 1:
            
            let home = storyboard.instantiateViewController(withIdentifier: "MyCertificatesViewController")as! MyCertificatesViewController
            newFrontController = home
            
            break
        case 2:
            
            let home = storyboard.instantiateViewController(withIdentifier: "SupportViewController")as! SupportViewController
            newFrontController = home
            
            break
        default: break
            
        }
        
        let navigationController = UINavigationController(rootViewController: newFrontController)
        revealViewController()?.pushFrontViewController(navigationController, animated: true)
        
    }

//MARK: - API🤞🏻🙄
    
    func   CallMyMethod()
    {
        let params : Parameters = ["device_id":"121"]
        print("param is \(params)")
        let url = URL(string: "\(Constants.API)\(Constants.LOGOUT)")!
        print("Url printed \(url)")
        
        let manager = Alamofire.SessionManager.default
        manager.session.configuration.timeoutIntervalForRequest = 120
        
        let login = UserDefaults.standard.string(forKey: "LoginToken")
        let UserID = UserDefaults.standard.string(forKey: "UserID")
        
        let headers = ["Authorization": "Basic YWRtaW46QVBJQEZFREVSQUwhIyRXRUIk",
                       "X-FEDERAL-API-KEY": "kkcoswggwgwkkc8w4ok808o48kswc40c0www4wss",
                       "X-FEDERAL-LOGIN-TOKEN": login!,
                       "USER-ID": UserID!]
        
        manager.request(url, method: .post, parameters: params, headers: headers).responseJSON() { response in
            switch (response.result) {
            case .success:
                //do json stuff
                
                UserDefaults.standard.removePersistentDomain(forName: Bundle.main.bundleIdentifier!)

                let jsonResponse = response.result.value as! NSDictionary
                print("JsonResponse printed \(jsonResponse)")
                
                let revealController: SWRevealViewController = self.revealViewController()
                let storyboard = UIStoryboard(name:"Main", bundle:nil)
                let home = storyboard.instantiateViewController(withIdentifier: "LoginViewController")as! LoginViewController
                self.newFrontController = home
                let navigationController = UINavigationController(rootViewController: self.newFrontController)
                self.revealViewController()?.pushFrontViewController(navigationController, animated: true)
                
                break
                
            case .failure(let error):
                if error._code == NSURLErrorTimedOut {
                    //HANDLE TIMEOUT HERE
                }
                print("\n\nAuth request failed with error:\n \(error)")
                break
            }
        }
    }
}
