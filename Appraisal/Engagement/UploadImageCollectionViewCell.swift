//
//  UploadImageCollectionViewCell.swift
//  Appraisal
//
//  Created by ADMIN on 4/4/18.
//  Copyright © 2018 empiereTech. All rights reserved.
//

import UIKit

class UploadImageCollectionViewCell: UICollectionViewCell {
    
    //ImageView
    @IBOutlet var Img_Vw: UIImageView!
    
    //Button
    @IBOutlet weak var Btn_Close: UIButton!
    
}
