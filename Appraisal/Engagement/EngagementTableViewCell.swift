//
//  EngagementTableViewCell.swift
//  Appraisal
//
//  Created by ADMIN on 4/21/18.
//  Copyright © 2018 empiereTech. All rights reserved.
//

import UIKit
import FlexibleSteppedProgressBar
class EngagementTableViewCell: UITableViewCell {
    
    //Outlets
    
    //Cell
    
    //View
    @IBOutlet var Txt_Name: UITextField!
    
    //CollectCell
    
    //View
    @IBOutlet weak var CollectCell: UICollectionView!
    
    
    
    var progressBar: FlexibleSteppedProgressBar!
    var progressBarWithoutLastState: FlexibleSteppedProgressBar!
    var progressBarWithDifferentDimensions: FlexibleSteppedProgressBar!
    
    var progressColor = UIColor(red: 19.0 / 255.0, green: 63.0 / 255.0, blue: 118.0 / 255.0, alpha: 1.0)
    var BGColor = UIColor(red: 188.0 / 255.0, green: 188.0 / 255.0, blue: 188.0 / 255.0, alpha: 1.0)
    var textColorHere = UIColor(red: 153.0 / 255.0, green: 153.0 / 255.0, blue: 153.0 / 255.0, alpha: 1.0)
    
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
        
        
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }
    
}
