        //
        //  EngagementStoneViewController.swift
        //  Appraisal
        //
        //  Created by ADMIN on 3/31/18.
        //  Copyright © 2018 empiereTech. All rights reserved.
        //
        
        import UIKit
        import BEMCheckBox
        import SWRevealViewController
        import SDWebImage
        import FlexibleSteppedProgressBar
        import AETextFieldValidator
        import RangeSeekSlider
        
        class EngagementStoneViewController: UIViewController,BEMCheckBoxDelegate,SWRevealViewControllerDelegate,UITextViewDelegate,UICollectionViewDataSource, UICollectionViewDelegate,UICollectionViewDelegateFlowLayout, UIImagePickerControllerDelegate, UINavigationControllerDelegate,FlexibleSteppedProgressBarDelegate,kDropDownListViewDelegate,UIScrollViewDelegate,RangeSeekSliderDelegate {
            
            //MARK: - Outlets🤔
            
            //View
            @IBOutlet var Vw_side: UIView!
            @IBOutlet var Vw_Matel: UIView!
            @IBOutlet var Vw_under: UIView!
            @IBOutlet var Scoll_Vw: UIScrollView!
            @IBOutlet var Vw_Collection: UICollectionView!
            @IBOutlet var Vw_CollectShape: UICollectionView!
            @IBOutlet var Vw_Progress: UIView!
            @IBOutlet var Vw_Clarity: RangeSeekSlider!
            @IBOutlet var Vw_Color: RangeSeekSlider!
            @IBOutlet var Vw_Metal: UIView!
            
            //TextFeild
            @IBOutlet var Txt_CustomeName: AETextFieldValidator!
            @IBOutlet var Txt_CaretWeight: AETextFieldValidator!
            @IBOutlet var Txt_Comments: UITextView!
            @IBOutlet var Txt_Estimated: AETextFieldValidator!
            @IBOutlet var Txt_Other: AETextFieldValidator!
            
            //Button
            @IBOutlet var Btn_Next: UIButton!
            @IBOutlet var Btn_Gem: UIButton!
            
            //CheckMark
            @IBOutlet var Check_Engagement: BEMCheckBox!
            @IBOutlet var Check_Loose: BEMCheckBox!
            @IBOutlet weak var Check_Terms: BEMCheckBox!
            @IBOutlet weak var Txt_settingcaratweight: AETextFieldValidator!
            @IBOutlet weak var lblcutshape: UILabel!
            @IBOutlet var claritytextview: UIView!
            @IBOutlet var colortextview: UIView!
            @IBOutlet var claritycolorvw: UIView!
            @IBOutlet var txtcolor: AETextFieldValidator!
            
            
            //MARK: - Declare Variables👇🏻😬
            let imagePicker = UIImagePickerController()
            let reuseIdentifier = "cell" // also enter this string as the cell identifier in the storyboard
            var items = [UIImage]()
            var image = UIImage()
            var imageData = NSData()
            var UserImageName = NSString()
            var EngagementOrLooseRing = NSString()
            var ShapeID = NSString()
            
            //Array
            var ShapeArr = [[String:AnyObject]]()
            var ShapeArrIndex = Int()
            var ClarityArr = [[String:AnyObject]]()
            var ClarityArrIndex = NSString()
            var ColorArr = [[String:AnyObject]]()
            var ColorArrIndex = NSString()
            var MaterialArr = [[String:AnyObject]]()
            var MaterialArrIndex = Int()
            var ArrGold = ["Gold 10K","Gold 14K","Gold 18K","Gold 21K","Platinum"]
            var ArrGoldIndex = NSString()
            
            //ProgressBar
            var progressBar: FlexibleSteppedProgressBar!
            var progressBarWithoutLastState: FlexibleSteppedProgressBar!
            var progressBarWithDifferentDimensions: FlexibleSteppedProgressBar!
            var progressBarWithDifferentDimensions2: FlexibleSteppedProgressBar!
            var progressBarWithDifferentDimensions3: FlexibleSteppedProgressBar!
            
            var maxIndex = -1
            
            var progressColor = UIColor(red: 19.0 / 255.0, green: 63.0 / 255.0, blue: 118.0 / 255.0, alpha: 1.0)
            var BGColor = UIColor(red: 188.0 / 255.0, green: 188.0 / 255.0, blue: 188.0 / 255.0, alpha: 1.0)
            var textColorHere = UIColor(red: 153.0 / 255.0, green: 153.0 / 255.0, blue: 153.0 / 255.0, alpha: 1.0)
            
            //DropDown
            var Dropobj = DropDownListView()
            var selectedIndexPath = NSIndexPath()
            
            var View = Bool()
            var OtherBool = Bool()
            var checkview = Bool()
            var NoneBool = Bool()
            
           var clearity_start = NSString()
           var clearity_end = NSString()
           var color_start = NSString()
           var color_end = NSString()
            
           var clearity_start_id = NSString()
           var clearity_end_id = NSString()
           var color_start_id = NSString()
           var color_end_id = NSString()
    
            //MARK: - Happy Coding😊
            
            //viewDidLoad
            override func viewDidLoad() {
                super.viewDidLoad()
                
                self.navigationController?.interactivePopGestureRecognizer?.isEnabled = false
                 self.revealViewController().panGestureRecognizer().isEnabled = false
                
                OtherBool = false
                // Do any additional setup after loading the view.
                
                imagePicker.delegate = self
                Check_Engagement.delegate = self
                Check_Loose.delegate = self
                Check_Terms.delegate = self
                ShapeID = ""
                ClarityArrIndex = ""
                ColorArrIndex = ""
                EngagementOrLooseRing = "1"
                MaterialArrIndex = -1
                
                //NavigationBar
                self.navigationController?.navigationBar.isHidden = false;
                let label = UILabel()
                label.backgroundColor = .clear
                label.numberOfLines = 2
                label.font = UIFont.systemFont(ofSize: 16)
                label.textAlignment = .center
                label.textColor = #colorLiteral(red: 0.07110074908, green: 0.2493948936, blue: 0.4626086354, alpha: 1)
                label.text = "Engagement Rings & Loose Stones"
                self.navigationItem.titleView = label
               // self.title = "Engagement Rings & Loose Stones"
                
                navigationController?.navigationBar.barTintColor = UIColor.white
                navigationController?.navigationBar.tintColor = UIColor(red: 18/255, green: 64/255, blue: 118/255, alpha: 1)
                /*self.navigationItem.setHidesBackButton(true, animated:true)
                 
                 let menuButton = UIButton(type: .system)
                 menuButton.setImage(#imageLiteral(resourceName: "Menu").withRenderingMode(.alwaysOriginal), for: .normal)
                 menuButton.frame = CGRect(x: 0, y: 0, width: 34, height: 34)
                 navigationItem.leftBarButtonItem = UIBarButtonItem(customView: menuButton)
                 
                 //SideBar
                 if revealViewController() != nil
                 {
                 self.revealViewController().delegate = self
                 menuButton.addTarget(revealViewController(), action: "revealToggle:", for: .touchUpInside)
                 self.navigationController?.navigationBar.addGestureRecognizer(self.revealViewController().panGestureRecognizer())
                 view.addGestureRecognizer(self.revealViewController().panGestureRecognizer())
                 }*/
                
                var dict = AppDelegate.shared().ArrGetAllType[0]
                ShapeArr = dict["Shape"]! as! [[String:AnyObject]]
                ClarityArr = dict["Clarity"]! as! [[String:AnyObject]]
                ColorArr = dict["Color"]! as! [[String:AnyObject]]
                MaterialArr = dict["MaterialType"]! as! [[String:AnyObject]]
                var Size = CGFloat()
                if ShapeArr.count <= 4 {
                    Size = 210
                }
                else if ShapeArr.count <= 8{
                    Size = 105
                }
                else if ShapeArr.count <= 12{
                    Size = 0
                }
                
                Vw_CollectShape.frame = CGRect.init(x: Vw_CollectShape.frame.origin.x, y: Vw_CollectShape.frame.origin.y, width: Vw_CollectShape.frame.size.width, height: Vw_CollectShape.frame.size.height-Size)
                Vw_Progress.frame = CGRect.init(x: Vw_Progress.frame.origin.x, y: Vw_Progress.frame.origin.y - Size, width: Vw_Progress.frame.size.width, height: Vw_Progress.frame.size.height)
//                Vw_under.frame = CGRect.init(x: Vw_under.frame.origin.x, y: Vw_under.frame.origin.y-130, width: Vw_under.frame.size.width, height: Vw_under.frame.size.height)
//                Vw_Matel.isHidden = true
                
                let group = BEMCheckBoxGroup(checkBoxes: [Check_Engagement, Check_Loose])
                group.selectedCheckBox = Check_Engagement
                group.mustHaveSelection = true
                
                Txt_CustomeName.layer.cornerRadius = 7.0
                Txt_CustomeName.layer.backgroundColor = UIColor.white.cgColor
                Txt_CustomeName.layer.masksToBounds = false
                Txt_CustomeName.layer.shadowOffset = CGSize.zero
                Txt_CustomeName.layer.shadowRadius = 5
                Txt_CustomeName.layer.shadowOpacity = 0.2
                Txt_CustomeName.leftView = UIView(frame: CGRect(x: 0, y: 0, width: 15, height: Txt_CustomeName.frame.height))
                Txt_CustomeName.leftViewMode = .always
                Txt_CustomeName.textColor = UIColor.black
                let CustomerAttributed = NSMutableAttributedString.init(string: "Customer Name :")
                let Customerterms:NSString = "Customer Name :"
                let Customerrange :NSRange = Customerterms.range(of: "")
                let Customerrangech :NSRange = Customerterms.range(of: "Customer Name :")
                CustomerAttributed.addAttribute(NSAttributedStringKey.foregroundColor, value: UIColor.red, range: Customerrange)
                CustomerAttributed.addAttribute(NSAttributedStringKey.foregroundColor, value: UIColor.darkGray, range: Customerrangech)
                Txt_CustomeName.attributedPlaceholder = CustomerAttributed

                
                Txt_CaretWeight.layer.cornerRadius = 7.0
                Txt_CaretWeight.layer.backgroundColor = UIColor.white.cgColor
                Txt_CaretWeight.layer.masksToBounds = false
                Txt_CaretWeight.layer.shadowOffset = CGSize.zero
                Txt_CaretWeight.layer.shadowRadius = 5
                Txt_CaretWeight.layer.shadowOpacity = 0.2
                Txt_CaretWeight.leftView = UIView(frame: CGRect(x: 0, y: 0, width: 15, height: Txt_CaretWeight.frame.height))
                Txt_CaretWeight.leftViewMode = .always
                Txt_CaretWeight.textColor = UIColor.black
                let DiamondAttributed = NSMutableAttributedString.init(string: "*Center Stone carat weight :")
                let Diamondterms:NSString = "*Center Stone carat weight :"
                let Diamondrange:NSRange = Diamondterms.range(of: "*")
                let Diamondrangech :NSRange = Diamondterms.range(of: "Center Stone carat weight :")
                DiamondAttributed.addAttribute(NSAttributedStringKey.foregroundColor, value: UIColor.red, range: Diamondrange)
                DiamondAttributed.addAttribute(NSAttributedStringKey.foregroundColor, value: UIColor.darkGray, range: Diamondrangech)
                Txt_CaretWeight.attributedPlaceholder = DiamondAttributed
                
                Txt_settingcaratweight.layer.cornerRadius = 7.0
                Txt_settingcaratweight.layer.backgroundColor = UIColor.white.cgColor
                Txt_settingcaratweight.layer.masksToBounds = false
                Txt_settingcaratweight.layer.shadowOffset = CGSize.zero
                Txt_settingcaratweight.layer.shadowRadius = 5
                Txt_settingcaratweight.layer.shadowOpacity = 0.2
                Txt_settingcaratweight.leftView = UIView(frame: CGRect(x: 0, y: 0, width: 15, height: Txt_CustomeName.frame.height))
                Txt_settingcaratweight.leftViewMode = .always
                Txt_settingcaratweight.textColor = UIColor.black
                let CenterStoneAttributed = NSMutableAttributedString.init(string: "*Setting Carat Weight :")
                let CenterStoneterms:NSString = "*Setting Carat Weight :"
                let CenterStonerange :NSRange = CenterStoneterms.range(of: "*")
                let CenterStonerangech :NSRange = CenterStoneterms.range(of: "Setting Carat Weight :")
                CenterStoneAttributed.addAttribute(NSAttributedStringKey.foregroundColor, value: UIColor.red, range: CenterStonerange)
                CenterStoneAttributed.addAttribute(NSAttributedStringKey.foregroundColor, value: UIColor.darkGray, range: CenterStonerangech)
                Txt_settingcaratweight.attributedPlaceholder = CenterStoneAttributed
                
                
                Txt_Comments.layer.cornerRadius = 7.0
                Txt_Comments.layer.backgroundColor = UIColor.white.cgColor
                Txt_Comments.layer.masksToBounds = false
                Txt_Comments.layer.shadowOffset = CGSize.zero
                Txt_Comments.layer.shadowRadius = 5
                Txt_Comments.layer.shadowOpacity = 0.2
                Txt_Comments.text = "Additional Info"
                Txt_Comments.textColor =  UIColor.darkGray
                Txt_Comments.delegate = self
                Txt_Comments.contentInset = UIEdgeInsetsMake(5, 12, 0, 0)
                
                
                
                Btn_Gem.layer.cornerRadius = 7.0
                Btn_Gem.isUserInteractionEnabled = true
                Btn_Gem.layer.masksToBounds = false
                Btn_Gem.layer.shadowOffset = CGSize.zero
                Btn_Gem.layer.shadowRadius = 5
                Btn_Gem.layer.shadowOpacity = 0.2
                Btn_Gem.contentEdgeInsets = UIEdgeInsets(top: 5, left: 12, bottom: 0, right: 0)
                
                Txt_Estimated.layer.cornerRadius = 7.0
                Txt_Estimated.layer.backgroundColor = UIColor.white.cgColor
                Txt_Estimated.layer.masksToBounds = false
                Txt_Estimated.layer.shadowOffset = CGSize.zero
                Txt_Estimated.layer.shadowRadius = 5
                Txt_Estimated.layer.shadowOpacity = 0.2
                let txtDollar = UITextView(frame: CGRect(x: 5, y:10, width: 15, height: Txt_Estimated.frame.height - 10))
                txtDollar.text = ""
                txtDollar.isEditable = false
                txtDollar.textColor =  UIColor.darkGray
                Txt_Estimated.leftView = txtDollar
                Txt_Estimated.leftViewMode = .always
                Txt_Estimated.textColor = #colorLiteral(red: 0, green: 0.4980392157, blue: 0.003921568627, alpha: 1)
                let EstimatedAttributed = NSMutableAttributedString.init(string: "Estimated Value:")
                let Estimatedterms:NSString = "Estimated Value:"
                let Estimatedrangech :NSRange = Estimatedterms.range(of: "Estimated Value:")
                EstimatedAttributed.addAttribute(NSAttributedStringKey.foregroundColor, value: UIColor.darkGray, range: Estimatedrangech)
                Txt_Estimated.attributedPlaceholder = EstimatedAttributed
                
                
                Txt_Other.layer.cornerRadius = 7.0
                Txt_Other.layer.backgroundColor = UIColor.white.cgColor
                Txt_Other.layer.masksToBounds = false
                Txt_Other.layer.shadowOffset = CGSize.zero
                Txt_Other.layer.shadowRadius = 5
                Txt_Other.layer.shadowOpacity = 0.2
                Txt_Other.leftView = UIView(frame: CGRect(x: 0, y: 0, width: 15, height: Txt_Other.frame.height))
                Txt_Other.leftViewMode = .always
                let OtherAttributed = NSMutableAttributedString.init(string: "Other :")
                let Otherterms:NSString = "Other :"
                let Otherrangech :NSRange = Otherterms.range(of: "Other :")
                OtherAttributed.addAttribute(NSAttributedStringKey.foregroundColor, value: UIColor.darkGray, range: Otherrangech)
                Txt_Other.attributedPlaceholder = OtherAttributed
                
                txtcolor.layer.cornerRadius = 7.0
                txtcolor.layer.backgroundColor = UIColor.white.cgColor
                txtcolor.layer.masksToBounds = false
                txtcolor.layer.shadowOffset = CGSize.zero
                txtcolor.layer.shadowRadius = 5
                txtcolor.layer.shadowOpacity = 0.2
                txtcolor.leftView = UIView(frame: CGRect(x: 0, y: 0, width: 15, height: txtcolor.frame.height))
                txtcolor.leftViewMode = .always
                let txtcolorAttributed = NSMutableAttributedString.init(string: "Color :")
                let txtcolorterms:NSString = "Color :"
                let txtcolorrangech :NSRange = txtcolorterms.range(of: "Color :")
                txtcolorAttributed.addAttribute(NSAttributedStringKey.foregroundColor, value: UIColor.darkGray, range: txtcolorrangech)
                txtcolor.attributedPlaceholder = txtcolorAttributed
                
                
                Btn_Next.layer.cornerRadius = Btn_Next.frame.size.height/2
                
                let nib = UINib(nibName: "HomeCollectionViewCell", bundle: nil)
                Vw_CollectShape?.register(nib, forCellWithReuseIdentifier: "cell")
                
               // ProgressBar()
              //  ProgressBar2()
                ProgressBar3()
                setupAlerts()
                
                //============================ RANGE SLIDER SETUP ====================================
                var xposition = 0
                
                for i in 0..<ClarityArr.count
                {
                    let screenwidth = claritytextview.frame.size.width
                    
                    let lbl = UILabel(frame: CGRect(x: xposition, y: 0, width: Int(screenwidth/CGFloat(ClarityArr.count)), height: 20))
                    lbl.textAlignment = .center
                    lbl.text = ClarityArr[i]["ClarityName"] as? String
                    lbl.font = lbl.font.withSize(10)
                    self.claritytextview.addSubview(lbl)
                    xposition = xposition + Int(screenwidth/CGFloat(ClarityArr.count))
                }
                
                Vw_Clarity.delegate = self
                Vw_Clarity.enableStep = true
                Vw_Clarity.step = 1 // CGFloat(ClarityArr.count-1)
                if let n = NumberFormatter().number(from: ClarityArr[0]["ClarityID"] as! String)
                {
                    let f = CGFloat(truncating: n)
                    Vw_Clarity.minValue = 0
                    Vw_Clarity.selectedMinValue = 0
                    clearity_start_id = ClarityArr[0]["ClarityID"] as! NSString
                    clearity_start = ClarityArr[0]["ClarityName"] as! NSString
                    
                }
                if let n = NumberFormatter().number(from: ClarityArr[ClarityArr.count-1]["ClarityID"] as! String)
                {
                    let f = CGFloat(truncating: n)
                    Vw_Clarity.maxValue = CGFloat(ClarityArr.count-1)
                    Vw_Clarity.selectedMaxValue = CGFloat(ClarityArr.count-1)
                    clearity_end_id = ClarityArr[ClarityArr.count-1]["ClarityID"] as! NSString
                    clearity_end = ClarityArr[ClarityArr.count-1]["ClarityName"] as! NSString
                }
                
                
                xposition = 0
                
                for i in 0..<ColorArr.count
                {
                    let screenwidth = colortextview.frame.size.width
                    
                    let lbl = UILabel(frame: CGRect(x: xposition, y: 0, width: Int(screenwidth/CGFloat(ColorArr.count)), height: 20))
                    lbl.textAlignment = .center
                    lbl.text = ColorArr[i]["ColorName"] as? String
                    lbl.font = lbl.font.withSize(10)
                    self.colortextview.addSubview(lbl)
                    xposition = xposition + Int(screenwidth/CGFloat(ColorArr.count))
                }
                
                Vw_Color.delegate = self
                Vw_Color.enableStep = true
                Vw_Color.step = 1 // CGFloat(ClarityArr.count-1)
                if let n = NumberFormatter().number(from: ColorArr[0]["ColorID"] as! String)
                {
                    let f = CGFloat(truncating: n)
                    Vw_Color.minValue = 0
                    Vw_Color.selectedMinValue = 0
                    color_start_id = ColorArr[0]["ColorID"] as! NSString
                    color_start = ColorArr[0]["ColorName"] as! NSString
                }
                if let n = NumberFormatter().number(from: ColorArr[ColorArr.count-1]["ColorID"] as! String)
                {
                    let f = CGFloat(truncating: n)
                    Vw_Color.maxValue = CGFloat(ColorArr.count-1)
                    Vw_Color.selectedMaxValue = CGFloat(ColorArr.count-1)
                    color_end_id = ColorArr[ColorArr.count-1]["ColorID"] as! NSString
                    color_end = ColorArr[ColorArr.count-1]["ColorName"] as! NSString
                }
                
                //============================ RANGE SLIDER SETUP END ====================================
                
                Btn_Next.alpha = 0.2
              //  Btn_Next.isEnabled = false
                
                claritycolorvw.frame = CGRect.init(x: claritycolorvw.frame.origin.x, y: 100, width: claritycolorvw.frame.size.width, height: claritycolorvw.frame.size.height)
                
                Vw_Matel.frame = CGRect.init(x: Vw_Matel.frame.origin.x, y: 320, width: Vw_Matel.frame.size.width, height: Vw_Matel.frame.size.height)
                
                Vw_under.frame = CGRect.init(x: Vw_under.frame.origin.x, y: 450, width: Vw_under.frame.size.width, height: Vw_under.frame.size.height)
                
                Vw_Matel.isHidden = false
                  
                if (UIScreen.main.bounds.size.height == 568.0)
                {
                    print("iphone5")
                    Scoll_Vw.contentSize = CGSize(width: self.view.frame.size.width, height: 1650)
                }
                else{
                    Scoll_Vw.contentSize = CGSize(width: self.view.frame.size.width, height: 1650)
                }
            }
            
            
            // MARK: - RangeSeekSliderDelegate
            
            func rangeSeekSlider(_ slider: RangeSeekSlider, didChange minValue: CGFloat, maxValue: CGFloat) {
                if slider === Vw_Clarity {
                    print("clarity slider updated. Min Value: \(minValue) Max Value: \(maxValue)")
                    clearity_start_id = ClarityArr[Int(minValue)]["ClarityID"] as! NSString
                    clearity_end_id = ClarityArr[Int(maxValue)]["ClarityID"] as! NSString
                    clearity_start = ClarityArr[Int(minValue)]["ClarityName"] as! NSString
                    clearity_end = ClarityArr[Int(maxValue)]["ClarityName"] as! NSString
                }
                else if slider === Vw_Color {
//                    if Int(maxValue) == ColorArr.count-1 {
//                        slider.selectedMaxValue = CGFloat(ColorArr.count-1)
//                        slider.selectedMinValue = 0
//
//                    }
//                     else if Int(maxValue) == ColorArr.count-2 {
//                            slider.selectedMaxValue = CGFloat(ColorArr.count-2)
//                            slider.selectedMinValue = 1
//                        }
//                    else if Int(minValue) == 0
//                    {
//                        slider.selectedMaxValue = CGFloat(ColorArr.count-1)
//                        slider.selectedMinValue = 0
//                    }
//
//                    else if Int(minValue) == 1
//                    {
//                        slider.selectedMaxValue = CGFloat(ColorArr.count-2)
//                        slider.selectedMinValue = 1
//                    }
                    
                    
                    
                    color_start_id = ColorArr[Int(minValue)]["ColorID"] as! NSString
                    color_end_id = ColorArr[Int(maxValue)]["ColorID"] as! NSString
                    color_start = ColorArr[Int(minValue)]["ColorName"] as! NSString
                    color_end = ColorArr[Int(maxValue)]["ColorName"] as! NSString
                    
                    if color_start_id == "11" || color_end_id == "11"{
                        txtcolor.isHidden = false
                        if OtherBool {
                            Vw_Matel.frame = CGRect.init(x: Vw_Matel.frame.origin.x, y: 370, width: Vw_Matel.frame.size.width, height: Vw_Matel.frame.size.height)
                            Vw_under.frame = CGRect.init(x: Vw_under.frame.origin.x, y: 500, width: Vw_under.frame.size.width, height: Vw_under.frame.size.height)
                        }
                        else
                        {
                        Vw_Matel.frame = CGRect.init(x: Vw_Matel.frame.origin.x, y: 320, width: Vw_Matel.frame.size.width, height: Vw_Matel.frame.size.height)
                        Vw_under.frame = CGRect.init(x: Vw_under.frame.origin.x, y: 450, width: Vw_under.frame.size.width, height: Vw_under.frame.size.height)
                        }
                    }
                    else
                    {
                         if OtherBool {
                            Vw_Matel.frame = CGRect.init(x: Vw_Matel.frame.origin.x, y: 370, width: Vw_Matel.frame.size.width, height: Vw_Matel.frame.size.height)
                            Vw_under.frame = CGRect.init(x: Vw_under.frame.origin.x, y: 500, width: Vw_under.frame.size.width, height: Vw_under.frame.size.height)
                            txtcolor.isHidden = true
                            txtcolor.text = ""
                        }
                        else
                         {
                        Vw_Matel.frame = CGRect.init(x: Vw_Matel.frame.origin.x, y: 320, width: Vw_Matel.frame.size.width, height: Vw_Matel.frame.size.height)
                        Vw_under.frame = CGRect.init(x: Vw_under.frame.origin.x, y: 450, width: Vw_under.frame.size.width, height: Vw_under.frame.size.height)
                        txtcolor.isHidden = true
                        txtcolor.text = ""
                        }
                    }
                    print("color slider updated. Min Value: \(minValue) Max Value: \(maxValue)")
                }
            }
            
            func didStartTouches(in slider: RangeSeekSlider) {
                print("did start touches")
            }
            
            func didEndTouches(in slider: RangeSeekSlider) {
                print("did end touches")
            }
            
            
            func textViewDidBeginEditing(_ textView: UITextView) {
                if textView.textColor == UIColor.darkGray {
                    textView.text = nil;
                    textView.textColor = UIColor.black
                }
            }
            
            func textViewDidEndEditing(_ textView: UITextView) {
                if textView.text.isEmpty {
                    textView.text = "Additional Info"
                    textView.textColor = UIColor.darkGray
                }
            }
            
            
            
            override func viewWillAppear(_ animated: Bool) {
                super.viewWillAppear(true)
                
                AppDelegate.shared().GETData.removeAll()
                AppDelegate.shared().ImageItem.removeAll()
                AppDelegate.shared().WatchImageitem.removeAll()
                
                if View == false
                {
                    items.insert(UIImage(named: "upload_image")!, at: 0)
                }
                
                Btn_Next.isUserInteractionEnabled = true
            }
            
            //MARK: - Actions👊🏻😠
            
            @IBAction func NextButtonPressed(_ sender: Any) {
                //  ClarityArrIndex = ""
                // ColorArrIndex = ""
                let a = "none"
                       let b = Btn_Gem.titleLabel?.text!
                       let result: ComparisonResult = a.compare(String(b ?? ""), options: NSString.CompareOptions.caseInsensitive, range: nil, locale: nil)
                
                if Check_Engagement.on
                {
//                    if(!Txt_CustomeName.validate()){
//                        Scoll_Vw.setContentOffset(CGPoint(x: 0, y: 0), animated: true)
//                        AppDelegate.shared().ShowAlert(title: Constants.ERROR_HEADER, msg:"Please Enter Customer Name \n (Character limit is 1-24 only)")
//                    }
                    if(!Txt_CaretWeight.validate()){
                        Scoll_Vw.setContentOffset(CGPoint(x: 0, y: 0), animated: true)
                        AppDelegate.shared().ShowAlert(title: Constants.ERROR_HEADER, msg:"Please Enter Total carat weight")
                    }else if(!Txt_settingcaratweight.validate()){
                        Scoll_Vw.setContentOffset(CGPoint(x: 0, y: 0), animated: true)
                        AppDelegate.shared().ShowAlert(title: Constants.ERROR_HEADER, msg:"Please Enter Setting Caret Weight")
                    }
                    else if(ShapeID == ""){
                        Scoll_Vw.setContentOffset(CGPoint(x: 0, y: 0), animated: true)
                        AppDelegate.shared().ShowAlert(title: Constants.ERROR_HEADER, msg:"Please Enter Cut / Shape")
                    }
//                    else if(ClarityArrIndex == ""){
//                        AppDelegate.shared().ShowAlert(title: Constants.ERROR_HEADER, msg:"Please Select Clarity Grade")
//                    }
//                    else if(ColorArrIndex == ""){
//                        AppDelegate.shared().ShowAlert(title: Constants.ERROR_HEADER, msg:"Please Select Color")
//                    }
                    else if(MaterialArrIndex == -1){
                        AppDelegate.shared().ShowAlert(title: Constants.ERROR_HEADER, msg:"Please Select GEM MATERIAL Type")
                    }
                    else if(!Txt_Estimated.validate()){
                        AppDelegate.shared().ShowAlert(title: Constants.ERROR_HEADER, msg:"Please Enter Estimated Value")
                    }
                    else if(items.count == 1 ){
                        AppDelegate.shared().ShowAlert(title: Constants.ERROR_HEADER, msg: "Please upload atleast one picture.")
                }
                    else if(!txtcolor.isHidden) && txtcolor.text == "" && result != .orderedSame{
                             AppDelegate.shared().ShowAlert(title: Constants.ERROR_HEADER, msg: "Please Enter Color info.")
                        }
                    else {
                        
                        Btn_Next.transform = CGAffineTransform(scaleX: 0.9, y: 0.9)
                       // Btn_Next.isUserInteractionEnabled = false
                        UIView.animate(withDuration: 2.0,
                                       delay: 0,
                                       usingSpringWithDamping: 0.2,
                                       initialSpringVelocity: 6.0,
                                       options: .allowUserInteraction,
                                       animations: { [weak self] in
                                        self?.Btn_Next.transform = .identity
                                        
                                        Timer.scheduledTimer(timeInterval: 0.7,
                                                             target: self!,
                                                             selector: #selector(EngagementStoneViewController.update),
                                                             userInfo: nil,
                                                             repeats: false)
                            },
                                       completion: nil)
                        
                    }
                }
               else
                {
//                    if(!Txt_CustomeName.validate()){
//                        Scoll_Vw.setContentOffset(CGPoint(x: 0, y: 0), animated: true)
//                        AppDelegate.shared().ShowAlert(title: Constants.ERROR_HEADER, msg:"Please Enter Customer Name \n (Character limit is 1-24 only)")
//                    }
                    if(!Txt_CaretWeight.validate()){
                        Scoll_Vw.setContentOffset(CGPoint(x: 0, y: 0), animated: true)
                        AppDelegate.shared().ShowAlert(title: Constants.ERROR_HEADER, msg:"Please Enter Total carat weight")
                    }else if(ShapeID == ""){
                        Scoll_Vw.setContentOffset(CGPoint(x: 0, y: 0), animated: true)
                        AppDelegate.shared().ShowAlert(title: Constants.ERROR_HEADER, msg:"Please Enter Cut / Shape")
                    }
//                    else if(ClarityArrIndex == ""){
//                        AppDelegate.shared().ShowAlert(title: Constants.ERROR_HEADER, msg:"Please Select Clarity Grade")
//                    } else if(ColorArrIndex == ""){
//                        AppDelegate.shared().ShowAlert(title: Constants.ERROR_HEADER, msg:"Please Select Color")
//                    }
                    else if(MaterialArrIndex == -1){
                        AppDelegate.shared().ShowAlert(title: Constants.ERROR_HEADER, msg:"Please Select Gem Meterial Type")
                    } else if(!Txt_Estimated.validate()){
                        AppDelegate.shared().ShowAlert(title: Constants.ERROR_HEADER, msg:"Please Enter Estimated Value")
                    } else if(items.count == 1 ){
                        AppDelegate.shared().ShowAlert(title: Constants.ERROR_HEADER, msg: "Please upload atleast one picture.")
                        
                    }
                        else if(!txtcolor.isHidden) && txtcolor.text == "" && result != .orderedSame{
                             AppDelegate.shared().ShowAlert(title: Constants.ERROR_HEADER, msg: "Please Enter Color info.")
                        }
                    else {
                        
                        Btn_Next.transform = CGAffineTransform(scaleX: 0.9, y: 0.9)
                       // Btn_Next.isUserInteractionEnabled = false
                        UIView.animate(withDuration: 2.0,
                                       delay: 0,
                                       usingSpringWithDamping: 0.2,
                                       initialSpringVelocity: 6.0,
                                       options: .allowUserInteraction,
                                       animations: { [weak self] in
                                        self?.Btn_Next.transform = .identity
                                        
                                        Timer.scheduledTimer(timeInterval: 0.7,
                                                             target: self!,
                                                             selector: #selector(EngagementStoneViewController.update),
                                                             userInfo: nil,
                                                             repeats: false)
                            },
                                       completion: nil)
                    }
                }
        }
        
        @IBAction func MaterialButtonPressed(_ sender: Any) {
            print("X Position\(Btn_Gem.frame.origin.x) Y Position \(Btn_Gem.frame.origin.y)")
            DropDown(Array: MaterialArr, Name: "MaterialType", ButtonName: "GEM MATERIAL", Button: Btn_Gem)
        }
        
        
        //MARK: - Methods(Functions)☝🏻😳
        
        func setupAlerts(){
            
           // Txt_CustomeName.addRegx(Constants.REGEX_Note_USER_NAME, withMsg: "Enter valid Customer Name")
            Txt_Estimated.addRegx(Constants.REGEX_Note_USER_LIMIT, withMsg: "Enter Valid Estimated Name")
            Txt_CaretWeight.addRegx(Constants.REGEX_Note_USER_LIMIT, withMsg: "Enter valid Total carat weight")
            txtcolor.addRegx(Constants.REGEX_Note_USER_LIMIT, withMsg: "Please Enter Color info.")
        }
        
        // @objc selector expected for Timer
        @objc func update() {
            // do what should happen when timer triggers an event
            View = false
            
            let storyboard = UIStoryboard(name:"Main", bundle:nil)
            let home = storyboard.instantiateViewController(withIdentifier: "ReportIssueViewController")as! ReportIssueViewController
            
            var Comment:String = ""
            
            if Txt_Comments.text == "Additional Info" {
                Comment = "N/A"
            }
            else{
                Comment = Txt_Comments.text!
            }
            
            var MaterialType:String = ""
            
            if OtherBool == true{
                MaterialType = Txt_Other.text!
            }
            else
            {
                MaterialType = Btn_Gem.titleLabel?.text ?? ""
            }
            
//            let Clarityint:Int = Int(ClarityArrIndex as String)!
//            let Colorint:Int = Int(ColorArrIndex as String)!
            let Shapeint:Int = Int(ShapeID as String)!
            
            
            //print("\(ClarityArr[getint]["ClarityName"] as! NSString)")
            var Gemselectstring : String = ""
            if (Check_Engagement.on == true)
            {
                Gemselectstring = Btn_Gem.titleLabel?.text! as! String
            }
            
            AppDelegate.shared().GETData = ["CustomerName": Txt_CustomeName.text ?? "",
                                            "SettingCaratWeight": Txt_settingcaratweight.text!,
                                            "CaratDiamondWeight": Txt_CaretWeight.text!,
                                            "EngagementOrLooseRing":EngagementOrLooseRing as String,
                                            "MetalType":ArrGoldIndex as String,
                                            "ShapeID":ShapeID as String,
                                            "ShapeName":ShapeArr[Shapeint-1]["ShapeName"] as! String,
                                           // "ClarityID":ClarityArrIndex as String,
                                          //  "ClarityName":ClarityArr[Clarityint]["ClarityName"] as! String,
                                           // "ColorID":ColorArrIndex as String,
                                           // "ColorName":ColorArr[Colorint]["ColorName"] as! String,
                                            "MaterialType":MaterialType,
                                            "MaterialTypeID":MaterialArr[MaterialArrIndex]["MaterialTypeID"] as! String,
                                            "CustomerComment": Comment,"EstimatedValue":Txt_Estimated.text!,
                                            "ItemTypeID":"1","GemMaterial":Gemselectstring,
                                            "IndexSelect": "\(selectedIndexPath.row)",
                "clearity_start":clearity_start,"clearity_end":clearity_end,"color_start":color_start,"color_end":color_end,"clearity_start_id":clearity_start_id,"clearity_end_id":clearity_end_id,"color_start_id":color_start_id,"color_end_id":color_end_id,"morecolor":txtcolor.text ?? ""
                ] as! [String : String]
            print("Dictionary \(AppDelegate.shared().GETData)")
            if items.count > 1 {
                items.remove(at: 0)
                AppDelegate.shared().ImageItem = items
            }
            
            self.navigationController?.pushViewController(home, animated: true)
        }
        
        func ProgressBar() {
            
            progressBarWithDifferentDimensions = FlexibleSteppedProgressBar()
            progressBarWithDifferentDimensions.translatesAutoresizingMaskIntoConstraints = false
            Vw_Clarity.addSubview(progressBarWithDifferentDimensions)
            
            progressBarWithDifferentDimensions.frame = CGRect.init(x: 20, y: -15, width: Vw_Clarity.frame.size.width - 50, height: Vw_Clarity.frame.size.height)
            progressBarWithDifferentDimensions.numberOfPoints = ClarityArr.count
            progressBarWithDifferentDimensions.lineHeight = 4
            progressBarWithDifferentDimensions.backgroundShapeColor = BGColor
            progressBarWithDifferentDimensions.radius = 4
            progressBarWithDifferentDimensions.progressRadius = 4
            progressBarWithDifferentDimensions.progressLineHeight = 0
            progressBarWithDifferentDimensions.delegate = self
            progressBarWithDifferentDimensions.useLastState = false
            progressBarWithDifferentDimensions.lastStateCenterColor = progressColor
            progressBarWithDifferentDimensions.selectedBackgoundColor = BGColor
            progressBarWithDifferentDimensions.selectedOuterCircleStrokeColor = progressColor
            progressBarWithDifferentDimensions.lastStateOuterCircleStrokeColor = progressColor
            progressBarWithDifferentDimensions.currentSelectedCenterColor = progressColor
            progressBarWithDifferentDimensions.stepTextColor = textColorHere
            progressBarWithDifferentDimensions.currentSelectedTextColor = progressColor
            progressBarWithDifferentDimensions.completedTillIndex = 0
            ClarityArrIndex = ClarityArr[0]["ClarityID"] as! NSString
        }
        
        func ProgressBar2() {
            
            progressBarWithDifferentDimensions2 = FlexibleSteppedProgressBar()
            progressBarWithDifferentDimensions2.translatesAutoresizingMaskIntoConstraints = false
            Vw_Color.addSubview(progressBarWithDifferentDimensions2)
            
            progressBarWithDifferentDimensions2.frame = CGRect.init(x: 20, y: -15, width: Vw_Color.frame.size.width - 50, height: Vw_Color.frame.size.height)
            progressBarWithDifferentDimensions2.numberOfPoints = ColorArr.count
            progressBarWithDifferentDimensions2.lineHeight = 4
            progressBarWithDifferentDimensions2.backgroundShapeColor = BGColor
            progressBarWithDifferentDimensions2.radius = 4
            progressBarWithDifferentDimensions2.progressRadius = 4
            progressBarWithDifferentDimensions2.progressLineHeight = 0
            progressBarWithDifferentDimensions2.delegate = self
            progressBarWithDifferentDimensions2.useLastState = false
            progressBarWithDifferentDimensions2.lastStateCenterColor = progressColor
            progressBarWithDifferentDimensions2.selectedBackgoundColor = BGColor
            progressBarWithDifferentDimensions2.selectedOuterCircleStrokeColor = progressColor
            progressBarWithDifferentDimensions2.lastStateOuterCircleStrokeColor = progressColor
            progressBarWithDifferentDimensions2.currentSelectedCenterColor = progressColor
            progressBarWithDifferentDimensions2.stepTextColor = textColorHere
            progressBarWithDifferentDimensions2.currentSelectedTextColor = progressColor
            progressBarWithDifferentDimensions2.completedTillIndex = 0
            ColorArrIndex = ColorArr[0]["ColorID"] as! NSString
        }
        
        func ProgressBar3() {
            
            progressBarWithDifferentDimensions3 = FlexibleSteppedProgressBar()
            progressBarWithDifferentDimensions3.translatesAutoresizingMaskIntoConstraints = false
            Vw_Metal.addSubview(progressBarWithDifferentDimensions3)
            
            progressBarWithDifferentDimensions3.frame = CGRect.init(x: 20, y: -15, width: Vw_Metal.frame.size.width - 50, height: Vw_Metal.frame.size.height)
            progressBarWithDifferentDimensions3.numberOfPoints = ArrGold.count
            progressBarWithDifferentDimensions3.lineHeight = 4
            progressBarWithDifferentDimensions3.backgroundShapeColor = BGColor
            progressBarWithDifferentDimensions3.radius = 4
            progressBarWithDifferentDimensions3.progressRadius = 4
            progressBarWithDifferentDimensions3.progressLineHeight = 0
            progressBarWithDifferentDimensions3.delegate = self
            progressBarWithDifferentDimensions3.useLastState = false
            progressBarWithDifferentDimensions3.lastStateCenterColor = progressColor
            progressBarWithDifferentDimensions3.selectedBackgoundColor = BGColor
            progressBarWithDifferentDimensions3.selectedOuterCircleStrokeColor = progressColor
            progressBarWithDifferentDimensions3.lastStateOuterCircleStrokeColor = progressColor
            progressBarWithDifferentDimensions3.currentSelectedCenterColor = progressColor
            progressBarWithDifferentDimensions3.stepTextColor = textColorHere
            progressBarWithDifferentDimensions3.currentSelectedTextColor = progressColor
            progressBarWithDifferentDimensions3.completedTillIndex = 0
            ArrGoldIndex = ArrGold[0] as NSString
        }
        
        
        func DropDown(Array : [[String:AnyObject]], Name : String, ButtonName : String, Button : UIButton)
        {
            self.view.endEditing(true)
            var array = [NSString]()
            for var i in (0..<Array.count)
            {
                var dict = Array[i][Name]
                print("ggg \(dict)")
                array.append(dict! as! NSString)
            }
            print(array)
            Dropobj = DropDownListView.init(title: ButtonName, options: array, xy: CGPoint.init(x: Button.frame.origin.x, y: Button.frame.origin.y+40), size: CGSize.init(width: Button.frame.size.width, height: 330), isMultiple: false, isQuestionClinicle: true)
            Dropobj.delegate = self
            Dropobj.show(in: Vw_Progress, animated: true)
            Dropobj.setBackGroundDropDown_R(18.0, g: 64.0, b: 118.0, alpha: 1.0)
        }
        
        
        //MARK: - Delegates✋🏻😇
        
        //MARK: -- CollectionView Delegates
        
        // tell the collection view how many cells to make
        func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
            if collectionView == Vw_CollectShape
            {
                return self.ShapeArr.count
            }
            else{
                return self.items.count
            }
        }
        
        // make a cell for each cell index path
        func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
            
            if collectionView == Vw_CollectShape
            {
                let cell = collectionView.dequeueReusableCell(withReuseIdentifier: "cell", for: indexPath as IndexPath) as! HomeCollectionViewCell
                cell.backgroundColor = UIColor.clear
                let url = NSURL(string: "\(ShapeArr[indexPath.row]["ShapeImagePath"]!)")
                print(url!)
                cell.Img_Items.sd_setImage(with: url! as URL, placeholderImage:nil)
                cell.Img_Items.layer.masksToBounds = true
                cell.Lbl_Item?.text = ShapeArr[indexPath.row]["ShapeName"]! as? String
                
                
                var borderColor: CGColor! = UIColor.clear.cgColor
                var borderWidth: CGFloat = 0
                
                if indexPath == selectedIndexPath as IndexPath{
                    borderColor = UIColor.init(red: 19/255.0, green: 63/255.0, blue: 118/255.0, alpha: 1.0).cgColor
                    borderWidth = 3 //or whatever you please
                   // collectionView.selectItem(at: indexPath, animated: false, scrollPosition: .top)

                }else{
                    borderColor = UIColor.clear.cgColor
                    borderWidth = 0
                }
                
                cell.Img_Items.layer.borderWidth = borderWidth
                cell.Img_Items.layer.borderColor = borderColor
                
                return cell
            }
            else{
                // get a reference to our storyboard cell
                let cell = collectionView.dequeueReusableCell(withReuseIdentifier: reuseIdentifier, for: indexPath as IndexPath) as! UploadImageCollectionViewCell
                
                // myButton.addTarget(self, action: "connected:", forControlEvents: .TouchUpInside)
                
                if indexPath.item != 0 {
                    cell.Btn_Close.isHidden = false
                    cell.Btn_Close.addTarget(self, action: #selector(EngagementStoneViewController.connected), for: .touchUpInside)
                    cell.Btn_Close.tag = indexPath.row
                }
                else{
                    cell.Btn_Close.isHidden = true
                }
                cell.Img_Vw.layer.cornerRadius = cell.Img_Vw.frame.size.height/2
                cell.Img_Vw.image = self.items[indexPath.row]
                cell.Img_Vw.layer.masksToBounds = true
                
                return cell
            }
        }
        
        @objc func connected(sender: UIButton){
            let buttonTag = sender.tag
            
            items.remove(at: buttonTag)
            Vw_Collection.reloadData()
        }
        
        //didSelectItemAt
        func collectionView(_ collectionView: UICollectionView, didSelectItemAt indexPath: IndexPath) {
            // handle tap events
            print("You selected cell #\(indexPath.item)!")
            
            if collectionView == Vw_CollectShape
            {
                selectedIndexPath = indexPath as NSIndexPath
                ShapeID = ShapeArr[indexPath.row]["ShapeID"] as! NSString
                collectionView.reloadData()
            }
            else{
                if indexPath.item == 0 {
                    //                imagePicker.allowsEditing = false
                    //                imagePicker.sourceType = .photoLibrary
                    //
                    //                present(imagePicker, animated: true, completion: nil)
                    print("Imageview Clicked")
                    let alertViewController = UIAlertController(title: nil, message: nil, preferredStyle: .actionSheet)
                    let camera = UIAlertAction(title: "Take Photo", style: .default, handler: { (alert) in
                        self.openCamera()
                    })
                    let gallery = UIAlertAction(title: "Choose from Gallery", style: .default) { (alert) in
                        self.openGallary()
                    }
                    let cancel = UIAlertAction(title: "Cancel", style: .cancel) { (alert) in
                        
                    }
                    alertViewController.addAction(camera)
                    alertViewController.addAction(gallery)
                    alertViewController.addAction(cancel)
                    self.present(alertViewController, animated: true, completion: nil)
                }
            }
        }
        
        
        //collectionViewLayout
        func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, sizeForItemAt indexPath: IndexPath) -> CGSize {
            if collectionView == Vw_CollectShape
            {
                if (UIScreen.main.bounds.size.height == 568.0)
                {
                    let width = UIScreen.main.bounds.width
                    return CGSize(width: (width - 30)/4, height: 110) // width & height are the same to make a square cell
                }else{
                    let width = UIScreen.main.bounds.width
                    return CGSize(width: (width - 30)/4, height: 110) // width & height are the same to make a square cell
                }
                
            }
            else{
                let width = UIScreen.main.bounds.width
                return CGSize(width: (width - 10)/4, height: (width - 10)/4) // width & height are the same to make a square cell
            }
        }
        
        //MARK: -- ImageView Delegates
        
        func openCamera() {
            if UIImagePickerController.isSourceTypeAvailable(UIImagePickerControllerSourceType.camera) {
                imagePicker.delegate = self
                imagePicker.sourceType = UIImagePickerControllerSourceType.camera
                imagePicker.allowsEditing = false
                self .present(imagePicker, animated: true, completion: nil)
            }
            else {
                let alertWarning = UIAlertView(title:"Warning", message: "You don't have camera", delegate:nil, cancelButtonTitle:"OK", otherButtonTitles:"")
                alertWarning.show()
            }
        }
        
        func openGallary() {
            if UIImagePickerController.isSourceTypeAvailable(UIImagePickerControllerSourceType.photoLibrary) {
                imagePicker.delegate = self
                imagePicker.sourceType = UIImagePickerControllerSourceType.photoLibrary
                imagePicker.allowsEditing = false
                self.present(imagePicker, animated: true, completion: nil)
            }
        }
        
        //imagePickerController
        public func imagePickerController(_ picker: UIImagePickerController, didFinishPickingMediaWithInfo info: [String : Any])
        {
            // assign pickup image into varibale
            var selectedimage : UIImage!
            
            if let editedImage = info[UIImagePickerControllerEditedImage] as? UIImage
            {
                selectedimage =  editedImage.fixedOrientation()
            }
            else
            {
                selectedimage = info[UIImagePickerControllerOriginalImage] as? UIImage
                selectedimage =  selectedimage.fixedOrientation()
                
            }
            
            View = true
            
            items.append(selectedimage)
            print(items)
            
            if items.count != 0 {
                Vw_Collection.reloadData()
            }
            
            dismiss(animated: true, completion: nil)
        }
        
        //imagePickerControllerDidCancel
        public func imagePickerControllerDidCancel(_ picker: UIImagePickerController)
        {
            dismiss(animated: true, completion: nil)
        }
        
        
        //MARK: -- ProgressBar Delegate
        
        func progressBar(_ progressBar: FlexibleSteppedProgressBar, didSelectItemAtIndex index: Int) {
            self.view.endEditing(true)
            progressBar.currentIndex = index
            if index > maxIndex {
                maxIndex = index
                progressBar.completedTillIndex = maxIndex
            }
        }
        
        func progressBar(_ progressBar: FlexibleSteppedProgressBar,
                         canSelectItemAtIndex index: Int) -> Bool {
            
            if progressBar == progressBarWithDifferentDimensions {
                print("Print Index \(index)")
                
                print("name is \(ClarityArr[index]["ClarityName"]!)")
                ClarityArrIndex = ClarityArr[index]["ClarityID"] as! NSString
            }
            if progressBar == progressBarWithDifferentDimensions2 {
                print("Print Index \(index)")
                
                print("name is \(ColorArr[index]["ColorName"]!)")
                ColorArrIndex = ColorArr[index]["ColorID"] as! NSString
            }
            if progressBar == progressBarWithDifferentDimensions3 {
                print("Print Index \(index)")
                
                print("name is \(ArrGold[index])")
                ArrGoldIndex = ArrGold[index] as NSString
            }
            
            
            return true
        }
        
        func progressBar(_ progressBar: FlexibleSteppedProgressBar,
                         textAtIndex index: Int, position: FlexibleSteppedProgressBarTextLocation) -> String
        {
            if progressBar == progressBarWithDifferentDimensions {
                if position == FlexibleSteppedProgressBarTextLocation.bottom {
                    
                    // print("Print Index \(index)")
                    
                    switch index {
                        
                    case 0...ClarityArr.count:
                        return ClarityArr[index]["ClarityName"] as! String
                        
                    default: break
                    }
                }
            }
            if progressBar == progressBarWithDifferentDimensions2 {
                if position == FlexibleSteppedProgressBarTextLocation.bottom {
                    
                    // print("Print Index \(index)")
                    
                    switch index {
                        
                    case 0...ColorArr.count:
                        return ColorArr[index]["ColorName"] as! String
                        
                    default: break
                    }
                }
            }
            if progressBar == progressBarWithDifferentDimensions3 {
                if position == FlexibleSteppedProgressBarTextLocation.bottom {
                    
                    // print("Print Index \(index)")
                    
                    switch index {
                        
                        /*case 0: return "10k GOLD"
                         case 1: return "14k GOLD"
                         case 2: return "18k GOLD"
                         case 3: return "21k GOLD"
                         case 4: return "PLATINUM"
                         default: return "10k GOLD"*/
                    case 0...ArrGold.count:
                        return self.ArrGold[index]
                        
                    default: break
                        // "10k GOLD","14k GOLD","18k GOLD","21k GOLD","PLATINUM"
                    }
                }
            }
            
            return ""
        }
        
        
        //MARK: -- DropDown Delegate
        
        func dropDownListView(_ dropdownListView: DropDownListView!, didSelectedIndex anIndex: Int) {
            MaterialArrIndex = anIndex
            let dict = MaterialArr[anIndex]["MaterialType"]
            if (dict?.isEqual("Other"))!
            {
               // Txt_CaretWeight.text = ""
                Txt_CaretWeight.isEnabled=true

                 if OtherBool == false{
                OtherBool = true
                Txt_Other.isHidden = false
                 Txt_Other.text = "";
                    claritycolorvw.frame = CGRect.init(x: claritycolorvw.frame.origin.x, y: 140, width: claritycolorvw.frame.size.width, height: claritycolorvw.frame.size.height)
                if EngagementOrLooseRing == "0"
                {
                    Vw_under.frame = CGRect.init(x: Vw_under.frame.origin.x, y: Vw_under.frame.origin.y+50, width: Vw_under.frame.size.width, height: Vw_under.frame.size.height)
                }
                else{
                    Vw_Matel.frame = CGRect.init(x: Vw_Matel.frame.origin.x, y: Vw_Matel.frame.origin.y+50, width: Vw_Matel.frame.size.width, height: Vw_Matel.frame.size.height)
                    Vw_under.frame = CGRect.init(x: Vw_under.frame.origin.x, y: Vw_under.frame.origin.y + 50, width: Vw_under.frame.size.width, height: Vw_under.frame.size.height)
                }
                if (UIScreen.main.bounds.size.height == 568.0)
                {
                    print("iphone5")
                    Scoll_Vw.contentSize = CGSize(width: self.view.frame.size.width, height: 1650)
                }
                else{
                    Scoll_Vw.contentSize = CGSize(width: self.view.frame.size.width, height: 1650)
                }
                }
            }else if (dict?.isEqual("None"))!
            {
                Txt_Other.isHidden = true
                Txt_CaretWeight.text = "0"
                Txt_CaretWeight.isEnabled=false
                if OtherBool == true{
                    OtherBool = false
                    Txt_Other.isHidden = true
                    claritycolorvw.frame = CGRect.init(x: claritycolorvw.frame.origin.x, y: 100, width: claritycolorvw.frame.size.width, height: claritycolorvw.frame.size.height)
                    if EngagementOrLooseRing == "0"
                    {
                        Vw_under.frame = CGRect.init(x: Vw_under.frame.origin.x, y: Vw_under.frame.origin.y-50, width: Vw_under.frame.size.width, height: Vw_under.frame.size.height)
                    }
                    else{
                        Vw_Matel.frame = CGRect.init(x: Vw_Matel.frame.origin.x, y: Vw_Matel.frame.origin.y-50, width: Vw_Matel.frame.size.width, height: Vw_Matel.frame.size.height)
                        
                        Vw_under.frame = CGRect.init(x: Vw_under.frame.origin.x, y: Vw_under.frame.origin.y - 50, width: Vw_under.frame.size.width, height: Vw_under.frame.size.height)
                    }
                    if (UIScreen.main.bounds.size.height == 568.0)
                    {
                        print("iphone5")
                        Scoll_Vw.contentSize = CGSize(width: self.view.frame.size.width, height: 1650)
                    }
                    else{
                        Scoll_Vw.contentSize = CGSize(width: self.view.frame.size.width, height: 1650)
                    }
                }
                else{
                    OtherBool = false
                }
                
            }
            else{
              ///  Txt_CaretWeight.text = ""
                Txt_CaretWeight.isEnabled=true
                if OtherBool == true{
                    OtherBool = false
                    Txt_Other.isHidden = true
                     claritycolorvw.frame = CGRect.init(x: claritycolorvw.frame.origin.x, y: 100, width: claritycolorvw.frame.size.width, height: claritycolorvw.frame.size.height)
                    if EngagementOrLooseRing == "0"
                    {
                        Vw_under.frame = CGRect.init(x: Vw_under.frame.origin.x, y: Vw_under.frame.origin.y-50, width: Vw_under.frame.size.width, height: Vw_under.frame.size.height)
                    }
                    else{
                        Vw_Matel.frame = CGRect.init(x: Vw_Matel.frame.origin.x, y: Vw_Matel.frame.origin.y-50, width: Vw_Matel.frame.size.width, height: Vw_Matel.frame.size.height)
                        
                        Vw_under.frame = CGRect.init(x: Vw_under.frame.origin.x, y: Vw_under.frame.origin.y - 50, width: Vw_under.frame.size.width, height: Vw_under.frame.size.height)
                    }
                    if (UIScreen.main.bounds.size.height == 568.0)
                    {
                        print("iphone5")
                        Scoll_Vw.contentSize = CGSize(width: self.view.frame.size.width, height: 1650)
                    }
                    else{
                        Scoll_Vw.contentSize = CGSize(width: self.view.frame.size.width, height: 1650)
                    }
                }
                else{
                    OtherBool = false
                }
            }
            Btn_Gem.setTitle(dict as? String, for: .normal)
        }
        
        func dropDownListView(_ dropdownListView: DropDownListView!, datalist ArryData: NSMutableArray!) {
            print("list \(ArryData.componentsJoined(by: ", "))")
            if (ArryData.count != 0){
                Btn_Gem.setTitle(ArryData.componentsJoined(by: ", "), for: .normal)
            }
            else{
                Btn_Gem.setTitle("GEM MATERIAL :", for: .normal)
            }
        }
        
        func dropDownListViewDidCancel() {
            
        }
        
         
        
        //MARK: -- SideBar Delegate
        func revealController(_ revealController: SWRevealViewController, didMoveTo position: FrontViewPosition) {
            if case .right = position {
                
                revealController.frontViewController.view.alpha = 0.5
                self.view.isUserInteractionEnabled = false
                self.revealViewController().frontViewController.view.addGestureRecognizer(self.revealViewController().panGestureRecognizer())
                self.revealViewController().frontViewController.view.addGestureRecognizer(self.revealViewController().tapGestureRecognizer())
            } else {
                
                revealController.frontViewController.view.alpha = 1
                self.revealViewController().tapGestureRecognizer()
                self.view.isUserInteractionEnabled = true
            }
        }
        
        
        //MARK: -- CheckBox Delegate
        func didTap(_ checkBox: BEMCheckBox) {
            self.view.endEditing(true)
            if (checkBox==Check_Engagement)
            {
                
                print("it's engagement")
                
                let DiamondAttributed = NSMutableAttributedString.init(string: "*Center Stone carat weight :")
                               let Diamondterms:NSString = "*Center Stone carat weight :"
                               let Diamondrange:NSRange = Diamondterms.range(of: "*")
                               let Diamondrangech :NSRange = Diamondterms.range(of: "Center Stone carat weight :")
                               DiamondAttributed.addAttribute(NSAttributedStringKey.foregroundColor, value: UIColor.red, range: Diamondrange)
                               DiamondAttributed.addAttribute(NSAttributedStringKey.foregroundColor, value: UIColor.darkGray, range: Diamondrangech)
                               Txt_CaretWeight.attributedPlaceholder = DiamondAttributed
                
                Txt_settingcaratweight.isHidden = false
                EngagementOrLooseRing = "1"
                
                lblcutshape.frame.origin.y = 260
                Vw_CollectShape.frame.origin.y = 295
                if OtherBool == true{
                    
                    Vw_Matel.frame = CGRect.init(x: Vw_Matel.frame.origin.x, y: Vw_Matel.frame.origin.y+50, width: Vw_Matel.frame.size.width, height: Vw_Matel.frame.size.height)
                    
                    Vw_under.frame = CGRect.init(x: Vw_under.frame.origin.x, y: Vw_under.frame.origin.y+130, width: Vw_under.frame.size.width, height: Vw_under.frame.size.height)
                }
                else{
                    Vw_under.frame = CGRect.init(x: Vw_under.frame.origin.x, y: Vw_under.frame.origin.y+130, width: Vw_under.frame.size.width, height: Vw_under.frame.size.height)
                    
                }
                
                Vw_Matel.isHidden = false
                
                if (UIScreen.main.bounds.size.height == 568.0)
                {
                    print("iphone5")
                    Scoll_Vw.contentSize = CGSize(width: self.view.frame.size.width, height: 1650)
                }
                else{
                    Scoll_Vw.contentSize = CGSize(width: self.view.frame.size.width, height: 1650)
                }
                
            }
            else if (checkBox==Check_Loose){
                print("it's Loose Stone")
                
                let DiamondAttributed = NSMutableAttributedString.init(string: "*Total carat weight :")
                               let Diamondterms:NSString = "*Total carat weight :"
                               let Diamondrange:NSRange = Diamondterms.range(of: "*")
                               let Diamondrangech :NSRange = Diamondterms.range(of: "*Total carat weight :")
                               DiamondAttributed.addAttribute(NSAttributedStringKey.foregroundColor, value: UIColor.red, range: Diamondrange)
                               DiamondAttributed.addAttribute(NSAttributedStringKey.foregroundColor, value: UIColor.darkGray, range: Diamondrangech)
                               Txt_CaretWeight.attributedPlaceholder = DiamondAttributed
                
                Txt_settingcaratweight.isHidden = true
                lblcutshape.frame.origin.y = Txt_settingcaratweight.frame.origin.y + 10
                Vw_CollectShape.frame.origin.y = lblcutshape.frame.origin.y + 40
                EngagementOrLooseRing = "0"
                
                if OtherBool == true{
                    
                    Vw_Matel.frame = CGRect.init(x: Vw_Matel.frame.origin.x, y: Vw_Matel.frame.origin.y-50, width: Vw_Matel.frame.size.width, height: Vw_Matel.frame.size.height)
                    
                    Vw_under.frame = CGRect.init(x: Vw_under.frame.origin.x, y: Vw_under.frame.origin.y-130, width: Vw_under.frame.size.width, height: Vw_under.frame.size.height)
                }
                else{
                    Vw_under.frame = CGRect.init(x: Vw_under.frame.origin.x, y: Vw_under.frame.origin.y-130, width: Vw_under.frame.size.width, height: Vw_under.frame.size.height)
                }
                
                Vw_Matel.isHidden = true
                
                if (UIScreen.main.bounds.size.height == 568.0)
                {
                    print("iphone5")
                    Scoll_Vw.contentSize = CGSize(width: self.view.frame.size.width, height: 1650)
                }
                else{
                    Scoll_Vw.contentSize = CGSize(width: self.view.frame.size.width, height: 1650)
                }
                
            }
            else{
                
                if (checkBox.on == true){
                    Btn_Next.alpha = 1
                    Btn_Next.isEnabled = true
                    Btn_Next.isUserInteractionEnabled = true
                } else {
                    Btn_Next.alpha = 0.2
                    Btn_Next.isEnabled = false
                    Btn_Next.isUserInteractionEnabled = false
                }
            }
        }
        
        
        //MARK: -- Scrollview Delegate
        func scrollViewWillBeginDragging(_ scrollView: UIScrollView) {
            // execute when you drag the scrollView
            self.view.endEditing(true)
            print("scrollViewWillBeginDragging")
        }
        }
