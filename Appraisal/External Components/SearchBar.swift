class SearchBar: UISearchBar {
    
    
    override func layoutSubviews() {
        super.layoutSubviews()
        allViews(view: self)
    }
    
    private func allViews(view: UIView) {
    
        for v in view.subviews {
//            v.backgroundColor = UIColor.clear
            
            allViews(view: v)
        }
    
        //        print("-----\n\(view)\n----")
        if view is UITextField, let textField = view as? UITextField {
            textField.textColor = UIColor.white
            textField.backgroundColor = UIColor.clear//UIColor(red: 0, green: 0, blue: 0, alpha: 0.8)
            textField.tintColor = UIColor.white
            textField.font = UIFont(name: "Gotham-Light",size: 15)
            
                        textField.leftView = nil //imageView
                        textField.leftViewMode = UITextFieldViewMode.always
                        textField.clearButtonMode = .never
                    }
     
        
        }
        
        
    }

