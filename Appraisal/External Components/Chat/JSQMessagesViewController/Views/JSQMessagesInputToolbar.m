//
//  Created by Jesse Squires
//  http://www.jessesquires.com
//
//
//  Documentation
//  http://cocoadocs.org/docsets/JSQMessagesViewController
//
//
//  GitHub
//  https://github.com/jessesquires/JSQMessagesViewController
//
//
//  License
//  Copyright (c) 2014 Jesse Squires
//  Released under an MIT license: http://opensource.org/licenses/MIT
//

#import "JSQMessagesInputToolbar.h"

#import "JSQMessagesComposerTextView.h"

#import "JSQMessagesToolbarButtonFactory.h"

#import "UIColor+JSQMessages.h"
#import "UIImage+JSQMessages.h"
#import "UIView+JSQMessages.h"

static void * kJSQMessagesInputToolbarKeyValueObservingContext = &kJSQMessagesInputToolbarKeyValueObservingContext;


@interface JSQMessagesInputToolbar ()

@property (assign, nonatomic) BOOL jsq_isObserving;

@end



@implementation JSQMessagesInputToolbar

@dynamic delegate;

#pragma mark - Initialization

- (void)awakeFromNib
{
    [super awakeFromNib];
    self.backgroundColor = [UIColor whiteColor];
    self.jsq_isObserving = NO;
    self.sendButtonLocation = JSQMessagesInputSendButtonLocationRight;
    self.enablesSendButtonAutomatically = YES;

    self.preferredDefaultHeight = 44.0f;
    self.maximumHeight = NSNotFound;

    JSQMessagesToolbarContentView *toolbarContentView = [self loadToolbarContentView];
    toolbarContentView.frame = self.frame;
    [self addSubview:toolbarContentView];
    
    [self jsq_pinAllEdgesOfSubview:toolbarContentView];
    [self setNeedsUpdateConstraints];
    _contentView = toolbarContentView;

    [self jsq_addObservers];

//    JSQMessagesToolbarButtonFactory *toolbarButtonFactory = [[JSQMessagesToolbarButtonFactory alloc] initWithFont:[UIFont preferredFontForTextStyle:UIFontTextStyleHeadline]];
//    self.contentView.leftBarButtonItem = [toolbarButtonFactory defaultAccessoryButtonItem];
//    self.contentView.rightBarButtonItem = [toolbarButtonFactory defaultSendButtonItem];

//    [self updateSendButtonEnabledState];

    self.contentView.recordBoxView.hidden = YES;
    
    [[NSNotificationCenter defaultCenter] addObserver:self
                                             selector:@selector(textViewTextDidChangeNotification:)
                                                 name:UITextViewTextDidChangeNotification
                                               object:_contentView.textView];
    
    [self.contentView.cameraBtn addTarget:self
                                   action:@selector(cameraButtonPressed:)
                         forControlEvents:UIControlEventTouchUpInside];

    [self.contentView.recordBtn addTarget:self
                                   action:@selector(recordButtonPressed:)
                         forControlEvents:UIControlEventTouchDown];
    
    [self.contentView.recordBtn addTarget:self
                                   action:@selector(recordCancelPressed:)
                         forControlEvents:UIControlEventTouchUpOutside];

    [self.contentView.recordBtn addTarget:self
                                   action:@selector(recordBtnUp:)
                         forControlEvents:UIControlEventTouchUpInside];
    
    [self.contentView.sendBtn addTarget:self
                                 action:@selector(sendButtonPressed:)
                       forControlEvents:UIControlEventTouchUpInside];
}

- (JSQMessagesToolbarContentView *)loadToolbarContentView
{
    NSArray *nibViews = [[NSBundle bundleForClass:[JSQMessagesInputToolbar class]] loadNibNamed:NSStringFromClass([JSQMessagesToolbarContentView class])
                                                                                          owner:nil
                                                                                        options:nil];
    return nibViews.firstObject;
}

- (void)dealloc
{
    [self jsq_removeObservers];
    [[NSNotificationCenter defaultCenter] removeObserver:self];
}

#pragma mark - Setters

- (void)setPreferredDefaultHeight:(CGFloat)preferredDefaultHeight
{
    NSParameterAssert(preferredDefaultHeight > 0.0f);
    _preferredDefaultHeight = preferredDefaultHeight;
}

- (void)setEnablesSendButtonAutomatically:(BOOL)enablesSendButtonAutomatically
{
    _enablesSendButtonAutomatically = enablesSendButtonAutomatically;
    [self updateSendButtonEnabledState];
}

#pragma mark - Actions

- (void)cameraButtonPressed:(UIButton *)sender
{
    [self.delegate messagesInputToolbar:self didPressCameraButton:sender];
}

- (void)sendButtonPressed:(UIButton *)sender
{
    [self.delegate messagesInputToolbar:self didPressSendButton:sender];
}

- (void)recordButtonPressed:(UIButton *)sender
{
    self.contentView.recordBoxView.hidden = NO;
    [self.delegate messagesInputToolbar:self didPressRecordButton:sender];
}

- (void)recordCancelPressed:(UIButton *)sender
{
    self.contentView.recordBoxView.hidden = YES;
    [self.delegate messagesInputToolbar:self didCancelRecordButton:sender];
}

- (void)recordBtnUp:(UIButton *)sender
{
    self.contentView.recordBoxView.hidden = YES;
    [self.delegate messagesInputToolbar:self didUpRecordButton:sender];
}


#pragma mark - Input toolbar

- (void)updateSendButtonEnabledState
{
    if (!self.enablesSendButtonAutomatically) {
        return;
    }

    BOOL enabled = [self.contentView.textView hasText];
    if (enabled) {
        [self.contentView setTextViewHorizontalSpacing:YES];
        self.contentView.attachBtnBox.hidden = YES;
        self.contentView.sendBtn.hidden = NO;
    } else {
        [self.contentView setTextViewHorizontalSpacing:NO];
        self.contentView.attachBtnBox.hidden = NO;
        self.contentView.sendBtn.hidden = YES;
    }
}

#pragma mark - Notifications

- (void)textViewTextDidChangeNotification:(NSNotification *)notification
{
    [self updateSendButtonEnabledState];
}

#pragma mark - Key-value observing

- (void)observeValueForKeyPath:(NSString *)keyPath ofObject:(id)object change:(NSDictionary *)change context:(void *)context
{
    
    if (context == kJSQMessagesInputToolbarKeyValueObservingContext) {
        if (object == self.contentView) {

            if ([keyPath isEqualToString:NSStringFromSelector(@selector(cameraBtn))]) {

                [self.contentView.cameraBtn removeTarget:self
                                                          action:NULL
                                                forControlEvents:UIControlEventTouchUpInside];

                [self.contentView.cameraBtn addTarget:self
                                                       action:@selector(cameraButtonPressed:)
                                             forControlEvents:UIControlEventTouchUpInside];
            }
            else if ([keyPath isEqualToString:NSStringFromSelector(@selector(recordBtn))]) {
//touch down event
//                [self.contentView.recordBtn removeTarget:self
//                                                           action:NULL
//                                                 forControlEvents:UIControlEventTouchDown];

                [self.contentView.recordBtn addTarget:self
                                                        action:@selector(recordButtonPressed:)
                                              forControlEvents:UIControlEventTouchDown];
                
//touch outside event - cancel recording
//                [self.contentView.recordBtn removeTarget:self
//                                                           action:NULL
//                                                 forControlEvents:UIControlEventTouchUpOutside];
                
                [self.contentView.recordBtn addTarget:self
                                                        action:@selector(recordCancelPressed:)
                                              forControlEvents:UIControlEventTouchUpOutside];
//touch up inside event - send voice message
//                [self.contentView.recordBtn removeTarget:self
//                                                           action:NULL
//                                                 forControlEvents:UIControlEventTouchUpInside];
                
                [self.contentView.recordBtn addTarget:self
                                                        action:@selector(recordBtnUp:)
                                              forControlEvents:UIControlEventTouchUpInside];

            } else if ([keyPath isEqualToString:NSStringFromSelector(@selector(sendBtn))]) {
                [self.contentView.sendBtn removeTarget:self
                                                          action:NULL
                                                forControlEvents:UIControlEventTouchUpInside];
                
                [self.contentView.sendBtn addTarget:self
                                                       action:@selector(sendButtonPressed:)
                                             forControlEvents:UIControlEventTouchUpInside];
            }

            [self updateSendButtonEnabledState];
        }
    }
}

- (void)jsq_addObservers
{
    if (self.jsq_isObserving) {
        return;
    }

    [self.contentView addObserver:self
                       forKeyPath:NSStringFromSelector(@selector(cameraBtn))
                          options:0
                          context:kJSQMessagesInputToolbarKeyValueObservingContext];

    [self.contentView addObserver:self
                       forKeyPath:NSStringFromSelector(@selector(recordBtn))
                          options:0
                          context:kJSQMessagesInputToolbarKeyValueObservingContext];

    [self.contentView addObserver:self
                       forKeyPath:NSStringFromSelector(@selector(sendBtn))
                          options:0
                          context:kJSQMessagesInputToolbarKeyValueObservingContext];

    self.jsq_isObserving = YES;
}

- (void)jsq_removeObservers
{
    if (!_jsq_isObserving) {
        return;
    }

    @try {
        [_contentView removeObserver:self
                          forKeyPath:NSStringFromSelector(@selector(cameraBtn))
                             context:kJSQMessagesInputToolbarKeyValueObservingContext];

        [_contentView removeObserver:self
                          forKeyPath:NSStringFromSelector(@selector(recordBtn))
                             context:kJSQMessagesInputToolbarKeyValueObservingContext];
        
        [_contentView removeObserver:self
                          forKeyPath:NSStringFromSelector(@selector(sendBtn))
                             context:kJSQMessagesInputToolbarKeyValueObservingContext];

    }
    @catch (NSException *__unused exception) { }
    
    _jsq_isObserving = NO;
}

@end
