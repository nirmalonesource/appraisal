//
//  DoctorPopupOutgoingTableViewCell.h
//  JSQMessages
//
//  Created by ADMIN on 1/22/18.
//  Copyright © 2018 Hexed Bits. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "JSQMessagesCollectionViewCell.h"
#import "HCSStarRatingView.h"

@interface DoctorPopupOutgoingTableViewCell : JSQMessagesCollectionViewCell

@property (weak, nonatomic) IBOutlet UILabel *Lbl_DoctorName;
@property (weak, nonatomic) IBOutlet UIImageView *Img_Vw;
@property (weak, nonatomic) IBOutlet UIButton *Btn_Profile;
@property (weak, nonatomic) IBOutlet UIButton *Btn_Book;
@property (weak, nonatomic) IBOutlet HCSStarRatingView *Vw_star;
@property (weak, nonatomic) IBOutlet UILabel *Lbl_RatingCount;
@property (weak, nonatomic) IBOutlet UIView *Vw_Back;
@property (strong, nonatomic) IBOutlet UILabel *Lbl_Speciality;

@end
