//
//  Created by Jesse Squires
//  http://www.jessesquires.com
//
//
//  Documentation
//  http://cocoadocs.org/docsets/JSQMessagesViewController
//
//
//  GitHub
//  https://github.com/jessesquires/JSQMessagesViewController
//
//
//  License
//  Copyright (c) 2014 Jesse Squires
//  Released under an MIT license: http://opensource.org/licenses/MIT
//

#import "JSQMessagesBubbleImageFactory.h"

#import "UIImage+JSQMessages.h"
#import "UIColor+JSQMessages.h"


@interface JSQMessagesBubbleImageFactory ()

@property (strong, nonatomic, readonly) UIImage *bubbleImage;

@property (assign, nonatomic, readonly) UIEdgeInsets capInsets;

@property (assign, nonatomic, readonly) BOOL isRightToLeftLanguage;

@end


@implementation JSQMessagesBubbleImageFactory

#pragma mark - Initialization

- (instancetype)initWithBubbleImage:(UIImage *)bubbleImage
                          capInsets:(UIEdgeInsets)capInsets
                    layoutDirection:(UIUserInterfaceLayoutDirection)layoutDirection
{
    NSParameterAssert(bubbleImage != nil);

    self = [super init];
    if (self) {
        _bubbleImage = bubbleImage;
        _capInsets = capInsets;
        _layoutDirection = layoutDirection;

        if (UIEdgeInsetsEqualToEdgeInsets(capInsets, UIEdgeInsetsZero)) {
            _capInsets = [self jsq_centerPointEdgeInsetsForImageSize:bubbleImage.size];
        }
    }
    return self;
}

- (instancetype)init
{
    return [self initWithBubbleImage:[UIImage jsq_bubbleCompactImage]
                           capInsets:UIEdgeInsetsZero
                     layoutDirection:[UIApplication sharedApplication].userInterfaceLayoutDirection];
}

#pragma mark - Public

- (JSQMessagesBubbleImage *)outgoingMessagesBubbleImageWithColor:(UIColor *)color
{
    return [self jsq_messagesBubbleImageWithColor:color flippedForIncoming:NO ^ self.isRightToLeftLanguage];
}

- (JSQMessagesBubbleImage *)incomingMessagesBubbleImageWithColor:(UIColor *)color
{
    return [self jsq_messagesBubbleImageWithColor:color flippedForIncoming:YES ^ self.isRightToLeftLanguage];
}

#pragma mark - Private

- (BOOL)isRightToLeftLanguage
{
    return (self.layoutDirection == UIUserInterfaceLayoutDirectionRightToLeft);
}

- (UIEdgeInsets)jsq_centerPointEdgeInsetsForImageSize:(CGSize)bubbleImageSize
{
    // make image stretchable from center point
    CGPoint center = CGPointMake(bubbleImageSize.width / 2.0f, bubbleImageSize.height / 2.0f);
    return UIEdgeInsetsMake(center.y, center.x, center.y, center.x);
}

- (JSQMessagesBubbleImage *)jsq_messagesBubbleImageWithColor:(UIColor *)color flippedForIncoming:(BOOL)flippedForIncoming
{
    NSParameterAssert(color != nil);

    UIImage *normalBubble; // = [self.bubbleImage jsq_imageMaskedWithColor:color];
    UIImage *highlightedBubble; // = [self.bubbleImage jsq_imageMaskedWithColor:[color jsq_colorByDarkeningColorWithValue:0.12f]];
    

    if ([color isEqual:[UIColor jsq_messageBubbleLightGrayColor]]) {
        normalBubble = [UIImage imageNamed:@"chat_recv"];
        highlightedBubble = [UIImage imageNamed:@"chat_recv"];
    } else {
        normalBubble = [UIImage imageNamed:@"chat_send"];
        highlightedBubble = [UIImage imageNamed:@"chat_send"];
    }

//    CGPoint center = CGPointMake(normalBubble.size.width / 2.0f, normalBubble.size.height / 2.0f);
    

    UIEdgeInsets edgeInset = UIEdgeInsetsMake(25, 15, 15, 25);
    
    normalBubble = [self jsq_stretchableImageFromImage:normalBubble withCapInsets:edgeInset];
    highlightedBubble = [self jsq_stretchableImageFromImage:highlightedBubble withCapInsets:edgeInset];
//
//    //add corner
//    UIImage *cornerImg;
//    
//    if ([color isEqual:[UIColor jsq_messageBubbleLightGrayColor]])
//    {
//        cornerImg = [UIImage imageNamed:@"receive_corner"];
//        normalBubble = [self imageByCombiningImage:normalBubble withImage:cornerImg isIncoming:YES];
//        highlightedBubble = [self imageByCombiningImage:highlightedBubble withImage:cornerImg isIncoming:YES];
//    } else {
//        cornerImg = [UIImage imageNamed:@"corner"];
//        normalBubble = [self imageByCombiningImage:normalBubble withImage:cornerImg isIncoming:NO];
//        highlightedBubble = [self imageByCombiningImage:highlightedBubble withImage:cornerImg isIncoming:NO];
//    }
    
    
    return [[JSQMessagesBubbleImage alloc] initWithMessageBubbleImage:normalBubble highlightedImage:highlightedBubble];
}

- (UIImage*)imageByCombiningImage:(UIImage*)firstImage withImage:(UIImage*)secondImage isIncoming: (BOOL)isIncoming {
    
    CGSize size = CGSizeMake(firstImage.size.width +10, firstImage.size.height);
    
    UIGraphicsBeginImageContext(size);
    
    if (isIncoming) {
        [firstImage drawInRect:CGRectMake(0,0,size.width - 10, firstImage.size.height)];
        [secondImage drawInRect:CGRectMake(size.width - 10, 20, 20, 20)];
    } else {
        [firstImage drawInRect:CGRectMake(10,0,size.width - 10, firstImage.size.height)];
        [secondImage drawInRect:CGRectMake(10, 20, 20, 20)];
    }
    
    UIImage *finalImage = UIGraphicsGetImageFromCurrentImageContext();
    
    UIGraphicsEndImageContext();
    
    return finalImage;
}

- (UIImage *)jsq_horizontallyFlippedImageFromImage:(UIImage *)image
{
    return [UIImage imageWithCGImage:image.CGImage
                               scale:image.scale
                         orientation:UIImageOrientationUpMirrored];
}

- (UIImage *)jsq_stretchableImageFromImage:(UIImage *)image withCapInsets:(UIEdgeInsets)capInsets
{
    return [image resizableImageWithCapInsets:capInsets resizingMode:UIImageResizingModeStretch];
}

@end
