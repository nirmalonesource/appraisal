//
//  Created by Jesse Squires
//  http://www.jessesquires.com
//
//
//  Documentation
//  http://cocoadocs.org/docsets/JSQMessagesViewController
//
//
//  GitHub
//  https://github.com/jessesquires/JSQMessagesViewController
//
//
//  License
//  Copyright (c) 2014 Jesse Squires
//  Released under an MIT license: http://opensource.org/licenses/MIT
//

#import "UIImage+JSQMessages.h"

#import "NSBundle+JSQMessages.h"


@implementation UIImage (JSQMessages)

- (UIImage *)jsq_imageMaskedWithColor:(UIColor *)maskColor
{
    NSParameterAssert(maskColor != nil);
    
    CGRect imageRect = CGRectMake(0.0f, 0.0f, self.size.width, self.size.height);
    CGRect cornerimageRect;
    
    if ([maskColor isEqual:[UIColor colorWithHue:240.0f / 360.0f
                                      saturation:0.02f
                                      brightness:0.92f
                                           alpha:1.0f]]){
        cornerimageRect = CGRectMake(0, 10, 20, 20);
    } else {
        
    }
    UIImage *newImage = nil;
    UIImage *cornernewImage = nil;
    
    UIGraphicsBeginImageContextWithOptions(imageRect.size, NO, self.scale);
    {
        CGContextRef context = UIGraphicsGetCurrentContext();
        
        CGContextSetFillColorWithColor(context, [maskColor CGColor]);
        CGContextFillRect(context, imageRect);
        newImage = UIGraphicsGetImageFromCurrentImageContext();
    }
    UIGraphicsEndImageContext();
    
    //corner round image
    UIGraphicsBeginImageContextWithOptions(imageRect.size, NO, 1.0);
    [[UIBezierPath bezierPathWithRoundedRect:imageRect
                                cornerRadius:10.0] addClip];
    [newImage drawInRect:imageRect];
    newImage = UIGraphicsGetImageFromCurrentImageContext();
    UIGraphicsEndImageContext();
    
    
    //corner image
    UIGraphicsBeginImageContextWithOptions(cornerimageRect.size, NO, self.scale);
    {
        CGContextRef context = UIGraphicsGetCurrentContext();
        
        CGContextSetFillColorWithColor(context, [maskColor CGColor]);
        CGContextFillRect(context, cornerimageRect);
        
        cornernewImage = UIGraphicsGetImageFromCurrentImageContext();
    }
    
    cornernewImage = [UIImage imageWithCGImage:cornernewImage.CGImage scale:1.0 orientation:UIImageOrientationLeft];
    
    UIGraphicsEndImageContext();
    
    //merge corner
    
    if ([maskColor isEqual:[UIColor colorWithHue:240.0f / 360.0f
                                      saturation:0.02f
                                      brightness:0.92f
                                           alpha:1.0f]]) {
        CGSize size = CGSizeMake(newImage.size.width +10, newImage.size.height);
        
        UIGraphicsBeginImageContext(size);
        [newImage drawInRect:CGRectMake(20,0,size.width - 10, newImage.size.height)];
        [cornernewImage drawInRect:CGRectMake(10, 10, 20, 20)];
        
        newImage = UIGraphicsGetImageFromCurrentImageContext();
        
        UIGraphicsEndImageContext();
    } else {
        CGSize size = CGSizeMake(newImage.size.width +10, newImage.size.height);
        
        UIGraphicsBeginImageContext(size);
        [newImage drawInRect:CGRectMake(0,0,size.width - 10, newImage.size.height)];
        [cornernewImage drawInRect:CGRectMake(size.width - 10, 10, 20, 20)];
        
        newImage = UIGraphicsGetImageFromCurrentImageContext();
        
        UIGraphicsEndImageContext();
    }
    
    return newImage;
}

- (UIImage *)image:(UIImage*)originalImage scaledToSize:(CGSize)size
{
    //avoid redundant drawing
    if (CGSizeEqualToSize(originalImage.size, size))
    {
        return originalImage;
    }
    
    //create drawing context
    UIGraphicsBeginImageContextWithOptions(size, NO, 0.0f);
    
    //draw
    [originalImage drawInRect:CGRectMake(0.0f, 0.0f, size.width, size.height)];
    
    //capture resultant image
    UIImage *image = UIGraphicsGetImageFromCurrentImageContext();
    UIGraphicsEndImageContext();
    
    //return image
    return image;
}

+ (UIImage *)jsq_bubbleImageFromBundleWithName:(NSString *)name
{
    NSBundle *bundle = [NSBundle jsq_messagesAssetBundle];
    NSString *path = [bundle pathForResource:name ofType:@"png" inDirectory:@"Images"];
    return [UIImage imageWithContentsOfFile:path];
}

+ (UIImage *)jsq_bubbleRegularImage
{
    return [UIImage jsq_bubbleImageFromBundleWithName:@"bubble_regular"];
}

+ (UIImage *)jsq_bubbleRegularTaillessImage
{
    return [UIImage jsq_bubbleImageFromBundleWithName:@"bubble_tailless"];
}

+ (UIImage *)jsq_bubbleRegularStrokedImage
{
    return [UIImage jsq_bubbleImageFromBundleWithName:@"bubble_stroked"];
}

+ (UIImage *)jsq_bubbleRegularStrokedTaillessImage
{
    return [UIImage jsq_bubbleImageFromBundleWithName:@"bubble_stroked_tailless"];
}

+ (UIImage *)jsq_bubbleCompactImage
{
    return [UIImage jsq_bubbleImageFromBundleWithName:@"bubble_min"];
}

+ (UIImage *)jsq_bubbleCompactTaillessImage
{
    return [UIImage jsq_bubbleImageFromBundleWithName:@"bubble_min_tailless"];
}

+ (UIImage *)jsq_defaultAccessoryImage
{
    return [UIImage jsq_bubbleImageFromBundleWithName:@"clip"];
}

+ (UIImage *)jsq_defaultTypingIndicatorImage
{
    return [UIImage jsq_bubbleImageFromBundleWithName:@"typing"];
}

+ (UIImage *)jsq_defaultPlayImage
{
    return [UIImage jsq_bubbleImageFromBundleWithName:@"play"];
}

+ (UIImage *)jsq_defaultPauseImage
{
    return [UIImage jsq_bubbleImageFromBundleWithName:@"pause"];
}

+ (UIImage *)jsq_shareActionImage
{
    return [UIImage jsq_bubbleImageFromBundleWithName:@"share"];
}
@end
