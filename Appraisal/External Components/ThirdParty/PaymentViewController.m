//
//  PaymentViewController.m
//
//  Created by Alex MacCaw on 2/14/13.
//  Copyright (c) 2013 Stripe. All rights reserved.
//

#import <Stripe/Stripe.h>
//#import "ViewController.h"
#import <Stripe/Stripe.h>
#import "Constant.h"
#import "PaymentViewController.h"
#import "Appraisal-Swift.h"
#import "Constant.h"
//#import "OrderSuccessViewController-Swift.h"
@interface PaymentViewController () <STPPaymentCardTextFieldDelegate>
@property (weak, nonatomic) STPPaymentCardTextField *paymentTextField;
@property (weak, nonatomic) UIActivityIndicatorView *activityIndicator;
@end

@implementation PaymentViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    
  //  if kstrStripePublishableKey != nil {
    Stripe.defaultPublishableKey = kstrStripePublishableKey ;
  //  }
    self.view.backgroundColor = [UIColor whiteColor];
    self.title = @"Check out";
    if ([self respondsToSelector:@selector(setEdgesForExtendedLayout:)]) {
        self.edgesForExtendedLayout = UIRectEdgeNone;
    }
    
    // Setup save button
    NSString *title = [NSString stringWithFormat:@"Pay $%@", self.amount];
    UIBarButtonItem *saveButton = [[UIBarButtonItem alloc] initWithTitle:title style:UIBarButtonItemStyleDone target:self action:@selector(save:)];
    UIBarButtonItem *cancelButton = [[UIBarButtonItem alloc] initWithBarButtonSystemItem:UIBarButtonSystemItemCancel target:self action:@selector(cancel:)];
    saveButton.enabled = NO;
    self.navigationItem.leftBarButtonItem = cancelButton;
    self.navigationItem.rightBarButtonItem = saveButton;
    
    // Setup payment view
    STPPaymentCardTextField *paymentTextField = [[STPPaymentCardTextField alloc] init];
    paymentTextField.delegate = self;
    self.paymentTextField = paymentTextField;
    [self.view addSubview:paymentTextField];
    
    // Setup Activity Indicator
    UIActivityIndicatorView *activityIndicator = [[UIActivityIndicatorView alloc] initWithActivityIndicatorStyle:UIActivityIndicatorViewStyleGray];
  //  activityIndicator.hidesWhenStopped = YES;
    self.activityIndicator = activityIndicator;
    [self.view addSubview:activityIndicator];
}

- (void)viewDidLayoutSubviews {
    [super viewDidLayoutSubviews];
    CGFloat padding = 15;
    CGFloat width = CGRectGetWidth(self.view.frame) - (padding * 2);
    self.paymentTextField.frame = CGRectMake(padding, padding, width, 44);
    
    self.activityIndicator.center = self.view.center;
}

- (void)paymentCardTextFieldDidChange:(nonnull STPPaymentCardTextField *)textField {
    self.navigationItem.rightBarButtonItem.enabled = textField.isValid;
}

- (void)cancel:(id)sender {
   // [self.presentingViewController dismissViewControllerAnimated:YES completion:nil];
   // [self.navigationController popToRootViewControllerAnimated:YES];
    [self.navigationController popViewControllerAnimated:YES];
}

- (void)save:(id)sender {
    if (![self.paymentTextField isValid]) {
        return;
    }
    if (![Stripe defaultPublishableKey]) {
        NSError *error = [NSError errorWithDomain:StripeDomain
                                             code:STPInvalidRequestError
                                         userInfo:@{
                                                    NSLocalizedDescriptionKey: @"Please specify a Stripe Publishable Key in Constant.h"
                                                    }];
      //  [self.delegate paymentViewController:self didFinish:error:Token];
        [self.delegate paymentViewController:self didFinish:error :@""];
        return;
    }
    [self.activityIndicator startAnimating];
    [[STPAPIClient sharedClient] createTokenWithCard:self.paymentTextField.cardParams
                                          completion:^(STPToken *token, NSError *error) {
                                             
                                              if (error == nil) {
                                              //completion(PKPaymentAuthorizationStatusSuccess);
        [self StripeCharge:token.tokenId :@"3" : NO];

                                              }
                                          }];
}
//- (void)createBackendChargeWithToken:(STPToken *)token completion:(void (^)(PKPaymentAuthorizationStatus))completion {
//    //We are printing Stripe token here, you can charge the Credit Card using this token from your backend.
//    NSLog(@"Stripe Token is %@",token);
//    completion(PKPaymentAuthorizationStatusSuccess);
//
//    //[self CallMyMethod:@"3" :[NSString stringWithFormat:@"%@",token]];
//
//  //  [self StripeCharge:token.ripeID :@"3" : NO];
//
//    //Displaying user Thank you message for payment.
//    //_thankYouMessage.hidden = false;
//    //_payButton.hidden = true;
//    //_donateAgainButton.hidden =false;
//}
- (void)createBackendChargeWithSource:(NSString *)sourceID completion:(STPSourceSubmissionHandler)completion {
    if (!BackendBaseURL) {
        NSError *error = [NSError errorWithDomain:StripeDomain
                                             code:STPInvalidRequestError
                                         userInfo:@{NSLocalizedDescriptionKey: @"You must set a backend base URL in Constants.m to create a charge."}];
        completion(STPBackendResultFailure, error);
        return;
    }
    
    // This passes the token off to our payment backend, which will then actually complete charging the card using your Stripe account's secret key
    NSURLSessionConfiguration *config = [NSURLSessionConfiguration defaultSessionConfiguration];
    NSURLSession *session = [NSURLSession sessionWithConfiguration:config];
    
    NSString *urlString = [BackendBaseURL stringByAppendingPathComponent:@"create_charge"];
    NSURL *url = [NSURL URLWithString:urlString];
    NSMutableURLRequest *request = [[NSMutableURLRequest alloc] initWithURL:url];
    request.HTTPMethod = @"POST";
    NSString *postBody = [NSString stringWithFormat:
                          @"source=%@&amount=%@&metadata[charge_request_id]=%@",
                          sourceID,
                          _amount,
                          // example-ios-backend allows passing metadata through to Stripe
                          @"B3E611D1-5FA1-4410-9CEC-00958A5126CB"];
    NSData *data = [postBody dataUsingEncoding:NSUTF8StringEncoding];
    
    NSURLSessionUploadTask *uploadTask = [session uploadTaskWithRequest:request
                                                               fromData:data
                                                      completionHandler:^(NSData *data, NSURLResponse *response, NSError *error) {
                                                          NSHTTPURLResponse *httpResponse = (NSHTTPURLResponse *)response;
                                                          if (!error && httpResponse.statusCode != 200) {
                                                              error = [NSError errorWithDomain:StripeDomain
                                                                                          code:STPInvalidRequestError
                                                                                      userInfo:@{NSLocalizedDescriptionKey: @"There was an error connecting to your payment backend."}];
                                                          }
                                                          if (error) {
                                                              completion(STPBackendResultFailure, error);
                                                          }
                                                          else {
                                                              completion(STPBackendResultSuccess, nil);
                                                          }
                                                      }];
    
    [uploadTask resume];
}
#pragma mark - GetAllPosts
-(void)StripeCharge:(NSString *)Token :(NSString *)Type :(BOOL)ISSave
{/*
  amount=1000 \
  -d currency=usd \
  -d customer=cus_7sqFSKcBzzYEAf*/
    
     NSString * Charge = [NSString stringWithFormat:@"%ld",[_amount integerValue]*100] ;

//    NSDictionary *headers = @{ @"authorization": [NSString stringWithFormat:@"Bearer %@",@"sk_test_8tWp3SdPGDrj3cNSnU0Ysefe"],
//                               @"content-type": @"application/x-www-form-urlencoded",
//                               @"cache-control": @"no-cache",
//                               @"postman-token": @"fc7c5030-35b5-4974-64a3-54a48f0ded96" };
    
    NSDictionary *headers = @{ @"authorization": [NSString stringWithFormat:@"Bearer %@",@"sk_live_hwRAqWmkEMEdk2F73HfUJK3c"],
                               @"content-type": @"application/x-www-form-urlencoded",
                              @"cache-control": @"no-cache",
                               @"postman-token": @"fc7c5030-35b5-4974-64a3-54a48f0ded96" };
    
    /*amount=1000 \
     -d currency=usd \
     -d customer=cus_7sqFSKcBzzYEAf*/
    
    NSMutableData *postData = [[NSMutableData alloc] initWithData:[[NSString stringWithFormat:@"amount=%@",Charge] dataUsingEncoding:NSUTF8StringEncoding]];

    
        [postData appendData:[[NSString stringWithFormat:@"&currency=usd"] dataUsingEncoding:NSUTF8StringEncoding]];
   // }
    

        [postData appendData:[[NSString stringWithFormat:@"&source=%@",Token] dataUsingEncoding:NSUTF8StringEncoding]];

    
    NSLog(@"postData %@",postData);
    
    NSMutableURLRequest *request = [NSMutableURLRequest requestWithURL:[NSURL URLWithString:@"https://api.stripe.com/v1/charges"]
                                                           cachePolicy:NSURLRequestUseProtocolCachePolicy
                                                       timeoutInterval:10.0];
    [request setHTTPMethod:@"POST"];
    [request setAllHTTPHeaderFields:headers];
    [request setHTTPBody:postData];
    
    NSURLSession *session = [NSURLSession sharedSession];
    NSURLSessionDataTask *dataTask = [session dataTaskWithRequest:request
                                                completionHandler:^(NSData *data, NSURLResponse *response, NSError *error) {
                                                    if (error) {
                                                        //[[AppDelegate sharedAppDelegate] hideProgress];
                                                        
                                                        NSLog(@"%@", error);
                                                    } else {
                                                        
                                                        if ([data length] > 0 && error == nil){
                                                            
                                                            NSError *parseError = nil;
                                                            NSDictionary *dictionary = [NSJSONSerialization JSONObjectWithData:data options:0 error:&parseError];
                                                            NSLog(@"dictionary %@",dictionary);
                                                            self->TransactionNo = [NSString stringWithFormat:@"%@",[dictionary valueForKey:@"id"]];
                                                            [self GetOfferListAPI];
                                                            //[self CallMyMethod:Type :[dictionary valueForKey:@"id"]];
                                                        }
                                                        else if ([data length] == 0 && error == nil){
                                                            //[[AppDelegate sharedAppDelegate] hideProgress];
                                                            NSLog(@"no data returned");
                                                            //no data, but tried
                                                        }
                                                        else if (error != nil)
                                                        {
                                                            NSLog(@"there was a download error");
                                                            //couldn't download
                                                        }
                                                    }
                                                }];
    [dataTask resume];
}
#pragma mark - GetAllPosts
-(void)CallMyMethod
{
    NSMutableDictionary *parameters = [[NSMutableDictionary alloc]init];
    
    
   //  params = ["Status":"apporved","OrderID":OrderID,"TransactionNo":TransactionNo,"FeeAmount":AppraisalCost,"Discount":"0","Total":Final,"Note":"abvc","ShippingAmount":Shipping]
    
      [parameters setValue:@"apporved" forKey:@" Status"];
     [parameters setValue:_orderid forKey:@" OrderID"];
     [parameters setValue:TransactionNo forKey:@" TransactionNo"];
     [parameters setValue:_AppraisalCosts forKey:@"FeeAmount"];
     [parameters setValue:@"0" forKey:@"Discount"];
     [parameters setValue:_amount forKey:@"Total"];
     [parameters setValue:@"abcd" forKey:@"Note"];
     [parameters setValue:_ShippingCosts forKey:@"ShippingAmount"];
    
    dispatch_async(dispatch_get_global_queue(QOS_CLASS_USER_INITIATED, 0), ^{
        httpSignin  = [[HttpWrapper alloc] init];
        httpSignin.delegate=self;
        httpSignin.getbool=NO;
       
        NSString * urlstring = [NSString stringWithFormat:@"https://faaausa.com/api/v3/web_services/order_payment"];
        NSString *encoded = [urlstring stringByAddingPercentEscapesUsingEncoding:NSUTF8StringEncoding];
        [httpSignin requestWithMethod:@"POST" url:[NSString stringWithFormat:@"https://faaausa.com/api/v3/web_services/order_payment"] param:[parameters copy]];
    });
}
-(void)HttpWrapper:(HttpWrapper *)wrapper fetchDataSuccess:(NSMutableDictionary *)dicsResponse
{
    if(wrapper == httpSignin && httpSignin != nil)
    {
        NSLog(@"dicresponce %@",dicsResponse);
        NSMutableDictionary *dic=[[NSMutableDictionary alloc]init];
        NSString * status = [NSString stringWithFormat:@"%@",[dicsResponse valueForKey:@"error_code"]];
        if ([status isEqualToString:@"0"]) {
            
             [self.activityIndicator stopAnimating];
            OrderSuccessViewController *s =[self.storyboard instantiateViewControllerWithIdentifier:@"OrderSuccessViewController"];
           [self.navigationController pushViewController:s animated:YES];
        }
        else{
            NSString * message = [NSString stringWithFormat:@"%@",[dicsResponse valueForKey:@"error_string"]];
             [self.activityIndicator stopAnimating];
         //   showAlert(AlertTitle, message);
        }
    }
     [self.activityIndicator stopAnimating];
  //  [APP_DELEGATE hideLoadingView];
}
- (void) HttpWrapper:(HttpWrapper *)wrapper fetchDataFail:(NSError *)error
{
     [self.activityIndicator stopAnimating];
    NSLog(@"Fetch Data Fail Error:%@",error);
   // [APP_DELEGATE hideLoadingView];
}
-(void)GetOfferListAPI{
    
    NSMutableURLRequest *request = [[NSMutableURLRequest alloc] init];
    
    //Set Params
    [request setHTTPShouldHandleCookies:NO];
    [request setTimeoutInterval:60];
    [request setHTTPMethod:@"POST"];
    
    //Create boundary, it can be anything
    NSString *boundary = @"------VohpleBoundary4QuqLuM1cE5lMwCy";
    NSString *UserID = [[NSUserDefaults standardUserDefaults]
                        stringForKey:@"UserID"];
    NSString *LoginToken = [[NSUserDefaults standardUserDefaults]
                            stringForKey:@"LoginToken"];
  
    NSString *contentType = [NSString stringWithFormat:@"multipart/form-data; boundary=%@", boundary];
    [request setValue:contentType forHTTPHeaderField: @"Content-Type"];
    [request addValue:@"Basic YWRtaW46QVBJQEZFREVSQUwhIyRXRUIk" forHTTPHeaderField:@"Authorization"];
    [request addValue:@"kkcoswggwgwkkc8w4ok808o48kswc40c0www4wss" forHTTPHeaderField:@"X-FEDERAL-API-KEY"];
    [request addValue:LoginToken forHTTPHeaderField:@"X-FEDERAL-LOGIN-TOKEN"];
    [request addValue:UserID forHTTPHeaderField:@"USER-ID"];
  
    
    NSMutableData *body = [NSMutableData data];
    
    //Populate a dictionary with all the regular values you would like to send.
    NSMutableDictionary *parameters = [[NSMutableDictionary alloc] init];

    [parameters setValue:@"apporved" forKey:@" Status"];
    [parameters setValue:_orderid forKey:@" OrderID"];
    [parameters setValue:TransactionNo forKey:@" TransactionNo"];
    [parameters setValue:_AppraisalCosts forKey:@"FeeAmount"];
    [parameters setValue:@"0" forKey:@"Discount"];
    [parameters setValue:_amount forKey:@"Total"];
    [parameters setValue:@"abcd" forKey:@"Note"];
    [parameters setValue:_ShippingCosts forKey:@"ShippingAmount"];
   
    for (NSString *param in parameters) {
        [body appendData:[[NSString stringWithFormat:@"--%@\r\n", boundary] dataUsingEncoding:NSUTF8StringEncoding]];
        [body appendData:[[NSString stringWithFormat:@"Content-Disposition: form-data; name=\"%@\"\r\n\r\n", param] dataUsingEncoding:NSUTF8StringEncoding]];
        [body appendData:[[NSString stringWithFormat:@"%@\r\n", [parameters objectForKey:param]] dataUsingEncoding:NSUTF8StringEncoding]];
    }
    NSString *FileParamConstant = @"user_image";
  
    
    
    
    //Close off the request with the boundary
    [body appendData:[[NSString stringWithFormat:@"--%@--\r\n", boundary] dataUsingEncoding:NSUTF8StringEncoding]];
    
    // setting the body of the post to the request
    [request setHTTPBody:body];
    str = [NSString stringWithFormat:@"https://faaausa.com/api/v3/web_services/order_payment"];  //,userid];
    // set URL
    [request setURL:[NSURL URLWithString:str]];
    
    [NSURLConnection sendAsynchronousRequest:request
                                       queue:[NSOperationQueue mainQueue]
                           completionHandler:^(NSURLResponse *response, NSData *data, NSError *error) {
                               
                               NSHTTPURLResponse* httpResponse = (NSHTTPURLResponse*)response;
                               
                               if ([httpResponse statusCode] == 200) {
                                   
                                   NSLog(@"success");
                                   
                                   NSMutableDictionary* json = [NSJSONSerialization JSONObjectWithData:data
                                                                                               options:kNilOptions
                                                                                                 error:&error];
                                   // [APP_DELEGATE hideLoadingView];
                                   
                                   NSLog(@"%@",json);
                                   UIStoryboard *mystoryboard = [UIStoryboard storyboardWithName:@"Main" bundle:nil];
                                   OrderSuccessViewController *s = [[OrderSuccessViewController alloc] init];
                                 s =[mystoryboard instantiateViewControllerWithIdentifier:@"OrderSuccessViewController"];
                                  
                                   NSString * status = [NSString stringWithFormat:@"%@",[json valueForKey:@"status"]];
                                       NSUserDefaults *defaults = [NSUserDefaults standardUserDefaults];
                                       [defaults setValue:status forKey:@"status"];
                                   [defaults setValue:[NSString stringWithFormat:@"Paid : $%@",_amount] forKey:@"Paid"];
                                   [defaults setValue:[NSString stringWithFormat:@"%@",_Certificateno] forKey:@"Certificate"];
                                   [defaults setValue:[NSString stringWithFormat:@"Certificate # %@",[[json valueForKey:@"data"] valueForKey:@"shipping_method"]] forKey:@"Order"];
                                   [defaults synchronize];
                                  
                                    [[self navigationController] pushViewController:s animated:YES];
                           
                                   
                                   
                               }
                               else{
                                   NSMutableDictionary* json = [NSJSONSerialization JSONObjectWithData:data
                                                                                               options:kNilOptions
                                                                                                 error:&error];
                                   // [APP_DELEGATE hideLoadingView];
                                   
                                   NSLog(@"%@",json);
                                  
                                   
                               }
                               
                               // [APP_DELEGATE hideLoadingView];
                           }];
    
}
@end
