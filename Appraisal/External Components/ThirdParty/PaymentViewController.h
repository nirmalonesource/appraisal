//
//  PaymentViewController.h
//  Stripe
//
//  Created by Alex MacCaw on 3/4/13.
//
//

#import <UIKit/UIKit.h>
#import <Stripe/Stripe.h>
#import "HttpWrapper.h"
//#import "Appraisal-Bridging-Header.h"
typedef NS_ENUM(NSInteger, STPBackendResult) {
    STPBackendResultSuccess,
    STPBackendResultFailure,
};

typedef void (^STPSourceSubmissionHandler)(STPBackendResult status, NSError *error);
typedef void (^STPPaymentIntentCreationHandler)(STPBackendResult status, NSString *clientSecret, NSError *error);

@class PaymentViewController;

@protocol PaymentViewControllerDelegate<NSObject>

- (void)paymentViewController:(PaymentViewController *)controller didFinish:(NSError *)error:(NSString *)Token;

@end

@interface PaymentViewController : UIViewController<HttpWrapperDelegate,UIApplicationDelegate>{
    HttpWrapper *httpSignin;
 // AppDelegate *appdelegate;
    NSMutableDictionary *dicOfferList;
    NSString * TransactionNo;
    NSString * str;
}
@property (nonatomic, strong) NSString *Certificateno;
@property (nonatomic, strong) NSString *AppraisalCosts;
@property (nonatomic, strong) NSString *ShippingCosts;
@property (nonatomic) NSString *orderid;
@property (nonatomic) NSDecimalNumber *amount;
@property (nonatomic, weak) id<PaymentViewControllerDelegate> delegate;

@end
