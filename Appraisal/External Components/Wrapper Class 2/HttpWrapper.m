//
//  HttpWrapper.m
//  Towing Service
//
//  Created by RAHUL MEHTA on 03/03/17.
//  Copyright © 2017 RAHUL MEHTA. All rights reserved.
//

#import "HttpWrapper.h"
#import "Reachability.h"

@implementation HttpWrapper

//AppDelegate *appDelegate;

@synthesize requestMain;
@synthesize delegate;

-(id)init
{
    self = [super init];
    if(self)
    {
        dispatch_async(dispatch_get_main_queue(), ^{
            // appDelegate = (AppDelegate *)[[UIApplication sharedApplication] delegate];
        });
        
    }
    return self;
}

-(void) requestWithMethod:(NSString*)method url:(NSString*)strUrl param:(NSString*)dictParam
{
    NSLog(@"dict is %@",dictParam);
    if(requestMain)
    {
        requestMain = nil;
    }
    
    if ([self isConnectedToInternet]){
        operation = [AFHTTPSessionManager manager];
        operation.requestSerializer = [AFHTTPRequestSerializer serializer];
        operation.responseSerializer = [AFJSONResponseSerializer serializer];
        
        operation.responseSerializer.acceptableContentTypes = [NSSet setWithObjects:@"application/json", @"text/json", @"text/javascript",@"text/html",@"charset=UTF-8",@"text/plain",nil];
        
        
        //  [operation.requestSerializer setAuthorizationHeaderFieldWithUsername:@"admin" password:@"API@TAKEOFF!#$WEB$"];
        
        [operation.requestSerializer setValue:@"kkcoswggwgwkkc8w4ok808o48kswc40c0www4wss" forHTTPHeaderField:@"X-FEDERAL-API-KEY"];
        
        
        NSString *UserID = [[NSUserDefaults standardUserDefaults]
                            stringForKey:@"UserID"];
        NSString *LoginToken = [[NSUserDefaults standardUserDefaults]
                                stringForKey:@"LoginToken"];
        
        if ( ( ( ![UserID isEqual:[NSNull null]] ) && ( [UserID length] != 0 ) ) || ( ( ![LoginToken isEqual:[NSNull null]] ) && ( [LoginToken length] != 0 ) )) {
            [operation.requestSerializer setValue:LoginToken forHTTPHeaderField:@"X-FEDERAL-LOGIN-TOKEN"];
            [operation.requestSerializer setValue:UserID forHTTPHeaderField:@"USER-ID"];
        }
        
        operation.responseSerializer = [AFJSONResponseSerializer serializerWithReadingOptions:NSJSONReadingAllowFragments];
        
        if (_getbool == YES)
        {
            [operation GET:strUrl parameters:dictParam success:^(NSURLSessionDataTask * _Nonnull task, id  _Nullable responseObject) {
                if (delegate != nil)
                {
                    NSLog(@"dictparam %@",dictParam);
                    
                    if([delegate respondsToSelector:@selector(fetchDataSuccess:)])
                    {
                        [delegate performSelector:@selector(fetchDataSuccess:) withObject:responseObject];
                    }
                    
                    if([delegate respondsToSelector:@selector(HttpWrapper:fetchDataSuccess:)])
                    {
                        [delegate HttpWrapper:self fetchDataSuccess:responseObject];
                    }
                }
            } failure:^(NSURLSessionDataTask * _Nullable task, NSError * _Nonnull error) {
                if (delegate == nil)
                    return;
                
                if([delegate respondsToSelector:@selector(fetchDataFail:)])
                    [delegate performSelector:@selector(fetchDataFail:) withObject:error];
                
                if([delegate respondsToSelector:@selector(HttpWrapper:fetchDataFail:)])
                    [delegate HttpWrapper:self fetchDataFail:error];
            }];
        }
        else
        {
            [operation POST:strUrl parameters:dictParam success:^(NSURLSessionDataTask *  task, id   responseObject) {
                if (delegate != nil)
                {
                    NSLog(@"dictparam %@",dictParam);
                    
                    if([delegate respondsToSelector:@selector(fetchDataSuccess:)])
                    {
                        [delegate performSelector:@selector(fetchDataSuccess:) withObject:responseObject];
                    }
                    
                    if([delegate respondsToSelector:@selector(HttpWrapper:fetchDataSuccess:)])
                    {
                        [delegate HttpWrapper:self fetchDataSuccess:responseObject];
                    }
                }
            } failure:^(NSURLSessionDataTask * _Nullable task, NSError * _Nonnull error) {
                if (delegate == nil)
                    return;
                
                if([delegate respondsToSelector:@selector(fetchDataFail:)])
                    [delegate performSelector:@selector(fetchDataFail:) withObject:error];
                
                if([delegate respondsToSelector:@selector(HttpWrapper:fetchDataFail:)])
                    [delegate HttpWrapper:self fetchDataFail:error];
            }];
        }
        
    }
    
    else {
        // no internet coonection
        UIAlertView *alert = [[UIAlertView alloc] initWithTitle:@"Connection Failed" message:@"Unable to connect Internet, Please try after some time." delegate:self cancelButtonTitle:@"OK" otherButtonTitles:nil, nil];
        
        [alert show];
    }
}

- (BOOL)isConnectedToInternet
{
    Reachability *reachability = [Reachability reachabilityForInternetConnection];
    NetworkStatus networkStatus = [reachability currentReachabilityStatus];
    return !(networkStatus == NotReachable);
}

-(void) cancelRequest
{
    //    [operation.operationQueue cancelAllOperations];
    //    requestMain.delegate = nil;
    //    [requestMain cancel];
}
@end
