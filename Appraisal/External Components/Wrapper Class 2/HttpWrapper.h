//
//  HttpWrapper.h
//  Towing Service
//
//  Created by RAHUL MEHTA on 03/03/17.
//  Copyright © 2017 RAHUL MEHTA. All rights reserved.
//

#import <UIKit/UIKit.h>
//#import "AFNetworking.h"
#import <AFNetworking/AFNetworking.h>
@class ASIFormDataRequest,HttpWrapper;

@protocol HttpWrapperDelegate

@optional

- (void) HttpWrapper:(HttpWrapper *)wrapper fetchDataSuccess:(NSMutableDictionary *)dicsResponse;
- (void) HttpWrapper1:(HttpWrapper *)wrapper fetchDataSuccess:(NSMutableDictionary *)dicsResponse;
- (void) HttpWrapper:(HttpWrapper *)wrapper fetchDataFail:(NSError *)error;

- (void) fetchImageSuccess:(NSString *)response;
- (void) fetchImageFail:(NSError *)error;
- (void) fetchDataSuccess:(NSString *)response;
- (void) fetchDataFail:(NSError *)error;

- (void) HttpWrapper:(HttpWrapper *)wrapper fetchsuccesswithVideoData:(NSData *)videoData;
- (void) HttpWrapper:(HttpWrapper *)wrapper fetchsuccesswithVideoUrl:(NSString *)videoUrlString;

- (BOOL)isConnectedToInternet;


@end

@interface HttpWrapper : NSObject
{
    AFHTTPSessionManager *operation;
    NSMutableDictionary *dics;
}

@property (nonatomic, assign) ASIFormDataRequest *requestMain;
@property (nonatomic, assign) NSObject<HttpWrapperDelegate> *delegate;
@property (nonatomic ,strong) NSString *CreditCardType;
@property (assign, nonatomic) BOOL getbool;

-(void) requestWithMethod:(NSString*)method url:(NSString*)strUrl param:(NSMutableDictionary*)dictParam;
-(void) cancelRequest;

@end
