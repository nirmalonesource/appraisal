//
//  Constant.h
//  StripeIntegrationIniOS
//
//  Created by TheAppGuruz-iOS-103 on 16/11/15.
//  Copyright © 2015 TheAppGuruz. All rights reserved.
//

#ifndef Constant_h
#define Constant_h

// This can be found at https://dashboard.stripe.com/account/apikeys

NSString *const kstrStripePublishableKey = @"pk_live_n9dZgqsWUMjSJjx6SB5GEl4Q"; // live key
//NSString *const kstrStripePublishableKey = @"pk_test_d1VHmq3QUwqKIKyrTVkOGozA"; // test key


NSString *const BackendBaseURL = @"https://api.stripe.com/v1/customers";
#endif /* Constant_h */
