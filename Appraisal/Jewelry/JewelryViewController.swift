//
//  JewelryViewController.swift
//  Appraisal
//
//  Created by ADMIN on 4/26/18.
//  Copyright © 2018 empiereTech. All rights reserved.
//

import UIKit
import SWRevealViewController
import BEMCheckBox
import FlexibleSteppedProgressBar
import AETextFieldValidator
import RangeSeekSlider

class JewelryViewController: UIViewController,SWRevealViewControllerDelegate,BEMCheckBoxDelegate,UITextViewDelegate,UICollectionViewDelegate,UICollectionViewDataSource,FlexibleSteppedProgressBarDelegate,kDropDownListViewDelegate,UIImagePickerControllerDelegate, UINavigationControllerDelegate,UIScrollViewDelegate,UIGestureRecognizerDelegate,RangeSeekSliderDelegate{
    
    //MARK: - Outlets🤔
    
    //TextView
    
    @IBOutlet var Txt_JewelyType: AETextFieldValidator!
    @IBOutlet weak var Txt_Customer: AETextFieldValidator!
    @IBOutlet weak var Txt_Caret: AETextFieldValidator!
    @IBOutlet weak var Txt_Estimate: AETextFieldValidator!
    @IBOutlet weak var Txt_Comment: UITextView!
    @IBOutlet weak var Txt_Other: AETextFieldValidator!
    
    @IBOutlet weak var Txt_Weight: AETextFieldValidator!
    //Button
    @IBOutlet weak var Btn_Material: UIButton!
    @IBOutlet weak var Btn_Next: UIButton!
    @IBOutlet weak var Btn_JewelryType: UIButton!
    
    //View
    @IBOutlet weak var Vw_Collection: UICollectionView!
    @IBOutlet weak var Vw_Matel: UIView!
    @IBOutlet weak var Vw_Scroll: UIScrollView!
    @IBOutlet weak var Vw_Other: UIView!
    
    //CheckMark
    @IBOutlet weak var Check_Terms: BEMCheckBox!
    
    @IBOutlet var diamondcslider: RangeSeekSlider!
    @IBOutlet var claritytextview: UIView!
    @IBOutlet var qualityslider: RangeSeekSlider!
    @IBOutlet var qualitytextview: UIView!
    @IBOutlet var bottomvw: UIView!
    @IBOutlet var lblquality: UILabel!
    @IBOutlet var lblclarity: UILabel!
    @IBOutlet var lblline: UILabel!
    @IBOutlet var txtcolor: AETextFieldValidator!
    //MARK: - Declare Variables👇🏻😬
    let imagePicker = UIImagePickerController()
    let reuseIdentifier = "cell" // also enter this string as the cell identifier in the storyboard
    var items = [UIImage]()
    var image = UIImage()
    var imageData = NSData()
    
    //ProgressBar
    var progressBar: FlexibleSteppedProgressBar!
    var progressBarWithoutLastState: FlexibleSteppedProgressBar!
    var progressBarWithDifferentDimensions: FlexibleSteppedProgressBar!
    
    var maxIndex = -1
    
    var progressColor = UIColor(red: 19.0 / 255.0, green: 63.0 / 255.0, blue: 118.0 / 255.0, alpha: 1.0)
    var BGColor = UIColor(red: 188.0 / 255.0, green: 188.0 / 255.0, blue: 188.0 / 255.0, alpha: 1.0)
    var textColorHere = UIColor(red: 153.0 / 255.0, green: 153.0 / 255.0, blue: 153.0 / 255.0, alpha: 1.0)
    
    var ArrGold = ["SILVER","Gold 10K","Gold 14K","Gold 18K","Gold 21K","Platinum"]
    var ArrGoldIndex = NSString()
    
    //Array
    var JewelryArr = [[String:AnyObject]]()
    var ClarityArr = [[String:AnyObject]]()
     var QuantyArr = [[String:AnyObject]]()
    var JewelryArrIndex = Int()
    var MaterialArr = [[String:AnyObject]]()
    var MaterialArrIndex = Int()
    var View = Bool()
    
    //DropDown
    var Dropobj = DropDownListView()
    var Dropobj1 = DropDownListView()
    var OtherBool = Bool()
    var NoneBool = Bool()
    
    var jew_clearity_start = NSString()
    var jew_clearity_end = NSString()
    var jew_color_start = NSString()
    var jew_color_end = NSString()
    
    var jew_clearity_start_id = NSString()
    var jew_clearity_end_id = NSString()
    var jew_color_start_id = NSString()
    var jew_color_end_id = NSString()
    
    func revealControllerPanGestureShouldBegin(_ revealController: SWRevealViewController!) -> Bool {
//        if ([revealController.panGestureRecognizer velocityInView:revealController.view].x < 0) {
//            // pan direction left, should open right side
//            // ...
//            return NO;
//        }
//        return YES;
        
        if revealController.panGestureRecognizer().velocity(in: revealController.view).x < 0
        {
            return false
        }
        return true
    }
   

    //MARK: - Happy Coding😊
    override func viewDidLoad() {
        super.viewDidLoad()
        
        // Do any additional setup after loading the view.
        self.navigationController?.interactivePopGestureRecognizer?.isEnabled = false
        self.revealViewController().panGestureRecognizer().isEnabled = false

      //  self.revealViewController()?.delegate = self
      
        
        //NavigationBar
        self.navigationController?.navigationBar.isHidden = false;
        self.title = "Jewelry"
        items.removeAll()
        
        navigationController?.navigationBar.barTintColor = UIColor.white
        if (UIScreen.main.bounds.size.height == 568.0)
        {
            print("iphone5")
            Vw_Scroll.contentSize = CGSize(width: self.view.frame.size.width, height: 1000)
            navigationController?.navigationBar.titleTextAttributes = [NSAttributedStringKey.foregroundColor: UIColor(red: 18/255, green: 64/255, blue: 118/255, alpha: 1),NSAttributedStringKey.font: UIFont(name: "Montserrat-Medium", size: 15)!]
        }
        else{
            Vw_Scroll.contentSize = CGSize(width: self.view.frame.size.width, height: 1000)
            navigationController?.navigationBar.titleTextAttributes = [NSAttributedStringKey.foregroundColor: UIColor(red: 18/255, green: 64/255, blue: 118/255, alpha: 1),NSAttributedStringKey.font: UIFont(name: "Montserrat-Medium", size: 17)!]
        }
        navigationController?.navigationBar.tintColor = UIColor(red: 18/255, green: 64/255, blue: 118/255, alpha: 1)
        
        OtherBool = false
        
        Check_Terms.delegate = self
        Btn_Next.isEnabled = false
        Btn_Next.alpha = 0.2
        imagePicker.delegate = self
        
        var dict = AppDelegate.shared().ArrGetAllType[0]
        JewelryArr = dict["JewelaryType"]! as! [[String:AnyObject]]
        MaterialArr = dict["MaterialJewelaryType"]! as! [[String:AnyObject]]
        ClarityArr = dict["Clarity"]! as! [[String:AnyObject]]
        QuantyArr = dict["Color"]! as! [[String:AnyObject]]
        
       
        Txt_Caret.layer.cornerRadius = 7.0
        Txt_Caret.layer.backgroundColor = UIColor.white.cgColor
        Txt_Caret.layer.masksToBounds = false
        Txt_Caret.layer.shadowOffset = CGSize.zero
        Txt_Caret.layer.shadowRadius = 5
        Txt_Caret.layer.shadowOpacity = 0.2
        Txt_Caret.leftView = UIView(frame: CGRect(x: 0, y: 0, width: 15, height: Txt_Caret.frame.height))
        Txt_Caret.leftViewMode = .always
        Txt_Caret.textColor = UIColor.black
        let DiamondAttributed = NSMutableAttributedString.init(string: "*Total carat weight :")
        let Diamondterms:NSString = "*Total carat weight :"
        let DiamonRange :NSRange = Diamondterms.range(of: "*")
        let Diamondrangech :NSRange = Diamondterms.range(of: "Total carat weight :")
        DiamondAttributed.addAttribute(NSAttributedStringKey.foregroundColor, value: UIColor.darkGray, range: Diamondrangech)
        DiamondAttributed.addAttribute(NSAttributedStringKey.foregroundColor, value: UIColor.red, range: DiamonRange)
        Txt_Caret.attributedPlaceholder = DiamondAttributed
        
        Txt_JewelyType.layer.cornerRadius = 7.0
        Txt_JewelyType.layer.backgroundColor = UIColor.white.cgColor
        Txt_JewelyType.layer.masksToBounds = false
        Txt_JewelyType.layer.shadowOffset = CGSize.zero
        Txt_JewelyType.layer.shadowRadius = 5
        Txt_JewelyType.layer.shadowOpacity = 0.2
        Txt_JewelyType.textColor = UIColor.black
        Txt_JewelyType.leftView = UIView(frame: CGRect(x: 0, y: 0, width: 15, height: Txt_JewelyType.frame.height))
        Txt_JewelyType.leftViewMode = .always
        let JewelyAttributed = NSMutableAttributedString.init(string: "*Jewelry type (Max char 1-22) :")
        let Jewelyterms:NSString = "*Jewelry type (Max char 1-22) :"
        let JweleryRange :NSRange = Jewelyterms.range(of: "*")
        let Jewelyrangech :NSRange = Jewelyterms.range(of: "Jewerly type (Max char 1-22) :")
        JewelyAttributed.addAttribute(NSAttributedStringKey.foregroundColor, value: UIColor.darkGray, range: Jewelyrangech)
        JewelyAttributed.addAttribute(NSAttributedStringKey.foregroundColor, value: UIColor.red, range: JweleryRange)
        Txt_JewelyType.attributedPlaceholder = JewelyAttributed
        
        Txt_Other.layer.cornerRadius = 7.0
        Txt_Other.layer.backgroundColor = UIColor.white.cgColor
        Txt_Other.textColor = UIColor.black
        Txt_Other.layer.masksToBounds = false
        Txt_Other.layer.shadowOffset = CGSize.zero
        Txt_Other.layer.shadowRadius = 5
        Txt_Other.layer.shadowOpacity = 0.2
        Txt_Other.textColor = UIColor.black
        Txt_Other.leftView = UIView(frame: CGRect(x: 0, y: 0, width: 15, height: Txt_Other.frame.height))
        Txt_Other.leftViewMode = .always
        let OtherAttributed = NSMutableAttributedString.init(string: "Other :")
        let Otherterms:NSString = "Other :"
        let Otherrangech :NSRange = Otherterms.range(of: "Other :")
        OtherAttributed.addAttribute(NSAttributedStringKey.foregroundColor, value: UIColor.darkGray, range: Otherrangech)
        //Txt_Other.attributedPlaceholder = OtherAttributed
        
        Txt_Weight.layer.cornerRadius = 7.0
        Txt_Weight.layer.backgroundColor = UIColor.white.cgColor
        Txt_Weight.layer.masksToBounds = false
        Txt_Weight.layer.shadowOffset = CGSize.zero
        Txt_Weight.layer.shadowRadius = 5
        Txt_Weight.layer.shadowOpacity = 0.2
        Txt_Weight.textColor = UIColor.black
        Txt_Weight.leftView = UIView(frame: CGRect(x: 0, y: 0, width: 15, height: Txt_Other.frame.height))
        Txt_Weight.leftViewMode = .always
        
        let WeightAttriubuted = NSMutableAttributedString.init(string: "*Weight of item (gm)")
        let WieghtTerms:NSString = "*Weight of item (gm)"
        let WeightRange :NSRange = WieghtTerms.range(of: "*")
        let WeightRangeSearch :NSRange = WieghtTerms.range(of: "Weight of item (gm)")
        WeightAttriubuted.addAttribute(NSAttributedStringKey.foregroundColor, value: UIColor.darkGray, range: WeightRangeSearch)
        WeightAttriubuted.addAttribute(NSAttributedStringKey.foregroundColor, value: UIColor.red, range: WeightRange)
        Txt_Weight.attributedPlaceholder = WeightAttriubuted
        
        Txt_Customer.layer.cornerRadius = 7.0
        Txt_Customer.layer.backgroundColor = UIColor.white.cgColor
        Txt_Customer.layer.masksToBounds = false
        Txt_Customer.layer.shadowOffset = CGSize.zero
        Txt_Customer.layer.shadowRadius = 5
        Txt_Customer.layer.shadowOpacity = 0.2
        Txt_Customer.textColor = UIColor.black
        Txt_Customer.leftView = UIView(frame: CGRect(x: 0, y: 0, width: 15, height: Txt_Customer.frame.height))
        Txt_Customer.leftViewMode = .always
        let CustomerAttributed = NSMutableAttributedString.init(string: "Customer Name :")
        let Customerterms:NSString = "Customer Name :"
        let Customerrange :NSRange = Customerterms.range(of: "")
        let Customerrangech :NSRange = Customerterms.range(of: "Customer Name :")
        CustomerAttributed.addAttribute(NSAttributedStringKey.foregroundColor, value: UIColor.red, range: Customerrange)
        CustomerAttributed.addAttribute(NSAttributedStringKey.foregroundColor, value: UIColor.darkGray, range: Customerrangech)
        Txt_Customer.attributedPlaceholder = CustomerAttributed
        
        Txt_Estimate.layer.cornerRadius = 7.0
        Txt_Estimate.layer.backgroundColor = UIColor.white.cgColor
        Txt_Estimate.layer.masksToBounds = false
        Txt_Estimate.textColor = #colorLiteral(red: 0, green: 0.4980392157, blue: 0.003921568627, alpha: 1)
        Txt_Estimate.layer.shadowOffset = CGSize.zero
        Txt_Estimate.layer.shadowRadius = 5
        Txt_Estimate.layer.shadowOpacity = 0.2
        //  Txt_Estimate.leftView = UIView(frame: CGRect(x: 0, y: 0, width: 15, height: Txt_Estimate.frame.height))
        
        let txtDollar = UITextView(frame: CGRect(x: 5, y:10, width: 15, height: Txt_Estimate.frame.height - 10))
        txtDollar.text = ""
        txtDollar.isEditable=false
        txtDollar.textColor = UIColor.darkGray
        Txt_Estimate.leftView = txtDollar
        Txt_Estimate.leftViewMode = .always
        let EstimatedAttributed = NSMutableAttributedString.init(string: "Estimated Value:")
        let Estimatedterms:NSString = "Estimated Value:"
        let Estimatedrangech :NSRange = Estimatedterms.range(of: "Estimated Value:")
        EstimatedAttributed.addAttribute(NSAttributedStringKey.foregroundColor, value: UIColor.darkGray, range: Estimatedrangech)
        Txt_Estimate.attributedPlaceholder = EstimatedAttributed
        
        
        txtcolor.layer.cornerRadius = 7.0
                      txtcolor.layer.backgroundColor = UIColor.white.cgColor
                      txtcolor.layer.masksToBounds = false
                      txtcolor.layer.shadowOffset = CGSize.zero
                      txtcolor.layer.shadowRadius = 5
                      txtcolor.layer.shadowOpacity = 0.2
                      txtcolor.leftView = UIView(frame: CGRect(x: 0, y: 0, width: 15, height: txtcolor.frame.height))
                      txtcolor.leftViewMode = .always
                      let txtcolorAttributed = NSMutableAttributedString.init(string: "Color :")
                      let txtcolorterms:NSString = "Color :"
                      let txtcolorrangech :NSRange = txtcolorterms.range(of: "Color :")
                      txtcolorAttributed.addAttribute(NSAttributedStringKey.foregroundColor, value: UIColor.darkGray, range: txtcolorrangech)
                      txtcolor.attributedPlaceholder = txtcolorAttributed
        
        
        
        Txt_Comment.layer.cornerRadius = 7.0
        Txt_Comment.layer.backgroundColor = UIColor.white.cgColor
        Txt_Comment.layer.masksToBounds = false
        Txt_Comment.layer.shadowOffset = CGSize.zero
        Txt_Comment.layer.shadowRadius = 5
        Txt_Comment.layer.shadowOpacity = 0.2
        Txt_Comment.text = "Additional Info"
        Txt_Comment.textColor = UIColor.darkGray
        
        Txt_Comment.delegate = self
        Txt_Comment.contentInset = UIEdgeInsetsMake(5, 12, 0, 0)
        
        Btn_Material.layer.cornerRadius = 7.0
        Btn_Material.isUserInteractionEnabled = true
        Btn_Material.layer.masksToBounds = false
        Btn_Material.layer.shadowOffset = CGSize.zero
        Btn_Material.layer.shadowRadius = 5
        Btn_Material.layer.shadowOpacity = 0.2
        Btn_Material.contentEdgeInsets = UIEdgeInsets(top: 5, left: 15, bottom: 0, right: 0)
        
        Btn_JewelryType.layer.cornerRadius = 7.0
        Btn_JewelryType.isUserInteractionEnabled = true
        Btn_JewelryType.layer.masksToBounds = false
        Btn_JewelryType.layer.shadowOffset = CGSize.zero
        Btn_JewelryType.layer.shadowRadius = 5
        Btn_JewelryType.layer.shadowOpacity = 0.2
        Btn_JewelryType.contentEdgeInsets = UIEdgeInsets(top: 5, left: 15, bottom: 0, right: 0)
        
        Btn_Next.layer.cornerRadius = Btn_Next.frame.size.height/2
        
        ProgressBar()
        setupAlerts()
        setslider()
        
         Txt_Caret.text = "0"
          MaterialArrIndex = 0
          Btn_Material.setTitle(MaterialArr[0]["MaterialType"] as? String, for: .normal)
    }
    
    func setslider()  {
        
         //============================ RANGE SLIDER SETUP ====================================
                var xposition = 0
                
                for i in 0..<ClarityArr.count
                {
                    let screenwidth = claritytextview.frame.size.width
                    
                    let lbl = UILabel(frame: CGRect(x: xposition, y: 0, width: Int(screenwidth/CGFloat(ClarityArr.count)), height: 20))
                    lbl.textAlignment = .center
                    lbl.text = ClarityArr[i]["ClarityName"] as? String
                    lbl.font = lbl.font.withSize(10)
                    self.claritytextview.addSubview(lbl)
                    xposition = xposition + Int(screenwidth/CGFloat(ClarityArr.count))
                }
                
                diamondcslider.delegate = self
                diamondcslider.enableStep = true
                diamondcslider.step = 1 // CGFloat(ClarityArr.count-1)
                if let n = NumberFormatter().number(from: ClarityArr[0]["ClarityID"] as! String)
                {
                    let f = CGFloat(truncating: n)
                     diamondcslider.minValue = 0
                    diamondcslider.selectedMinValue = 0
                    jew_clearity_start_id = ClarityArr[0]["ClarityID"] as! NSString
                    jew_clearity_start = ClarityArr[0]["ClarityName"] as! NSString
                    
                }
                if let n = NumberFormatter().number(from: ClarityArr[ClarityArr.count-1]["ClarityID"] as! String)
                {
                    let f = CGFloat(truncating: n)
                    diamondcslider.maxValue = CGFloat(ClarityArr.count-1)
                    diamondcslider.selectedMaxValue = CGFloat(ClarityArr.count-1)
                    jew_clearity_end_id = ClarityArr[ClarityArr.count-1]["ClarityID"] as! NSString
                    jew_clearity_end = ClarityArr[ClarityArr.count-1]["ClarityName"] as! NSString
                }
                
                xposition = 0
        //
                for i in 0..<QuantyArr.count
                {
                    let screenwidth = qualitytextview.frame.size.width

                    let lbl = UILabel(frame: CGRect(x: xposition, y: 0, width: Int(screenwidth/CGFloat(QuantyArr.count)), height: 20))
                    lbl.textAlignment = .center
                    lbl.text = QuantyArr[i]["ColorName"] as? String
                    lbl.font = lbl.font.withSize(10)
                    self.qualitytextview.addSubview(lbl)
                    xposition = xposition + Int(screenwidth/CGFloat(QuantyArr.count))
                }

                qualityslider.delegate = self
                qualityslider.enableStep = true
                qualityslider.step = 1 // CGFloat(ClarityArr.count-1)
                if let n = NumberFormatter().number(from: QuantyArr[0]["ColorID"] as! String)
                {
                    let f = CGFloat(truncating: n)
                    qualityslider.minValue = 0
                    qualityslider.selectedMinValue = 0
                    jew_color_start_id = QuantyArr[0]["ColorID"] as! NSString
                    jew_color_start = QuantyArr[0]["ColorName"] as! NSString
                }
                if let n = NumberFormatter().number(from: QuantyArr[ClarityArr.count-1]["ColorID"] as! String)
                {
                    let f = CGFloat(truncating: n)
                    qualityslider.maxValue = CGFloat(QuantyArr.count-1)
                    qualityslider.selectedMaxValue = CGFloat(QuantyArr.count-1)
                    jew_color_end_id = QuantyArr[QuantyArr.count-1]["ColorID"] as! NSString
                    jew_color_end = QuantyArr[QuantyArr.count-1]["ColorName"] as! NSString
                }
                
               bottomvw.frame.origin.y = 0;
                              diamondcslider.isHidden = true;
                                             claritytextview.isHidden = true;
                                             qualityslider.isHidden = true;
                                             qualitytextview.isHidden = true;
                                          //   lblline.isHidden = true;
                                             lblquality.isHidden = true;
                                             lblclarity.isHidden = true;
                                             Txt_Caret.isHidden = true;
              
                
                //============================ RANGE SLIDER SETUP END ====================================
    }
    // MARK: - RangeSeekSliderDelegate
    
    func rangeSeekSlider(_ slider: RangeSeekSlider, didChange minValue: CGFloat, maxValue: CGFloat) {
        if slider === diamondcslider {
            print("clarity slider updated. Min Value: \(minValue) Max Value: \(maxValue)")
            jew_clearity_start_id = ClarityArr[Int(minValue)]["ClarityID"] as! NSString
            jew_clearity_end_id = ClarityArr[Int(maxValue)]["ClarityID"] as! NSString
            
            jew_clearity_start = ClarityArr[Int(minValue)]["ClarityName"] as! NSString
            jew_clearity_end = ClarityArr[Int(maxValue)]["ClarityName"] as! NSString
            
        }
        else if slider === qualityslider {
            print("quantity slider updated. Min Value: \(minValue) Max Value: \(maxValue)")
            jew_color_start_id = QuantyArr[Int(minValue)]["ColorID"] as! NSString
            jew_color_end_id = QuantyArr[Int(maxValue)]["ColorID"] as! NSString
            
            jew_color_start = QuantyArr[Int(minValue)]["ColorName"] as! NSString
            jew_color_end = QuantyArr[Int(maxValue)]["ColorName"] as! NSString
            
            if jew_color_start_id == "11" || jew_color_end_id == "11"{
                                   txtcolor.isHidden = false
                                  
                                   bottomvw.frame = CGRect.init(x: bottomvw.frame.origin.x, y: 250, width: bottomvw.frame.size.width, height: bottomvw.frame.size.height)
                                   
                               }
                               else
                               {
                                   bottomvw.frame = CGRect.init(x: bottomvw.frame.origin.x, y: 250, width: bottomvw.frame.size.width, height: bottomvw.frame.size.height)
                                
                                   txtcolor.isHidden = true
                                   txtcolor.text = ""
                                   
                               }
        }
    }
    
    func didStartTouches(in slider: RangeSeekSlider) {
        print("did start touches")
    }
    
    func didEndTouches(in slider: RangeSeekSlider) {
        print("did end touches")
    }
    
     func textViewDidBeginEditing(_ textView: UITextView) {
        if textView.textColor == UIColor.darkGray {
            textView.text = nil;
            textView.textColor = UIColor.black
        }
    }
    
     func textViewDidEndEditing(_ textView: UITextView) {
        if textView.text.isEmpty {
            textView.text = "Additional Info"
            textView.textColor = UIColor.darkGray
        }
    }
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(true)
        
        //items.append(UIImage(named: "upload_image")!)
        if View == false
        {
            items.insert(UIImage(named: "upload_image")!, at: 0)
        }
        
        Btn_Next.isUserInteractionEnabled = true
    }
    
    func setupAlerts(){
        
       // Txt_Customer.addRegx(Constants.REGEX_Note_USER_NAME, withMsg: "Enter valid Customer Name")
        Txt_Estimate.addRegx(Constants.REGEX_Note_USER_LIMIT, withMsg: "Enter Valid Estimate Value")
        Txt_Caret.addRegx(Constants.REGEX_Note_USER_LIMIT, withMsg: "Enter valid Total carat weight.")
        Txt_Weight.addRegx(Constants.REGEX_Note_USER_LIMIT, withMsg: "Enter valid Weight")
        Txt_JewelyType.addRegx(Constants.REGEX_Note_USER_LIMIT, withMsg: "Character limit is 2 to 35 only")
        txtcolor.addRegx(Constants.REGEX_USER_NAME_LIMIT, withMsg: "Enter Color info.")
    }
    
    func ProgressBar() {
        
        progressBarWithDifferentDimensions = FlexibleSteppedProgressBar()
        progressBarWithDifferentDimensions.translatesAutoresizingMaskIntoConstraints = false
        Vw_Matel.addSubview(progressBarWithDifferentDimensions)
        
        progressBarWithDifferentDimensions.frame = CGRect.init(x: 20, y: -15, width: Vw_Matel.frame.size.width - 50, height: Vw_Matel.frame.size.height)
        progressBarWithDifferentDimensions.numberOfPoints = ArrGold.count
        progressBarWithDifferentDimensions.lineHeight = 4
        progressBarWithDifferentDimensions.backgroundShapeColor = BGColor
        progressBarWithDifferentDimensions.radius = 4
        progressBarWithDifferentDimensions.progressRadius = 4
        progressBarWithDifferentDimensions.progressLineHeight = 0
        progressBarWithDifferentDimensions.delegate = self
        progressBarWithDifferentDimensions.useLastState = false
        progressBarWithDifferentDimensions.lastStateCenterColor = progressColor
        progressBarWithDifferentDimensions.selectedBackgoundColor = BGColor
        progressBarWithDifferentDimensions.selectedOuterCircleStrokeColor = progressColor
        progressBarWithDifferentDimensions.lastStateOuterCircleStrokeColor = progressColor
        progressBarWithDifferentDimensions.currentSelectedCenterColor = progressColor
        progressBarWithDifferentDimensions.stepTextColor = textColorHere
        progressBarWithDifferentDimensions.currentSelectedTextColor = progressColor
        progressBarWithDifferentDimensions.completedTillIndex = 0
        progressBarWithDifferentDimensions.stepTextFont = UIFont(name: "Montserrat-Medium", size:10.0)
        
        ArrGoldIndex = ArrGold[0] as NSString
    }
    
    
    //MARK: - Delegates✋🏻😇
    
    //MARK: -- CollectionView Delegates
    
    // tell the collection view how many cells to make
    func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
        return self.items.count
    }
    
    // make a cell for each cell index path
    func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
        
        // get a reference to our storyboard cell
        let cell = collectionView.dequeueReusableCell(withReuseIdentifier: reuseIdentifier, for: indexPath as IndexPath) as! UploadImageCollectionViewCell
        
        if indexPath.item != 0 {
            cell.Btn_Close.isHidden = false
            cell.Btn_Close.addTarget(self, action: #selector(JewelryViewController.connected), for: .touchUpInside)
            cell.Btn_Close.tag = indexPath.row
        }
        else{
            cell.Btn_Close.isHidden = true
        }
        
        cell.Img_Vw.layer.cornerRadius = cell.Img_Vw.frame.size.height/2
        cell.Img_Vw.image = self.items[indexPath.row]
        cell.Img_Vw.layer.masksToBounds = true
        
        return cell
    }
    
    @objc func connected(sender: UIButton){
        let buttonTag = sender.tag
        
        items.remove(at: buttonTag)
        Vw_Collection.reloadData()
    }
    
    //didSelectItemAt
    func collectionView(_ collectionView: UICollectionView, didSelectItemAt indexPath: IndexPath) {
        // handle tap events
        print("You selected cell #\(indexPath.item)!")
        
        if indexPath.item == 0 {
            //imagePicker.allowsEditing = false
            //imagePicker.sourceType = .photoLibrary
            
            // present(imagePicker, animated: true, completion: nil)
            
            print("Imageview Clicked")
            let alertViewController = UIAlertController(title: nil, message: nil, preferredStyle: .actionSheet)
            let camera = UIAlertAction(title: "Take Photo", style: .default, handler: { (alert) in
                self.openCamera()
            })
            let gallery = UIAlertAction(title: "Choose from Gallery", style: .default) { (alert) in
                self.openGallary()
            }
            let cancel = UIAlertAction(title: "Cancel", style: .cancel) { (alert) in
                
            }
            alertViewController.addAction(camera)
            alertViewController.addAction(gallery)
            alertViewController.addAction(cancel)
            self.present(alertViewController, animated: true, completion: nil)
        }
        
    }
    
    //didDeselectItemAt
    func collectionView(_ collectionView: UICollectionView, didDeselectItemAt indexPath: IndexPath) {
        
        let cell = collectionView.cellForItem(at: indexPath)
        cell?.backgroundColor = UIColor.cyan
        
    }
    
    //collectionViewLayout
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, sizeForItemAt indexPath: IndexPath) -> CGSize {
        return CGSize(width: 75, height: 75)
        // let width = UIScreen.main.bounds.width
        // return CGSize(width: (width - 10)/4, height: (width - 10)/4) // width & height are the same to make a square cell
    }
    
    //MARK: -- ImageView Delegates
    
    func openCamera() {
        if UIImagePickerController.isSourceTypeAvailable(UIImagePickerControllerSourceType.camera) {
            imagePicker.delegate = self
            imagePicker.sourceType = UIImagePickerControllerSourceType.camera
            imagePicker.allowsEditing = false
            self .present(imagePicker, animated: true, completion: nil)
        }
        else {
            let alertWarning = UIAlertView(title:"Warning", message: "You don't have camera", delegate:nil, cancelButtonTitle:"OK")//, otherButtonTitles:"")
            alertWarning.show()
        }
    }
    
    func openGallary() {
        if UIImagePickerController.isSourceTypeAvailable(UIImagePickerControllerSourceType.photoLibrary) {
            imagePicker.delegate = self
            imagePicker.sourceType = UIImagePickerControllerSourceType.photoLibrary
            imagePicker.allowsEditing = false
            self.present(imagePicker, animated: true, completion: nil)
        }
    }
    
    //imagePickerController
    public func imagePickerController(_ picker: UIImagePickerController, didFinishPickingMediaWithInfo info: [String : Any])
    {
        // assign pickup image into varibale
        var selectedimage : UIImage!
       
        if let editedImage = info[UIImagePickerControllerEditedImage] as? UIImage
        {
            selectedimage =  editedImage.fixedOrientation()
        }
        else
        {
            selectedimage = info[UIImagePickerControllerOriginalImage] as? UIImage
              selectedimage =  selectedimage.fixedOrientation()
         
        }
        
        View = true
        items.append(selectedimage)
        print(items)
        
        if items.count != 0 {
            Vw_Collection.reloadData()
        }
        
        dismiss(animated: true, completion: nil)
    }
    
    //imagePickerControllerDidCancel
    public func imagePickerControllerDidCancel(_ picker: UIImagePickerController)
    {
        dismiss(animated: true, completion: nil)
    }
 
    
    //MARK: -- ProgressBar Delegate
    
    func progressBar(_ progressBar: FlexibleSteppedProgressBar, didSelectItemAtIndex index: Int) {
        progressBar.currentIndex = index
        if index > maxIndex {
            maxIndex = index
            progressBar.completedTillIndex = maxIndex
        }
    }
    
    func progressBar(_ progressBar: FlexibleSteppedProgressBar,
                     canSelectItemAtIndex index: Int) -> Bool {
        
        if progressBar == progressBarWithDifferentDimensions {
            print("Print Index \(index)")
            
            print("name is \(ArrGold[index])")
            ArrGoldIndex = ArrGold[index] as NSString
        }
        
        
        return true
    }
    
    func progressBar(_ progressBar: FlexibleSteppedProgressBar,
                     textAtIndex index: Int, position: FlexibleSteppedProgressBarTextLocation) -> String
    {
        
        if progressBar == progressBarWithDifferentDimensions {
            if position == FlexibleSteppedProgressBarTextLocation.bottom {
                
                switch index {
                case 0...ArrGold.count:
                    return self.ArrGold[index]
                    
                default: break
                    // "10k GOLD","14k GOLD","18k GOLD","21k GOLD","PLATINUM"
                }
            }
        }
        
        return ""
    }
    
    // @objc selector expected for Timer
    @objc func update() {
        // do what should happen when timer triggers an event
        View = false
        
        let storyboard = UIStoryboard(name:"Main", bundle:nil)
        let home = storyboard.instantiateViewController(withIdentifier: "ConfirmOrderViewController")as! ConfirmOrderViewController
        home.ISJewlary = true
        
        var Comment:String = ""
        
        if Txt_Comment.text == "Additional Info" {
            Comment = "N/A"
        }
        else{
            Comment = Txt_Comment.text!
        }
        
        var MaterialType:String = ""
        
        if OtherBool == true{
            MaterialType = Txt_Other.text!
        }
        else
                   {
                       MaterialType = Btn_Material.titleLabel?.text ?? ""
                   }
        
        AppDelegate.shared().GETData = ["CustomerName": Txt_Customer.text ?? "", "CaratDiamondWeight": Txt_Caret.text!, "JewlaryStyleTypeID":Txt_JewelyType.text!, "MetalType":ArrGoldIndex as String,"MaterialType":MaterialType,"MaterialTypeID":MaterialArr[MaterialArrIndex]["MaterialTypeID"] as! String, "CustomerComment": Comment,"EstimatedValue":Txt_Estimate.text!,"weight_of_item":Txt_Weight.text!, "ItemTypeID":"3","GemMaterial":Btn_Material.titleLabel!.text!,"jew_clearity_start":jew_clearity_start,"jew_clearity_end":jew_clearity_end,"jew_color_start":jew_color_start,"jew_color_end":jew_color_end,"jew_clearity_start_id":jew_clearity_start_id,"jew_clearity_end_id":jew_clearity_end_id,"jew_color_start_id":jew_color_start_id,"jew_color_end_id":jew_color_end_id,"morecolor":txtcolor.text ?? ""] as! [String : String]
        print("Dictionary \(AppDelegate.shared().GETData)")
        if items.count > 1 {
            items.remove(at: 0)
            AppDelegate.shared().ImageItem = items
        }
        
        self.navigationController?.pushViewController(home, animated: true)
    }
    
    func DropDown(Array : [[String:AnyObject]], Name : String, ButtonName : String, Button : UIButton)
    {
        var array = [NSString]()
        for var i in (0..<Array.count)
        {
            var dict = Array[i][Name]
            print("ggg \(dict)")
            array.append(dict! as! NSString)
        }
        print(array)
        Dropobj = DropDownListView.init(title: ButtonName, options: array, xy: CGPoint.init(x: Button.frame.origin.x, y: Button.frame.origin.y+40), size: CGSize.init(width: Button.frame.size.width, height: 330), isMultiple: false, isQuestionClinicle: true)
        Dropobj.delegate = self
        Dropobj.show(in: Vw_Scroll, animated: true)
        Dropobj.setBackGroundDropDown_R(18.0, g: 64.0, b: 118.0, alpha: 1.0)
    }
    
    func DropDown1(Array : [[String:AnyObject]], Name : String, ButtonName : String, Button : UIButton)
    {
        self.view.endEditing(true)
        var array = [NSString]()
        for var i in (0..<Array.count)
        {
            var dict = Array[i][Name]
            print("ggg \(dict)")
            array.append(dict! as! NSString)
        }
        print(array)
        Dropobj1 = DropDownListView.init(title: ButtonName, options: array, xy: CGPoint.init(x: Button.frame.origin.x, y: Button.frame.origin.y+40), size: CGSize.init(width: Button.frame.size.width, height: 330), isMultiple: false, isQuestionClinicle: true)
        Dropobj1.delegate = self
        Dropobj1.show(in: Vw_Scroll, animated: true)
        Dropobj1.setBackGroundDropDown_R(18.0, g: 64.0, b: 118.0, alpha: 1.0)
    }
    
    //MARK: -- DropDown Delegate
    
    func dropDownListView(_ dropdownListView: DropDownListView!, didSelectedIndex anIndex: Int) {
        if (dropdownListView == Dropobj)
        {
            MaterialArrIndex = anIndex
            let dict = MaterialArr[anIndex]["MaterialType"]
            if (dict?.isEqual("Other"))!
            {
                 setslider()
                OtherBool = true
                Txt_Caret.text = ""
                Txt_Caret.text = ""
                Txt_Caret.isEnabled=true
                Txt_Other.isHidden = false
                Txt_Other.isEnabled=true;
                Vw_Other.frame = CGRect.init(x: Vw_Other.frame.origin.x, y: 415, width: Vw_Other.frame.size.width, height: Vw_Other.frame.size.height)
                 bottomvw.frame.origin.y = 250;
                diamondcslider.isHidden = false;
                claritytextview.isHidden = false;
//                qualityslider.isHidden = false;
//                qualitytextview.isHidden = false;
                //lblline.isHidden = false;
                lblquality.isHidden = false;
                lblclarity.isHidden = false;
                Txt_Caret.isHidden = false;
                 Vw_Scroll.contentSize = CGSize(width: self.view.frame.size.width, height: 1250)
            }else if (dict?.isEqual("None"))!
            {
                OtherBool = false
                Txt_Caret.text = "0"
                Txt_Caret.isEnabled=false
               
                 bottomvw.frame.origin.y = 0;
                diamondcslider.isHidden = true;
                               claritytextview.isHidden = true;
                               qualityslider.isHidden = true;
                               qualitytextview.isHidden = true;
                             //  lblline.isHidden = true;
                               lblquality.isHidden = true;
                               lblclarity.isHidden = true;
                Txt_Caret.isHidden = true;
                
                jew_clearity_start = "0"
                jew_clearity_end = "0"
                jew_clearity_start_id = "0"
                jew_clearity_end_id = "0"
                jew_color_start = "0"
                jew_color_end = "0"
                jew_color_start_id = "0"
                jew_color_end_id = "0"
                
               
                
                Txt_Other.isHidden = true
                Vw_Other.frame = CGRect.init(x: Vw_Other.frame.origin.x, y: 365, width: Vw_Other.frame.size.width, height: Vw_Other.frame.size.height)
                 Vw_Scroll.contentSize = CGSize(width: self.view.frame.size.width, height: 1000)
            }
            else{
                 setslider()
                 bottomvw.frame.origin.y = 250;
                 Vw_Scroll.contentSize = CGSize(width: self.view.frame.size.width, height: 1250)
                diamondcslider.isHidden = false;
                claritytextview.isHidden = false;
//                qualityslider.isHidden = false;
//                qualitytextview.isHidden = false;
              //  lblline.isHidden = false;
                lblquality.isHidden = false;
                lblclarity.isHidden = false;
                Txt_Caret.text = ""
                Txt_Caret.isEnabled=true
                Txt_Caret.isHidden = false;
                if OtherBool == true{
                    OtherBool = false
                     NoneBool = false
                    Txt_Other.isHidden = true
                    Vw_Other.frame = CGRect.init(x: Vw_Other.frame.origin.x, y: 365, width: Vw_Other.frame.size.width, height: Vw_Other.frame.size.height)
                    
                }
                else{
                    OtherBool = false
                }
            }
            
            Btn_Material.setTitle(dict as? String, for: .normal)
        }
        else{
            JewelryArrIndex = anIndex
            let dict = JewelryArr[anIndex]["JewelarystyleType"]
            Btn_JewelryType.setTitle(dict as? String, for: .normal)
        }
    }
    
    func dropDownListView(_ dropdownListView: DropDownListView!, datalist ArryData: NSMutableArray!) {
        if (dropdownListView == Dropobj)
        {
            print("list \(ArryData.componentsJoined(by: ", "))")
            
            if (ArryData.count != 0){
                Btn_Material.setTitle(ArryData.componentsJoined(by: ", "), for: .normal)
            }
            else{
                Btn_Material.setTitle("None", for: .normal)
            }
        }
        else{
            print("list \(ArryData.componentsJoined(by: ", "))")
            Btn_JewelryType.setTitle(ArryData.componentsJoined(by: ", "), for: .normal)
        }
    }
    
    func dropDownListViewDidCancel() {
        
    }
    
    //MARK: -- CheckBox Delegate
    func didTap(_ checkBox: BEMCheckBox) {
        self.view.endEditing(true)
        if (checkBox==Check_Terms)
        {
            if (checkBox.on == true)
            {
                print("It's Chc")
                Btn_Next.isEnabled = true
                Btn_Next.alpha = 1.0
            }
            else{
                print("it's Loose Stone")
                Btn_Next.isEnabled = false
                Btn_Next.alpha = 0.2
            }
            
        }
    }
    
    //MARK: -- Scrollview Delegate
    func scrollViewWillBeginDragging(_ scrollView: UIScrollView) {
        // execute when you drag the scrollView
        self.view.endEditing(true)
        print("scrollViewWillBeginDragging")
    }
    
    //MARK: - Actions👊🏻😠
    @IBAction func NextButtonPressed(_ sender: Any) {
        
//        if(!Txt_Customer.validate()){
//            //[Vw_Scroll setContentOffset:CGPointMake(self.Tex, y) animated:YES];
//            Vw_Scroll.setContentOffset(CGPoint(x: 0, y: 0), animated: true)
//            AppDelegate.shared().ShowAlert(title: Constants.ERROR_HEADER, msg:"Please Enter Customer name \n (Character limit is 1-24 only)")
//            // Vw_Scroll.scrollToView(self.Txt_Customer, animated: true)
//        }
        
        let a = "none"
               let b = Btn_Material.titleLabel?.text!
               let result: ComparisonResult = a.compare(String(b ?? ""), options: NSString.CompareOptions.caseInsensitive, range: nil, locale: nil)
        
        if(!Txt_JewelyType.validate()){
            Vw_Scroll.setContentOffset(CGPoint(x: 0, y: 0), animated: true)
            AppDelegate.shared().ShowAlert(title: Constants.ERROR_HEADER, msg:"Please Enter Jewelry Type \n (Character limit is 1-22 only)")
        }  else if(MaterialArrIndex == -1){
            Vw_Scroll.setContentOffset(CGPoint(x: 0, y: 0), animated: true)
            AppDelegate.shared().ShowAlert(title: Constants.ERROR_HEADER, msg:"Please Select Metal type")
        }else if(Btn_Material.titleLabel?.text == "GEM MATERIAL:"){
            AppDelegate.shared().ShowAlert(title: Constants.ERROR_HEADER, msg: "Please select GEM MATERIAL")
        }else if(!Txt_Caret.validate()){
            Vw_Scroll.setContentOffset(CGPoint(x: 0, y: 0), animated: true)
            AppDelegate.shared().ShowAlert(title: Constants.ERROR_HEADER, msg:"Please Enter Total carat weight")
        } else if(!Txt_Estimate.validate()){
            Vw_Scroll.setContentOffset(CGPoint(x: 0, y: 0), animated: true)
            AppDelegate.shared().ShowAlert(title: Constants.ERROR_HEADER, msg:"Please Enter Estimated value")
        } else if(!Txt_Weight.validate()){
            Vw_Scroll.setContentOffset(CGPoint(x: 0, y: 0), animated: true)
            AppDelegate.shared().ShowAlert(title: Constants.ERROR_HEADER, msg:"Please Enter Weight of Item")
        }
        else if(items.count == 1 ){
            AppDelegate.shared().ShowAlert(title: Constants.ERROR_HEADER, msg: "Please upload atleast one picture.")
        }
        else if(!txtcolor.isHidden) && txtcolor.text == "" && result != .orderedSame{
            Vw_Scroll.setContentOffset(CGPoint(x: 0, y: 0), animated: true)
            AppDelegate.shared().ShowAlert(title: Constants.ERROR_HEADER, msg: "Enter Color info.")
        }
        else {
            
            Btn_Next.transform = CGAffineTransform(scaleX: 0.9, y: 0.9)
            Btn_Next.isUserInteractionEnabled = false
            UIView.animate(withDuration: 2.0,
                           delay: 0,
                           usingSpringWithDamping: 0.2,
                           initialSpringVelocity: 6.0,
                           options: .allowUserInteraction,
                           animations: { [weak self] in
                            self?.Btn_Next.transform = .identity
                            
                            Timer.scheduledTimer(timeInterval: 0.7,
                                                 target: self!,
                                                 selector: #selector(JewelryViewController.update),
                                                 userInfo: nil,
                                                 repeats: false)
                },
                completion: nil)
        }
        
        
    }
    
    
    @IBAction func JewelryButtonPressed(_ sender: Any) {
        Dropobj.isHidden = true
        Dropobj1.isHidden = true
        DropDown1(Array: JewelryArr, Name: "JewelarystyleType", ButtonName: "Jewelry Style / type :", Button: Btn_JewelryType)
    }
    
    @IBAction func MaterialButtonPressed(_ sender: Any) {
        Dropobj.isHidden = true
        Dropobj1.isHidden = true
        DropDown(Array: MaterialArr, Name: "MaterialType", ButtonName: "GEM MATERIAL:", Button: Btn_Material)
    }
    
}
