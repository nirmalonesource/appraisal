//
//  SupportViewController.swift
//  Appraisal
//
//  Created by ADMIN on 3/28/18.
//  Copyright © 2018 empiereTech. All rights reserved.
//

import UIKit
import SWRevealViewController
import Alamofire
import SwiftyJSON

class SupportViewController: UIViewController,SWRevealViewControllerDelegate,UITextViewDelegate {
    
//MARK: - Outlets🤔
    
    //Views
    @IBOutlet var Vw_Side: UIView!
    
    //TextFeild // TextView
    @IBOutlet var Txt_Comment: UITextView!
    @IBOutlet var Txt_Subject: UITextField!
    
    //Button
    @IBOutlet var Btn_Next: UIButton!
    
    
    //MARK: - Declare Variables👇🏻😬
    
    
//MARK: - Happy Coding😊
    override func viewDidLoad() {
        super.viewDidLoad()
        
        self.title = "Support"
        
        //NavigationBar
        navigationController?.navigationBar.barTintColor = UIColor.white
        navigationController?.navigationBar.titleTextAttributes = [NSAttributedStringKey.foregroundColor: UIColor(red: 18/255, green: 64/255, blue: 118/255, alpha: 1),NSAttributedStringKey.font: UIFont(name: "Montserrat-Medium", size: 17)!]
        self.navigationItem.setHidesBackButton(true, animated:true);
        
        let menuButton = UIButton(type: .system)
        menuButton.setImage(#imageLiteral(resourceName: "Menu").withRenderingMode(.alwaysOriginal), for: .normal)
        menuButton.frame = CGRect(x: 0, y: 0, width: 34, height: 34)
        navigationItem.leftBarButtonItem = UIBarButtonItem(customView: menuButton)
        
        //SideBar
        if revealViewController() != nil
        {
            self.revealViewController().delegate = self
            menuButton.addTarget(revealViewController(), action: "revealToggle:", for: .touchUpInside)
            self.navigationController?.navigationBar.addGestureRecognizer(self.revealViewController().panGestureRecognizer())
            view.addGestureRecognizer(self.revealViewController().panGestureRecognizer())
        }
        
        Txt_Subject.layer.cornerRadius = 7.0
        Txt_Subject.layer.backgroundColor = UIColor.white.cgColor
        Txt_Subject.layer.masksToBounds = false
        Txt_Subject.layer.shadowOffset = CGSize.zero
        Txt_Subject.layer.shadowRadius = 5
        Txt_Subject.layer.shadowOpacity = 0.2
        Txt_Subject.leftView = UIView(frame: CGRect(x: 0, y: 0, width: 15, height: Txt_Subject.frame.height))
        Txt_Subject.leftViewMode = .always
        let CustomerAttributed = NSMutableAttributedString.init(string: "Subject")
        let Customerterms:NSString = "Subject"
        let Customerrangech :NSRange = Customerterms.range(of: "Subject")
        CustomerAttributed.addAttribute(NSAttributedStringKey.foregroundColor, value: UIColor.darkGray, range: Customerrangech)
        Txt_Subject.attributedPlaceholder = CustomerAttributed
        
        Txt_Comment.layer.cornerRadius = 7.0
        Txt_Comment.layer.backgroundColor = UIColor.white.cgColor
        Txt_Comment.layer.masksToBounds = false
        Txt_Comment.layer.shadowOffset = CGSize.zero
        Txt_Comment.layer.shadowRadius = 5
        Txt_Comment.layer.shadowOpacity = 0.2
        Txt_Comment.text = "Your Comments"
        Txt_Comment.textColor = UIColor.darkGray
        Txt_Comment.delegate = self
        Txt_Comment.contentInset = UIEdgeInsetsMake(5, 12, 0, 0)
        
        Btn_Next.layer.cornerRadius = Btn_Next.frame.size.height/2
    }
    
    
//MARK: - Actions👊🏻😠
    
    //NextButtonPressed
    @IBAction func NextButtonPressed(_ sender: Any) {
        
        if (Txt_Subject.text?.isEmpty)! {
            AppDelegate.shared().ShowAlert(title: Constants.ERROR_HEADER, msg:"Please Enter Subject")
        }
        else
        {
            if (Txt_Comment.text == "Your Comments" || (Txt_Comment.text?.isEmpty)!) {
                AppDelegate.shared().ShowAlert(title: Constants.ERROR_HEADER, msg:"Please Enter Comment")
            }
            else{
                AppDelegate.shared().ShowHUD()
                Btn_Next.transform = CGAffineTransform(scaleX: 0.9, y: 0.9)
                Btn_Next.isUserInteractionEnabled = false
                UIView.animate(withDuration: 2.0,
                               delay: 0,
                               usingSpringWithDamping: 0.2,
                               initialSpringVelocity: 6.0,
                               options: .allowUserInteraction,
                               animations: { [weak self] in
                                self?.Btn_Next.transform = .identity
                                
                                Timer.scheduledTimer(timeInterval: 0.7,
                                                     target: self!,
                                                     selector: #selector(SupportViewController.update),
                                                     userInfo: nil,
                                                     repeats: false)
                    },
                               completion: nil)
            }
            }
        
    }
    
    
//MARK: - Methods(Functions)☝🏻😳
    
    // @objc selector expected for Timer
    @objc func update() {
        // do what should happen when timer triggers an event
          if NetworkReachabilityManager()!.isReachable {
             CallMyMethod()
        } else {
         AppDelegate.shared().HideHUD()
            AppDelegate.shared().ShowAlert(title: Constants.ERROR_NO_INTERNET, msg: "Unable to connect Internet, Please try after some time.")
        }
        
    }
    
    func textViewDidBeginEditing(_ textView: UITextView) {
        if textView.textColor == UIColor.darkGray {
            textView.text = nil
            textView.textColor = UIColor.black
        }
    }
    
    func textViewDidEndEditing(_ textView: UITextView) {
        if textView.text.isEmpty {
            textView.text = "Additional Info"
            textView.textColor = UIColor.darkGray
        }
    }
    
    func revealController(_ revealController: SWRevealViewController, didMoveTo position: FrontViewPosition) {
        if case .right = position {
            
            revealController.frontViewController.view.alpha = 0.5
            self.view.isUserInteractionEnabled = false
            self.revealViewController().frontViewController.view.addGestureRecognizer(self.revealViewController().panGestureRecognizer())
            self.revealViewController().frontViewController.view.addGestureRecognizer(self.revealViewController().tapGestureRecognizer())
        } else {
            
            revealController.frontViewController.view.alpha = 1
            self.revealViewController().tapGestureRecognizer()
            self.view.isUserInteractionEnabled = true
        }
    }
    
    
//MARK: - API🤞🏻🙄
    
    func  CallMyMethod()
    {
        let params : Parameters = ["subject":Txt_Subject.text!,"comment":Txt_Comment.text!]
        print("param is \(params)")
        let url = URL(string: "\(Constants.API)\(Constants.Support)")!
        print("Url printed \(url)")
        
        let manager = Alamofire.SessionManager.default
        manager.session.configuration.timeoutIntervalForRequest = 120
        
        let login = UserDefaults.standard.string(forKey: "LoginToken")
        let UserID = UserDefaults.standard.string(forKey: "UserID")
        
        let headers = ["Authorization": "Basic YWRtaW46QVBJQEZFREVSQUwhIyRXRUIk",
                       "X-FEDERAL-API-KEY": "kkcoswggwgwkkc8w4ok808o48kswc40c0www4wss",
                       "X-FEDERAL-LOGIN-TOKEN": login!,
                       "USER-ID": UserID!]
        
        Btn_Next.isUserInteractionEnabled = true
        manager.request(url, method: .post, parameters: params, headers: headers).responseJSON() { response in
            switch (response.result) {
            case .success:
                //do json stuff
                let jsonResponse = response.result.value as! NSDictionary
                print("JsonResponse printed \(jsonResponse)")
                
                AppDelegate.shared().HideHUD()
                let myInt: Bool = jsonResponse["status"]! as! Bool
                
                if(myInt == true)
                {
                    print("Success")
                    self.Txt_Comment.text = "Your Comments"
                    self.Txt_Comment.textColor = UIColor.darkGray
                    
                    self.Txt_Subject.text = ""
                }
                
                AppDelegate.shared().ShowAlert(title: Constants.ERROR_HEADER, msg: jsonResponse.value(forKeyPath: "message") as! String)
                
                break
                
            case .failure(let error):
                AppDelegate.shared().HideHUD()
                if error._code == NSURLErrorTimedOut {
                    //HANDLE TIMEOUT HERE
                }
                print("\n\nAuth request failed with error:\n \(error)")
                break
            }
        }
    }
}
