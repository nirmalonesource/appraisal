//
//  WebViewController.swift
//  Appraisal
//
//  Created by ADMIN on 4/30/18.
//  Copyright © 2018 empiereTech. All rights reserved.
//

import UIKit
import SWRevealViewController

class WebViewController: UIViewController,SWRevealViewControllerDelegate {

//MARK: - Outlets🤔
    
    //View
    @IBOutlet weak var Vw_WebView: UIWebView!
    
    
//MARK: - Declare Variables👇🏻😬
    var url = NSURL()
    var TitleName = NSString()
    
    
//MARK: - Happy Coding😊
    
    //viewDidLoad
    override func viewDidLoad() {
        super.viewDidLoad()

        //NavigationBar
        self.navigationController?.navigationBar.isHidden = false;
        self.title = TitleName as String
        
        //navigationController?.navigationBar.barTintColor = UIColor(red: 255/255, green: 98/255, blue: 53/255, alpha: 1)
        navigationController?.navigationBar.barTintColor = UIColor.white
        if (UIScreen.main.bounds.size.height == 568.0)
        {
            print("iphone5")
            navigationController?.navigationBar.titleTextAttributes = [NSAttributedStringKey.foregroundColor: UIColor(red: 18/255, green: 64/255, blue: 118/255, alpha: 1),NSAttributedStringKey.font: UIFont(name: "Montserrat-Medium", size: 15)!]
        }
        else{
            navigationController?.navigationBar.titleTextAttributes = [NSAttributedStringKey.foregroundColor: UIColor(red: 18/255, green: 64/255, blue: 118/255, alpha: 1),NSAttributedStringKey.font: UIFont(name: "Montserrat-Medium", size: 17)!]
        }
        
        navigationController?.navigationBar.tintColor = UIColor(red: 18/255, green: 64/255, blue: 118/255, alpha: 1)
        
        
        let SearchButton = UIButton(type: .system)
        SearchButton.setImage(#imageLiteral(resourceName: "ShareIcon").withRenderingMode(.alwaysOriginal), for: .normal)
        SearchButton.frame = CGRect(x: 0, y: 0, width: 34, height: 34)
        SearchButton.addTarget(self, action: #selector(WebViewController.ShareMethod), for: .touchUpInside)
        navigationItem.rightBarButtonItem = UIBarButtonItem(customView: SearchButton)
        
        
        
        let request = URLRequest(url: url as URL)
        Vw_WebView.loadRequest(request)
    }

    
//MARK: - Actions👊🏻😠
    
    
//MARK: - Methods(Functions)☝🏻😳
    
    @objc func ShareMethod(sender: UIButton) {
        let buttonTag = sender.tag
        print("Edit Certi")
        let myWebsite = url
        let shareAll = [myWebsite]
        let activityViewController = UIActivityViewController(activityItems: shareAll, applicationActivities: nil)
        activityViewController.popoverPresentationController?.sourceView = self.view
        self.present(activityViewController, animated: true, completion: nil)
    }
    
//MARK: - Delegates✋🏻😇
    
    

}
