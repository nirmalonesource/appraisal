//
//  ReportIssueViewController.swift
//  Appraisal
//
//  Created by ADMIN on 3/24/18.
//  Copyright © 2018 empiereTech. All rights reserved.
//

import UIKit
import SWRevealViewController
import SDWebImage

protocol MyProtocol {
    func setResultOfBusinessLogic(valueSent: UIImage)
}

class ReportIssueViewController: UIViewController,SWRevealViewControllerDelegate {

//MARK: - Outlets🤔
    
    //View
    @IBOutlet var Vw_Side: UIView!
    @IBOutlet var Vw_Scroll: UIScrollView!
    @IBOutlet var Vw_1: UIView!
    @IBOutlet var Vw_2: UIView!
    @IBOutlet var Vw_3: UIView!
    
    //Button
    @IBOutlet var Btn_Submit: UIButton!
    @IBOutlet var Btn_Skip: UIButton!
    
    //ImageView
    @IBOutlet weak var Img_1: UIImageView!
    @IBOutlet weak var Img_2: UIImageView!
    @IBOutlet var Img_3: UIImageView!
    @IBOutlet weak var Img_Watcher: UIImageView!
    
    
//MARK: - Declare Variables👇🏻😬
    
    //Array
    var ShapeArr = [[String:AnyObject]]()
    var GetChat = Bool()
    var GetChatImage = UIImage()
    var GetChatscreenShot = [UIImage]()
    var CheckTAp = Bool()
    var delegate:MyProtocol?
    var GetTAP = Bool()
    var GETscreenShot = UIImage()
    var screenShot1 = UIImage()
    var screenShot2 = UIImage()
    
    
//MARK: - Happy Coding😊
    override func viewDidLoad() {
        super.viewDidLoad()

        // Do any additional setup after loading the view.
        CheckTAp = false
        
        //NavigationBar
        self.navigationController?.navigationBar.isHidden = false;
        self.title = "Authenticator"
        
        navigationController?.navigationBar.barTintColor = UIColor.white
        navigationController?.navigationBar.titleTextAttributes = [NSAttributedStringKey.foregroundColor: UIColor(red: 18/255, green: 64/255, blue: 118/255, alpha: 1),NSAttributedStringKey.font: UIFont(name: "Montserrat-Medium", size: 17)!]
        navigationController?.navigationBar.tintColor = UIColor(red: 18/255, green: 64/255, blue: 118/255, alpha: 1)
        
        let menuButton2 = UIButton(type: .system)
        menuButton2.setTitle("Reset", for: .normal)
        menuButton2.frame = CGRect(x: 0, y: 0, width: 40, height: 25)
        menuButton2.addTarget(self, action: #selector(ReportIssueViewController.ResetButton), for: .touchUpInside)
        navigationItem.rightBarButtonItem = UIBarButtonItem(customView: menuButton2)
        // if you want disable
        self.navigationItem.rightBarButtonItem?.isEnabled = false
        AppDelegate.shared().ShowHUD()
        
        Btn_Submit.layer.cornerRadius = Btn_Submit.frame.size.height/2.0
        
        if GetChat == false {
            Vw_1.isHidden = false
            Vw_2.isHidden = false
            Vw_3.isHidden = true
            var dict = AppDelegate.shared().ArrGetAllType[0]
            ShapeArr = dict["Shape"]! as! [[String:AnyObject]]
            
            let url = NSURL(string: "\(ShapeArr[Int(AppDelegate.shared().GETData["IndexSelect"]!)!]["FirstImage"]!)")
            let url2 = NSURL(string: "\(ShapeArr[Int(AppDelegate.shared().GETData["IndexSelect"]!)!]["SecondImage"]!)")
            print(url!)
            
            
//            Img_1.sd_setImage(with: url! as URL, placeholderImage:nil)
//            if Img_1.image != nil{
//                AppDelegate.shared().HideHUD()
//                let gestureRecognizer = UITapGestureRecognizer(target: self, action: #selector(ReportIssueViewController.handleTap))
//                Img_1.addGestureRecognizer(gestureRecognizer)
//                Img_1.isUserInteractionEnabled = true
//            }
            
            Img_1.sd_setImage(with: url! as URL, placeholderImage: nil , options:SDWebImageOptions.avoidAutoSetImage, completed: { (image, error, cacheType, url) in
                if cacheType == SDImageCacheType.none {
                    UIView.transition(with: self.view!, duration: 0.2, options:  [.transitionCrossDissolve ,.allowUserInteraction, .curveEaseIn], animations: {
                        self.Img_1.image = image
                        AppDelegate.shared().HideHUD()
                        let gestureRecognizer = UITapGestureRecognizer(target: self, action: #selector(ReportIssueViewController.handleTap))
                        self.Img_1.addGestureRecognizer(gestureRecognizer)
                        self.Img_1.isUserInteractionEnabled = true
                    }, completion: { (completed) in
                        AppDelegate.shared().HideHUD()
                        if self.Img_1.image == nil{
                            AppDelegate.shared().ShowAlert(title: Constants.ERROR_HEADER, msg: "No Image to Show")
                        }
                    })
                } else {
                    self.Img_1.image = image
                    AppDelegate.shared().HideHUD()
                    let gestureRecognizer = UITapGestureRecognizer(target: self, action: #selector(ReportIssueViewController.handleTap))
                    self.Img_1.addGestureRecognizer(gestureRecognizer)
                    self.Img_1.isUserInteractionEnabled = true
                }
            })
            
            //Img_2.sd_setImage(with: url2! as URL, placeholderImage:nil)
            
            Img_2.sd_setImage(with: url2! as URL, placeholderImage: nil , options:SDWebImageOptions.avoidAutoSetImage, completed: { (image, error, cacheType, url) in
                if cacheType == SDImageCacheType.none {
                    UIView.transition(with: self.view!, duration: 0.2, options:  [.transitionCrossDissolve ,.allowUserInteraction, .curveEaseIn], animations: {
                        self.Img_2.image = image
                        AppDelegate.shared().HideHUD()
                        let gestureRecognizer2 = UITapGestureRecognizer(target: self, action: #selector(ReportIssueViewController.handleTap2))
                        self.Img_2.addGestureRecognizer(gestureRecognizer2)
                        self.Img_2.isUserInteractionEnabled = true
                    }, completion: { (completed) in
                        AppDelegate.shared().HideHUD()
                        if self.Img_2.image == nil{
                            AppDelegate.shared().ShowAlert(title: Constants.ERROR_HEADER, msg: "No Image to Show")
                        }
                        
                    })
                    
                } else {
                    self.Img_2.image = image
                    AppDelegate.shared().HideHUD()
                    let gestureRecognizer2 = UITapGestureRecognizer(target: self, action: #selector(ReportIssueViewController.handleTap2))
                    self.Img_2.addGestureRecognizer(gestureRecognizer2)
                    self.Img_2.isUserInteractionEnabled = true
                }
            })
            /*
            if Img_2.image != nil{
                AppDelegate.shared().HideHUD()
                let gestureRecognizer2 = UITapGestureRecognizer(target: self, action: #selector(ReportIssueViewController.handleTap2))
                Img_2.addGestureRecognizer(gestureRecognizer2)
                Img_2.isUserInteractionEnabled = true
            }*/
        }
        else{
            self.navigationItem.setHidesBackButton(true, animated:true);
            Vw_1.isHidden = true
            Vw_2.isHidden = true
            Vw_3.isHidden = false
            
            Img_3.image = GetChatImage
            if Img_3.image != nil{
                AppDelegate.shared().HideHUD()
                let gestureRecognizer = UITapGestureRecognizer(target: self, action: #selector(ReportIssueViewController.handleTap3))
                Img_3.addGestureRecognizer(gestureRecognizer)
                Img_3.isUserInteractionEnabled = true
            }
        }
        
    
        if (UIDevice.current.userInterfaceIdiom == .pad)
        {
            print("iPad")
        }
        else if (UIScreen.main.bounds.size.height == 812.0)
        {
            print("iphoneX")
            Vw_Scroll.frame = CGRect.init(x: Vw_Scroll.frame.origin.x, y: Vw_Scroll.frame.origin.y + 50, width: Vw_Scroll.frame.size.width, height: Vw_Scroll.frame.size.height)
        }
        else if (UIScreen.main.bounds.size.height == 568.0)
        {
            print("iphone5")
            Vw_Scroll.contentSize = CGSize(width: self.view.frame.size.width, height: 1000)
        }
            
        else if (UIScreen.main.bounds.size.height == 480.0)
        {
            print("iphone4")
            Vw_Scroll.contentSize = CGSize(width: self.view.frame.size.width, height: 800)
        }
        else
        {
            print("OtherPhones")
            Vw_Scroll.contentSize = CGSize(width: self.view.frame.size.width, height: 900)
        }
    }
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(true)
        screenShot1 = UIImage()
        screenShot2 = UIImage()
        GETscreenShot = UIImage()
        
    
        GetTAP = false
        AppDelegate.shared().WatchImageitem.removeAll()
        for subview in Img_1.subviews {
            subview.removeFromSuperview()
        }
        
        for subview in Img_2.subviews {
            subview.removeFromSuperview()
        }
        
        for subview in Img_3.subviews {
            subview.removeFromSuperview()
        }
    }


//MARK: - Actions👊🏻😠
    
    //SubmitButtonPressed
    @IBAction func SubmitButtonPressed(_ sender: Any) {
        
        Btn_Submit.transform = CGAffineTransform(scaleX: 0.9, y: 0.9)
        UIView.animate(withDuration: 2.0,
                       delay: 0,
                       usingSpringWithDamping: 0.2,
                       initialSpringVelocity: 6.0,
                       options: .allowUserInteraction,
                       animations: { [weak self] in
                        self?.Btn_Submit.transform = .identity
                        
                        Timer.scheduledTimer(timeInterval: 0.7,
                                             target: self!,
                                             selector: #selector(ReportIssueViewController.update1),
                                             userInfo: nil,
                                             repeats: false)
            },
                       completion: nil)
        
     
    }
    
    @objc func update1() {
        // do what should happen when timer triggers an event
        if GetChat == false {
            if GetTAP == true {
                let storyboard = UIStoryboard(name:"Main", bundle:nil)
                let home = storyboard.instantiateViewController(withIdentifier: "ConfirmOrderViewController")as! ConfirmOrderViewController
                home.ISEngagementSide = true
                
                let size = CGSize(width: 0, height: 0)
                
                //                screenShot1.cropAlpha()
                //                screenShot2.cropAlpha()
                
                
                if (screenShot1.size.width != size.width) && (screenShot2.size.width != size.width)
                {
                    AppDelegate.shared().WatchImageitem.append(screenShot1)
                    AppDelegate.shared().WatchImageitem.append(screenShot2)
                }
                else if (screenShot1.size.width != size.width){
                    AppDelegate.shared().WatchImageitem.append(screenShot1)
                }
                else if (screenShot2.size.width != size.width){
                    AppDelegate.shared().WatchImageitem.append(screenShot2)
                }
                
                
                self.navigationController?.pushViewController(home, animated: true)
            }
            else{
                AppDelegate.shared().ShowAlert(title: Constants.ERROR_HEADER, msg: "Please tap issue in image")
            }
        }
        else{
            
            if CheckTAp == false
            {
                delegate?.setResultOfBusinessLogic(valueSent: GetChatImage)
            }
            else{
                GetChatscreenShot.append(GETscreenShot)
                delegate?.setResultOfBusinessLogic(valueSent: GetChatscreenShot[0])
            }
            
            self.navigationController?.popViewController(animated: true)
            
        }
    }
    
    
    //MARK: - Actions👊🏻😠
    
    //SubmitBlankimage
    @objc func SubmitBlankimage() {
        
        if GetChat == false {
                let storyboard = UIStoryboard(name:"Main", bundle:nil)
                let home = storyboard.instantiateViewController(withIdentifier: "ConfirmOrderViewController")as! ConfirmOrderViewController
                home.ISEngagementSide = true
                
                let size = CGSize(width: 0, height: 0)
           
            UIGraphicsBeginImageContext(Vw_1.bounds.size);
            Vw_1.layer.render(in: UIGraphicsGetCurrentContext()!)
            screenShot1 = UIGraphicsGetImageFromCurrentImageContext()!;
            UIGraphicsEndImageContext();
            
            UIGraphicsBeginImageContext(Vw_2.bounds.size);
            Vw_2.layer.render(in: UIGraphicsGetCurrentContext()!)
            screenShot2 = UIGraphicsGetImageFromCurrentImageContext()!;
            UIGraphicsEndImageContext();
            
            
//                screenShot1.cropAlpha()
//                screenShot2.cropAlpha()
            
                
                if (screenShot1.size.width != size.width) && (screenShot2.size.width != size.width)
                {
                    AppDelegate.shared().WatchImageitem.append(screenShot1)
                    AppDelegate.shared().WatchImageitem.append(screenShot2)
                }
                else if (screenShot1.size.width != size.width){
                    AppDelegate.shared().WatchImageitem.append(screenShot1)
                }
                else if (screenShot2.size.width != size.width){
                    AppDelegate.shared().WatchImageitem.append(screenShot2)
                }
                 
                self.navigationController?.pushViewController(home, animated: true)
            
        }
    }
    
    //SkipButtonPressed
    @IBAction func SkipButtonPressed(_ sender: UIButton) {
        self.ResetButton(sender: sender as! UIButton)
        Btn_Skip.transform = CGAffineTransform(scaleX: 0.9, y: 0.9)
        UIView.animate(withDuration: 2.0,
                       delay: 0,
                       usingSpringWithDamping: 0.2,
                       initialSpringVelocity: 6.0,
                       options: .allowUserInteraction,
                       animations: { [weak self] in
                        self?.Btn_Skip.transform = .identity
                        
                        Timer.scheduledTimer(timeInterval: 0.7,
                                             target: self!,
                                             selector: #selector(ReportIssueViewController.update),
                                             userInfo: nil,
                                             repeats: false)
            },
                       completion: nil)
        
        
    }
    
    @objc func update() {
        // do what should happen when timer triggers an event
       
        if GetChat == false {
            
           
            
            
            let storyboard = UIStoryboard(name:"Main", bundle:nil)
            let home = storyboard.instantiateViewController(withIdentifier: "ConfirmOrderViewController")as! ConfirmOrderViewController
            home.ISEngagementSide = true
            
            sleep(2)
            self.SubmitBlankimage()
        }
        else{
            // Perform some business logic. Anything!
            delegate?.setResultOfBusinessLogic(valueSent: GetChatImage)
            self.navigationController?.popViewController(animated: true)
        }
    }
    
    //MARK: - Methods(Functions)☝🏻😳
    @objc func ResetButton(sender: UIButton) {
        super.viewWillAppear(true)
        screenShot1 = UIImage()
        screenShot2 = UIImage()
        GETscreenShot = UIImage()
        
        
        GetTAP = false
        AppDelegate.shared().WatchImageitem.removeAll()
        for subview in Img_1.subviews {
            subview.removeFromSuperview()
        }
        
        for subview in Img_2.subviews {
            subview.removeFromSuperview()
        }
        
        for subview in Img_3.subviews {
            subview.removeFromSuperview()
        }
    }
    
//MARK: - Methods(Functions)☝🏻😳

    func revealController(_ revealController: SWRevealViewController, didMoveTo position: FrontViewPosition) {
        if case .right = position {
            
            revealController.frontViewController.view.alpha = 0.5
            self.view.isUserInteractionEnabled = false
            self.revealViewController().frontViewController.view.addGestureRecognizer(self.revealViewController().panGestureRecognizer())
            self.revealViewController().frontViewController.view.addGestureRecognizer(self.revealViewController().tapGestureRecognizer())
        } else {
            
            revealController.frontViewController.view.alpha = 1
            self.revealViewController().tapGestureRecognizer()
            self.view.isUserInteractionEnabled = true
        }
    }
    
    @objc func handleTap(tap: UITapGestureRecognizer) {
        self.navigationItem.rightBarButtonItem?.isEnabled = true
        GetTAP = true
        let circle = UIView()
        circle.center = tap.location(in: Img_1)
        circle.frame.size = CGSize(width: 10, height: 10)
        circle.layer.backgroundColor = UIColor.green.cgColor
        circle.layer.cornerRadius = circle.frame.size.height/2
        Img_1.addSubview(circle)
        
        UIGraphicsBeginImageContext(Vw_1.bounds.size);
        Vw_1.layer.render(in: UIGraphicsGetCurrentContext()!)
        screenShot1 = UIGraphicsGetImageFromCurrentImageContext()!;
        UIGraphicsEndImageContext();
        
        //AppDelegate.shared().WatchImageitem.append(screenShot!)
    }
    
    @objc func handleTap2(tap: UITapGestureRecognizer) {
        self.navigationItem.rightBarButtonItem?.isEnabled = true
        GetTAP = true
        let circle = UIView()
        circle.center = tap.location(in: Img_2)
        circle.frame.size = CGSize(width: 10, height: 10)
        circle.layer.backgroundColor = UIColor.green.cgColor
        circle.layer.cornerRadius = circle.frame.size.height/2
        Img_2.addSubview(circle)
        
        UIGraphicsBeginImageContext(Vw_2.bounds.size);
        Vw_2.layer.render(in: UIGraphicsGetCurrentContext()!)
        screenShot2 = UIGraphicsGetImageFromCurrentImageContext()!;
        UIGraphicsEndImageContext();
        
        //AppDelegate.shared().WatchImageitem.append(screenShot!)
    }
    
    @objc func handleTap3(tap: UITapGestureRecognizer) {
        self.navigationItem.rightBarButtonItem?.isEnabled = true
        
        let circle = UIView()
        circle.center = tap.location(in: Img_3)
        circle.frame.size = CGSize(width: 10, height: 10)
        circle.layer.backgroundColor = UIColor.green.cgColor
        circle.layer.cornerRadius = circle.frame.size.height/2
        Img_3.addSubview(circle)
        
        UIGraphicsBeginImageContext(Vw_3.bounds.size);
        Vw_3.layer.render(in: UIGraphicsGetCurrentContext()!)
        GETscreenShot = UIGraphicsGetImageFromCurrentImageContext()!;
        UIGraphicsEndImageContext();
        
        CheckTAp = true
    }
}


