//
//  ProfileViewController.swift
//  Appraisal
//
//  Created by ADMIN on 3/28/18.
//  Copyright © 2018 empiereTech. All rights reserved.
//

import UIKit
import SWRevealViewController
import Alamofire
import SwiftyJSON
import AETextFieldValidator

class ProfileViewController: UIViewController,SWRevealViewControllerDelegate {

//MARK: - Outlets🤔
    
    //View
    @IBOutlet var Vw_Side: UIView!
    
    //TextFeild
    @IBOutlet var Txt_Username: AETextFieldValidator!
    @IBOutlet var Txt_LastName: AETextFieldValidator!
    @IBOutlet var Txt_Email: AETextFieldValidator!
    @IBOutlet var Txt_OldPass: UITextField!
    @IBOutlet var Txt_NewPass: UITextField!
    @IBOutlet var Txt_ConfirmPass: UITextField!
    
    
    //Button
    @IBOutlet var Btn_Update: UIButton!
    
    //Segment
    @IBOutlet var SegmentNoti: UISegmentedControl!
    
    
//MARK: - Declare Variables👇🏻😬
    
    
//MARK: - Happy Coding😊
    
    //viewDidLoad
    override func viewDidLoad() {
        super.viewDidLoad()

        //NavigationBar
        self.navigationController?.navigationBar.isHidden = false;
        self.title = "Edit Profile"
        
        navigationController?.navigationBar.barTintColor = UIColor.white
        navigationController?.navigationBar.titleTextAttributes = [NSAttributedStringKey.foregroundColor: UIColor(red: 18/255, green: 64/255, blue: 118/255, alpha: 1),NSAttributedStringKey.font: UIFont(name: "Montserrat-Medium", size: 17)!]
        self.navigationItem.setHidesBackButton(true, animated:true)
        
        let menuButton = UIButton(type: .system)
        menuButton.setImage(#imageLiteral(resourceName: "Menu").withRenderingMode(.alwaysOriginal), for: .normal)
        menuButton.frame = CGRect(x: 0, y: 0, width: 34, height: 34)
        navigationItem.leftBarButtonItem = UIBarButtonItem(customView: menuButton)
        navigationController?.navigationBar.tintColor = UIColor(red: 18/255, green: 64/255, blue: 118/255, alpha: 1)
        
        //SideBar
        if revealViewController() != nil
        {
            self.revealViewController().delegate = self
            menuButton.addTarget(revealViewController(), action: "revealToggle:", for: .touchUpInside)
            self.navigationController?.navigationBar.addGestureRecognizer(self.revealViewController().panGestureRecognizer())
            view.addGestureRecognizer(self.revealViewController().panGestureRecognizer())
        }
        
        //ButtonLayoutChanges
        Btn_Update.layer.cornerRadius = Btn_Update.frame.size.height/2.0
        
        setValue(textFeild: Txt_Username)
        setValue(textFeild: Txt_LastName)
        setValue(textFeild: Txt_Email)
        setValue(textFeild: Txt_OldPass)
        setValue(textFeild: Txt_NewPass)
        setValue(textFeild: Txt_ConfirmPass)
        
        SegmentNoti.layer.cornerRadius = 15.0
        SegmentNoti.layer.borderWidth = 1.0
        SegmentNoti.layer.borderColor = UIColor(red: 18/255, green: 64/255, blue: 118/255, alpha: 1).cgColor
        SegmentNoti.layer.masksToBounds = true
        
        
          if NetworkReachabilityManager()!.isReachable {
             CallMyMethod()
        } else {
         AppDelegate.shared().HideHUD()
            AppDelegate.shared().ShowAlert(title: Constants.ERROR_NO_INTERNET, msg: "Unable to connect Internet, Please try after some time.")
        }
        setupAlerts()
    }
    

//MARK: - Actions👊🏻😠
    
    //UpdateButtonPressed
    @IBAction func UpdateButtonPressed(_ sender: Any) {
        self.view.endEditing(true)
        if Txt_Username.validate() && Txt_LastName.validate() && Txt_Email.validate(){
            Btn_Update.transform = CGAffineTransform(scaleX: 0.9, y: 0.9)
            Btn_Update.isUserInteractionEnabled = false
            UIView.animate(withDuration: 2.0,
                           delay: 0,
                           usingSpringWithDamping: 0.2,
                           initialSpringVelocity: 6.0,
                           options: .allowUserInteraction,
                           animations: { [weak self] in
                            self?.Btn_Update.transform = .identity
                            
                            Timer.scheduledTimer(timeInterval: 0.7,
                                                 target: self!,
                                                 selector: #selector(ProfileViewController.update),
                                                 userInfo: nil,
                                                 repeats: false)
                },
                           completion: nil)
        }
        
        
        
    }
    
    //SegmentSelect
    @IBAction func SegmentSelect(_ sender: Any) {
        switch SegmentNoti.selectedSegmentIndex {
        case 0:
            break;
        case 1:
            break;
        default:
            break;
        }
    }
    
    
//MARK: - Methods(Functions)☝🏻😳
    
    func revealController(_ revealController: SWRevealViewController, didMoveTo position: FrontViewPosition) {
        if case .right = position {
            
            revealController.frontViewController.view.alpha = 0.5
            self.view.isUserInteractionEnabled = false
            self.revealViewController().frontViewController.view.addGestureRecognizer(self.revealViewController().panGestureRecognizer())
            self.revealViewController().frontViewController.view.addGestureRecognizer(self.revealViewController().tapGestureRecognizer())
        } else {
            
            revealController.frontViewController.view.alpha = 1
            self.revealViewController().tapGestureRecognizer()
            self.view.isUserInteractionEnabled = true
        }
    }
    
    // @objc selector expected for Timer
    @objc func update() {
        // do what should happen when timer triggers an event
        EditProfile()
        
    }
    
    
//MARK: - API🤞🏻🙄
    
    func   CallMyMethod()
    {
        AppDelegate.shared().ShowHUD()
        let url = URL(string: "\(Constants.API)\(Constants.Profile)")!
        print("Url printed \(url)")
        
        let manager = Alamofire.SessionManager.default
        manager.session.configuration.timeoutIntervalForRequest = 120
        
        let login = UserDefaults.standard.string(forKey: "LoginToken")
        let UserID = UserDefaults.standard.string(forKey: "UserID")
        
        let headers = ["Authorization": "Basic YWRtaW46QVBJQEZFREVSQUwhIyRXRUIk",
                       "X-FEDERAL-API-KEY": "kkcoswggwgwkkc8w4ok808o48kswc40c0www4wss",
                       "X-FEDERAL-LOGIN-TOKEN": login!,
                       "USER-ID": UserID!]
        
        manager.request(url, method: .get, parameters: nil, headers: headers).responseJSON() { response in
            switch (response.result) {
            case .success:
                //do json stuff
                let jsonResponse = response.result.value as! NSDictionary
                print("JsonResponse printed \(jsonResponse)")
                
                AppDelegate.shared().HideHUD()
                let myInt: Bool = jsonResponse["status"]! as! Bool
                
                if(myInt == true)
                {
                    
                    self.Txt_Email.text = (jsonResponse.value(forKeyPath: "data.email") as! String)
                    self.Txt_Username.text = (jsonResponse.value(forKeyPath: "data.first_name") as! String)
                    self.Txt_LastName.text = (jsonResponse.value(forKeyPath: "data.last_name") as! String)
                    print("Get Notification \(jsonResponse.value(forKeyPath: "data.notification") as! String)")
                    
                    let GetNotification = jsonResponse.value(forKeyPath: "data.notification") as! String
                }
                
                break
                
            case .failure(let error):
                AppDelegate.shared().HideHUD()
                if error._code == NSURLErrorTimedOut {
                    //HANDLE TIMEOUT HERE
                }
                print("\n\nAuth request failed with error:\n \(error)")
                break
            }
        }
    }
    
    func EditProfile()
    {
        let params : Parameters = ["first_name":Txt_Username.text!,"last_name":Txt_LastName.text!,"email":Txt_Email.text!,"notification":"1"]
        print("param is \(params)")
        let url = URL(string: "\(Constants.API)\(Constants.Profile)")!
        print("Url printed \(url)")
        
        let manager = Alamofire.SessionManager.default
        manager.session.configuration.timeoutIntervalForRequest = 120
        
        let login = UserDefaults.standard.string(forKey: "LoginToken")
        let UserID = UserDefaults.standard.string(forKey: "UserID")
        
        let headers = ["Authorization": "Basic YWRtaW46QVBJQEZFREVSQUwhIyRXRUIk",
                       "X-FEDERAL-API-KEY": "kkcoswggwgwkkc8w4ok808o48kswc40c0www4wss",
                       "X-FEDERAL-LOGIN-TOKEN": login!,
                       "USER-ID": UserID!]
        
        manager.request(url, method: .post, parameters: params, headers: headers).responseJSON() { response in
            switch (response.result) {
            case .success:
                //do json stuff
                let jsonResponse = response.result.value as! NSDictionary
                print("JsonResponse printed \(jsonResponse)")
                
                AppDelegate.shared().HideHUD()
                let myInt: Bool = jsonResponse["status"]! as! Bool
                
                AppDelegate.shared().ShowAlert(title: Constants.ERROR_HEADER, msg: jsonResponse.value(forKeyPath: "message") as! String)
                
                break
                
            case .failure(let error):
                AppDelegate.shared().HideHUD()
                if error._code == NSURLErrorTimedOut {
                    //HANDLE TIMEOUT HERE
                }
                print("\n\nAuth request failed with error:\n \(error)")
                break
            }
        }
    }
    
    func setupAlerts(){
        Txt_Email.addRegx(Constants.REGEX_EMAIL, withMsg: "Enter valid email.")
        Txt_Username.addRegx(Constants.REGEX_Note_USER_NAME, withMsg: "Enter valid firstname")
        Txt_LastName.addRegx(Constants.REGEX_Note_USER_NAME, withMsg: "Enter valid lastname")
    }

    
//MARK: - Methods(Functions)☝🏻😳
    func setValue(textFeild:UITextField){
        textFeild.layer.cornerRadius = 7.0
        textFeild.layer.backgroundColor = UIColor.white.cgColor
        textFeild.layer.masksToBounds = false
        textFeild.layer.shadowOffset = CGSize.zero
        textFeild.layer.shadowRadius = 3
        textFeild.layer.shadowOpacity = 0.2
        textFeild.leftView = UIView(frame: CGRect(x: 0, y: 0, width: 15, height: textFeild.frame.height))
        textFeild.leftViewMode = .always
        let DiamondAttributed = NSMutableAttributedString.init(string: textFeild.placeholder!)
        let Diamondterms:NSString = textFeild.placeholder! as NSString
        let Diamondrangech :NSRange = Diamondterms.range(of: Diamondterms as String)
        DiamondAttributed.addAttribute(NSAttributedStringKey.foregroundColor, value: UIColor.darkGray, range: Diamondrangech)
        textFeild.attributedPlaceholder = DiamondAttributed
    }
    
    func setValueButton(Button:UIButton){
        Button.layer.cornerRadius = 7.0
        Button.isUserInteractionEnabled = true
        Button.layer.masksToBounds = false
        Button.layer.shadowOffset = CGSize.zero
        Button.layer.shadowRadius = 3
        Button.layer.shadowOpacity = 0.2
        Button.titleEdgeInsets = UIEdgeInsets(top: 0.0, left: 15.0, bottom: 0.0, right: 0.0)
    }
    
}
