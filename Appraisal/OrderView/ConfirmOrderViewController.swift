//
//  ConfirmOrderViewController.swift
//  Appraisal
//
//  Created by ADMIN on 3/28/18.
//  Copyright © 2018 empiereTech. All rights reserved.
//

import UIKit
import SWRevealViewController

class ConfirmOrderViewController: UIViewController,SWRevealViewControllerDelegate {
//MARK: - Outlets🤔
    
    //View
    @IBOutlet var Vw_Confirm: UIView!
   // @IBOutlet weak var Vw_Confirm2: UIView!
 
    @IBOutlet var Vw_side: UIView!
    @IBOutlet var Vw_Scroll: UIScrollView!
    
    //Label
    @IBOutlet var Lbl_CutomerName: UILabel!
    @IBOutlet var Lbl_Orderfor: UILabel!
    @IBOutlet var Lbl_Dail: UILabel!
    @IBOutlet var Lbl_JewellryType: UILabel!
    @IBOutlet var Lbl_MatelType: UILabel!
    @IBOutlet var Lbl_CaratWeight: UILabel!
    @IBOutlet var Lbl_Estimated: UILabel!
    @IBOutlet var Lbl_Comments: UILabel!
    @IBOutlet var Lbl_Pictures: UILabel!
    @IBOutlet var Lbl_Color: UILabel!
    @IBOutlet var Lbl_CutShape: UILabel!
    @IBOutlet var Lbl_Clarity: UILabel!
    
    //LabelSide
    @IBOutlet weak var Lable_Side1: UILabel!
    @IBOutlet weak var Label_Side2: UILabel!
    @IBOutlet weak var Label_Side3: UILabel!
    @IBOutlet weak var Label_Side4: UILabel!
    @IBOutlet weak var Label_Side5: UILabel!
    @IBOutlet weak var Label_Side6: UILabel!
    @IBOutlet weak var Label_Side7: UILabel!
    @IBOutlet weak var Label_Side8: UILabel!
    @IBOutlet weak var Label_Side9: UILabel!
    @IBOutlet weak var Label_Side10: UILabel!
    @IBOutlet weak var Label_Side11: UILabel!
    @IBOutlet weak var Label_Side111: UILabel!
     @IBOutlet weak var Label_Side12: UILabel!
     @IBOutlet weak var Label_Side13: UILabel!
    @IBOutlet weak var Label_Side14: UILabel!
     @IBOutlet weak var Label_Side15: UILabel!
    @IBOutlet var lblmetal: UILabel!
    
    @IBOutlet var clarity: UILabel!
    @IBOutlet var color: UILabel!
    @IBOutlet var gemmaterial: UILabel!
    
    //Button
    @IBOutlet var Btn_Next: UIButton!
    
    @IBOutlet var dialheight: NSLayoutConstraint!
    @IBOutlet var lbl6top: NSLayoutConstraint!
    @IBOutlet var lbl6bottom: NSLayoutConstraint!
    @IBOutlet var lbl6height: NSLayoutConstraint!
    @IBOutlet var lbl7height: NSLayoutConstraint!
    
    @IBOutlet var lbl12height: NSLayoutConstraint!
     @IBOutlet var lbl13height: NSLayoutConstraint!
     @IBOutlet var lbl14height: NSLayoutConstraint!
     @IBOutlet var lbl15height: NSLayoutConstraint!
    
//MARK: - Declare Variables👇🏻😬
    var ISEngagementSide = Bool()
    var ISWebSide = Bool()
    var ISWatchSide = Bool()
    var ISJewlary = Bool()
    var selectedpart:NSMutableString = ""
//MARK: - Happy Coding😊
    
    //viewDidLoad
    override func viewDidLoad() {
        super.viewDidLoad()

        // Do any additional setup after loading the view.
        
        self.title = "Order Detail"
        
        //NavigationBar
        navigationController?.navigationBar.barTintColor = UIColor.white
        navigationController?.navigationBar.titleTextAttributes = [NSAttributedStringKey.foregroundColor: UIColor(red: 18/255, green: 64/255, blue: 118/255, alpha: 1),NSAttributedStringKey.font: UIFont(name: "Montserrat-Medium", size: 17)!]
        /*self.navigationItem.setHidesBackButton(true, animated:true);
        
        let menuButton = UIButton(type: .system)
        menuButton.setImage(#imageLiteral(resourceName: "Menu").withRenderingMode(.alwaysOriginal), for: .normal)
        menuButton.frame = CGRect(x: 0, y: 0, width: 34, height: 34)
        navigationItem.leftBarButtonItem = UIBarButtonItem(customView: menuButton)
        
        //SideBar
        if revealViewController() != nil
        {
            self.revealViewController().delegate = self
            menuButton.addTarget(revealViewController(), action: "revealToggle:", for: .touchUpInside)
            self.navigationController?.navigationBar.addGestureRecognizer(self.revealViewController().panGestureRecognizer())
            view.addGestureRecognizer(self.revealViewController().panGestureRecognizer())
        }*/
        
//        if (txtcardnumber.text?.isEmpty)!
//        {
//            allalert(title: "Alert", message:"Please Enter Card Number")
//        }
        
       
        
        Vw_Confirm.layer.cornerRadius = 10.0
        Vw_Confirm.isUserInteractionEnabled = true
        Vw_Confirm.layer.masksToBounds = false
        Vw_Confirm.layer.shadowOffset = CGSize.zero
        Vw_Confirm.layer.shadowRadius = 5
        Vw_Confirm.layer.shadowOpacity = 0.2
        
        if(AppDelegate.shared().GETData["dial"] != nil || AppDelegate.shared().GETData["dial"] != "")
        {
            Vw_Confirm.isHidden = false;
        }
        else
        {
            Vw_Confirm.isHidden = true;
        }
        
        Btn_Next.layer.cornerRadius = Btn_Next.frame.size.height/2
        
        Lbl_Dail.isHidden = true
        dialheight.constant = 0
        
       
        
        if (self.ISEngagementSide == true)
        {
            self.ISEngagementSide = false
            
           // AppDelegate.shared().GETData = ["CustomerName": Txt_CustomeName.text!, "CaratDiamondWeight": Txt_CaretWeight.text!, "EngagementOrLooseRing":EngagementOrLooseRing as String, "MetalType":ArrGoldIndex as String,"ShapeID":ShapeID as String,"ClarityID":ClarityArrIndex as String,"ColorID":ColorArrIndex as String,"MaterialTypeID":MaterialArr[MaterialArrIndex]["MaterialTypeID"] as! String, "CustomerComment": Txt_Comments.text!,"EstimatedValue":Txt_Estimated.text!, "ItemTypeID":"1"]
            
            Lbl_CutomerName.text = AppDelegate.shared().GETData["CustomerName"]
            
            Label_Side2.text = "Order for :"
            if (AppDelegate.shared().GETData["EngagementOrLooseRing"] == "1"){
                Lbl_Orderfor.text = "Engagement Rings"
                Label_Side12.isHidden = false
                clarity.isHidden = false
               
                Label_Side12.text = "Estimated value :"
                clarity.text = "$ \(AppDelegate.shared().GETData["EstimatedValue"]!)"
                 clarity.textColor = #colorLiteral(red: 0.001419739613, green: 0.5, blue: 0.002202855277, alpha: 1)
            }
            else{
                Lbl_Orderfor.text = "Loose Stones"
//                Label_Side12.isHidden = true
//                clarity.isHidden = true
                Label_Side12.text = "Estimated value :"
                               clarity.text = "$ \(AppDelegate.shared().GETData["EstimatedValue"]!)"
                                clarity.textColor = #colorLiteral(red: 0.001419739613, green: 0.5, blue: 0.002202855277, alpha: 1)
                // lbl12height.constant = 0
                lbl7height.constant = 0
            }
            
           
            
            Lbl_Dail.isHidden = true
            dialheight.constant = 0
            
            Label_Side3.text = "Metal type :"
            Lbl_JewellryType.text = AppDelegate.shared().GETData["MetalType"]
            
            
             Label_Side4.text = "Diamond Clarity :"
                                         Lbl_MatelType.text = "\(AppDelegate.shared().GETData["clearity_start"] ?? "") - \(AppDelegate.shared().GETData["clearity_end"] ?? "")"
                         
                          Label_Side5.text = "Diamond Color :"
                          
                          if (AppDelegate.shared().GETData["morecolor"] != "") {
                               Lbl_CaratWeight.text = "\(AppDelegate.shared().GETData["morecolor"] ?? "")"
                          }
                          else
                          {
                               Lbl_CaratWeight.text = "\(AppDelegate.shared().GETData["color_start"] ?? "") - \(AppDelegate.shared().GETData["color_end"] ?? "")"
                          }
            
            let EngagementOrLooseRing = AppDelegate.shared().GETData["EngagementOrLooseRing"] ?? ""
            
            if EngagementOrLooseRing == "1" {
                Label_Side6.text = "Center Stone carat weight :"
            }
            else
            {
              Label_Side6.text = "Total carat weight :"
            }
                                      Lbl_Estimated.text = AppDelegate.shared().GETData["CaratDiamondWeight"]
            
                          
                                       Label_Side7.text = "Setting Carat Weight:"
                                      Lbl_Comments.text = AppDelegate.shared().GETData["SettingCaratWeight"]
                          
                          Label_Side8.text = "Shape :"
                          Lbl_Pictures.text = AppDelegate.shared().GETData["ShapeName"]
                          
                          
                          Label_Side9.text = "Gem Material :"
                          Lbl_Color.text = AppDelegate.shared().GETData["MaterialType"]
                          
                          
                        
                          Label_Side10.text = "Comments :"
                          Lbl_CutShape.text = AppDelegate.shared().GETData["CustomerComment"]
                          
                          Label_Side11.text = "Pictures:"
                          Lbl_Clarity.text = "\(AppDelegate.shared().ImageItem.count) Files"
                          
                          Label_Side13.isHidden = true
                          color.isHidden = true
                          Label_Side14.isHidden = true
                          gemmaterial.isHidden = true
                      Label_Side15.isHidden = true
                                lblmetal.isHidden = true
                          
                          lbl13height.constant = 0
                          lbl14height.constant = 0
                          lbl15height.constant = 0
            
        }
        else if (self.ISJewlary == true)
        {
            self.ISJewlary = false
            
            Lbl_CutomerName.text = AppDelegate.shared().GETData["CustomerName"]
            
            Label_Side2.text = "Order for :"
            Lbl_Orderfor.text = "Jewelry"
            
            
            
           Lbl_Dail.isHidden = true
            dialheight.constant = 0
            
            
            Label_Side3.text = "Jewelry Type :"
            Lbl_JewellryType.text = AppDelegate.shared().GETData["JewlaryStyleTypeID"]
            
            Label_Side4.text = "Metal Type :"
            Lbl_MatelType.text = AppDelegate.shared().GETData["MetalType"]
            

            Label_Side5.text = "Weight of item:"
            Lbl_CaratWeight.text = AppDelegate.shared().GETData["weight_of_item"]
            
            
            Label_Side6.text = "Comments:"
            Lbl_Estimated.text = AppDelegate.shared().GETData["CustomerComment"]
            
            
            Label_Side7.text = "Pictures:"
            Lbl_Comments.text = "\(AppDelegate.shared().ImageItem.count) Files"

            
                        let a = "none"
                       let b = AppDelegate.shared().GETData["GemMaterial"] ?? ""
                       let result: ComparisonResult = a.compare(b, options: NSString.CompareOptions.caseInsensitive, range: nil, locale: nil)
                       if result == .orderedSame
                      {
                        
                        Label_Side8.text = "Gem Material :"
                        Lbl_Pictures.text = AppDelegate.shared().GETData["MaterialType"]
                        
                        Label_Side9.text = "Estimated Value:"
                        Lbl_Color.text = AppDelegate.shared().GETData["EstimatedValue"]
                        Lbl_Color.textColor = #colorLiteral(red: 0, green: 0.4980392157, blue: 0.003921568627, alpha: 1)

//                        Label_Side8.isHidden = true
//                        Lbl_Pictures.isHidden = true
//                       Label_Side9.isHidden = true
//                       Lbl_Color.isHidden = true
                       Label_Side10.isHidden = true
                       Lbl_CutShape.isHidden = true
                        Label_Side11.isHidden = true
                        Lbl_Clarity.isHidden = true
                        Label_Side12.isHidden = true
                        clarity.isHidden = true
                       }
            else
                       {
                        Label_Side8.text = "Carat Weight :"
                        Lbl_Pictures.text =  AppDelegate.shared().GETData["CaratDiamondWeight"]
                                   
                        Label_Side9.text = "Diamond Color:"
                        if (AppDelegate.shared().GETData["morecolor"] != "")
                        {
                            Lbl_Color.text = "\(AppDelegate.shared().GETData["morecolor"] ?? "")"
                        }
                        else
                        {
                            Lbl_Color.text = "\(AppDelegate.shared().GETData["jew_color_start"] ?? "") - \(AppDelegate.shared().GETData["jew_color_end"] ?? "")"
                        }
                       // Lbl_Color.text = "\(AppDelegate.shared().GETData["jew_color_start"] ?? "") - \(AppDelegate.shared().GETData["jew_color_end"] ?? "")"
                                   
                        Label_Side10.text = "Diamond Clarity:"
                        Lbl_CutShape.text =  "\(AppDelegate.shared().GETData["jew_clearity_start"] ?? "") - \(AppDelegate.shared().GETData["jew_clearity_end"] ?? "")"
            }
            
           Label_Side11.text = "Gem Material :"
            Lbl_Clarity.text = AppDelegate.shared().GETData["MaterialType"]
            
            Label_Side12.text = "Estimated Value:"
            clarity.text = "$ \(AppDelegate.shared().GETData["EstimatedValue"] ?? "")"
            clarity.textColor = #colorLiteral(red: 0, green: 0.4980392157, blue: 0.003921568627, alpha: 1)
            
            
           
            Label_Side13.isHidden = true
            color.isHidden = true
            Label_Side14.isHidden = true
            gemmaterial.isHidden = true
         Label_Side15.isHidden = true
            lblmetal.isHidden = true
            
            lbl13height.constant = 0
             lbl14height.constant = 0
             lbl15height.constant = 0
            
            

        }
        else if (self.ISWebSide == true)
        {
            self.ISWebSide = false;
            Lbl_CutomerName.text = AppDelegate.shared().GETData["CustomerName"]
            
            
            
           Label_Side2.text = "Paste Weblink :"
            Lbl_Orderfor.text = AppDelegate.shared().GETData["webURL"]
            
            if(AppDelegate.shared().GETData["dial"] != nil || AppDelegate.shared().GETData["dial"] != "")
            {
                Vw_Confirm.isHidden = false;
            }
            else
            {
                Vw_Confirm.isHidden = true;
            }
            
            Label_Side3.text = "Comments:"
            Lbl_JewellryType.text = AppDelegate.shared().GETData["CustomerComment"]
            
            
            if(AppDelegate.shared().GETData["dial"] != nil || AppDelegate.shared().GETData["dial"] != "")
            {
                Vw_Confirm.isHidden = false;
            }
            else
            {
                Vw_Confirm.isHidden = true;
            }
            
            
            Lbl_MatelType.isHidden = true
            Lbl_CaratWeight.isHidden = true
            Lbl_Estimated.isHidden = true
            Lbl_Comments.isHidden = true
            Lbl_Pictures.isHidden = true
            
            Label_Side4.isHidden = true
            Label_Side5.isHidden = true
            Label_Side6.isHidden = true
            Label_Side7.isHidden = true
            Label_Side8.isHidden = true
            
            Label_Side10.isHidden = true
            Lbl_CutShape.isHidden = true
            
            Label_Side11.isHidden = true
            Lbl_Clarity.isHidden = true
            
            Label_Side9.isHidden = true
            Lbl_Color.isHidden = true
            
            Label_Side12.isHidden = true
            clarity.isHidden = true
            Label_Side13.isHidden = true
            color.isHidden = true
            Label_Side14.isHidden = true
            gemmaterial.isHidden = true
            Label_Side15.isHidden = true
                      lblmetal.isHidden = true
            
        }
        else if (self.ISWatchSide == true)
        {
            self.ISWebSide = false;
            
            Lbl_CutomerName.text = AppDelegate.shared().GETData["CustomerName"]
            
            
           // AppDelegate.shared().GETData = ["CustomerName": Txt_CustomerName.text!, "CaratDiamondWeight": Txt_Diamond.text!, "BrandName": Txt_BandName.text!, "SerialNo": Txt_Serial.text!, "WindTypeID": WindID as String, "ModelName" : Txt_ModelName.text!, "caseSize" : Txt_Case.text!, "isAllPartOriginal" : SelectSeg as String, "MaterialTypeID" : MatelID as String, "EstimatedValue" : Txt_Estimated.text!, "CustomerComment": Txt_Comments.text!, "ItemTypeID":"2", "Material": Btn_Matel.titleLabel?.text as! String, "Wind": Btn_Wind.titleLabel?.text as! String]
            
            Label_Side2.text = "Order for :"
            Lbl_Orderfor.text = "Watches"
            
            
            Lbl_Dail.isHidden = false
            
            
            Label_Side111.text = AppDelegate.shared().GETData["dial"]
            Lbl_Dail.text = "Dial :"
            
            
            Label_Side3.text = "Brand Name :"
            Lbl_JewellryType.text = AppDelegate.shared().GETData["BrandName"]
            
           
            Label_Side4.text = "Model Name :"
            Lbl_MatelType.text = AppDelegate.shared().GETData["ModelName"]
            
            
            
            Label_Side5.text = "Wind Type :"
            Lbl_CaratWeight.text = AppDelegate.shared().GETData["Wind"]
           
            
            if AppDelegate.shared().GETData["CaratDiamondWeight"] != "" {
                Label_Side6.text = "Diamond Weight :"
                Lbl_Estimated.text = AppDelegate.shared().GETData["CaratDiamondWeight"]
            }
            else
            {
                Label_Side6.text = ""
                Lbl_Estimated.text = ""
                lbl6height.constant = 0
                lbl6top.constant = 0
                lbl6bottom.constant = 0
                self.view.layoutIfNeeded()
            }
            
            
            
//            Label_Side7.text = "Estimated value :"
//            Lbl_Comments.text = "$ \(AppDelegate.shared().GETData["EstimatedValue"]!)"
//            Lbl_Comments.textColor = #colorLiteral(red: 0.001419739613, green: 0.5, blue: 0.002202855277, alpha: 1)
            
            
            Label_Side7.text = "Serial :"
            Lbl_Comments.text = AppDelegate.shared().GETData["SerialNo"]
            
           
            
            
            Label_Side8.text = "Case Size :"
            Lbl_Pictures.text = AppDelegate.shared().GETData["caseSize"]
            
           
            
            
            Label_Side9.text = "Comments :"
            Lbl_Color.text = AppDelegate.shared().GETData["CustomerComment"]
            
            
            
            Label_Side10.text = "Pictures :"
            Lbl_CutShape.text = "\(AppDelegate.shared().ImageItem.count) Files"
            
            
            Label_Side11.text = "Material :"
            Lbl_Clarity.text = AppDelegate.shared().GETData["Material"] ?? ""
           
                       
            Label_Side12.text = "Gem Material :"
            clarity.text = AppDelegate.shared().GETData["GemMaterial"] ?? ""
            let a = "none"
            let b = AppDelegate.shared().GETData["GemMaterial"] ?? ""
            let result: ComparisonResult = a.compare(b, options: NSString.CompareOptions.caseInsensitive, range: nil, locale: nil)
            if result == .orderedSame
           {
            Label_Side13.text = "Estimated value :"
            color.text = "$ \(AppDelegate.shared().GETData["EstimatedValue"]!)"
            color.textColor = #colorLiteral(red: 0.001419739613, green: 0.5, blue: 0.002202855277, alpha: 1)
            
            Label_Side14.isHidden = true
            gemmaterial.isHidden = true
            Label_Side15.isHidden = true
            lblmetal.isHidden = true
            
            lbl14height.constant = 0
            lbl15height.constant = 0
            
                        
//            Lbl_Estimated.translatesAutoresizingMaskIntoConstraints = false
//            Lbl_Estimated.addConstraint(NSLayoutConstraint(item: Lbl_Estimated, attribute: .height, relatedBy: .equal, toItem: nil, attribute: .notAnAttribute, multiplier: 1, constant: 0))
//
//            NSLayoutConstraint(item: Lbl_Estimated, attribute: .height, relatedBy: .equal, toItem: nil, attribute: .notAnAttribute, multiplier: 1, constant: 0).isActive = true
          //  Lbl_Estimated.addConstraint(NSLayoutConstraint(item: Lbl_Estimated, attribute: .width, relatedBy: .equal, toItem: nil, attribute: .notAnAttribute, multiplier: 1, constant: 200))
            
//            var drawingHeightConstraint: NSLayoutConstraint?
//            drawingHeightConstraint = NSLayoutConstraint(item: Lbl_Estimated, attribute: .height, relatedBy: .equal, toItem: Lbl_Estimated, attribute: .height, multiplier: CGFloat(1), constant: 0)
//            drawingHeightConstraint!.isActive = true
          //  lbl6height =  Lbl_Estimated.heightAnchor.constraint(equalToConstant: 0)
            
            Label_Side6.text = ""
            Lbl_Estimated.text = ""
            lbl6height.constant = 0
            lbl6top.constant = 0
            lbl6bottom.constant = 0
            self.view.layoutIfNeeded()
            }
            else
            {
                                       
                Label_Side13.text = "Color :"
                if (AppDelegate.shared().GETData["morecolor"] != "")
                {
                    color.text = "\(AppDelegate.shared().GETData["morecolor"] ?? "")"
                }
                else
                {
                    color.text = "\(AppDelegate.shared().GETData["color_start"] ?? "") - \(AppDelegate.shared().GETData["color_end"] ?? "")"
                }
              //  clarity.text = "\(AppDelegate.shared().GETData["color_start"] ?? "") - \(AppDelegate.shared().GETData["color_end"] ?? "")"
                            
            
                            
                Label_Side14.text =   "Clarity :"
                gemmaterial.text = "\(AppDelegate.shared().GETData["clearity_start"] ?? "") - \(AppDelegate.shared().GETData["clearity_end"] ?? "")"
                            
                Label_Side15.text = "Estimated value :"
                lblmetal.text = "$ \(AppDelegate.shared().GETData["EstimatedValue"]!)"
                lblmetal.textColor = #colorLiteral(red: 0.001419739613, green: 0.5, blue: 0.002202855277, alpha: 1)
            }
            
        }
        print("Get Details \(AppDelegate.shared().GETData)")
    }

    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(true)
        Btn_Next.isUserInteractionEnabled = true
    }
    
    
//MARK: - Actions👊🏻😠
    
    //NextButtonPressed
    @IBAction func NextButtonPressed(_ sender: Any) {
        Btn_Next.transform = CGAffineTransform(scaleX: 0.9, y: 0.9)
        Btn_Next.isUserInteractionEnabled = false
        UIView.animate(withDuration: 2.0,
                       delay: 0,
                       usingSpringWithDamping: 0.2,
                       initialSpringVelocity: 6.0,
                       options: .allowUserInteraction,
                       animations: { [weak self] in
                        self?.Btn_Next.transform = .identity
                        
                        Timer.scheduledTimer(timeInterval: 0.7,
                                             target: self!,
                                             selector: #selector(ConfirmOrderViewController.update),
                                             userInfo: nil,
                                             repeats: false)
            },
                       completion: nil)
    }

    
//MARK: - Methods(Functions)☝🏻😳
    
    // @objc selector expected for Timer
    @objc func update() {
        // do what should happen when timer triggers an event
        let storyboard = UIStoryboard(name:"Main", bundle:nil)
        let home = storyboard.instantiateViewController(withIdentifier: "ShippingViewController")as! ShippingViewController
        home.selectedpart = selectedpart
        self.navigationController?.pushViewController(home, animated: true)
    }
    
    func revealController(_ revealController: SWRevealViewController, didMoveTo position: FrontViewPosition) {
        if case .right = position {
            
            revealController.frontViewController.view.alpha = 0.5
            self.view.isUserInteractionEnabled = false
            self.revealViewController().frontViewController.view.addGestureRecognizer(self.revealViewController().panGestureRecognizer())
            self.revealViewController().frontViewController.view.addGestureRecognizer(self.revealViewController().tapGestureRecognizer())
        } else {
            
            revealController.frontViewController.view.alpha = 1
            self.revealViewController().tapGestureRecognizer()
            self.view.isUserInteractionEnabled = true
        }
    }

    
    
}
