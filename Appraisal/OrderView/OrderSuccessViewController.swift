//
//  OrderSuccessViewController.swift
//  Appraisal
//
//  Created by ADMIN on 3/28/18.
//  Copyright © 2018 empiereTech. All rights reserved.
//

import UIKit
import SWRevealViewController

class OrderSuccessViewController: UIViewController,SWRevealViewControllerDelegate {

//MARK: - Outlets🤔
    
    //View
    @IBOutlet var Vw_Confirm: UIView!
    @IBOutlet var Vw_side: UIView!
    
    //Label
    @IBOutlet var Lbl_Back: UILabel!
    @IBOutlet var Lbl_Certificate: UILabel!
    @IBOutlet var Lbl_Paid: UILabel!
    @IBOutlet var Lbl_OrderType: UILabel!
    @IBOutlet var Lbl_ErrorMessage: UIButton!
    @IBOutlet weak var Lbl_Error: UILabel!
    
    //Button
    @IBOutlet var Btn_OK: UIButton!
    
    //ImageView
    @IBOutlet var Img_Status: UIImageView!
    
    
//MARK: - Declare Variables👇🏻😬
    var Success = Bool()
    var PaidValue = String()
    var OrderType = String()
    var Certificate = String()
    var errormessage = String()
    var error = String()
    var imgstatus = UIImage()
    
  
   
//MARK: - Happy Coding😊
    
    //viewDidLoad
    override func viewDidLoad() {
        super.viewDidLoad()
        
        // Do any additional setup after loading the view.
        
        self.title = "Order Detail"
        
        
        
        //NavigationBar
        navigationController?.navigationBar.barTintColor = UIColor.white
        navigationController?.navigationBar.titleTextAttributes = [NSAttributedStringKey.foregroundColor: UIColor(red: 18/255, green: 64/255, blue: 118/255, alpha: 1),NSAttributedStringKey.font: UIFont(name: "Montserrat-Medium", size: 17)!]
        self.navigationItem.setHidesBackButton(true, animated:true);
        /// crash here
        PaidValue = (UserDefaults.standard.string(forKey: "Paid") as String!)
        OrderType = (UserDefaults.standard.string(forKey: "Order") as! String)
        Certificate = (UserDefaults.standard.string(forKey: "Certificate") as! String)
       
       
        Lbl_Paid.text = PaidValue
        Lbl_Certificate.text = Certificate
        Lbl_OrderType.text = OrderType
      
        
        Vw_Confirm.layer.cornerRadius = 10.0
        Vw_Confirm.isUserInteractionEnabled = true
        Vw_Confirm.layer.masksToBounds = false
        Vw_Confirm.layer.shadowOffset = CGSize.zero
        Vw_Confirm.layer.shadowRadius = 5
        Vw_Confirm.layer.shadowOpacity = 0.2
        
        let path = UIBezierPath(roundedRect:Lbl_Back.bounds,
                                byRoundingCorners:[.topRight, .topLeft],
                                cornerRadii: CGSize(width: 10, height:  10))
        
        let maskLayer = CAShapeLayer()
        
        maskLayer.path = path.cgPath
        Lbl_Back.layer.mask = maskLayer
        let status = UserDefaults.standard.string(forKey: "status")
        
        if (status == "1") {
            Lbl_Back.backgroundColor = UIColor.green
            Img_Status.image = UIImage(named: "OrderSuccess")!
            Lbl_ErrorMessage.isHidden = true
            Lbl_Error.isHidden = true
            Vw_Confirm.frame = CGRect(x: Vw_Confirm.frame.origin.x, y: Vw_Confirm.frame.origin.y, width: Vw_Confirm.frame.size.width, height: Vw_Confirm.frame.size.height - 40)
        }
        else{
            Lbl_Back.backgroundColor = UIColor.red
            Img_Status.image = UIImage(named: "OrderFaild")!
            Lbl_ErrorMessage.isHidden = false
            Lbl_Error.isHidden = false
            Vw_Confirm.frame = CGRect(x: Vw_Confirm.frame.origin.x, y: Vw_Confirm.frame.origin.y, width: Vw_Confirm.frame.size.width, height: Vw_Confirm.frame.size.height + 40)
        }

        Btn_OK.layer.cornerRadius = Btn_OK.frame.size.height/2
    }
    
    
//MARK: - Actions👊🏻😠
    
    //NextButtonPressed
    @IBAction func OKButtonPressed(_ sender: Any) {
        Btn_OK.transform = CGAffineTransform(scaleX: 0.9, y: 0.9)
        Btn_OK.isUserInteractionEnabled = false
        UIView.animate(withDuration: 2.0,
                       delay: 0,
                       usingSpringWithDamping: 0.2,
                       initialSpringVelocity: 6.0,
                       options: .allowUserInteraction,
                       animations: { [weak self] in
                        self?.Btn_OK.transform = .identity
                        
                        Timer.scheduledTimer(timeInterval: 0.7,
                                             target: self!,
                                             selector: #selector(OrderSuccessViewController.update),
                                             userInfo: nil,
                                             repeats: false)
            },
                       completion: nil)
    }
    
    @IBAction func ErrorMessagePressed(_ sender: Any) {
        
    }
    
    
//MARK: - Methods(Functions)☝🏻😳
    
    // @objc selector expected for Timer
    @objc func update() {
        // do what should happen when timer triggers an event
        let storyboard = UIStoryboard(name:"Main", bundle:nil)
        let home = storyboard.instantiateViewController(withIdentifier: "HomeViewController")as! HomeViewController
        self.navigationController?.pushViewController(home, animated: true)
    }
    
    func revealController(_ revealController: SWRevealViewController, didMoveTo position: FrontViewPosition) {
        if case .right = position {
            
            revealController.frontViewController.view.alpha = 0.5
            self.view.isUserInteractionEnabled = false
            self.revealViewController().frontViewController.view.addGestureRecognizer(self.revealViewController().panGestureRecognizer())
            self.revealViewController().frontViewController.view.addGestureRecognizer(self.revealViewController().tapGestureRecognizer())
        } else {
            
            revealController.frontViewController.view.alpha = 1
            self.revealViewController().tapGestureRecognizer()
            self.view.isUserInteractionEnabled = true
        }
    }

}
