//
//  ShippingViewController.swift
//  Appraisal
//
//  Created by ADMIN on 3/28/18.
//  Copyright © 2018 empiereTech. All rights reserved.
//

import UIKit
import SWRevealViewController
import BEMCheckBox
import Alamofire
import SwiftyJSON
import Stripe

class ShippingViewController: UIViewController,SWRevealViewControllerDelegate,BEMCheckBoxDelegate{
    
    
    
    
    // MARK: - rest fields
    var environment = String()
    var selectedpart:NSMutableString = ""
    
    //  var payPalConfig = PayPalConfiguration() // default
    
    //View
    @IBOutlet var Vw_Shipping: UIView!
    
    //Button
    @IBOutlet var Btn_PayNow: UIButton!
    
    //CheckBox
    @IBOutlet var Check_ShipOnline: BEMCheckBox!
    @IBOutlet var Check_LocalOnline: BEMCheckBox!
    @IBOutlet var Check_LocalPerson: BEMCheckBox!
    @IBOutlet var Check_ShipOvernight: BEMCheckBox!
    
    //Label
    @IBOutlet var Lbl_ShippingCharge: UILabel!
    @IBOutlet weak var Lbl_ShipAppraisal: UILabel!
    @IBOutlet weak var lbl_LocalPickOnline: UILabel!
    @IBOutlet weak var Lbl_CashinPerson: UILabel!
    @IBOutlet var Lbl_ShipOverCharge: UILabel!
    @IBOutlet var Lbl_ShipOvernight: UILabel!
    @IBOutlet var Lbl_ShippingPrice: UILabel!
    @IBOutlet weak var Lbl_AppraisalCost: UILabel!
    @IBOutlet weak var Lbl_FinalAmount: UILabel!
    
    
    var ArrGetAll = [AnyObject]()
    var ArrGetID = [AnyObject]()
    var ArrGetPrice = [AnyObject]()
    var image = UIImage()
    var imageData = NSData()
    var AppraisalCost = [[String:AnyObject]]()
    var OrderID = NSString()
    var Certificate_No = NSString()
    
    //MARK: - Happy Coding😊
    
    //viewDidLoad
    override func viewDidLoad() {
        super.viewDidLoad()
        
        // Do any additional setup after loading the view.
        
        self.title = "Shipping / Payment"
        
        //NavigationBar
        navigationController?.navigationBar.barTintColor = UIColor.white
        navigationController?.navigationBar.titleTextAttributes = [NSAttributedStringKey.foregroundColor: UIColor(red: 18/255, green: 64/255, blue: 118/255, alpha: 1),NSAttributedStringKey.font: UIFont(name: "Montserrat-Medium", size: 17)!]
        
        Vw_Shipping.layer.cornerRadius = 10.0
        Vw_Shipping.isUserInteractionEnabled = true
        Vw_Shipping.layer.masksToBounds = false
        Vw_Shipping.layer.shadowOffset = CGSize.zero
        Vw_Shipping.layer.shadowRadius = 5
        Vw_Shipping.layer.shadowOpacity = 0.2
        
        Btn_PayNow.layer.cornerRadius = Btn_PayNow.frame.size.height/2.0
        
        let group = BEMCheckBoxGroup(checkBoxes: [Check_ShipOnline, Check_LocalOnline, Check_LocalPerson, Check_ShipOvernight])
        group.selectedCheckBox = Check_ShipOnline
        group.mustHaveSelection = true
        
      
        if NetworkReachabilityManager()!.isReachable {
            CallMyMethod()
        } else {
            AppDelegate.shared().HideHUD()
            AppDelegate.shared().ShowAlert(title: Constants.ERROR_NO_INTERNET, msg: "Unable to connect Internet, Please try after some time.")
        }
    }
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(true)
        Btn_PayNow.isUserInteractionEnabled = true
        
        //  PayPalMobile.preconnect(withEnvironment: environment)
    }
    
    
    //MARK: - Actions👊🏻😠
    
    //NextButtonPressed
    @IBAction func PayNowButtonPressed(_ sender: Any) {
        
        Btn_PayNow.transform = CGAffineTransform(scaleX: 0.9, y: 0.9)
      //  Btn_PayNow.isUserInteractionEnabled = false
        UIView.animate(withDuration: 2.0,
                       delay: 0,
                       usingSpringWithDamping: 0.2,
                       initialSpringVelocity: 6.0,
                       options: .allowUserInteraction,
                       animations: { [weak self] in
                        self?.Btn_PayNow.transform = .identity
                        
                        Timer.scheduledTimer(timeInterval: 0.7,
                                             target: self!,
                                             selector: #selector(ShippingViewController.update),
                                             userInfo: nil,
                                             repeats: false)
            },
                       completion: nil)
    }
    
    
    //MARK: - Methods(Functions)☝🏻😳
    
    // @objc selector expected for Timer
    @objc func update() {
        // do what should happen when timer triggers an event
        PaymentShipping()
        //
    }
    
    func revealController(_ revealController: SWRevealViewController, didMoveTo position: FrontViewPosition) {
        if case .right = position {
            
            revealController.frontViewController.view.alpha = 0.5
            self.view.isUserInteractionEnabled = false
            self.revealViewController().frontViewController.view.addGestureRecognizer(self.revealViewController().panGestureRecognizer())
            self.revealViewController().frontViewController.view.addGestureRecognizer(self.revealViewController().tapGestureRecognizer())
        } else {
            
            revealController.frontViewController.view.alpha = 1
            self.revealViewController().tapGestureRecognizer()
            self.view.isUserInteractionEnabled = true
        }
    }
    
    func didTap(_ checkBox: BEMCheckBox) {
        if (checkBox==Check_ShipOnline)
        {
            if (checkBox.on)
            {
                // Add a new enter code herekey with a value
                AppDelegate.shared().GETData["ShippingTypeID"] = self.ArrGetID[0] as? String
                Lbl_ShippingPrice.text = "$ \(self.ArrGetPrice[0])"
                
                self.Lbl_FinalAmount.text = String(format: "$ %.2f", (self.AppraisalCost[0]["AppraisalCost"]! as! NSString).floatValue + (self.ArrGetPrice[0] as! NSString).floatValue)
                Btn_PayNow.setTitle("Pay Now", for: UIControlState.normal)
            }
        }
        else if (checkBox==Check_LocalOnline){
            if (checkBox.on)
            {
                AppDelegate.shared().GETData["ShippingTypeID"] = self.ArrGetID[2] as? String
                Lbl_ShippingPrice.text = "$ \(self.ArrGetPrice[2])"
                self.Lbl_FinalAmount.text = String(format: "$ %.2f", (self.AppraisalCost[0]["AppraisalCost"]! as! NSString).floatValue + (self.ArrGetPrice[2] as! NSString).floatValue)
                Btn_PayNow.setTitle("Pay Now", for: UIControlState.normal)
            }
        }
        else if (checkBox==Check_LocalPerson){
//            if (checkBox.on)
//            {
//                AppDelegate.shared().GETData["ShippingTypeID"] = self.ArrGetID[2] as? String
//                Lbl_ShippingPrice.text = "$ \(self.ArrGetPrice[2])"
//                self.Lbl_FinalAmount.text = String(format: "$ %.2f", (self.AppraisalCost[0]["AppraisalCost"]! as! NSString).floatValue + (self.ArrGetPrice[2] as! NSString).floatValue)
//                Btn_PayNow.setTitle("Finish", for: UIControlState.normal)
//
//            }
        }
        else{
//            if (checkBox.on)
//            {
//                AppDelegate.shared().GETData["ShippingTypeID"] = self.ArrGetID[3] as? String
//                Lbl_ShippingPrice.text = "$ \(self.ArrGetPrice[3])"
//                self.Lbl_FinalAmount.text = String(format: "$ %.2f", (self.AppraisalCost[0]["AppraisalCost"]! as! NSString).floatValue + (self.ArrGetPrice[3] as! NSString).floatValue)
//                Btn_PayNow.setTitle("Pay Now", for: UIControlState.normal)
//
//            }
            
            if (checkBox.on)
            {
                AppDelegate.shared().GETData["ShippingTypeID"] = self.ArrGetID[1] as? String
                Lbl_ShippingPrice.text = "$ \(self.ArrGetPrice[1])"
                self.Lbl_FinalAmount.text = String(format: "$ %.2f", (self.AppraisalCost[0]["AppraisalCost"]! as! NSString).floatValue + (self.ArrGetPrice[1] as! NSString).floatValue)
                Btn_PayNow.setTitle("Finish", for: UIControlState.normal)
                
            }
        }
    }
    
    //MARK: - API🤞🏻🙄
    
    func  CallMyMethod()
    {
        AppDelegate.shared().ShowHUD()
        let url = URL(string: "\(Constants.API)\(Constants.GetShippingMethod)")!
        print("Url printed \(url)")
        
        let manager = Alamofire.SessionManager.default
        manager.session.configuration.timeoutIntervalForRequest = 120
        
        let login = UserDefaults.standard.string(forKey: "LoginToken")
        let UserID = UserDefaults.standard.string(forKey: "UserID")
        
        let headers = ["Authorization": "Basic YWRtaW46QVBJQEZFREVSQUwhIyRXRUIk",
                       "X-FEDERAL-API-KEY": "kkcoswggwgwkkc8w4ok808o48kswc40c0www4wss",
                       "X-FEDERAL-LOGIN-TOKEN": login!,
                       "USER-ID": UserID!]
        
        manager.request(url, method: .get, parameters: nil, headers: headers).responseJSON() { response in
            switch (response.result) {
            case .success:
                //do json stuff
                let jsonResponse = response.result.value as! NSDictionary
                print("JsonResponse printed \(jsonResponse)")
                
                AppDelegate.shared().HideHUD()
                let myInt: Bool = jsonResponse["status"]! as! Bool
                
                if(myInt == true)
                {
                    //AppDelegate.shared().GETData = ["CustomerName": Txt_Customer.text!]
                    self.ArrGetAll = jsonResponse.value(forKeyPath: "data.ShippingMethod") as! [AnyObject]
                    self.ArrGetID = jsonResponse.value(forKeyPath: "data.ShippingMethodID") as! [AnyObject]
                    self.ArrGetPrice = jsonResponse.value(forKeyPath: "data.ShippingPrice") as! [AnyObject]
                    AppDelegate.shared().GETData["ShippingTypeID"] = self.ArrGetID[0] as? String
                    
                    self.Lbl_ShippingPrice.text = "$ \(self.ArrGetPrice[0])"
                    
                    
                    self.Lbl_ShipAppraisal.text = self.ArrGetAll[0] as? String
                    self.Lbl_ShippingCharge.attributedText = self.GetMutable(StringMutable: "Shipping charges $ \(self.ArrGetPrice[0]) additional in order during payment." as NSString, stringGettext: "$ \(self.ArrGetPrice[0])" as NSString)
                    
                    self.Lbl_ShipOvernight.text = self.ArrGetAll[1] as? String
                    self.Lbl_ShipOverCharge.attributedText = self.GetMutable(StringMutable: "Shipping charges $ \(self.ArrGetPrice[1]) additional in order during payment." as NSString, stringGettext: "$ \(self.ArrGetPrice[1])" as NSString)
                    
                    self.lbl_LocalPickOnline.text = self.ArrGetAll[2] as? String
                    
                    self.Lbl_CashinPerson.attributedText = self.GetMutable(StringMutable: "Shipping charges $ \(self.ArrGetPrice[2]) additional in order during payment." as NSString, stringGettext: "$ \(self.ArrGetPrice[2])" as NSString)
                    
                    
                    
                    
                    var dict = AppDelegate.shared().ArrGetAllType[0]
                    
                    self.AppraisalCost = dict["ItemType"]! as! [[String:AnyObject]]
                    
                    if (AppDelegate.shared().GETData["ItemTypeID"] == "1")
                    {
                        self.Lbl_AppraisalCost.text = "$ \(self.AppraisalCost[0]["AppraisalCost"]! as! NSString)"
                        self.Lbl_FinalAmount.text = String(format: "$ %.2f", (self.AppraisalCost[0]["AppraisalCost"]! as! NSString).floatValue + (self.ArrGetPrice[0] as! NSString).floatValue)
                    }
                    if (AppDelegate.shared().GETData["ItemTypeID"] == "2")
                    {
                        self.Lbl_AppraisalCost.text = "$ \(self.AppraisalCost[1]["AppraisalCost"]! as! NSString)"
                        self.Lbl_FinalAmount.text = String(format: "$ %.2f", (self.AppraisalCost[1]["AppraisalCost"]! as! NSString).floatValue + (self.ArrGetPrice[0] as! NSString).floatValue)
                    }
                    if (AppDelegate.shared().GETData["ItemTypeID"] == "3")
                    {
                        self.Lbl_AppraisalCost.text = "$ \(self.AppraisalCost[2]["AppraisalCost"]! as! NSString)"
                        self.Lbl_FinalAmount.text = String(format: "$ %.2f", (self.AppraisalCost[2]["AppraisalCost"]! as! NSString).floatValue + (self.ArrGetPrice[0] as! NSString).floatValue)
                    }
                    if (AppDelegate.shared().GETData["ItemTypeID"] == "4")
                    {
                        self.Lbl_AppraisalCost.text = "$ \(self.AppraisalCost[3]["AppraisalCost"]! as! NSString)"
                        self.Lbl_FinalAmount.text = String(format: "$ %.2f", (self.AppraisalCost[3]["AppraisalCost"]! as! NSString).floatValue + (self.ArrGetPrice[0] as! NSString).floatValue)
                    }
                }
                
                break
                
            case .failure(let error):
                AppDelegate.shared().HideHUD()
                if error._code == NSURLErrorTimedOut {
                    //HANDLE TIMEOUT HERE
                    AppDelegate.shared().ShowAlert(title: Constants.ERROR_HEADER, msg: error.localizedDescription)
                }
                print("\n\nAuth request failed with error:\n \(error)")
                break
            }
        }
    }
    
    
    
    func GetMutable(StringMutable:NSString, stringGettext:NSString)->NSMutableAttributedString
    {
        let CustomerAttributed = NSMutableAttributedString.init(string: StringMutable as String)
        let Customerterms:NSString = StringMutable
        let Customerrange :NSRange = Customerterms.range(of: stringGettext as String)
        CustomerAttributed.addAttribute(NSAttributedStringKey.foregroundColor, value: UIColor.init(red: 0/255.0, green: 126/255.0, blue: 213/255.0, alpha: 1), range: Customerrange)
        //self.Lbl_ShippingCharge.attributedText = CustomerAttributed
        
        return CustomerAttributed
    }
    
    func PaymentShipping()
    {
        
        var params = [String:String]() //Array of dictionary
        
        if (AppDelegate.shared().GETData["ItemTypeID"] == "1")
        {
            if AppDelegate.shared().GETData["MaterialType"]! == ""{
                params  = ["ItemTypeID":AppDelegate.shared().GETData["ItemTypeID"]!,
                           "CustomerName":AppDelegate.shared().GETData["CustomerName"]!,
                           "CaratDiamondWeight":AppDelegate.shared().GETData["CaratDiamondWeight"]!,
                           "ShapeID":AppDelegate.shared().GETData["ShapeID"]!,
//                     "ClarityID":AppDelegate.shared().GETData["ClarityID"]!,
//                     "ColorID":AppDelegate.shared().GETData["ColorID"]!,
                           "MaterialTypeID":AppDelegate.shared().GETData["MaterialTypeID"]!,
                           "EstimatedValue":AppDelegate.shared().GETData["EstimatedValue"]!,
                           "ShippingTypeID":AppDelegate.shared().GETData["ShippingTypeID"]!,
                           "CustomerComment":AppDelegate.shared().GETData["CustomerComment"]!,
                           "EngagementOrLooseRing":AppDelegate.shared().GETData["EngagementOrLooseRing"]!,
                           "MetalType":AppDelegate.shared().GETData["MetalType"]!,
                           "SettingCaratWeight":AppDelegate.shared().GETData["SettingCaratWeight"]!,
                     
                           "clearity_start":AppDelegate.shared().GETData["clearity_start_id"]!,
                           "clearity_end":AppDelegate.shared().GETData["clearity_end_id"]!,
                           "color_start":AppDelegate.shared().GETData["color_start_id"]!,
                           "color_end":AppDelegate.shared().GETData["color_end_id"]!,
                           "color":AppDelegate.shared().GETData["morecolor"]!
                ]
            }
            else{
                params  = ["ItemTypeID":AppDelegate.shared().GETData["ItemTypeID"]!,
                           "CustomerName":AppDelegate.shared().GETData["CustomerName"]!,
                           "CaratDiamondWeight":AppDelegate.shared().GETData["CaratDiamondWeight"]!,
                           "ShapeID":AppDelegate.shared().GETData["ShapeID"]!,
//                           "ClarityID":AppDelegate.shared().GETData["ClarityID"]!,
//                           "ColorID":AppDelegate.shared().GETData["ColorID"]!,
                           "MaterialTypeID":AppDelegate.shared().GETData["MaterialTypeID"]!,
                           "MaterialType":AppDelegate.shared().GETData["MaterialType"]!,
                           "EstimatedValue":AppDelegate.shared().GETData["EstimatedValue"]!,
                           "ShippingTypeID":AppDelegate.shared().GETData["ShippingTypeID"]!,
                           "CustomerComment":AppDelegate.shared().GETData["CustomerComment"]!,
                           "EngagementOrLooseRing":AppDelegate.shared().GETData["EngagementOrLooseRing"]!,
                           "MetalType":AppDelegate.shared().GETData["MetalType"]!,
                            "SettingCaratWeight":AppDelegate.shared().GETData["SettingCaratWeight"]!,
                            
                           "clearity_start":AppDelegate.shared().GETData["clearity_start_id"]!,
                           "clearity_end":AppDelegate.shared().GETData["clearity_end_id"]!,
                           "color_start":AppDelegate.shared().GETData["color_start_id"]!,
                           "color_end":AppDelegate.shared().GETData["color_end_id"]!,
                            "color":AppDelegate.shared().GETData["morecolor"]!
                ]
            }
            
            
        }
        else if (AppDelegate.shared().GETData["ItemTypeID"] == "2"){
            
            if AppDelegate.shared().GETData["MaterialType"]! == ""{
                params  = ["ItemTypeID":AppDelegate.shared().GETData["ItemTypeID"]!,"CustomerName":AppDelegate.shared().GETData["CustomerName"]!,"CaratDiamondWeight":AppDelegate.shared().GETData["CaratDiamondWeight"]!,"BrandName":AppDelegate.shared().GETData["BrandName"]!,"SerialNo":AppDelegate.shared().GETData["SerialNo"]!,
                           "WindTypeID":AppDelegate.shared().GETData["WindTypeID"]!,
                           "ModelName":AppDelegate.shared().GETData["ModelName"]!,
                           "caseSize":AppDelegate.shared().GETData["caseSize"]!,
                           "isAllPartOriginal":AppDelegate.shared().GETData["isAllPartOriginal"]!,
                           "MaterialTypeID":AppDelegate.shared().GETData["MaterialTypeID"]!,
                           "EstimatedValue":AppDelegate.shared().GETData["EstimatedValue"]!,
                           "ShippingTypeID":AppDelegate.shared().GETData["ShippingTypeID"]!,
                           "CustomerComment":AppDelegate.shared().GETData["CustomerComment"]!,
                           "dial":AppDelegate.shared().GETData["dial"]!,
                           "WatchMaterialTypeID":AppDelegate.shared().GETData["WatchMaterialTypeID"]!,
                           "selected_part":selectedpart,
                           "watch_clearity_start":AppDelegate.shared().GETData["clearity_start_id"]!,
                           "watch_clearity_end":AppDelegate.shared().GETData["clearity_end_id"]!,
                           "watch_color_start":AppDelegate.shared().GETData["color_start_id"]!,
                           "watch_color_end":AppDelegate.shared().GETData["color_end_id"]!,
                            "color":AppDelegate.shared().GETData["morecolor"]!,
                            "GemMaterialOther":AppDelegate.shared().GETData["GemMaterial"]!
                    ] as! [String : String]
            }
            else{
                
                params  = ["ItemTypeID":AppDelegate.shared().GETData["ItemTypeID"]!,
                           "CustomerName":AppDelegate.shared().GETData["CustomerName"]!,
                           "CaratDiamondWeight":AppDelegate.shared().GETData["CaratDiamondWeight"]!,
                           "BrandName":AppDelegate.shared().GETData["BrandName"]!,
                           "SerialNo":AppDelegate.shared().GETData["SerialNo"]!,
                           "WindTypeID":AppDelegate.shared().GETData["WindTypeID"]!,
                           "ModelName":AppDelegate.shared().GETData["ModelName"]!,
                           "caseSize":AppDelegate.shared().GETData["caseSize"]!,
                           "isAllPartOriginal":AppDelegate.shared().GETData["isAllPartOriginal"]!,
                           "MaterialTypeID":AppDelegate.shared().GETData["MaterialTypeID"]!,
                           "metal_other":AppDelegate.shared().GETData["MaterialType"]!,
                           "EstimatedValue":AppDelegate.shared().GETData["EstimatedValue"]!,
                           "ShippingTypeID":AppDelegate.shared().GETData["ShippingTypeID"]!,
                           "CustomerComment":AppDelegate.shared().GETData["CustomerComment"]!,
                           "dial":AppDelegate.shared().GETData["dial"]!,"selected_part":selectedpart,
                           "WatchMaterialTypeID":AppDelegate.shared().GETData["WatchMaterialTypeID"]!,
                      "watch_clearity_start":AppDelegate.shared().GETData["clearity_start_id"]!,
                       "watch_clearity_end":AppDelegate.shared().GETData["clearity_end_id"]!,
                       "watch_color_start":AppDelegate.shared().GETData["color_start_id"]!,
                       "watch_color_end":AppDelegate.shared().GETData["color_end_id"]!,
                        "color":AppDelegate.shared().GETData["morecolor"]!,
                        "GemMaterialOther":AppDelegate.shared().GETData["GemMaterial"]!
                    ] as! [String : String]
                /*
                 params  = ["ItemTypeID":AppDelegate.shared().GETData["ItemTypeID"]!,"CustomerName":AppDelegate.shared().GETData["CustomerName"]!,"CaratDiamondWeight":AppDelegate.shared().GETData["CaratDiamondWeight"]!,"ShapeID":AppDelegate.shared().GETData["ShapeID"]!,"ClarityID":AppDelegate.shared().GETData["ClarityID"]!,"ColorID":AppDelegate.shared().GETData["ColorID"]!,"MaterialTypeID":AppDelegate.shared().GETData["MaterialTypeID"]!,"MaterialType":AppDelegate.shared().GETData["MaterialType"]!,"EstimatedValue":AppDelegate.shared().GETData["EstimatedValue"]!,"ShippingTypeID":AppDelegate.shared().GETData["ShippingTypeID"]!,"CustomerComment":AppDelegate.shared().GETData["CustomerComment"]!,"EngagementOrLooseRing":AppDelegate.shared().GETData["EngagementOrLooseRing"]!,"MetalType":AppDelegate.shared().GETData["MetalType"]!]*/
            }
            print ("watcch params : " ,params)

        }
        else if (AppDelegate.shared().GETData["ItemTypeID"] == "3"){
            
            //MaterialType
            
            if AppDelegate.shared().GETData["MaterialType"]! == ""{
                params  = ["ItemTypeID":AppDelegate.shared().GETData["ItemTypeID"]!,
                           "CustomerName":AppDelegate.shared().GETData["CustomerName"]!,
                           "CaratDiamondWeight":AppDelegate.shared().GETData["CaratDiamondWeight"]!,
                           "JewlaryStyleTypeID":AppDelegate.shared().GETData["JewlaryStyleTypeID"]!,
                           "MetalType":AppDelegate.shared().GETData["MetalType"]!,
                           "MaterialTypeID":AppDelegate.shared().GETData["MaterialTypeID"]!,
                           "EstimatedValue":AppDelegate.shared().GETData["EstimatedValue"]!,
                           "ShippingTypeID":AppDelegate.shared().GETData["ShippingTypeID"]!,
                           "CustomerComment":AppDelegate.shared().GETData["CustomerComment"]!,
                           "weight_of_item":AppDelegate.shared().GETData["weight_of_item"]!,
                    "jew_clearity_start":AppDelegate.shared().GETData["jew_clearity_start_id"]!,
                    "jew_clearity_end":AppDelegate.shared().GETData["jew_clearity_end_id"]!,
                    "jew_color_start":AppDelegate.shared().GETData["jew_color_start_id"]!,
                    "jew_color_end":AppDelegate.shared().GETData["jew_color_end_id"]!,
                     "color":AppDelegate.shared().GETData["morecolor"]!
                ]
            }
            else{
                params  = ["ItemTypeID":AppDelegate.shared().GETData["ItemTypeID"]!,
                           "CustomerName":AppDelegate.shared().GETData["CustomerName"]!,
                           "CaratDiamondWeight":AppDelegate.shared().GETData["CaratDiamondWeight"]!,
                           "JewlaryStyleTypeID":AppDelegate.shared().GETData["JewlaryStyleTypeID"]!,
                           "MetalType":AppDelegate.shared().GETData["MetalType"]!,
                           "MaterialTypeID":AppDelegate.shared().GETData["MaterialTypeID"]!,
                           "MaterialType":AppDelegate.shared().GETData["MaterialType"]!,
                           "EstimatedValue":AppDelegate.shared().GETData["EstimatedValue"]!,
                           "ShippingTypeID":AppDelegate.shared().GETData["ShippingTypeID"]!,
                           "CustomerComment":AppDelegate.shared().GETData["CustomerComment"]!,
                           "weight_of_item":AppDelegate.shared().GETData["weight_of_item"]!,
                           "jew_clearity_start":AppDelegate.shared().GETData["jew_clearity_start_id"]!,
                           "jew_clearity_end":AppDelegate.shared().GETData["jew_clearity_end_id"]!,
                           "jew_color_start":AppDelegate.shared().GETData["jew_color_start_id"]!,
                           "jew_color_end":AppDelegate.shared().GETData["jew_color_end_id"]!,
                            "color":AppDelegate.shared().GETData["morecolor"]!
                ]
            }
        }
        
        AppDelegate.shared().ShowHUD()
        
        if (AppDelegate.shared().GETData["ItemTypeID"] == "4")
        {
            var params : Parameters = [String: String]()
            
            params = ["ItemTypeID":AppDelegate.shared().GETData["ItemTypeID"]!,"CustomerName":AppDelegate.shared().GETData["CustomerName"]!,"webURL":AppDelegate.shared().GETData["webURL"]!,"ShippingTypeID":AppDelegate.shared().GETData["ShippingTypeID"]!,"CustomerComment":AppDelegate.shared().GETData["CustomerComment"]!]
            
            print("param is \(params)")
            let url = URL(string: "\(Constants.API)\(Constants.AddOrder)")!
            print("Url printed \(url)")
            
            let manager = Alamofire.SessionManager.default
            manager.session.configuration.timeoutIntervalForRequest = 120
            
            let login = UserDefaults.standard.string(forKey: "LoginToken")
            let UserID = UserDefaults.standard.string(forKey: "UserID")
            
            let headers = ["Authorization": "Basic YWRtaW46QVBJQEZFREVSQUwhIyRXRUIk",
                           "X-FEDERAL-API-KEY": "kkcoswggwgwkkc8w4ok808o48kswc40c0www4wss",
                           "X-FEDERAL-LOGIN-TOKEN": login!,
                           "USER-ID": UserID!]
            
            manager.request(url, method: .post, parameters: params, headers: headers).responseJSON() { response in
                switch (response.result) {
                case .success:
                    //do json stuff
                    
                    let null = NSNull()
                    
                    if response.result.value as? NSNull != null && response.result.value as? String != ""
                    {
                        let jsonResponse = response.result.value as! NSDictionary
                        print("JsonResponse printed \(jsonResponse)")
                        
                        AppDelegate.shared().HideHUD()
                        let myInt: Bool = jsonResponse["status"]! as! Bool
                        
                        if (myInt == true)
                        {
                            
                            self.OrderID = "\(jsonResponse.value(forKeyPath: "data.order_id") as! NSInteger)" as NSString
                            self.Certificate_No = "\(jsonResponse.value(forKeyPath: "data.certificate_no") as! String)" as NSString
                            if (self.Check_ShipOnline.on)
                            {
                                self.ShipmentyMethod(OrderID: self.OrderID as String, Certificate_No : self.Certificate_No as String, TransactionNo: "Local_0")
                            }
                            else{
                                self.GetStripePayment()
                            }
                         
                        }
                        else{
                            AppDelegate.shared().ShowAlert(title: Constants.ERROR_HEADER, msg: jsonResponse.value(forKeyPath: "message") as! String)
                          
                        }
                    }
                    
                    
                    break
                    
                case .failure(let error):
                    AppDelegate.shared().HideHUD()
                    if error._code == NSURLErrorTimedOut {
                        //HANDLE TIMEOUT HERE
                        AppDelegate.shared().ShowAlert(title: Constants.ERROR_HEADER, msg: error.localizedDescription)
                    }
                    print("\n\nAuth request failed with error:\n \(error)")
                    break
                }
            }
        }
        else{
            
            let url = URL(string: "\(Constants.API)\(Constants.AddOrder)")!
            print("Url printed \(url)")
            
            let manager = Alamofire.SessionManager.default
            manager.session.configuration.timeoutIntervalForRequest = 120
            
            let login = UserDefaults.standard.string(forKey: "LoginToken")
            let UserID = UserDefaults.standard.string(forKey: "UserID")
            
            let headers = ["Authorization": "Basic YWRtaW46QVBJQEZFREVSQUwhIyRXRUIk",
                           "X-FEDERAL-API-KEY": "kkcoswggwgwkkc8w4ok808o48kswc40c0www4wss",
                           "X-FEDERAL-LOGIN-TOKEN": login!,
                           "USER-ID": UserID!]
            print("PARAMETERS:",params)
            //Upload Image
            Alamofire.upload(multipartFormData: { (multipartFormData) in
//                DispatchQueue.main.async
//                    {
                        if (AppDelegate.shared().GETData["ItemTypeID"] == "3"){
                            for var i in 0..<AppDelegate.shared().ImageItem.count
                            {
                                self.imageData = UIImagePNGRepresentation(AppDelegate.shared().ImageItem[i])! as NSData
                                self.image = UIImage(data:self.imageData as Data,scale:1.0)!
                                
                                multipartFormData.append(UIImageJPEGRepresentation(self.image, 0.5)!, withName: "image\([i])", fileName: "photos.jpg", mimeType: "image/jpeg")
                                for (key, value) in params {
                                    multipartFormData.append(value.data(using: String.Encoding.utf8)!, withName: key)
                                }
                            }
                        }
                        else{
                            for var i in 0..<AppDelegate.shared().ImageItem.count
                            {
                                self.imageData = UIImagePNGRepresentation(AppDelegate.shared().ImageItem[i])! as NSData
                                self.image = UIImage(data:self.imageData as Data,scale:1.0)!
                                
                                multipartFormData.append(UIImageJPEGRepresentation(self.image, 0.5)!, withName: "image\([i])", fileName: "photos.jpg", mimeType: "image/jpeg")
                                for (key, value) in params {
                                    multipartFormData.append(value.data(using: String.Encoding.utf8)!, withName: key)
                                }
                            }
                            for var i in 0..<AppDelegate.shared().WatchImageitem.count
                            {
                                self.imageData = UIImagePNGRepresentation(AppDelegate.shared().WatchImageitem[i])! as NSData
                                self.image = UIImage(data:self.imageData as Data,scale:0.5)!
                                
                                multipartFormData.append(UIImagePNGRepresentation(self.image)!, withName: "issue_image\([i])", fileName: "photos.png", mimeType: "image/png")
                                for (key, value) in params {
                                    multipartFormData.append(value.data(using: String.Encoding.utf8)!, withName: key)
                                }
                            }
                            print("PARAMETERS:",params)
                        }
                        
             //   }
                
                
            }, to:url, method:.post,headers:headers)
            { (result) in
                
                switch result {
                case .success(let upload, _, _):
                    
                    upload.uploadProgress(closure: { (progress) in
                        //Print progress
                        print(progress)
                    })
                    
                    upload.responseJSON { response in
                        //print response.result
                        print("Success \(response.result.value ?? "empty")")
                      
                        let null = NSNull()
                        
                        if response.result.value as? NSNull != null && response.result.value as? String != ""
                        {
                            let jsonResponse = response.result.value as! NSDictionary
                            print("JsonResponse printed \(jsonResponse)")
                            
                            AppDelegate.shared().HideHUD()
                            let myInt: Bool = jsonResponse["status"]! as! Bool
                            
                            if(myInt == true)
                            {
                                /*var Chatarray = [[String:AnyObject]]() //Array of dictionary
                                 let dict = jsonResponse
                                 Chatarray = dict["data"]! as! [[String:AnyObject]]*/
                                // print("Get Data \(jsonResponse.value(forKeyPath: "data.order_id") as! NSInteger)")
                                
                                self.OrderID = "\(jsonResponse.value(forKeyPath: "data.order_id") as! NSInteger)" as NSString
                                self.Certificate_No = "\(jsonResponse.value(forKeyPath: "data.certificate_no") as! String)" as NSString
                                
                                if (self.Check_ShipOnline.on)
                                {
                                    self.ShipmentyMethod(OrderID: self.OrderID as String, Certificate_No : self.Certificate_No as String, TransactionNo: "Local_0")
                                }
                                else{
                                    self.GetStripePayment()
                                }
                                
                                
                             
                                
                            }
                            else{
                                AppDelegate.shared().ShowAlert(title: Constants.ERROR_HEADER, msg: jsonResponse.value(forKeyPath: "message") as! String)
                            }
                        }
                        else{
                            AppDelegate.shared().HideHUD()
                        }
                    }
                    break
                case .failure(let encodingError):
                    //print encodingError.description
                    AppDelegate.shared().HideHUD()
                    //HUD Hide With Error
                    
                    break
                }
            }
        }
    }
    
    
    func GetStripePayment(){
       
        
        AppDelegate.shared().HideHUD()
        
        var GetDescription = String()
        if (AppDelegate.shared().GETData["ItemTypeID"] == "1")
        {
            GetDescription = "Engagement Rings & Loose Stones"
        }
        if (AppDelegate.shared().GETData["ItemTypeID"] == "2")
        {
            GetDescription = "Watches"
        }
        if (AppDelegate.shared().GETData["ItemTypeID"] == "3")
        {
            GetDescription = "Jewelry"
        }
        if (AppDelegate.shared().GETData["ItemTypeID"] == "4")
        {
            GetDescription = "Weblinks"
        }
        let aString = self.Lbl_FinalAmount.text!
        let newString = aString.replacingOccurrences(of: "$ ", with: "")
        var numberOne: NSDecimalNumber = NSDecimalNumber(string: newString)
     
        let AppraisalCost = aString.replacingOccurrences(of: "$ ", with: "")
        
        
        
        let GString = self.Lbl_ShippingPrice.text!
        let Shipping = GString.replacingOccurrences(of: "$ ", with: "")
        
   
//        let fvc = self.storyboard?.instantiateViewController(withIdentifier: "stripeviewcontroller") as! stripeviewcontroller
//        fvc.amount = numberOne as Decimal
//        fvc.orderid = OrderID as String
//        fvc.Certificateno = Certificate_No as String
//        fvc.AppraisalCosts = AppraisalCost
//        fvc.ShippingCosts = Shipping
//
//        self.navigationController?.pushViewController(fvc, animated: true)
        
        let fvc = self.storyboard?.instantiateViewController(withIdentifier: "TabBarController") as! TabBarController
        let mydic = [
            "amount": numberOne ,
            "orderid":  OrderID ,
            "Certificateno":  Certificate_No ,
            "AppraisalCosts":  AppraisalCost,
            "ShippingCosts":  Shipping,

            ] as [String : Any]
        
        UserDefaults.standard.set(mydic, forKey: "storedata")
        self.navigationController?.pushViewController(fvc, animated: true)
       
        
        
    }
    
    
    func ShipmentyMethod(OrderID : String, Certificate_No : String, TransactionNo : String)
    {
        AppDelegate.shared().ShowHUD()
        let url = URL(string: "\(Constants.API)\(Constants.AddOrderPayment)")!
        print("Url printed \(url)")
        
        var params : Parameters = [String: String]()
        
        let aString = self.Lbl_AppraisalCost.text!
        let AppraisalCost = aString.replacingOccurrences(of: "$ ", with: "")
        
        let NString = self.Lbl_FinalAmount.text!
        let Final = NString.replacingOccurrences(of: "$ ", with: "")
        
        let GString = self.Lbl_ShippingPrice.text!
        let Shipping = GString.replacingOccurrences(of: "$ ", with: "")
        
        params = ["Status":"Confirmed","OrderID":OrderID,"TransactionNo":TransactionNo,"FeeAmount":AppraisalCost,"Discount":"0","Total":Final,"Note":"Order Payment","ShippingAmount":Shipping]
        print("Get Param \(params)")
        
        
        print("Url printed \(url)")
        
        let manager = Alamofire.SessionManager.default
        manager.session.configuration.timeoutIntervalForRequest = 120
        
        let login = UserDefaults.standard.string(forKey: "LoginToken")
        let UserID = UserDefaults.standard.string(forKey: "UserID")
        
        let headers = ["Authorization": "Basic YWRtaW46QVBJQEZFREVSQUwhIyRXRUIk",
                       "X-FEDERAL-API-KEY": "kkcoswggwgwkkc8w4ok808o48kswc40c0www4wss",
                       "X-FEDERAL-LOGIN-TOKEN": login!,
                       "USER-ID": UserID!]
        
        manager.request(url, method: .post, parameters: params, headers: headers).validate().responseJSON() { response in
            switch (response.result) {
            case .success:
                //do json stuff
                let jsonResponse = response.result.value as! NSDictionary
                print("JsonResponse printed \(jsonResponse)")
                
                AppDelegate.shared().HideHUD()
                let myInt: Int = jsonResponse["status"]! as! Int

                if(myInt == 1)
                {
                    let storyboard = UIStoryboard(name:"Main", bundle:nil)
                    let home = storyboard.instantiateViewController(withIdentifier: "OrderSuccessViewController")as! OrderSuccessViewController
                    UserDefaults.standard.set(myInt, forKey: "status")

                    let Paid : String = "\("Paid : $ ")\(jsonResponse.value(forKeyPath: "data.paid") as! String)"
                    UserDefaults.standard.set(Paid, forKey: "Paid")

                    let Shipping : String = jsonResponse.value(forKeyPath: "data.shipping_method") as! String
                    UserDefaults.standard.set(Shipping, forKey: "Order")

                    let Cert : String = "\("Certificate # ") \(jsonResponse.value(forKeyPath: "data.certificate_no") as! String)"
                    UserDefaults.standard.set(Cert, forKey: "Certificate")


                    self.navigationController?.pushViewController(home, animated: true)
                }

               
              
                break
            case .failure(let error):
                AppDelegate.shared().HideHUD()
                if error._code == NSURLErrorTimedOut {
                    //HANDLE TIMEOUT HERE
                    AppDelegate.shared().ShowAlert(title: Constants.ERROR_HEADER, msg: error.localizedDescription)
                }
                print("\n\nAuth request failed with error:\n \(error)")
                break
            }
        }
    }
    
    
   
    func showSuccess() {
        UIView.beginAnimations(nil, context: nil)
        UIView.setAnimationDuration(0.5)
        UIView.setAnimationDelay(2.0)
        UIView.commitAnimations()
    }
    
    
}
