//
//  WeblinkViewController.swift
//  Appraisal
//
//  Created by ADMIN on 3/26/18.
//  Copyright © 2018 empiereTech. All rights reserved.
//

import UIKit
import BEMCheckBox
import SWRevealViewController
import Alamofire
import SwiftyJSON

class WeblinkViewController: UIViewController,BEMCheckBoxDelegate,SWRevealViewControllerDelegate,UITextViewDelegate {

//MARK: - Outlets🤔
    
    //Views
    @IBOutlet var Vw_Side: UIView!
    
    //TextFeild // TextView
    @IBOutlet var Txt_Customer: UITextField!
    @IBOutlet var Txt_Weblink: UITextField!
    @IBOutlet var Txt_Comment: UITextView!
    
    //CheckMark
    @IBOutlet var CheckMark: BEMCheckBox!
    
    //Label
    @IBOutlet var Lbl_AcceptTerms: UILabel!
    
    //Button
    @IBOutlet var Btn_Next: UIButton!
    
    
//MARK: - Declare Variables👇🏻😬
    
    
//MARK: - Happy Coding😊
    override func viewDidLoad() {
        super.viewDidLoad()

        CheckMark.delegate = self
        
        self.title = "Weblinks"
        
        //NavigationBar
        navigationController?.navigationBar.barTintColor = UIColor.white
        navigationController?.navigationBar.titleTextAttributes = [NSAttributedStringKey.foregroundColor: UIColor(red: 18/255, green: 64/255, blue: 118/255, alpha: 1),NSAttributedStringKey.font: UIFont(name: "Montserrat-Medium", size: 17)!]
        //self.navigationItem.backBarButtonItem = [[UIBarButtonItem alloc] initWithTitle:@"Back" style:UIBarButtonItemStylePlain target:nil action:nil];
        self.navigationItem.backBarButtonItem = UIBarButtonItem.init(title: "Back", style: .plain, target: nil, action: nil)
         navigationController?.navigationBar.tintColor = UIColor(red: 18/255, green: 64/255, blue: 118/255, alpha: 1)

        //self.navigationItem.setHidesBackButton(true, animated:true);
        /*
        let menuButton = UIButton(type: .system)
        menuButton.setImage(#imageLiteral(resourceName: "Menu").withRenderingMode(.alwaysOriginal), for: .normal)
        menuButton.frame = CGRect(x: 0, y: 0, width: 34, height: 34)
        navigationItem.leftBarButtonItem = UIBarButtonItem(customView: menuButton)
        
        //SideBar
        if revealViewController() != nil
        {
            self.revealViewController().delegate = self
            menuButton.addTarget(revealViewController(), action: "revealToggle:", for: .touchUpInside)
            self.navigationController?.navigationBar.addGestureRecognizer(self.revealViewController().panGestureRecognizer())
            view.addGestureRecognizer(self.revealViewController().panGestureRecognizer())
        }*/
        
        AppDelegate.shared().GETData.removeAll()
        
        Txt_Customer.layer.cornerRadius = 7.0
        Txt_Customer.layer.backgroundColor = UIColor.white.cgColor
        Txt_Customer.layer.masksToBounds = false
        Txt_Customer.layer.shadowOffset = CGSize.zero
        Txt_Customer.layer.shadowRadius = 5
        Txt_Customer.layer.shadowOpacity = 0.2
        Txt_Customer.textColor = UIColor.black
        
        Txt_Customer.leftView = UIView(frame: CGRect(x: 0, y: 0, width: 15, height: Txt_Customer.frame.height))
        Txt_Customer.leftViewMode = .always
        let CustomerAttributed = NSMutableAttributedString.init(string: "Customer Name :")
        let Customerterms:NSString = "Customer Name :"
        let Customerrange :NSRange = Customerterms.range(of: "")
        let Customerrangech :NSRange = Customerterms.range(of: "Customer Name :")
        CustomerAttributed.addAttribute(NSAttributedStringKey.foregroundColor, value: UIColor.red, range: Customerrange)
        CustomerAttributed.addAttribute(NSAttributedStringKey.foregroundColor, value: UIColor.darkGray, range: Customerrangech)
        Txt_Customer.attributedPlaceholder = CustomerAttributed
 
        
        Txt_Weblink.layer.cornerRadius = 7.0
        Txt_Weblink.layer.backgroundColor = UIColor.white.cgColor
        Txt_Weblink.layer.masksToBounds = false
        Txt_Weblink.layer.shadowOffset = CGSize.zero
        Txt_Weblink.layer.shadowRadius = 5
        Txt_Weblink.layer.shadowOpacity = 0.2
        Txt_Weblink.textColor = UIColor.black
        Txt_Weblink.leftView = UIView(frame: CGRect(x: 0, y: 0, width: 15, height: Txt_Weblink.frame.height))
        Txt_Weblink.leftViewMode = .always
        let WebAttributed = NSMutableAttributedString.init(string: "*Paste Weblink")
        let Webterms:NSString = "*Paste Weblink"
        let webrange :NSRange = Webterms.range(of: "*")
        let webrangech :NSRange = Webterms.range(of: "Paste Weblink")
        WebAttributed.addAttribute(NSAttributedStringKey.foregroundColor, value: UIColor.red, range: webrange)
        WebAttributed.addAttribute(NSAttributedStringKey.foregroundColor, value: UIColor.darkGray, range: webrangech)
        Txt_Weblink.attributedPlaceholder = WebAttributed
    
        
        Txt_Comment.layer.cornerRadius = 7.0
        Txt_Comment.layer.backgroundColor = UIColor.white.cgColor
        Txt_Comment.layer.masksToBounds = false
        Txt_Comment.layer.shadowOffset = CGSize.zero
        Txt_Comment.layer.shadowRadius = 5
        Txt_Comment.layer.shadowOpacity = 0.2
        Txt_Comment.text = "Additional Info"
        Txt_Comment.textColor = UIColor.darkGray
        Txt_Comment.delegate = self
        Txt_Comment.contentInset = UIEdgeInsetsMake(5, 12, 0, 0)
        let stringAttributed = NSMutableAttributedString.init(string: Lbl_AcceptTerms.text!)
        
        let terms:NSString = "Yes, I Accept Terms & Conditions"
        
        let range :NSRange = terms.range(of: "Terms & Conditions")
        
        stringAttributed.addAttribute(NSAttributedStringKey.foregroundColor, value: UIColor.init(red: 18/255.0, green: 64/255.0, blue: 118/255.0, alpha: 1.0), range: range)
        stringAttributed.addAttribute(NSAttributedStringKey.underlineStyle , value: NSUnderlineStyle.styleSingle.rawValue, range: range)
        
        Lbl_AcceptTerms?.attributedText = stringAttributed
        
        
        Btn_Next.layer.cornerRadius = Btn_Next.frame.size.height/2
        Btn_Next.isEnabled = false
        Btn_Next.alpha = 0.2
        
     Txt_Comment.text = "Additional Info"
        //Txt_Customer.text = ""
        //Txt_Weblink.text = ""
    }

    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(true)
        Btn_Next.isUserInteractionEnabled = true
    }
    
    
//MARK: - Actions👊🏻😠
    
    //NextButtonPressed
    @IBAction func NextButtonPressed(_ sender: Any) {
        
//        if (Txt_Customer.text?.isEmpty)! {
//            AppDelegate.shared().ShowAlert(title: Constants.ERROR_HEADER, msg:"Please Enter Customer Name")
//        }
//         if ((Txt_Customer.text?.count)! < 4) {
//            AppDelegate.shared().ShowAlert(title: Constants.ERROR_HEADER, msg:"Please Enter Minimum 4 Character name")
//        }
        if (Txt_Weblink.text?.isEmpty)! {
            AppDelegate.shared().ShowAlert(title: Constants.ERROR_HEADER, msg:"Please Enter Any Weblink")
        }
        else if (!self.validateUrl(urlString: Txt_Weblink.text! as NSString)) {
            AppDelegate.shared().ShowAlert(title: Constants.ERROR_HEADER, msg:"Please Enter valid URL link")
        }
            
        else
        {
            
                Btn_Next.transform = CGAffineTransform(scaleX: 0.9, y: 0.9)
                Btn_Next.isUserInteractionEnabled = false
                UIView.animate(withDuration: 2.0,
                               delay: 0,
                               usingSpringWithDamping: 0.2,
                               initialSpringVelocity: 6.0,
                               options: .allowUserInteraction,
                               animations: { [weak self] in
                                self?.Btn_Next.transform = .identity
                                
                                Timer.scheduledTimer(timeInterval: 0.7,
                                                     target: self!,
                                                     selector: #selector(WeblinkViewController.update),
                                                     userInfo: nil,
                                                     repeats: false)
                    },
                               completion: nil)
            
        }
        
    }
    
    
/// Validate URL
    func validateUrl (urlString: NSString) -> Bool {
        let urlRegEx = "((?:http|https)://)?(?:www\\.)?[\\w\\d\\-_]+\\.\\w{2,3}(\\.\\w{2})?(/(?<=/)(?:[\\w\\d\\-./_]+)?)?"
        return NSPredicate(format: "SELF MATCHES %@", urlRegEx).evaluate(with: urlString)
    }
    
//func validateUrl(urlString: String?) -> Bool {
//        guard let urlString = urlString,
//            let url = URL(string: urlString) else {
//                return false
//        }
//        
//        var Result : Bool = UIApplication.shared.canOpenURL(url)
//        
//        if(!Result){
//            Result = (urlString.range(of: "www") != nil);
//            
//        }
//        return Result
//    }
//MARK: - Methods(Functions)☝🏻😳
    
    // @objc selector expected for Timer
    @objc func update() {
        // do what should happen when timer triggers an event
        let storyboard = UIStoryboard(name:"Main", bundle:nil)
        let home = storyboard.instantiateViewController(withIdentifier: "ConfirmOrderViewController")as! ConfirmOrderViewController
        var Comment:String = ""
        
        if Txt_Comment.text == "Any Comments" {
            Comment = "N/A"
        }
        else{
            Comment = Txt_Comment.text!
        }
        
        AppDelegate.shared().GETData = ["CustomerName": Txt_Customer.text ?? "", "webURL": Txt_Weblink.text!, "CustomerComment": Comment, "ItemTypeID":"4"]
        home.ISWebSide = true
        self.navigationController?.pushViewController(home, animated: true)
        
      
    }
    
    func textViewDidBeginEditing(_ textView: UITextView) {
        if textView.textColor == UIColor.darkGray {
            textView.text = nil
            textView.textColor = UIColor.black
        }
    }
    
    func textViewDidEndEditing(_ textView: UITextView) {
        if textView.text.isEmpty {
            textView.text = "Any Comments / Change shipping address:"
            textView.textColor = UIColor.darkGray
        }
    }
    
    func revealController(_ revealController: SWRevealViewController, didMoveTo position: FrontViewPosition) {
        if case .right = position {
            
            revealController.frontViewController.view.alpha = 0.5
            self.view.isUserInteractionEnabled = false
            self.revealViewController().frontViewController.view.addGestureRecognizer(self.revealViewController().panGestureRecognizer())
            self.revealViewController().frontViewController.view.addGestureRecognizer(self.revealViewController().tapGestureRecognizer())
        } else {
            
            revealController.frontViewController.view.alpha = 1
            self.revealViewController().tapGestureRecognizer()
            self.view.isUserInteractionEnabled = true
        }
    }
    
    func didTap(_ checkBox: BEMCheckBox) {
        self.view.endEditing(true)
        if (checkBox==CheckMark)
        {
            if (checkBox.on == true)
            {
                print("It's Chc")
                Btn_Next.isEnabled = true
                Btn_Next.alpha = 1.0
            }
            else{
                print("it's Loose Stone")
                Btn_Next.isEnabled = false
                Btn_Next.alpha = 0.2
            }
            
        }
    }
}
