//
//  AppDelegate.swift
//  Appraisal
//
//  Created by ADMIN on 3/22/18.
//  Copyright © 2018 empiereTech. All rights reserved.
//

import UIKit
import IQKeyboardManager
import SWRevealViewController
import PKHUD
import OneSignal
import Stripe

//import PayPalMobile

@UIApplicationMain
class AppDelegate: UIResponder, UIApplicationDelegate, OSPermissionObserver, OSSubscriptionObserver {

    var window: UIWindow?
    var viewController: SWRevealViewController?
    var ArrGetAllType = [[String:AnyObject]]() //Array of dictionary
    var GETData = [String: String]()
    var ImageItem = [UIImage]()
    var WatchImageitem = [UIImage]()
    
    //shared
    class func shared() -> AppDelegate
    {
        return UIApplication.shared.delegate as! AppDelegate
    }
    
    //application didFinishLaunchingWithOptions
    func application(_ application: UIApplication, didFinishLaunchingWithOptions launchOptions: [UIApplicationLaunchOptionsKey: Any]?) -> Bool {
        // Override point for customization after application launch.
        
           STPPaymentConfiguration.shared().publishableKey = Constants.PUBLISHKEY_LIVE
    //    STPPaymentConfiguration.shared().publishableKey = Constants.PUBLISHKEY_TEST

       // STPPaymentConfiguration.shared().appleMerchantIdentifier = "your apple merchant identifier"
        
  
        //OneSignal
        let onesignalInitSettings = [kOSSettingsKeyAutoPrompt: false]
        
        // Replace 'YOUR_APP_ID' with your OneSignal App ID.
        OneSignal.initWithLaunchOptions(launchOptions,
                                        appId: "bf3a5aba-3ed7-428f-9ce3-94f50cbf7239",
                                        handleNotificationAction: nil,
                                        settings: onesignalInitSettings)
        
        OneSignal.inFocusDisplayType = OSNotificationDisplayType.notification;
        OneSignal.add(self as OSPermissionObserver)
        OneSignal.add(self as OSSubscriptionObserver)
        
        
        // Recommend moving the below line to prompt for push after informing the user about
        //   how your app will use them.
        OneSignal.promptForPushNotifications(userResponse: { accepted in
            print("User accepted notifications: \(accepted)")
        })
        
        
        //PayPal
        //TODO: - Enter your credentials
//        PayPalMobile.initializeWithClientIds(forEnvironments: [PayPalEnvironmentProduction: Constants.PayPalEnvironmentProduction_Key,
//                                                                PayPalEnvironmentSandbox: Constants.PayPalEnvironmentSandbox_Key])
        
        
        //Keyboard
        IQKeyboardManager.shared().isEnabled = true
        IQKeyboardManager.shared().isEnableAutoToolbar = true
        IQKeyboardManager.shared().shouldShowToolbarPlaceholder = true
        IQKeyboardManager.shared().shouldResignOnTouchOutside = true
        IQKeyboardManager.shared().keyboardDistanceFromTextField = 50.0;
        
        let UserID = UserDefaults.standard.string(forKey: "UserID")
        
        if !(UserID == nil)
        {
            let mainStoryboardIpad : UIStoryboard = UIStoryboard(name: "Main", bundle: nil)
            let frontViewController : UIViewController = mainStoryboardIpad.instantiateViewController(withIdentifier: "HomeViewController") as! HomeViewController
            let rearViewController :SideViewController = mainStoryboardIpad.instantiateViewController(withIdentifier: "SideViewController") as! SideViewController
            let FrontNavigationController = UINavigationController.init(rootViewController: frontViewController)
            let revealController : SWRevealViewController = SWRevealViewController.init(rearViewController: rearViewController, frontViewController: FrontNavigationController)
            self.viewController = revealController
            self.window = UIWindow(frame: UIScreen.main.bounds)
            self.window?.rootViewController = self.viewController
            self.window?.makeKeyAndVisible()
        } else {
            let mainStoryboardIpad : UIStoryboard = UIStoryboard(name: "Main", bundle: nil)
            let frontViewController : UIViewController = mainStoryboardIpad.instantiateViewController(withIdentifier: "LoginViewController") as! LoginViewController
            let rearViewController :SideViewController = mainStoryboardIpad.instantiateViewController(withIdentifier: "SideViewController") as! SideViewController
            let FrontNavigationController = UINavigationController.init(rootViewController: frontViewController)
            let revealController : SWRevealViewController = SWRevealViewController.init(rearViewController: rearViewController, frontViewController: FrontNavigationController)
            self.viewController = revealController
            self.window = UIWindow(frame: UIScreen.main.bounds)
            self.window?.rootViewController = self.viewController
            self.window?.makeKeyAndVisible()
            
        }
        
        return true
    }
    
    //ShowHUD
    func ShowHUD(){
        //let loadingNotification = MBProgressHUD.showAdded(to: self.window!, animated: true)
       // loadingNotification.mode = MBProgressHUDMode.indeterminate
       // loadingNotification.label.text = "Loading..."
        PKHUD.sharedHUD.contentView = PKHUDProgressView()
        PKHUD.sharedHUD.show()
    }
    
    //HideHUD
    func HideHUD(){
        //MBProgressHUD.hide(for: self.window!, animated: true)
        PKHUD.sharedHUD.hide()/*
        PKHUD.sharedHUD.hide(afterDelay: 1.0) { success in
            // Completion Handler
            HUD.flash(.success, delay: 1.0)
        }*/
    }

    //ShowAlert
    func ShowAlert(title:String, msg:String) {
        //let alert = UIAlertController(title: title, message: msg, preferredStyle: UIAlertControllerStyle.alert)
       // alert.show(self.window!, sender: self)
        let alert = UIAlertController(title: title, message:msg, preferredStyle: UIAlertControllerStyle.alert)
        
        // add an action (button)
        alert.addAction(UIAlertAction(title: "OK", style: UIAlertActionStyle.default, handler: nil))
        
        // show the alert
        self.window?.rootViewController?.present(alert, animated: true, completion: nil)
    }
    
    
    func onOSPermissionChanged(_ stateChanges: OSPermissionStateChanges!) {
        
        // Example of detecting answering the permission prompt
        if stateChanges.from.status == OSNotificationPermission.notDetermined {
            if stateChanges.to.status == OSNotificationPermission.authorized {
                print("Thanks for accepting notifications!")
            } else if stateChanges.to.status == OSNotificationPermission.denied {
                print("Notifications not accepted. You can turn them on later under your iOS settings.")
            }
        }
        // prints out all properties
        print("PermissionStateChanges: \n\(stateChanges)")
    }
    
    // Output:
    /*
     Thanks for accepting notifications!
     PermissionStateChanges:
     Optional(<OSSubscriptionStateChanges:
     from: <OSPermissionState: hasPrompted: 0, status: NotDetermined>,
     to:   <OSPermissionState: hasPrompted: 1, status: Authorized>
     >
     */
    
    // TODO: update docs to change method name
    // Add this new method
    func onOSSubscriptionChanged(_ stateChanges: OSSubscriptionStateChanges!) {
        if !stateChanges.from.subscribed && stateChanges.to.subscribed {
            print("Subscribed for OneSignal push notifications!")
        }
        print("SubscriptionStateChange: \n\(stateChanges)")
        
        if let playerId = stateChanges.to.userId {
            UserDefaults.standard.setValue(playerId, forKeyPath: "deviceToken")
        }
    }
    
    
    //applicationWillResignActive
    func applicationWillResignActive(_ application: UIApplication) {
        // Sent when the application is about to move from active to inactive state. This can occur for certain types of temporary interruptions (such as an incoming phone call or SMS message) or when the user quits the application and it begins the transition to the background state.
        // Use this method to pause ongoing tasks, disable timers, and invalidate graphics rendering callbacks. Games should use this method to pause the game.
    }

    //applicationDidEnterBackground
    func applicationDidEnterBackground(_ application: UIApplication) {
        // Use this method to release shared resources, save user data, invalidate timers, and store enough application state information to restore your application to its current state in case it is terminated later.
        // If your application supports background execution, this method is called instead of applicationWillTerminate: when the user quits.
    }

    //applicationWillEnterForeground
    func applicationWillEnterForeground(_ application: UIApplication) {
        // Called as part of the transition from the background to the active state; here you can undo many of the changes made on entering the background.
    }

    //applicationDidBecomeActive
    func applicationDidBecomeActive(_ application: UIApplication) {
        // Restart any tasks that were paused (or not yet started) while the application was inactive. If the application was previously in the background, optionally refresh the user interface.
    }

    //applicationWillTerminate
    func applicationWillTerminate(_ application: UIApplication) {
        // Called when the application is about to terminate. Save data if appropriate. See also applicationDidEnterBackground:.
    }


}

