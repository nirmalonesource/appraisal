//
//  WatchesViewController.swift
//  Appraisal
//
//  Created by ADMIN on 3/31/18.
//  Copyright © 2018 empiereTech. All rights reserved.
//

import UIKit
import BEMCheckBox
import SWRevealViewController
import AETextFieldValidator
 import RangeSeekSlider

class WatchesViewController: UIViewController,BEMCheckBoxDelegate,SWRevealViewControllerDelegate,UITextViewDelegate,UICollectionViewDataSource, UICollectionViewDelegate,UICollectionViewDelegateFlowLayout, UIImagePickerControllerDelegate, UINavigationControllerDelegate,kDropDownListViewDelegate,UIScrollViewDelegate,RangeSeekSliderDelegate {

//MARK: - Outlets🤔
    
    //View
    @IBOutlet var Vw_side: UIView!
    @IBOutlet var Scoll_Vw: UIScrollView!
    @IBOutlet var Vw_Collection: UICollectionView!
    @IBOutlet var Vw_Under: UIView!
    
    //TextFeild
    @IBOutlet var Txt_Other: AETextFieldValidator!
    @IBOutlet var Txt_CustomerName: AETextFieldValidator!
    @IBOutlet var Txt_BandName: AETextFieldValidator!
    @IBOutlet weak var Txt_Dial: AETextFieldValidator!
    @IBOutlet var Txt_ModelName: AETextFieldValidator!
    @IBOutlet var Txt_Serial: AETextFieldValidator!
    @IBOutlet var Txt_Case: AETextFieldValidator!
    @IBOutlet var Txt_Estimated: AETextFieldValidator!
    @IBOutlet var Txt_Diamond: AETextFieldValidator!
    @IBOutlet var Txt_Comments: UITextView!
    
    //Button
    @IBOutlet var Btn_Wind: UIButton!
    @IBOutlet var Btn_Next: UIButton!
    @IBOutlet var Btn_Matel: UIButton!
      @IBOutlet weak var Btn_Material: UIButton!
    @IBOutlet var bottomvw: UIView!
      @IBOutlet var lblquality: UILabel!
      @IBOutlet var lblclarity: UILabel!
      @IBOutlet var lblline: UILabel!
    
    //Segment
    @IBOutlet var Segment_Parts: UISegmentedControl!
    
    //CheckMark
    @IBOutlet var Check_Terms: BEMCheckBox!
    
    //Label
    @IBOutlet var Lbl_Metalapplicable: UILabel!
    
    //ImageView
    @IBOutlet weak var Img_Watch: UIImageView!
    
    @IBOutlet var Vw_Clarity: RangeSeekSlider!
    @IBOutlet var Vw_Color: RangeSeekSlider!
    @IBOutlet var claritytextview: UIView!
    @IBOutlet var colortextview: UIView!
    @IBOutlet var txtcolor: AETextFieldValidator!
    
    @IBOutlet var vwgem: UIView!
    @IBOutlet var txtother: AETextFieldValidator!
    
    
      var ClarityArr = [[String:AnyObject]]()
       var ClarityArrIndex = NSString()
       var ColorArr = [[String:AnyObject]]()
       var ColorArrIndex = NSString()
    var MaterialArr = [[String:AnyObject]]()
     var MaterialArrIndex = Int()
    
     var clearity_start = NSString()
     var clearity_end = NSString()
     var color_start = NSString()
     var color_end = NSString()
      
     var clearity_start_id = NSString()
     var clearity_end_id = NSString()
     var color_start_id = NSString()
     var color_end_id = NSString()
    
    
//MARK: - Declare Variables👇🏻😬
    let imagePicker = UIImagePickerController()
    let reuseIdentifier = "cell" // also enter this string as the cell identifier in the storyboard
    var items = [UIImage]()
    var image = UIImage()
    var imageData = NSData()
    var UserImageName = NSString()

    var ArrMatel = [[String:AnyObject]]()
    var MatelID = NSString()
    var ArrWind = [[String:AnyObject]]()
    var WindID = NSString()
    var Dropobj = DropDownListView()
    var NameString = NSString()
    var SelectSeg = NSString()
    
    var View = Bool()
    var GetOther = Bool()
     var GetOtherGem = Bool()
//MARK: - Happy Coding😊
    
    //viewDidLoad
    override func viewDidLoad() {
        super.viewDidLoad()
       
        // Do any additional setup after loading the view.
         self.revealViewController().panGestureRecognizer().isEnabled = false
        GetOther = false
         GetOtherGem = false
        AppDelegate.shared().WatchImageitem.removeAll()
        //NavigationBar
        self.navigationController?.navigationBar.isHidden = false;
        self.title = "Watches"
        WindID = ""
        
        navigationController?.navigationBar.barTintColor = UIColor.white
        if (UIScreen.main.bounds.size.height == 568.0)
        {
            print("iphone5")
            Scoll_Vw.contentSize = CGSize(width: self.view.frame.size.width, height: 1500)
            navigationController?.navigationBar.titleTextAttributes = [NSAttributedStringKey.foregroundColor: UIColor(red: 18/255, green: 64/255, blue: 118/255, alpha: 1),NSAttributedStringKey.font: UIFont(name: "Montserrat-Medium", size: 15)!]
        }
        else{
            Scoll_Vw.contentSize = CGSize(width: self.view.frame.size.width, height: 1500)
            navigationController?.navigationBar.titleTextAttributes = [NSAttributedStringKey.foregroundColor: UIColor(red: 18/255, green: 64/255, blue: 118/255, alpha: 1),NSAttributedStringKey.font: UIFont(name: "Montserrat-Medium", size: 17)!]
        }
        navigationController?.navigationBar.tintColor = UIColor(red: 18/255, green: 64/255, blue: 118/255, alpha: 1)
         Check_Terms.delegate = self
        imagePicker.delegate = self
        
        AppDelegate.shared().GETData.removeAll()
        AppDelegate.shared().ImageItem.removeAll()
        
        
        if (UIDevice.current.userInterfaceIdiom == .pad)
        {
            print("iPad")
            Scoll_Vw.contentSize = CGSize(width: self.view.frame.size.width, height: 1520)
        }
        else if (UIScreen.main.bounds.size.height == 568.0)
        {
            print("iphone5")
            Scoll_Vw.contentSize = CGSize(width: self.view.frame.size.width, height: 1520)
        }
            
        else if (UIScreen.main.bounds.size.height == 480.0)
        {
            print("iphone4")
            Scoll_Vw.contentSize = CGSize(width: self.view.frame.size.width, height: 1520)
        }
        else
        {
            print("OtherPhones")
            Scoll_Vw.contentSize = CGSize(width: self.view.frame.size.width, height: 1520)
        }
        
        var dict = AppDelegate.shared().ArrGetAllType[0]
        
        ClarityArr = dict["Clarity"]! as! [[String:AnyObject]]
        ColorArr = dict["Color"]! as! [[String:AnyObject]]

        ArrMatel = dict["MaterialWatchType"]! as! [[String:AnyObject]]
        print(ArrMatel.count)
        
        ArrWind = dict["Wind"]! as! [[String:AnyObject]]
        print("ArrWind \(ArrWind)")
        
          MaterialArr = dict["GemMaterialWatchType"]! as! [[String:AnyObject]]
         print("MaterialArr \(MaterialArr)")
        
        Txt_CustomerName.layer.cornerRadius = 7.0
        Txt_CustomerName.layer.backgroundColor = UIColor.white.cgColor
        Txt_CustomerName.layer.masksToBounds = false
        Txt_CustomerName.layer.shadowOffset = CGSize.zero
        Txt_CustomerName.layer.shadowRadius = 5
        Txt_CustomerName.layer.shadowOpacity = 0.2
        Txt_CustomerName.textColor = UIColor.black
        Txt_CustomerName.leftView = UIView(frame: CGRect(x: 0, y: 0, width: 15, height: Txt_CustomerName.frame.height))
        Txt_CustomerName.leftViewMode = .always
        let CustomerAttributed = NSMutableAttributedString.init(string: "Customer Name :")
        let Customerterms:NSString = "Customer Name :"
        let Customerrange :NSRange = Customerterms.range(of: "")
        let Customerrangech :NSRange = Customerterms.range(of: "Customer Name :")
        CustomerAttributed.addAttribute(NSAttributedStringKey.foregroundColor, value: UIColor.red, range: Customerrange)
        CustomerAttributed.addAttribute(NSAttributedStringKey.foregroundColor, value: UIColor.darkGray, range: Customerrangech)
        Txt_CustomerName.attributedPlaceholder = CustomerAttributed
        
       
        
        Txt_BandName.layer.cornerRadius = 7.0
        Txt_BandName.layer.backgroundColor = UIColor.white.cgColor
        Txt_BandName.layer.masksToBounds = false
        Txt_BandName.layer.shadowOffset = CGSize.zero
        Txt_BandName.layer.shadowRadius = 5
        Txt_BandName.layer.shadowOpacity = 0.2
        Txt_BandName.textColor = UIColor.black
        Txt_BandName.leftView = UIView(frame: CGRect(x: 0, y: 0, width: 15, height: Txt_BandName.frame.height))
        Txt_BandName.leftViewMode = .always
        let BandAttributed = NSMutableAttributedString.init(string: "*Brand Name :")
        let Bandterms:NSString = "*Brand Name :"
        let Bandrange :NSRange = Bandterms.range(of: "*")
        let Bandrangech :NSRange = Bandterms.range(of: "Brand Name :")
        BandAttributed.addAttribute(NSAttributedStringKey.foregroundColor, value: UIColor.red, range: Bandrange)
        BandAttributed.addAttribute(NSAttributedStringKey.foregroundColor, value: UIColor.darkGray, range: Bandrangech)
        Txt_BandName.attributedPlaceholder = BandAttributed
        
        
        Txt_ModelName.layer.cornerRadius = 7.0
        Txt_ModelName.layer.backgroundColor = UIColor.white.cgColor
        Txt_ModelName.layer.masksToBounds = false
        Txt_ModelName.layer.shadowOffset = CGSize.zero
        Txt_ModelName.layer.shadowRadius = 5
        Txt_ModelName.layer.shadowOpacity = 0.2
        Txt_ModelName.leftView = UIView(frame: CGRect(x: 0, y: 0, width: 15, height: Txt_ModelName.frame.height))
        Txt_ModelName.leftViewMode = .always
        let ModelAttributed = NSMutableAttributedString.init(string: "*Model/Reference (Max char 1-20):")
        let Modelterms :NSString = "*Model/Reference (Max char 1-20):"
        let ModelRange :NSRange = Modelterms.range(of: "*")
        let Modelrangech :NSRange = Modelterms.range(of: "Model/Reference (Max char 1-20):")
        ModelAttributed.addAttribute(NSAttributedStringKey.foregroundColor, value: UIColor.darkGray, range: Modelrangech)
        ModelAttributed.addAttribute(NSAttributedStringKey.foregroundColor, value: UIColor.red, range: ModelRange)
        Txt_ModelName.attributedPlaceholder = ModelAttributed
        
        
        Txt_Dial.layer.cornerRadius = 7.0
        Txt_Dial.layer.backgroundColor = UIColor.white.cgColor
        Txt_Dial.layer.masksToBounds = false
        Txt_Dial.layer.shadowOffset = CGSize.zero
        Txt_Dial.layer.shadowRadius = 5
        Txt_Dial.layer.shadowOpacity = 0.2
        Txt_Dial.leftView = UIView(frame: CGRect(x: 0, y: 0, width: 15, height: Txt_ModelName.frame.height))
        Txt_Dial.leftViewMode = .always
        let DialAttributed = NSMutableAttributedString.init(string: "*Dial (Max char 1-20):")
        let Dialterms :NSString = "*Dial (Max char 1-20): :"
        let DialRange :NSRange = Dialterms.range(of: "*")
        let Dialrangech :NSRange = Dialterms.range(of: "Dial (Max char 1-20):")
        DialAttributed.addAttribute(NSAttributedStringKey.foregroundColor, value: UIColor.darkGray, range: Dialrangech)
        DialAttributed.addAttribute(NSAttributedStringKey.foregroundColor, value: UIColor.red, range: DialRange)
        Txt_Dial.attributedPlaceholder = DialAttributed
        
        Txt_Other.layer.cornerRadius = 7.0
        Txt_Other.layer.backgroundColor = UIColor.white.cgColor
        Txt_Other.layer.masksToBounds = false
        Txt_Other.layer.shadowOffset = CGSize.zero
        Txt_Other.layer.shadowRadius = 5
        Txt_Other.layer.shadowOpacity = 0.2
        Txt_Other.leftView = UIView(frame: CGRect(x: 0, y: 0, width: 15, height: Txt_Other.frame.height))
        Txt_Other.leftViewMode = .always
        let OtherAttributed = NSMutableAttributedString.init(string: "Other Material :")
        let Otherterms:NSString = "Other Material :"
        let Otherrangech :NSRange = Otherterms.range(of: "Other Material :")
        OtherAttributed.addAttribute(NSAttributedStringKey.foregroundColor, value: UIColor.darkGray, range: Otherrangech)
        Txt_Other.attributedPlaceholder = OtherAttributed
        
        txtother.layer.cornerRadius = 7.0
        txtother.layer.backgroundColor = UIColor.white.cgColor
        txtother.layer.masksToBounds = false
        txtother.layer.shadowOffset = CGSize.zero
        txtother.layer.shadowRadius = 5
        txtother.layer.shadowOpacity = 0.2
        txtother.leftView = UIView(frame: CGRect(x: 0, y: 0, width: 15, height: txtother.frame.height))
        txtother.leftViewMode = .always
        let OtherAttributed1 = NSMutableAttributedString.init(string: "Other Gem Material :")
        let Otherterms1:NSString = "Other Gem Material :"
        let Otherrangech1 :NSRange = Otherterms1.range(of: "Other Gem Material :")
        OtherAttributed1.addAttribute(NSAttributedStringKey.foregroundColor, value: UIColor.darkGray, range: Otherrangech1)
        txtother.attributedPlaceholder = OtherAttributed1
        
        Txt_Serial.layer.cornerRadius = 7.0
        Txt_Serial.layer.backgroundColor = UIColor.white.cgColor
        Txt_Serial.layer.masksToBounds = false
        Txt_Serial.layer.shadowOffset = CGSize.zero
        Txt_Serial.layer.shadowRadius = 5
        Txt_Serial.layer.shadowOpacity = 0.2
        Txt_Serial.leftView = UIView(frame: CGRect(x: 0, y: 0, width: 15, height: Txt_Serial.frame.height))
        Txt_Serial.leftViewMode = .always
        let SerialAttributed = NSMutableAttributedString.init(string: "*Serial # (Max char 1-20):")
        let Serialterms:NSString = "*Serial # (Max char 1-20):"
        let Serialrange :NSRange = Serialterms.range(of: "*")
        let Serialrangech :NSRange = Serialterms.range(of: "Serial # (Max char 1-20):")
        SerialAttributed.addAttribute(NSAttributedStringKey.foregroundColor, value: UIColor.red, range: Serialrange)
        SerialAttributed.addAttribute(NSAttributedStringKey.foregroundColor, value: UIColor.darkGray, range: Serialrangech)
        Txt_Serial.attributedPlaceholder = SerialAttributed
        
        Txt_Case.layer.cornerRadius = 7.0
        Txt_Case.layer.backgroundColor = UIColor.white.cgColor
        Txt_Case.layer.masksToBounds = false
        Txt_Case.layer.shadowOffset = CGSize.zero
        Txt_Case.layer.shadowRadius = 5
        Txt_Case.layer.shadowOpacity = 0.2
        Txt_Case.leftView = UIView(frame: CGRect(x: 0, y: 0, width: 15, height: Txt_Case.frame.height))
        Txt_Case.leftViewMode = .always
        let CaseAttributed = NSMutableAttributedString.init(string: "*Case Size :")
        let Caseterms:NSString = "*Case Size :"
         let CaseRange :NSRange = Caseterms.range(of: "*")
        let Caserangech :NSRange = Caseterms.range(of: "Case Size :")
        CaseAttributed.addAttribute(NSAttributedStringKey.foregroundColor, value: UIColor.red, range: CaseRange)
        CaseAttributed.addAttribute(NSAttributedStringKey.foregroundColor, value: UIColor.darkGray, range: Caserangech)
        Txt_Case.attributedPlaceholder = CaseAttributed
        
        Txt_Estimated.layer.cornerRadius = 7.0
        Txt_Estimated.layer.backgroundColor = UIColor.white.cgColor
        Txt_Estimated.layer.masksToBounds = false
        Txt_Estimated.layer.shadowOffset = CGSize.zero
        Txt_Estimated.layer.shadowRadius = 5
        Txt_Estimated.layer.shadowOpacity = 0.2
       /* let txtDollar = UITextView(frame: CGRect(x: 5, y:10, width: 15, height: Txt_Estimated.frame.height - 10))
        txtDollar.text = "$"
        txtDollar.textColor = UIColor.black
        Txt_Estimated.leftView = txtDollar
        Txt_Estimated.leftViewMode = .always
        let EstimatedAttributed = NSMutableAttributedString.init(string: "Estimated value :")
        let Estimatedterms:NSString = "Estimated value :"
        let Estimatedrangech :NSRange = Estimatedterms.range(of: "Estimated value :")
        EstimatedAttributed.addAttribute(NSAttributedStringKey.foregroundColor, value: UIColor.darkGray, range: Estimatedrangech)
        Txt_Estimated.attributedPlaceholder = EstimatedAttributed
 */
        let txtDollar = UITextView(frame: CGRect(x: 5, y:10, width: 15, height: Txt_Estimated.frame.height - 10))
        txtDollar.text = ""
        txtDollar.isEditable = false
        txtDollar.textColor =  UIColor.darkGray
        Txt_Estimated.leftView = txtDollar
        Txt_Estimated.leftViewMode = .always
        Txt_Estimated.textColor = #colorLiteral(red: 0, green: 0.4980392157, blue: 0.003921568627, alpha: 1)
        let EstimatedAttributed = NSMutableAttributedString.init(string: "Estimated Value:")
        let Estimatedterms:NSString = "Estimated Value:"
        let Estimatedrangech :NSRange = Estimatedterms.range(of: "Estimated Value:")
        EstimatedAttributed.addAttribute(NSAttributedStringKey.foregroundColor, value: UIColor.darkGray, range: Estimatedrangech)
        Txt_Estimated.attributedPlaceholder = EstimatedAttributed
        
        Txt_Diamond.layer.cornerRadius = 7.0
        Txt_Diamond.layer.backgroundColor = UIColor.white.cgColor
        Txt_Diamond.layer.masksToBounds = false
        Txt_Diamond.layer.shadowOffset = CGSize.zero
        Txt_Diamond.layer.shadowRadius = 5
        Txt_Diamond.layer.shadowOpacity = 0.2
        Txt_Diamond.leftView = UIView(frame: CGRect(x: 0, y: 0, width: 15, height: Txt_Diamond.frame.height))
        Txt_Diamond.leftViewMode = .always
        let DiamondAttributed = NSMutableAttributedString.init(string: "Diamond Weight :")
        let Diamondterms:NSString = "Diamond Weight :"
        let DiamondRange :NSRange = Caseterms.range(of: "")
        Txt_Diamond.textColor = UIColor.black
        let Diamondrangech :NSRange = Diamondterms.range(of: "Diamond Weight :")
        DiamondAttributed.addAttribute(NSAttributedStringKey.foregroundColor, value: UIColor.red, range: DiamondRange)
        DiamondAttributed.addAttribute(NSAttributedStringKey.foregroundColor, value: UIColor.darkGray, range: Diamondrangech)
        Txt_Diamond.attributedPlaceholder = DiamondAttributed
        
        txtcolor.layer.cornerRadius = 7.0
                   txtcolor.layer.backgroundColor = UIColor.white.cgColor
                   txtcolor.layer.masksToBounds = false
                   txtcolor.layer.shadowOffset = CGSize.zero
                   txtcolor.layer.shadowRadius = 5
                   txtcolor.layer.shadowOpacity = 0.2
                   txtcolor.leftView = UIView(frame: CGRect(x: 0, y: 0, width: 15, height: txtcolor.frame.height))
                   txtcolor.leftViewMode = .always
                   let txtcolorAttributed = NSMutableAttributedString.init(string: "Color :")
                   let txtcolorterms:NSString = "Color :"
                   let txtcolorrangech :NSRange = txtcolorterms.range(of: "Color :")
                   txtcolorAttributed.addAttribute(NSAttributedStringKey.foregroundColor, value: UIColor.darkGray, range: txtcolorrangech)
                   txtcolor.attributedPlaceholder = txtcolorAttributed
        
        
        Txt_Comments.layer.cornerRadius = 7.0
        Txt_Comments.layer.backgroundColor = UIColor.white.cgColor
        Txt_Comments.layer.masksToBounds = false
        Txt_Comments.layer.shadowOffset = CGSize.zero
        Txt_Comments.layer.shadowRadius = 5
        Txt_Comments.layer.shadowOpacity = 0.2
        Txt_Comments.text = "Additional Info"
        Txt_Comments.textColor = UIColor.black
        Txt_Comments.textColor = UIColor.darkGray
        Txt_Comments.delegate = self
        Txt_Comments.contentInset = UIEdgeInsetsMake(5, 12, 0, 0)
        
        setupAlerts()
        
               Btn_Material.layer.cornerRadius = 7.0
               Btn_Material.isUserInteractionEnabled = true
               Btn_Material.layer.masksToBounds = false
               Btn_Material.layer.shadowOffset = CGSize.zero
               Btn_Material.layer.shadowRadius = 5
               Btn_Material.layer.shadowOpacity = 0.2
               Btn_Material.contentEdgeInsets = UIEdgeInsets(top: 5, left: 15, bottom: 0, right: 0)
        
        Btn_Wind.layer.cornerRadius = 7.0
        Btn_Wind.isUserInteractionEnabled = true
        Btn_Wind.layer.masksToBounds = false
        Btn_Wind.layer.shadowOffset = CGSize.zero
        Btn_Wind.layer.shadowRadius = 5
        Btn_Wind.layer.shadowOpacity = 0.2
        Btn_Wind.contentEdgeInsets = UIEdgeInsets(top: 5, left: 12, bottom: 0, right: 0)
        
        Btn_Matel.layer.cornerRadius = 7.0
        Btn_Matel.isUserInteractionEnabled = true
        Btn_Matel.layer.masksToBounds = false
        Btn_Matel.layer.shadowOffset = CGSize.zero
        Btn_Matel.layer.shadowRadius = 5
        Btn_Matel.layer.shadowOpacity = 0.2
        Btn_Matel.contentEdgeInsets = UIEdgeInsets(top: 5, left: 12, bottom: 0, right: 0)
        
        Btn_Next.layer.cornerRadius = Btn_Next.frame.size.height/2
        Btn_Next.isEnabled = false
        Btn_Next.alpha = 0.2;
        
        Segment_Parts.layer.cornerRadius = 15.0
        Segment_Parts.layer.borderWidth = 1.0
        Segment_Parts.layer.borderColor = UIColor(red: 18/255, green: 64/255, blue: 118/255, alpha: 1).cgColor
        Segment_Parts.layer.masksToBounds = true
        Segment_Parts.selectedSegmentIndex = 0
        SelectSeg = "1"
        
        MaterialArrIndex = 0
        Btn_Material.setTitle(MaterialArr[0]["MaterialType"] as? String, for: .normal)
        
     setslider()

    }
    
     func setslider()
     {
           //============================ RANGE SLIDER SETUP ====================================
                var xposition = 0
                
                for i in 0..<ClarityArr.count
                {
                    let screenwidth = claritytextview.frame.size.width
                    
                    let lbl = UILabel(frame: CGRect(x: xposition, y: 0, width: Int(screenwidth/CGFloat(ClarityArr.count)), height: 20))
                    lbl.textAlignment = .center
                    lbl.text = ClarityArr[i]["ClarityName"] as? String
                    lbl.font = lbl.font.withSize(10)
                    self.claritytextview.addSubview(lbl)
                    xposition = xposition + Int(screenwidth/CGFloat(ClarityArr.count))
                }
                
                Vw_Clarity.delegate = self
                Vw_Clarity.enableStep = true
                Vw_Clarity.step = 1 // CGFloat(ClarityArr.count-1)
                if let n = NumberFormatter().number(from: ClarityArr[0]["ClarityID"] as! String)
                {
                    let f = CGFloat(truncating: n)
                    Vw_Clarity.minValue = 0
                    Vw_Clarity.selectedMinValue = 0
                    clearity_start_id = ClarityArr[0]["ClarityID"] as! NSString
                    clearity_start = ClarityArr[0]["ClarityName"] as! NSString
                    
                }
                if let n = NumberFormatter().number(from: ClarityArr[ClarityArr.count-1]["ClarityID"] as! String)
                {
                    let f = CGFloat(truncating: n)
                    Vw_Clarity.maxValue = CGFloat(ClarityArr.count-1)
                    Vw_Clarity.selectedMaxValue = CGFloat(ClarityArr.count-1)
                    clearity_end_id = ClarityArr[ClarityArr.count-1]["ClarityID"] as! NSString
                    clearity_end = ClarityArr[ClarityArr.count-1]["ClarityName"] as! NSString
                }
                
                
                xposition = 0
                
                for i in 0..<ColorArr.count
                {
                    let screenwidth = colortextview.frame.size.width
                    
                    let lbl = UILabel(frame: CGRect(x: xposition, y: 0, width: Int(screenwidth/CGFloat(ColorArr.count)), height: 20))
                    lbl.textAlignment = .center
                    lbl.text = ColorArr[i]["ColorName"] as? String
                    lbl.font = lbl.font.withSize(10)
                    self.colortextview.addSubview(lbl)
                    xposition = xposition + Int(screenwidth/CGFloat(ColorArr.count))
                }
                
                Vw_Color.delegate = self
                Vw_Color.enableStep = true
                Vw_Color.step = 1 // CGFloat(ClarityArr.count-1)
                if let n = NumberFormatter().number(from: ColorArr[0]["ColorID"] as! String)
                {
                    let f = CGFloat(truncating: n)
                    Vw_Color.minValue = 0
                    Vw_Color.selectedMinValue = 0
                    color_start_id = ColorArr[0]["ColorID"] as! NSString
                    color_start = ColorArr[0]["ColorName"] as! NSString
                }
                if let n = NumberFormatter().number(from: ColorArr[ColorArr.count-1]["ColorID"] as! String)
                {
                    let f = CGFloat(truncating: n)
                    Vw_Color.maxValue = CGFloat(ColorArr.count-1)
                    Vw_Color.selectedMaxValue = CGFloat(ColorArr.count-1)
                    color_end_id = ColorArr[ColorArr.count-1]["ColorID"] as! NSString
                    color_end = ColorArr[ColorArr.count-1]["ColorName"] as! NSString
                }
                
                    bottomvw.frame.origin.y = 180;
                    Vw_Clarity.isHidden = true;
                    claritytextview.isHidden = true;
                    Vw_Color.isHidden = true;
                    colortextview.isHidden = true;
                    lblline.isHidden = false;
                    lblquality.isHidden = true;
                    lblclarity.isHidden = true;
                Txt_Diamond.isHidden = true;
        //        clearity_start = "0"
        //                       clearity_end = "0"
        //                       clearity_start_id = "0"
        //                       clearity_end_id = "0"
        //                       color_start = "0"
        //                       color_end = "0"
        //                       color_start_id = "0"
        //                       color_end_id = "0"
               
                //============================ RANGE SLIDER SETUP END ====================================
    }
    
    // MARK: - RangeSeekSliderDelegate
               
               func rangeSeekSlider(_ slider: RangeSeekSlider, didChange minValue: CGFloat, maxValue: CGFloat) {
                   if slider === Vw_Clarity {
                       print("clarity slider updated. Min Value: \(minValue) Max Value: \(maxValue)")
                       clearity_start_id = ClarityArr[Int(minValue)]["ClarityID"] as! NSString
                       clearity_end_id = ClarityArr[Int(maxValue)]["ClarityID"] as! NSString
                       clearity_start = ClarityArr[Int(minValue)]["ClarityName"] as! NSString
                       clearity_end = ClarityArr[Int(maxValue)]["ClarityName"] as! NSString
                   }
                   else if slider === Vw_Color {
                       color_start_id = ColorArr[Int(minValue)]["ColorID"] as! NSString
                       color_end_id = ColorArr[Int(maxValue)]["ColorID"] as! NSString
                       color_start = ColorArr[Int(minValue)]["ColorName"] as! NSString
                       color_end = ColorArr[Int(maxValue)]["ColorName"] as! NSString
                    
                    if color_start_id == "11" || color_end_id == "11"{
                        txtcolor.isHidden = false
                   
                        bottomvw.frame = CGRect.init(x: bottomvw.frame.origin.x, y: 450, width: bottomvw.frame.size.width, height: bottomvw.frame.size.height)
                        
                    }
                    else
                    {
                        
                        bottomvw.frame = CGRect.init(x: bottomvw.frame.origin.x, y: 450, width: bottomvw.frame.size.width, height: bottomvw.frame.size.height)
                        txtcolor.isHidden = true
                        txtcolor.text = ""
                        
                    }

                       print("color slider updated. Min Value: \(minValue) Max Value: \(maxValue)")
                   }
               }
    

    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(true)
        
        //items.append(UIImage(named: "upload_image")!)
        if View == false
        {
            items.insert(UIImage(named: "upload_image")!, at: 0)
        }
        
        Btn_Next.isUserInteractionEnabled = true
    }
    
//MARK: - Actions👊🏻😠
    
    //WindButtonPressed
    @IBAction func WindButtonPressed(_ sender: Any) {
        NameString = "Wind"
        Dropobj.fadeOut()
        DropDown1(Array: ArrWind, Name: "WindType", ButtonName: "Select Wind", Button: Btn_Wind)
    }
    
    //MatelButtonPressed
    @IBAction func MatelButtonPressed(_ sender: Any) {
        NameString = "Matel"
        Dropobj.fadeOut()
        DropDown(Array: ArrMatel, Name: "MaterialType", ButtonName: "Select Metal/Material", Button: Btn_Matel)
    }
    
    //NextButtonPressed
    @IBAction func NextButtonPressed(_ sender: Any) {
        
        let a = "none"
        let b = Btn_Material.titleLabel?.text!
        let result: ComparisonResult = a.compare(String(b ?? ""), options: NSString.CompareOptions.caseInsensitive, range: nil, locale: nil)
        
//        if(!Txt_CustomerName.validate()){
//               AppDelegate.shared().ShowAlert(title: Constants.ERROR_HEADER, msg:"Please enter customer name \n (Character limit is 1-24 only)")
//        }
        if(!Txt_BandName.validate()){
               AppDelegate.shared().ShowAlert(title: Constants.ERROR_HEADER, msg:"Please enter brand name")
        }else if(!Txt_ModelName.validate()){
            AppDelegate.shared().ShowAlert(title: Constants.ERROR_HEADER, msg:"Please enter valid model/reference \n (Character limit is 1-20 only)")
        }else if(!Txt_Dial.validate()){
           AppDelegate.shared().ShowAlert(title: Constants.ERROR_HEADER, msg:"Please enter dial \n (Character limit is 1-20 only)")
        }
        else  if(!Txt_Serial.validate()){
            AppDelegate.shared().ShowAlert(title: Constants.ERROR_HEADER, msg:"Please enter valid serial number \n (Character limit is 1-20 only)")
            
        } else  if(!Txt_Estimated.validate()){
            AppDelegate.shared().ShowAlert(title: Constants.ERROR_HEADER, msg:"Please enter estimated value")
        }
        else  if(!Txt_Case.validate()){
            AppDelegate.shared().ShowAlert(title: Constants.ERROR_HEADER, msg:"Please enter case size")
        }
            
        else  if(Btn_Matel.titleLabel?.text == "Other") && Txt_Other.text == "" {
            AppDelegate.shared().ShowAlert(title: Constants.ERROR_HEADER, msg:"Please enter other material")
        }
        else if(Btn_Material.titleLabel?.text == "Other") && txtother.text == "" {
                AppDelegate.shared().ShowAlert(title: Constants.ERROR_HEADER, msg: "Please enter other GEM MATERIAL")
            }
            
        else  if(WindID == ""){
           AppDelegate.shared().ShowAlert(title: Constants.ERROR_HEADER, msg: "Please select any wind")
            
        }   else  if(items.count == 1){
            AppDelegate.shared().ShowAlert(title: Constants.ERROR_HEADER, msg: "Please upload at least one picture")
            
        }
        else if(!txtcolor.isHidden) && txtcolor.text == "" && result != .orderedSame{
                 AppDelegate.shared().ShowAlert(title: Constants.ERROR_HEADER, msg: "Please Enter Color info.")
            }
        else {
            
                        Btn_Next.transform = CGAffineTransform(scaleX: 0.9, y: 0.9)
                        Btn_Next.isUserInteractionEnabled = true
                        UIView.animate(withDuration: 2.0,
                                       delay: 0,
                                       usingSpringWithDamping: 0.2,
                                       initialSpringVelocity: 6.0,
                                       options: .allowUserInteraction,
                                       animations: { [weak self] in
                                        self?.Btn_Next.transform = .identity
                                        
                                        Timer.scheduledTimer(timeInterval: 0.7,
                                                             target: self!,                                                            selector:#selector(WatchesViewController.update),
                                                             userInfo: nil,
                                                             repeats: false)
                                        
 
                                        
 
                            },
                                       completion: nil)
            
            
            
        }
    }
    
    @IBAction func SegmentParts(_ sender: Any) {
        switch Segment_Parts.selectedSegmentIndex {
        case 0:
            SelectSeg = "1"
            break;
        case 1:
            SelectSeg = "0"
            break;
        default:
            SelectSeg = "1"
            break;
        }
    }
    
    
//MARK: - Methods(Functions)☝🏻😳
    
    // @objc selector expected for Timer
    @objc func update() {
        
        View = false
        
        var Comment:String = ""
        
        if Txt_Comments.text == "Additional Info" {
            Comment = "N/A"
        }
        else{
            Comment = Txt_Comments.text!
        }
        
        var MatelTypeID:String = ""
        if MatelID != ""
        {
            MatelTypeID = MatelID as String
        }
        
        var WindTypeID:String = ""
        if WindID != ""{
            WindTypeID = WindID as String
        }
        
        var GetWind:String = ""
        if Btn_Wind.titleLabel?.text != "Wind :"
        {
            GetWind = (Btn_Wind.titleLabel?.text!)!
        }
        
        var MaterialType:String = ""
        if GetOther == true{
            MaterialType = Txt_Other.text!
        }
        
        var GetMatel:String = ""
        if GetOther == true
        {
            GetMatel = Txt_Other.text!
        }
        else
        {
             GetMatel = (Btn_Matel.titleLabel?.text!)!
        }
        
        var GetMatelGem:String = ""
        if GetOtherGem == true{
            GetMatelGem = txtother.text!
        }
        else
        {
           GetMatelGem = Btn_Material.titleLabel!.text!
        }
            
        AppDelegate.shared().GETData = ["CustomerName": Txt_CustomerName.text ?? "", "CaratDiamondWeight": Txt_Diamond.text ?? "0", "BrandName": Txt_BandName.text!,"dial": Txt_Dial.text!, "SerialNo": Txt_Serial.text!, "WindTypeID": WindTypeID, "ModelName" : Txt_ModelName.text!, "caseSize" : Txt_Case.text!, "isAllPartOriginal" : SelectSeg as String, "MaterialTypeID" : MatelTypeID, "EstimatedValue" : Txt_Estimated.text!, "MaterialType":MaterialType,"CustomerComment": Comment, "ItemTypeID":"2", "Material": GetMatel, "Wind": GetWind,
       "GemMaterial":GetMatelGem,
       "WatchMaterialTypeID":MaterialArr[MaterialArrIndex]["MaterialTypeID"] as! String, "clearity_start":clearity_start,"clearity_end":clearity_end,"color_start":color_start,"color_end":color_end,"clearity_start_id":clearity_start_id,"clearity_end_id":clearity_end_id,"color_start_id":color_start_id,"color_end_id":color_end_id,
       "morecolor":txtcolor.text ?? ""
        ] as! [String : String]
        print("Dictionary \(AppDelegate.shared().GETData)")
        
        if items.count > 1 {
            items.remove(at: 0)
            AppDelegate.shared().ImageItem = items
        }
        
        if (SelectSeg == "1")
        {
           // Img_Watch.image = UIImage(named : "BandBezalDialWatch")
            AppDelegate.shared().WatchImageitem.append(UIImage(named : "BandBezalDialWatch")!)
//            let storyboard = UIStoryboard(name:"Main", bundle:nil)
//            let home = storyboard.instantiateViewController(withIdentifier: "ShippingViewController")as! ShippingViewController
//            self.navigationController?.pushViewController(home, animated: true)
            
            let storyboard = UIStoryboard(name:"Main", bundle:nil)
            let home = storyboard.instantiateViewController(withIdentifier: "ConfirmOrderViewController")as! ConfirmOrderViewController
            print("Array \(AppDelegate.shared().ImageItem)")
            home.selectedpart = NSMutableString(string: "123")
            home.ISWatchSide = true
            self.navigationController?.pushViewController(home, animated: true)
        }
        else{
            let storyboard = UIStoryboard(name:"Main", bundle:nil)
            let home = storyboard.instantiateViewController(withIdentifier: "WatchPartViewController")as! WatchPartViewController
            print("Array \(AppDelegate.shared().ImageItem)")
            self.navigationController?.pushViewController(home, animated: true)
        }
    }
    
    func DropDown(Array : [[String:AnyObject]], Name : String, ButtonName : String, Button : UIButton)
    {
        self.view.endEditing(true)
        var array = [NSString]()
        for var i in (0..<Array.count)
        {
            var dict = Array[i][Name]
            print("ggg \(dict)")
            array.append(dict as! NSString)
        }
        
        Dropobj = DropDownListView.init(title: ButtonName, options: array, xy: CGPoint.init(x: Button.frame.origin.x, y: Button.frame.origin.y+40), size: CGSize.init(width: Button.frame.size.width, height: 330), isMultiple: false, isQuestionClinicle: true)
        Dropobj.delegate = self
        Dropobj.show(in: Scoll_Vw, animated: true)
        Dropobj.setBackGroundDropDown_R(18.0, g: 64.0, b: 118.0, alpha: 1.0)
    }
    
    func DropDown1(Array : [[String:AnyObject]], Name : String, ButtonName : String, Button : UIButton)
    {
        self.view.endEditing(true)
        var array = [NSString]()
        for var i in (0..<Array.count)
        {
            var dict = Array[i][Name]
            print("ggg \(dict)")
            array.append(dict as! NSString)
        }
        
        Dropobj = DropDownListView.init(title: ButtonName, options: array, xy: CGPoint.init(x: Button.frame.origin.x, y: Button.frame.origin.y+40), size: CGSize.init(width: Button.frame.size.width, height: 330), isMultiple: false, isQuestionClinicle: true)
        Dropobj.delegate = self
        Dropobj.show(in: bottomvw, animated: true)
        Dropobj.setBackGroundDropDown_R(18.0, g: 64.0, b: 118.0, alpha: 1.0)
    }
    
    func DropDown2(Array : [[String:AnyObject]], Name : String, ButtonName : String, Button : UIButton)
    {
        var array = [NSString]()
        for var i in (0..<Array.count)
        {
            var dict = Array[i][Name]
            print("ggg \(dict)")
            array.append(dict! as! NSString)
        }
        print(array)
         Btn_Material.setTitle(Array[0][Name] as? String, for: .normal)
        Dropobj = DropDownListView.init(title: ButtonName, options: array, xy: CGPoint.init(x: Button.frame.origin.x, y: Button.frame.origin.y+40), size: CGSize.init(width: Button.frame.size.width, height: 330), isMultiple: false, isQuestionClinicle: true)
        Dropobj.delegate = self
        Dropobj.show(in: Vw_Under, animated: true)
        Dropobj.setBackGroundDropDown_R(18.0, g: 64.0, b: 118.0, alpha: 1.0)
    }


    func setupAlerts(){
        
       // Txt_CustomerName.addRegx(Constants.REGEX_Note_USER_NAME, withMsg: "Limit of characters allowed 1 - 24 only")
        Txt_BandName.addRegx(Constants.REGEX_Note_USER_NAME, withMsg: "Enter Valid Brand Name")
        Txt_ModelName.addRegx(Constants.REGEX_SERIALNO, withMsg: "Enter valid Model Name.")
        Txt_Serial.addRegx(Constants.REGEX_SERIALNO, withMsg: "Enter Valid Serial")
        Txt_Dial.addRegx(Constants.REGEX_SERIALNO, withMsg: "Enter Valid Dial Value")
        Txt_Case.addRegx(Constants.REGEX_WEIGHT_DEFAULT, withMsg: "Enter Valid Case Size")
        Txt_Estimated.addRegx(Constants.REGEX_WEIGHT_DEFAULT, withMsg: "Enter valid Estimated Value")
        Txt_Diamond.addRegx(Constants.REGEX_WEIGHT_DEFAULT, withMsg: "Enter Valid Diamond Weight")
         txtcolor.addRegx(Constants.REGEX_Note_USER_LIMIT, withMsg: "Please Enter Color info.")
         txtother.addRegx(Constants.REGEX_Note_USER_LIMIT, withMsg: "Please Enter other Gem Material")
         Txt_Other.addRegx(Constants.REGEX_Note_USER_LIMIT, withMsg: "Other material is required")
    }
    
//MARK: - Delegates✋🏻😇
    
    //MARK: -- CollectionView Delegates
    
    // tell the collection view how many cells to make
    func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
        return self.items.count
    }
    
    // make a cell for each cell index path
    func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
        
        // get a reference to our storyboard cell
        let cell = collectionView.dequeueReusableCell(withReuseIdentifier: reuseIdentifier, for: indexPath as IndexPath) as! UploadImageCollectionViewCell
        
        if indexPath.item != 0 {
            cell.Btn_Close.isHidden = false
            cell.Btn_Close.addTarget(self, action: #selector(WatchesViewController.connected), for: .touchUpInside)
            cell.Btn_Close.tag = indexPath.row
        }
        else{
            cell.Btn_Close.isHidden = true
        }
        
        cell.Img_Vw.layer.cornerRadius = cell.Img_Vw.frame.size.height/2
        cell.Img_Vw.image = self.items[indexPath.row]
        cell.Img_Vw.layer.masksToBounds = true
        
        return cell
    }
    
    @objc func connected(sender: UIButton){
        let buttonTag = sender.tag
        
        items.remove(at: buttonTag)
        Vw_Collection.reloadData()
    }
    
    //didSelectItemAt
    func collectionView(_ collectionView: UICollectionView, didSelectItemAt indexPath: IndexPath) {
        // handle tap events
        print("You selected cell #\(indexPath.item)!")
        
        if indexPath.item == 0 {
//            imagePicker.allowsEditing = false
//            imagePicker.sourceType = .photoLibrary
//
//            present(imagePicker, animated: true, completion: nil)
            print("Imageview Clicked")
            let alertViewController = UIAlertController(title: nil, message: nil, preferredStyle: .actionSheet)
            let camera = UIAlertAction(title: "Take Photo", style: .default, handler: { (alert) in
                self.openCamera()
            })
            let gallery = UIAlertAction(title: "Choose from Gallery", style: .default) { (alert) in
                self.openGallary()
            }
            let cancel = UIAlertAction(title: "Cancel", style: .cancel) { (alert) in
                
            }
            alertViewController.addAction(camera)
            alertViewController.addAction(gallery)
            alertViewController.addAction(cancel)
            self.present(alertViewController, animated: true, completion: nil)
        }
    }
    
    //didDeselectItemAt
    func collectionView(_ collectionView: UICollectionView, didDeselectItemAt indexPath: IndexPath) {
        let cell = collectionView.cellForItem(at: indexPath)
        cell?.backgroundColor = UIColor.cyan
    }
    
    //collectionViewLayout
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, sizeForItemAt indexPath: IndexPath) -> CGSize {
        let width = UIScreen.main.bounds.width
        return CGSize(width: (width - 10)/4, height: (width - 10)/4) // width & height are the same to make a square cell
    }
    
//MARK: -- ImageView Delegates
    
    func openCamera() {
        View = true
        if UIImagePickerController.isSourceTypeAvailable(UIImagePickerControllerSourceType.camera) {
            imagePicker.delegate = self
            imagePicker.sourceType = UIImagePickerControllerSourceType.camera
            imagePicker.allowsEditing = false
            self .present(imagePicker, animated: true, completion: nil)
        }
        else {
            let alertWarning = UIAlertView(title:"Warning", message: "You don't have camera", delegate:nil, cancelButtonTitle:"OK", otherButtonTitles:"")
            alertWarning.show()
        }
    }
    
    func openGallary() {
        View = true
        if UIImagePickerController.isSourceTypeAvailable(UIImagePickerControllerSourceType.photoLibrary) {
            imagePicker.delegate = self
            imagePicker.sourceType = UIImagePickerControllerSourceType.photoLibrary
            imagePicker.allowsEditing = false
            self.present(imagePicker, animated: true, completion: nil)
        }
    }
    
    //imagePickerController
    public func imagePickerController(_ picker: UIImagePickerController, didFinishPickingMediaWithInfo info: [String : Any])
    {
        // assign pickup image into varibale
        var selectedimage : UIImage!
        
        if let editedImage = info[UIImagePickerControllerEditedImage] as? UIImage
        {
            selectedimage =  editedImage.fixedOrientation()
        }
        else
        {
            selectedimage = info[UIImagePickerControllerOriginalImage] as? UIImage
            selectedimage =  selectedimage.fixedOrientation()
            
        }
        
        View = true
        
        items.append(selectedimage)
        print(items)
        
        if items.count != 0 {
            Vw_Collection.reloadData()
        }
        
        dismiss(animated: true, completion: nil)
    }
    
    //imagePickerControllerDidCancel
    public func imagePickerControllerDidCancel(_ picker: UIImagePickerController)
    {
        dismiss(animated: true, completion: nil)
    }
    
    
//MARK: -- DropDown Delegate
    func dropDownListView(_ dropdownListView: DropDownListView!, didSelectedIndex anIndex: Int) {
       print(dropdownListView)
        if (NameString .isEqual(to: "Wind"))
        {
            print("list\(ArrWind[anIndex])")
            
            let dict = ArrWind[anIndex]["WindType"]
            Btn_Wind.setTitle(dict as? String, for: .normal)
            
            let ID = ArrWind[anIndex]["WindTypeID"]
            WindID = ID as! NSString
        }
        else if (NameString .isEqual(to: "Matel")){
            print("list\(ArrMatel[anIndex])")
            
            let dict = ArrMatel[anIndex]["MaterialType"]
            Btn_Matel.setTitle(dict as? String, for: .normal)
            
            let ID = ArrMatel[anIndex]["MaterialTypeID"]
            MatelID = ID as! NSString
            
            if (dict?.isEqual("Other"))!
            {
                GetOther = true
                
                Vw_Under.frame = CGRect.init(x: Vw_Under.frame.origin.x, y: Vw_Under.frame.origin.y + 50, width: Vw_Under.frame.size.width, height: Vw_Under.frame.height)
                Txt_Other.isHidden = false
            }
            else{
                if GetOther == true{
                    GetOther = false
                    
                    Vw_Under.frame = CGRect.init(x: Vw_Under.frame.origin.x, y: Vw_Under.frame.origin.y - 50, width: Vw_Under.frame.size.width, height: Vw_Under.frame.height)
                    Txt_Other.isHidden = true
                }
            }
        }
        else if (NameString .isEqual(to: "Gem"))
        {
            GetOtherGem = false
            let dict = MaterialArr[anIndex]["MaterialType"]
            if (dict?.isEqual("Other"))!
            {
                GetOtherGem = true
                setslider()
                vwgem.frame.origin.y = 250;
                 txtother.isHidden = false;
                 bottomvw.frame.origin.y = 500;
                Vw_Clarity.isHidden = false;
                claritytextview.isHidden = false;
              //  Vw_Color.isHidden = false;
               // colortextview.isHidden = false;
                lblline.isHidden = false;
                lblquality.isHidden = false;
                lblclarity.isHidden = false;
                Txt_Diamond.isHidden = false;
            }
                
            else if (dict?.isEqual("None"))!
            {
                setslider()
                 vwgem.frame.origin.y = 200;
                 txtother.isHidden = true;
                 bottomvw.frame.origin.y = 180;
                 Vw_Clarity.isHidden = true;
                 claritytextview.isHidden = true;
                 Vw_Color.isHidden = true;
                 colortextview.isHidden = true;
                 lblline.isHidden = false;
                 lblquality.isHidden = true;
                 lblclarity.isHidden = true;
                 Txt_Diamond.isHidden = true;
                
                clearity_start = "0"
                clearity_end = "0"
                clearity_start_id = "0"
                clearity_end_id = "0"
                color_start = "0"
                color_end = "0"
                color_start_id = "0"
                color_end_id = "0"
                
               
            }
                
            else{
                 setslider()
                vwgem.frame.origin.y = 200;
                 txtother.isHidden = true;
                 bottomvw.frame.origin.y = 450;
                Vw_Clarity.isHidden = false;
                claritytextview.isHidden = false;
             //   Vw_Color.isHidden = false;
              //  colortextview.isHidden = false;
                lblline.isHidden = false;
                lblquality.isHidden = false;
                lblclarity.isHidden = false;
                Txt_Diamond.isHidden = false;
//                Txt_Caret.text = ""
//                Txt_Caret.isEnabled=true
               // Vw_Under.frame = CGRect.init(x: Vw_Under.frame.origin.x, y: 440, width: Vw_Under.frame.size.width, height: Vw_Under.frame.size.height)
//                if OtherBool == true{
//                    OtherBool = false
//                     NoneBool = false
//                    Txt_Other.isHidden = true
//                    Vw_Other.frame = CGRect.init(x: Vw_Other.frame.origin.x, y: 365, width: Vw_Other.frame.size.width, height: Vw_Other.frame.size.height)
//                }
//                else{
//                    OtherBool = false
//                }
            }
            
            MaterialArrIndex = anIndex

            Btn_Material.setTitle(dict as? String, for: .normal)
        }
    }
    
    func dropDownListView(_ dropdownListView: DropDownListView!, datalist ArryData: NSMutableArray!) {
        //NSLog(@"list %@",[ArryData componentsJoinedByString:@"--"]);
        print(dropdownListView)
        if (ArryData.count != 0){
            
            if (NameString .isEqual(to: "Wind"))
            {
                print("list \(ArryData.componentsJoined(by: ", "))")
                Btn_Wind.setTitle(ArryData.componentsJoined(by: ", "), for: .normal)
            }
            else if (NameString .isEqual(to: "Matel")){
                print("list \(ArryData.componentsJoined(by: ", "))")
                Btn_Matel.setTitle(ArryData.componentsJoined(by: ", "), for: .normal)
            }
            else if (NameString .isEqual(to: "Gem"))
            {
                print("list \(ArryData.componentsJoined(by: ", "))")
                Btn_Material.setTitle(ArryData.componentsJoined(by: ", "), for: .normal)
            }
            
        }
        else{
            
            if (NameString .isEqual(to: "Wind"))
            {
                Btn_Wind.setTitle("Wind :", for: .normal)
            }
            else if (NameString .isEqual(to: "Matel")){
                Btn_Matel.setTitle("Metal/Material :", for: .normal)
            }
            else if (NameString .isEqual(to: "Gem"))
            {
                Btn_Material.setTitle("None", for: .normal)
            }
            
        }
    }
    
    func dropDownListViewDidCancel() {
        print("Cancel")
    }
    
    
//MARK: -- TextView Delegate
    
    func textViewDidBeginEditing(_ textView: UITextView) {
        if textView.textColor == UIColor.darkGray {
            textView.text = nil;
            textView.textColor = UIColor.black
        }
    }
    
    func textViewDidEndEditing(_ textView: UITextView) {
        if textView.text.isEmpty {
            textView.text = "Additional Info"
            textView.textColor = UIColor.darkGray
        }
    }
    
    
    
//MARK: -- SideBar Delegate
    func revealController(_ revealController: SWRevealViewController, didMoveTo position: FrontViewPosition) {
        if case .right = position {
            
            revealController.frontViewController.view.alpha = 0.5
            self.view.isUserInteractionEnabled = false
            self.revealViewController().frontViewController.view.addGestureRecognizer(self.revealViewController().panGestureRecognizer())
            self.revealViewController().frontViewController.view.addGestureRecognizer(self.revealViewController().tapGestureRecognizer())
        } else {
            
            revealController.frontViewController.view.alpha = 1
            self.revealViewController().tapGestureRecognizer()
            self.view.isUserInteractionEnabled = true
        }
    }
    
    
//MARK: -- CheckMark Delegate
    func didTap(_ checkBox: BEMCheckBox) {
        self.view.endEditing(true)
        if (checkBox==Check_Terms)
        {
            if (checkBox.on == true)
            {
                print("It's Chc")
                Btn_Next.isEnabled = true
                Btn_Next.alpha = 1.0
            }
            else{
                print("it's Loose Stone")
                Btn_Next.isEnabled = false
                Btn_Next.alpha = 0.2
            }
            
        }
    }
    

//MARK: -- Scrollview Delegate
    func scrollViewWillBeginDragging(_ scrollView: UIScrollView) {
        // execute when you drag the scrollView
        self.view.endEditing(true)
        print("scrollViewWillBeginDragging")
    }
    
    @IBAction func MaterialButtonPressed(_ sender: Any) {
//        Dropobj.isHidden = true
//        Dropobj1.isHidden = true
        NameString = "Gem"
        DropDown2(Array: MaterialArr, Name: "MaterialType", ButtonName: "GEM MATERIAL:", Button: Btn_Material)
      
    }
    
    
}
