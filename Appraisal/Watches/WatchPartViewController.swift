//
//  WatchPartViewController.swift
//  Appraisal
//
//  Created by ADMIN on 3/28/18.
//  Copyright © 2018 empiereTech. All rights reserved.
//

import UIKit
import BEMCheckBox
import SWRevealViewController

class WatchPartViewController: UIViewController,BEMCheckBoxDelegate,SWRevealViewControllerDelegate {

//MARK: - Outlets🤔
    
    //View
    @IBOutlet var Vw_side: UIView!
    @IBOutlet var Vw_Scroll: UIScrollView!
    
    //Button
    @IBOutlet var Btn_Band: UIButton!
    @IBOutlet var Btn_Bezel: UIButton!
    @IBOutlet var Btn_Dial: UIButton!
    @IBOutlet var Btn_Submit: UIButton!
    
    //CheckBox
    @IBOutlet var Check_Band: BEMCheckBox!
    @IBOutlet var Check_Bezel: BEMCheckBox!
    @IBOutlet var Check_Dial: BEMCheckBox!
    
    //ImageView
    @IBOutlet var Img_Watch: UIImageView!
    
    
//MARK: - Declare Variables👇🏻😬
    var WatcheNameBand:NSString = ""
    var WatcheNameBezel:NSString = ""
    var WatcheNameDial:NSString = ""
    //write this in your class
    var BandHighLighted:Bool = false
    var BezelHighLighted:Bool = false
    var DialHighLighted:Bool = false
    var selectedpart:NSMutableString = ""
    var a:String = ""
    var b:String = ""
    var c:String = ""

//MARK: - Happy Coding😊
    
    //viewDidLoad
    override func viewDidLoad() {
        super.viewDidLoad()

        // Do any additional setup after loading the view.
        
        //NavigationBar
        self.navigationController?.navigationBar.isHidden = false;
        self.title = "Choose Authentic Parts"
        
        navigationController?.navigationBar.barTintColor = UIColor.white
        navigationController?.navigationBar.titleTextAttributes = [NSAttributedStringKey.foregroundColor: UIColor(red: 18/255, green: 64/255, blue: 118/255, alpha: 1),NSAttributedStringKey.font: UIFont(name: "Montserrat-Medium", size: 17)!]
        navigationController?.navigationBar.tintColor = UIColor(red: 18/255, green: 64/255, blue: 118/255, alpha: 1)
        /*self.navigationItem.setHidesBackButton(true, animated:true)
        
        let menuButton = UIButton(type: .system)
        menuButton.setImage(#imageLiteral(resourceName: "Menu").withRenderingMode(.alwaysOriginal), for: .normal)
        menuButton.frame = CGRect(x: 0, y: 0, width: 34, height: 34)
        navigationItem.leftBarButtonItem = UIBarButtonItem(customView: menuButton)
        
        //SideBar
        if revealViewController() != nil
        {
            self.revealViewController().delegate = self
            menuButton.addTarget(revealViewController(), action: "revealToggle:", for: .touchUpInside)
            self.navigationController?.navigationBar.addGestureRecognizer(self.revealViewController().panGestureRecognizer())
            view.addGestureRecognizer(self.revealViewController().panGestureRecognizer())
        }*/
        
        //ButtonLayoutChanges
        Btn_Submit.layer.cornerRadius = Btn_Submit.frame.size.height/2.0
        Btn_Band.layer.cornerRadius = Btn_Band.frame.size.height/2.0
        Check_Band.setOn(true, animated: true)
        Check_Band.isUserInteractionEnabled = false
        Btn_Dial.layer.cornerRadius = Btn_Dial.frame.size.height/2.0
        Check_Dial.setOn(true, animated: true)
        Check_Dial.isUserInteractionEnabled = false
        Btn_Bezel.layer.cornerRadius = Btn_Bezel.frame.size.height/2.0
        Check_Bezel.setOn(true, animated: true)
        Check_Bezel.isUserInteractionEnabled = false
        
        if (UIDevice.current.userInterfaceIdiom == .pad)
        {
            print("iPad")
        }
        else if (UIScreen.main.bounds.size.height == 568.0)
        {
            print("iphone5")
            Vw_Scroll.contentSize = CGSize(width: self.view.frame.size.width, height: 700)
        }
            
        else if (UIScreen.main.bounds.size.height == 480.0)
        {
            print("iphone4")
            Vw_Scroll.contentSize = CGSize(width: self.view.frame.size.width, height: 600)
        }
        else
        {
            print("OtherPhones")
            Vw_Scroll.contentSize = CGSize(width: self.view.frame.size.width, height: 700)
        }
    }

    
//MARK: - Actions👊🏻😠
    
    //BandButtonPressed
    @IBAction func BandButtonPressed(_ sender: Any) {
        
        if let button = sender as? UIButton {
            if BandHighLighted == false{
                a = "1"
               //  a.append("1")
                button.isHighlighted = true;
                Btn_Band.backgroundColor = UIColor(red: 18/255.0, green: 64/255.0, blue: 118/255.0, alpha: 1.0)
                Check_Band.isHidden = false
                BandHighLighted = true
                WatcheNameBand = "Band"
                if WatcheNameBezel.length>0 {
                    if WatcheNameBezel.isEqual(to: "Bezel")
                    {
                        Img_Watch.image = UIImage(named : "BandBezalWatch")
                    }
                    else{
                        Img_Watch.image = UIImage(named : "BandWatch")
                    }
                }
                if WatcheNameDial.length>0 {
                    if WatcheNameDial.isEqual(to: "Dial")
                    {
                        Img_Watch.image = UIImage(named : "BandDialWatch")
                    }
                    else{
                        Img_Watch.image = UIImage(named : "BandWatch")
                    }
                }
                if WatcheNameBezel.length>0 && WatcheNameDial.length>0 {
                    if WatcheNameBezel.isEqual(to: "Bezel") && WatcheNameDial.isEqual(to: "Dial")
                    {
                        Img_Watch.image = UIImage(named : "BandBezalDialWatch")
                    }
                    else{
                        Img_Watch.image = UIImage(named : "BandWatch")
                    }
                }
                if WatcheNameBezel.length == 0 && WatcheNameDial.length == 0{
                     Img_Watch.image = UIImage(named : "BandWatch")
                }
                
            }else{
                WatcheNameBand = ""
                a = ""
             // a = a.replacingOccurrences(of: "1", with: "")
                button.isHighlighted = false;
                Btn_Band.backgroundColor = UIColor.gray
                Check_Band.isHidden = true
                BandHighLighted = false
                
                if WatcheNameBezel.length>0 {
                    if WatcheNameBezel.isEqual(to: "Bezel")
                    {
                        Img_Watch.image = UIImage(named : "BezalWatch")
                    }
                    else{
                        Img_Watch.image = UIImage(named : "Watch")
                    }
                }
                if WatcheNameDial.length>0 {
                    if WatcheNameDial.isEqual(to: "Dial")
                    {
                        Img_Watch.image = UIImage(named : "DialWatch")
                    }
                    else{
                        Img_Watch.image = UIImage(named : "Watch")
                    }
                }
                if WatcheNameBezel.length>0 && WatcheNameDial.length>0 {
                    if WatcheNameBezel.isEqual(to: "Bezel") && WatcheNameDial.isEqual(to: "Dial")
                    {
                        Img_Watch.image = UIImage(named : "BezalDial")
                    }
                    else{
                        Img_Watch.image = UIImage(named : "Watch")
                    }
                }
                if WatcheNameBezel.length == 0 && WatcheNameDial.length == 0{
                    Img_Watch.image = UIImage(named : "Watch")
                }
            }
        }
    }
    
    //BezelButtonPressed
    @IBAction func BezelButtonPressed(_ sender: Any) {
        
        if let button = sender as? UIButton {
            if BezelHighLighted == false{
                b = "3"
             //  a.append("3")
                button.isHighlighted = true;
                BezelHighLighted = true
                Btn_Bezel.backgroundColor = UIColor(red: 18/255.0, green: 64/255.0, blue: 118/255.0, alpha: 1.0)
                Check_Bezel.isHidden = false
                WatcheNameBezel = "Bezel"
                
                if WatcheNameBand.length>0 {
                    if WatcheNameBand.isEqual(to: "Band")
                    {
                        Img_Watch.image = UIImage(named : "BandBezalWatch")
                    }
                    else{
                        Img_Watch.image = UIImage(named : "BezalWatch")
                    }
                }
                if WatcheNameDial.length>0 {
                    if WatcheNameDial.isEqual(to: "Dial")
                    {
                        Img_Watch.image = UIImage(named : "BezalDial")
                    }
                    else{
                        Img_Watch.image = UIImage(named : "BezalWatch")
                    }
                }
                if WatcheNameBand.length>0 && WatcheNameDial.length>0 {
                    if WatcheNameDial.isEqual(to: "Dial") && WatcheNameBand.isEqual(to: "Band")
                    {
                        Img_Watch.image = UIImage(named : "BandBezalDialWatch")
                    }
                    else{
                        Img_Watch.image = UIImage(named : "BezalWatch")
                    }
                }
                if WatcheNameBand.length == 0 && WatcheNameDial.length == 0{
                    Img_Watch.image = UIImage(named : "BezalWatch")
                }
                
            }else{
                
                WatcheNameBezel = ""
                b = ""
               // a = a.replacingOccurrences(of: "3", with: "")
                button.isHighlighted = false;
                Btn_Bezel.backgroundColor = UIColor.gray
                Check_Bezel.isHidden = true
                BezelHighLighted = false
                
                if WatcheNameBand.length>0 {
                    if WatcheNameBand.isEqual(to: "Band")
                    {
                        Img_Watch.image = UIImage(named : "BandWatch")
                    }
                    else{
                        Img_Watch.image = UIImage(named : "Watch")
                    }
                }
                if WatcheNameDial.length>0 {
                    if WatcheNameDial.isEqual(to: "Dial")
                    {
                        Img_Watch.image = UIImage(named : "DialWatch")
                    }
                    else{
                        Img_Watch.image = UIImage(named : "Watch")
                    }
                }
                if WatcheNameBand.length>0 && WatcheNameDial.length>0 {
                    if WatcheNameDial.isEqual(to: "Dial") && WatcheNameBand.isEqual(to: "Band")
                    {
                        Img_Watch.image = UIImage(named : "BandDialWatch")
                    }
                    else{
                        Img_Watch.image = UIImage(named : "Watch")
                    }
                }
                if WatcheNameBand.length == 0 && WatcheNameDial.length == 0{
                    Img_Watch.image = UIImage(named : "Watch")
                }
            }
        }
    }
    
    //DialButtonPressed
    @IBAction func DialButtonPressed(_ sender: Any) {
        
        
        if let button = sender as? UIButton {
            if DialHighLighted == false{
                c = "2"
               // a.append("2")
                button.isHighlighted = true;
                DialHighLighted = true
                Btn_Dial.backgroundColor = UIColor(red: 18/255.0, green: 64/255.0, blue: 118/255.0, alpha: 1.0)
                Check_Dial.isHidden = false
                WatcheNameDial = "Dial"
                
                if WatcheNameBand.length>0 {
                    if WatcheNameBand.isEqual(to: "Band")
                    {
                        Img_Watch.image = UIImage(named : "BandDialWatch")
                    }
                    else{
                        Img_Watch.image = UIImage(named : "DialWatch")
                    }
                }
                if WatcheNameBezel.length>0 {
                    if WatcheNameBezel.isEqual(to: "Bezel")
                    {
                        Img_Watch.image = UIImage(named : "BezalDial")
                    }
                    else{
                        Img_Watch.image = UIImage(named : "DialWatch")
                    }
                }
                if WatcheNameBand.length>0 && WatcheNameBezel.length>0 {
                    if WatcheNameBezel.isEqual(to: "Bezel") &&  WatcheNameBand.isEqual(to: "Band")
                    {
                        Img_Watch.image = UIImage(named : "BandBezalDialWatch")
                    }
                    else{
                        Img_Watch.image = UIImage(named : "DialWatch")
                    }
                }
                if WatcheNameBand.length == 0 && WatcheNameBezel.length == 0{
                    Img_Watch.image = UIImage(named : "DialWatch")
                }
                
            }else{
                WatcheNameDial = ""
                c = ""
               // a = a.replacingOccurrences(of: "2", with: "")
                button.isHighlighted = false;
                Btn_Dial.backgroundColor = UIColor.gray
                Check_Dial.isHidden = true
                DialHighLighted = false
                
                if WatcheNameBand.length>0 {
                    if WatcheNameBand.isEqual(to: "Band")
                    {
                        Img_Watch.image = UIImage(named : "BandWatch")
                    }
                    else{
                        Img_Watch.image = UIImage(named : "Watch")
                    }
                }
                if WatcheNameBezel.length>0 {
                    if WatcheNameBezel.isEqual(to: "Bezel")
                    {
                        Img_Watch.image = UIImage(named : "BezalWatch")
                    }
                    else{
                        Img_Watch.image = UIImage(named : "Watch")
                    }
                }
                if WatcheNameBand.length>0 && WatcheNameBezel.length>0 {
                    if WatcheNameBezel.isEqual(to: "Bezel") &&  WatcheNameBand.isEqual(to: "Band")
                    {
                        Img_Watch.image = UIImage(named : "BandBezalWatch")
                    }
                    else{
                        Img_Watch.image = UIImage(named : "Watch")
                    }
                }
                if WatcheNameBand.length == 0 && WatcheNameBezel.length == 0{
                    Img_Watch.image = UIImage(named : "Watch")
                }
            }
        }
    }
    
    @IBAction func SubmitButtonPressed(_ sender: Any) {
        
        print("SELECTEDVALUE:",a,c,b)
        Btn_Submit.transform = CGAffineTransform(scaleX: 0.9, y: 0.9)
      //  Btn_Submit.isUserInteractionEnabled = false
        UIView.animate(withDuration: 2.0,
                       delay: 0,
                       usingSpringWithDamping: 0.2,
                       initialSpringVelocity: 6.0,
                       options: .allowUserInteraction,
                       animations: { [weak self] in
                        self?.Btn_Submit.transform = .identity
                        
                        Timer.scheduledTimer(timeInterval: 0.7,
                                             target: self!,
                                             selector: #selector(WatchesViewController.update),
                                             userInfo: nil,
                                             repeats: false)
            },
                       completion: nil)
    }
    
    
//MARK: - Methods(Functions)☝🏻😳
    
    // @objc selector expected for Timer
    @objc func update() {
        // do what should happen when timer triggers an event
        let storyboard = UIStoryboard(name:"Main", bundle:nil)
        let home = storyboard.instantiateViewController(withIdentifier: "ConfirmOrderViewController")as! ConfirmOrderViewController
        
        AppDelegate.shared().WatchImageitem.removeAll()
        AppDelegate.shared().WatchImageitem.append(Img_Watch.image!)
        home.ISWatchSide = true
        home.selectedpart = NSMutableString(string: a + c + b)
        self.navigationController?.pushViewController(home, animated: true)
    }
    
    func revealController(_ revealController: SWRevealViewController, didMoveTo position: FrontViewPosition) {
        if case .right = position {
            
            revealController.frontViewController.view.alpha = 0.5
            self.view.isUserInteractionEnabled = false
            self.revealViewController().frontViewController.view.addGestureRecognizer(self.revealViewController().panGestureRecognizer())
            self.revealViewController().frontViewController.view.addGestureRecognizer(self.revealViewController().tapGestureRecognizer())
        } else {
            
            revealController.frontViewController.view.alpha = 1
            self.revealViewController().tapGestureRecognizer()
            self.view.isUserInteractionEnabled = true
        }
    }
    
}
