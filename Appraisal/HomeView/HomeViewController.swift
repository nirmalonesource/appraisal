
//
//  HomeViewController.swift
//  Appraisal
//
//  Created by ADMIN on 3/24/18.
//  Copyright © 2018 empiereTech. All rights reserved.
//

import UIKit
import SWRevealViewController
import Alamofire
import SwiftyJSON

class HomeViewController: UIViewController,SWRevealViewControllerDelegate {
    
//MARK: - Outlets🤔
    
    //View
    @IBOutlet var Vw_Engagement: UIView!
    @IBOutlet var Vw_Watches: UIView!
    @IBOutlet var Vw_Jewelry: UIView!
    @IBOutlet var Vw_Weblinks: UIView!
    @IBOutlet var Vw_Side: UIView!
    @IBOutlet var Vw_Scroll: UIScrollView!
    
    @IBOutlet var btncallus: UIButton!
    //MARK: - Declare Variables👇🏻😬
    
    var ViewsName = NSString()
    var arrRes = [[String:AnyObject]]() //Array of dictionary
    
//MARK: - Happy Coding😊
    
    //viewDidLoad
    override func viewDidLoad() {
        super.viewDidLoad()

        //NavigationBar
        self.title = "Federal Appraisal"
        
        self.navigationController?.navigationBar.isHidden = false;
        navigationController?.navigationBar.barTintColor = UIColor.white
        if (UIScreen.main.bounds.size.height == 568.0)
        {
            print("iphone5")
            navigationController?.navigationBar.titleTextAttributes = [NSAttributedStringKey.foregroundColor: UIColor(red: 18/255, green: 64/255, blue: 118/255, alpha: 1),NSAttributedStringKey.font: UIFont(name: "Montserrat-Medium", size: 15)!]
        }
        else{
            navigationController?.navigationBar.titleTextAttributes = [NSAttributedStringKey.foregroundColor: UIColor(red: 18/255, green: 64/255, blue: 118/255, alpha: 1),NSAttributedStringKey.font: UIFont(name: "Montserrat-Medium", size: 17)!]
        }
        self.navigationItem.setHidesBackButton(true, animated:true);
        
       /* let SearchButton = UIButton(type: .system)
        SearchButton.setImage(#imageLiteral(resourceName: "SearchIcon").withRenderingMode(.alwaysOriginal), for: .normal)
        SearchButton.frame = CGRect(x: 0, y: 0, width: 34, height: 34)
        navigationItem.rightBarButtonItem = UIBarButtonItem(customView: SearchButton)*/
        
        let menuButton = UIButton(type: .system)
        menuButton.setImage(#imageLiteral(resourceName: "Menu").withRenderingMode(.alwaysOriginal), for: .normal)
        menuButton.frame = CGRect(x: 0, y: 0, width: 34, height: 34)
        navigationItem.leftBarButtonItem = UIBarButtonItem(customView: menuButton)
        
        //SideBar
        if revealViewController() != nil
        {
            self.revealViewController().delegate = self
            menuButton.addTarget(revealViewController(), action: "revealToggle:", for: .touchUpInside)
            self.navigationController?.navigationBar.addGestureRecognizer(self.revealViewController().panGestureRecognizer())
            view.addGestureRecognizer(self.revealViewController().panGestureRecognizer())
        }

        
        if (UIDevice.current.userInterfaceIdiom == .pad)
        {
            print("iPad")
        }
        else if (UIScreen.main.bounds.size.height == 568.0)
        {
            print("iphone5")
            Vw_Scroll.contentSize = CGSize(width: self.view.frame.size.width, height: 600)
            let desiredOffset = CGPoint(x: 0, y: -Vw_Scroll.contentInset.top)
            Vw_Scroll.setContentOffset(desiredOffset, animated: true)
        }
            
        else if (UIScreen.main.bounds.size.height == 480.0)
        {
            print("iphone4")
            Vw_Scroll.contentSize = CGSize(width: self.view.frame.size.width, height: 600)
        }
        else
        {
            print("OtherPhones")
            Vw_Scroll.contentSize = CGSize(width: self.view.frame.size.width, height: 600)
        }
        
        //Methods
        ViewLayout()
        if !(AppDelegate.shared().ArrGetAllType.count > 0)
        {
              if NetworkReachabilityManager()!.isReachable {
             CallMyMethod()
        } else {
         AppDelegate.shared().HideHUD()
            AppDelegate.shared().ShowAlert(title: Constants.ERROR_NO_INTERNET, msg: "Unable to connect Internet, Please try after some time.")
        }
        }
    }
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(true)
        
        Vw_Engagement.isUserInteractionEnabled = true
        Vw_Jewelry.isUserInteractionEnabled = true
        Vw_Watches.isUserInteractionEnabled = true
        Vw_Weblinks.isUserInteractionEnabled = true
    }
    
    
    
    
//MARK: - Actions👊🏻😠
    @objc func buttonAction(_ sender: UIBarButtonItem) {
        
    }
    
    @IBAction func callus_click(_ sender: UIButton) {
        
        if let url = URL(string: "tel://6468087797"), UIApplication.shared.canOpenURL(url) {
            if #available(iOS 10, *) {
                UIApplication.shared.open(url)
            } else {
                UIApplication.shared.openURL(url)
            }
        }

    }
    
//MARK: - Methods(Functions)☝🏻😳
    
    func revealController(_ revealController: SWRevealViewController, didMoveTo position: FrontViewPosition) {
        if case .right = position {
            
            revealController.frontViewController.view.alpha = 0.5
            self.view.isUserInteractionEnabled = false
            self.revealViewController().frontViewController.view.addGestureRecognizer(self.revealViewController().panGestureRecognizer())
            self.revealViewController().frontViewController.view.addGestureRecognizer(self.revealViewController().tapGestureRecognizer())
        } else {
            
            revealController.frontViewController.view.alpha = 1
            self.revealViewController().tapGestureRecognizer()
            self.view.isUserInteractionEnabled = true
        }
    }
    
    func ViewLayout()
    {
        btncallus.layer.cornerRadius = 10.0
        btncallus.isUserInteractionEnabled = true
        btncallus.layer.masksToBounds = false
        btncallus.layer.shadowOffset = CGSize.zero
        btncallus.layer.shadowRadius = 5
        btncallus.layer.shadowOpacity = 0.2
        btncallus.backgroundColor = #colorLiteral(red: 0.003921568627, green: 0.2666666667, blue: 0.1294117647, alpha: 1)
        
        Vw_Engagement.layer.cornerRadius = 10.0
        Vw_Engagement.isUserInteractionEnabled = true
        Vw_Engagement.layer.masksToBounds = false
        Vw_Engagement.layer.shadowOffset = CGSize.zero
        Vw_Engagement.layer.shadowRadius = 5
        Vw_Engagement.layer.shadowOpacity = 0.2
        let Engagementtap = UITapGestureRecognizer(target: self, action: #selector(EngagementAction))
        Engagementtap.numberOfTapsRequired = 1
        Vw_Engagement.addGestureRecognizer(Engagementtap)
        
        Vw_Jewelry.layer.cornerRadius = 10.0
        Vw_Jewelry.isUserInteractionEnabled = true
        Vw_Jewelry.layer.masksToBounds = false
        Vw_Jewelry.layer.shadowOffset = CGSize.zero
        Vw_Jewelry.layer.shadowRadius = 5
        Vw_Jewelry.layer.shadowOpacity = 0.2
        let Jewelrytap = UITapGestureRecognizer(target: self, action: #selector(JewelryAction))
        Jewelrytap.numberOfTapsRequired = 1
        Vw_Jewelry.addGestureRecognizer(Jewelrytap)
        
        Vw_Watches.layer.cornerRadius = 10.0
        Vw_Watches.isUserInteractionEnabled = true
        Vw_Watches.layer.masksToBounds = false
        Vw_Watches.layer.shadowOffset = CGSize.zero
        Vw_Watches.layer.shadowRadius = 5
        Vw_Watches.layer.shadowOpacity = 0.2
        let Watchestap = UITapGestureRecognizer(target: self, action: #selector(WatchesAction))
        Watchestap.numberOfTapsRequired = 1
        Vw_Watches.addGestureRecognizer(Watchestap)
        
        Vw_Weblinks.layer.cornerRadius = 10.0
        Vw_Weblinks.isUserInteractionEnabled = true
        Vw_Weblinks.layer.masksToBounds = false
        Vw_Weblinks.layer.shadowOffset = CGSize.zero
        Vw_Weblinks.layer.shadowRadius = 5
        Vw_Weblinks.layer.shadowOpacity = 0.2
        let Weblinkstap = UITapGestureRecognizer(target: self, action: #selector(WeblinksAction))
        Weblinkstap.numberOfTapsRequired = 1
        Vw_Weblinks.addGestureRecognizer(Weblinkstap)
    }

    
    @objc func EngagementAction(){
        // do other task
        
        Vw_Engagement.transform = CGAffineTransform(scaleX: 0.9, y: 0.9)
        Vw_Engagement.isUserInteractionEnabled = false
        UIView.animate(withDuration: 2.0,
                       delay: 0,
                       usingSpringWithDamping: 0.2,
                       initialSpringVelocity: 6.0,
                       options: .allowUserInteraction,
                       animations: { [weak self] in
                       self?.Vw_Engagement.transform = .identity
                        
                        Timer.scheduledTimer(timeInterval: 0.7,
                                             target: self!,
                                             selector: #selector(HomeViewController.update),
                                             userInfo: nil,
                                             repeats: false)
                        
                        self?.ViewsName = "Engagement"
                        
            },
                       completion: nil)
    }
    
    @objc func JewelryAction(){
        // do other task
        
        Vw_Jewelry.transform = CGAffineTransform(scaleX: 0.9, y: 0.9)
        Vw_Jewelry.isUserInteractionEnabled = false
        UIView.animate(withDuration: 2.0,
                       delay: 0,
                       usingSpringWithDamping: 0.2,
                       initialSpringVelocity: 6.0,
                       options: .allowUserInteraction,
                       animations: { [weak self] in
                        self?.Vw_Jewelry.transform = .identity
                        
                        self?.ViewsName = "Jewelry"
                        
                        Timer.scheduledTimer(timeInterval: 0.7,
                                             target: self!,
                                             selector: #selector(HomeViewController.update),
                                             userInfo: nil,
                                             repeats: false)
            },
                       completion: nil)
    }
    
    @objc func WatchesAction(){
        // do other task
        
        Vw_Watches.transform = CGAffineTransform(scaleX: 0.9, y: 0.9)
        Vw_Watches.isUserInteractionEnabled = false
        UIView.animate(withDuration: 2.0,
                       delay: 0,
                       usingSpringWithDamping: 0.2,
                       initialSpringVelocity: 6.0,
                       options: .allowUserInteraction,
                       animations: { [weak self] in
                        self?.Vw_Watches.transform = .identity
                        
                        self?.ViewsName = "Watches"
                        
                        Timer.scheduledTimer(timeInterval: 0.7,
                                             target: self!,
                                             selector: #selector(HomeViewController.update),
                                             userInfo: nil,
                                             repeats: false)
            },
                       completion: nil)
    }

    
    @objc func WeblinksAction(){
        // Do what you want
        
        Vw_Weblinks.transform = CGAffineTransform(scaleX: 0.9, y: 0.9)
        Vw_Weblinks.isUserInteractionEnabled = false
        UIView.animate(withDuration: 2.0,
                       delay: 0,
                       usingSpringWithDamping: 0.2,
                       initialSpringVelocity: 6.0,
                       options: .allowUserInteraction,
                       animations: { [weak self] in
                        self?.Vw_Weblinks.transform = .identity
                        
                        self?.ViewsName = "WebLinks"
                        
                        Timer.scheduledTimer(timeInterval: 0.7,
                                             target: self!,
                                             selector: #selector(HomeViewController.update),
                                             userInfo: nil,
                                             repeats: false)
            },
                       completion: nil)
    }
    

    //Timer
    
    // must be internal or public.
    @objc func update() {
        // Something cool
        let storyboard = UIStoryboard(name:"Main", bundle:nil)
        
        if self.ViewsName == "Engagement" {
            let storyboard = UIStoryboard(name:"Main", bundle:nil)
            let home = storyboard.instantiateViewController(withIdentifier: "EngagementStoneViewController")as! EngagementStoneViewController
            self.navigationController?.pushViewController(home, animated: true)
            
        }
        else if self.ViewsName == "Jewelry" {
            
            let storyboard = UIStoryboard(name:"Main", bundle:nil)
            let home = storyboard.instantiateViewController(withIdentifier: "JewelryViewController")as! JewelryViewController
            self.navigationController?.pushViewController(home, animated: true)
        }
        else if self.ViewsName == "Watches" {
            let storyboard = UIStoryboard(name:"Main", bundle:nil)
            let home = storyboard.instantiateViewController(withIdentifier: "WatchesViewController")as! WatchesViewController
            self.navigationController?.pushViewController(home, animated: true)
        }
        else{
            let home = storyboard.instantiateViewController(withIdentifier: "WeblinkViewController")as! WeblinkViewController
            self.navigationController?.pushViewController(home, animated: true)
        }
    }
    
    
//MARK: - API🤞🏻🙄
    
    func CallMyMethod()
    {
        AppDelegate.shared().ShowHUD()
        let url = URL(string: "\(Constants.API)\(Constants.GetAllType)")!
        print("Url printed \(url)")
        
        let manager = Alamofire.SessionManager.default
        manager.session.configuration.timeoutIntervalForRequest = 120
        
        let login = UserDefaults.standard.string(forKey: "LoginToken")
        let UserID = UserDefaults.standard.string(forKey: "UserID")
        
        let headers = ["Authorization": "Basic YWRtaW46QVBJQEZFREVSQUwhIyRXRUIk",
                       "X-FEDERAL-API-KEY": "kkcoswggwgwkkc8w4ok808o48kswc40c0www4wss",
                       "X-FEDERAL-LOGIN-TOKEN": login!,
                       "USER-ID": UserID!]
        
        manager.request(url, method: .get, parameters: nil, headers: headers).responseJSON() { response in
            switch (response.result) {
            case .success:
                
                //do json stuff
                let jsonResponse = response.result.value as! NSDictionary
                print("JsonResponse printed \(jsonResponse)")
                
                AppDelegate.shared().HideHUD()
                let myInt: Bool = jsonResponse["status"]! as! Bool
                
                if(myInt == true)
                {
                    /*
                    let getstring = jsonResponse.value(forKey: "data")
                    AppDelegate.shared().ArrGetAllType = getstring as! [String : Any]*/
                    
                    let swiftyJsonVar = JSON(response.result.value!)
                    
                    if let resData = swiftyJsonVar["data"].dictionaryObject {
                        AppDelegate.shared().ArrGetAllType = [resData as [String:AnyObject]]
                    }
                    print("GET DATA \(AppDelegate.shared().ArrGetAllType)")
                }

                
                break
                
            case .failure(let error):
                AppDelegate.shared().HideHUD()
                if error._code == NSURLErrorTimedOut {
                    //HANDLE TIMEOUT HERE
                }
                print("\n\nAuth request failed with error:\n \(error)")
                break
            }
        }
    }
}
