//
//  ForgotPasswordViewController.swift
//  Appraisal
//
//  Created by ADMIN on 4/16/18.
//  Copyright © 2018 empiereTech. All rights reserved.
//

import UIKit
import Alamofire
import SwiftyJSON
import AETextFieldValidator

class ForgotPasswordViewController: UIViewController {
    
//MARK: - Outlets🤔
    
    //TextFeild
    @IBOutlet var Txt_Email: AETextFieldValidator!
    
    //Buttons
    @IBOutlet weak var Btn_ResetPassword: UIButton!
    @IBOutlet weak var Btn_Back: UIButton!
    
//MARK: - Declare Variables👇🏻😬
    var API: String = Constants.API
    
//MARK: - Happy Coding😊
    
    //viewDidLoad
    override func viewDidLoad() {
        super.viewDidLoad()
        
        //NavigationBar
        self.navigationController?.navigationBar.isHidden = true;
        
        //ButtonLayoutChanges
        Btn_ResetPassword.layer.cornerRadius = Btn_ResetPassword.frame.size.height/2.0
        Btn_ResetPassword.isUserInteractionEnabled = true
        Btn_ResetPassword.layer.borderWidth = 1.5;
        Btn_ResetPassword.layer.borderColor = UIColor.white.cgColor
        Btn_ResetPassword.layer.masksToBounds = false
        Btn_ResetPassword.layer.shadowOffset = CGSize.zero
        Btn_ResetPassword.layer.shadowRadius = 7
        Btn_ResetPassword.layer.shadowOpacity = 0.5
        
        Txt_Email.attributedPlaceholder = NSAttributedString(string: "Email",
                                                             attributes: [NSAttributedStringKey.foregroundColor: UIColor.init(red: 145/255, green: 158/255, blue: 181/255, alpha: 1)])
        Txt_Email.validateOnResign = true
        
        setupAlerts()
    }
    
    
//MARK: - Actions👊🏻😠
    
    //ResetButtonPressed
    @IBAction func ResetButtonPressed(_ sender: Any) {
        if Txt_Email.validate(){
            AppDelegate.shared().ShowHUD()
            self.Btn_ResetPassword.transform = CGAffineTransform(scaleX: 0.9, y: 0.9)
            self.Btn_ResetPassword.isUserInteractionEnabled = false
            UIView.animate(withDuration: 2.0,
                           delay: 0,
                           usingSpringWithDamping: 0.2,
                           initialSpringVelocity: 6.0,
                           options: .allowUserInteraction,
                           animations: { [weak self] in
                            self?.Btn_ResetPassword.transform = .identity
                            
                            Timer.scheduledTimer(timeInterval: 0.7,
                                                 target: self!,
                                                 selector: #selector(ForgotPasswordViewController.update),
                                                 userInfo: nil,
                                                 repeats: false)
                },
                           completion: nil)
        }
    }
    
    @IBAction func BackButtonPressed(_ sender: Any) {
        self.navigationController?.popViewController(animated: true)
    }
    
//MARK: - Methods(Functions)☝🏻😳
    
    // @objc selector expected for Timer
    @objc func update() {
        // do what should happen when timer triggers an event
        
          if NetworkReachabilityManager()!.isReachable {
             CallMyMethod()
        } else {
         AppDelegate.shared().HideHUD()
            AppDelegate.shared().ShowAlert(title: Constants.ERROR_NO_INTERNET, msg: "Unable to connect Internet, Please try after some time.")
        }
        
    }
    
    
//MARK: - API🤞🏻🙄
    
    func   CallMyMethod(){
        let params : Parameters = ["email":Txt_Email.text!]
        print("param is \(params)")
        let url = URL(string: "\(Constants.API)\(Constants.ForgotPassword)")!
        print("Url printed \(url)")
        
        let manager = Alamofire.SessionManager.default
        manager.session.configuration.timeoutIntervalForRequest = 120
        
        let headers = ["Authorization": "Basic YWRtaW46QVBJQEZFREVSQUwhIyRXRUIk",
                       "X-FEDERAL-API-KEY": "kkcoswggwgwkkc8w4ok808o48kswc40c0www4wss"]
        
        manager.request(url, method: .post, parameters: params, headers: headers).responseJSON() { response in
            switch (response.result) {
            case .success:
                //do json stuff
                let jsonResponse = response.result.value as! NSDictionary
                print("JsonResponse printed \(jsonResponse)")
                
                AppDelegate.shared().HideHUD()
                let myInt: Bool = jsonResponse["status"]! as! Bool
                
                if (myInt == true)
                {
                    self.navigationController?.popViewController(animated: true)
                    AppDelegate.shared().ShowAlert(title: Constants.ERROR_HEADER, msg: jsonResponse.value(forKeyPath: "message") as! String)
                }
                else{
                    AppDelegate.shared().ShowAlert(title: Constants.ERROR_HEADER, msg: jsonResponse.value(forKeyPath: "message") as! String)
                }
                
                break
                
            case .failure(let error):
                AppDelegate.shared().HideHUD()
                if error._code == NSURLErrorTimedOut {
                    //HANDLE TIMEOUT HERE
                }
                print("\n\nAuth request failed with error:\n \(error)")
                break
            }
        }
    }
    
    func setupAlerts(){
        Txt_Email.addRegx(Constants.REGEX_EMAIL, withMsg: "Enter valid email.")
    }
    
}
