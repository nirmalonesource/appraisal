
//
//  LoginViewController.swift
//  Appraisal
//
//  Created by ADMIN on 3/24/18.
//  Copyright © 2018 empiereTech. All rights reserved.
// Login Cred : kamlesh.sorathiya67@gmail.com  Pass : 123456

import UIKit
import Alamofire
import SwiftyJSON
import AETextFieldValidator

class LoginViewController: UIViewController {
    
//MARK: - Outlets🤔
    
    //TextFeild
    @IBOutlet var Txt_Email: AETextFieldValidator!
    @IBOutlet var Txt_Password: AETextFieldValidator!
    
    //Buttons
    @IBOutlet var Btn_Forgot: UIButton!
    @IBOutlet var Btn_Login: UIButton!
    
//MARK: - Declare Variables👇🏻😬
    var API: String = Constants.API
    
//MARK: - Happy Coding😊
    
    //viewDidLoad
    fileprivate func extractedFunc() {
        setupAlerts()
    }
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        //NavigationBar
        self.navigationController?.navigationBar.isHidden = true;
        
        //ButtonLayoutChanges
        Btn_Login.layer.cornerRadius = Btn_Login.frame.size.height/2.0
        Btn_Login.isUserInteractionEnabled = true
        Btn_Login.layer.borderWidth = 1.5;
        Btn_Login.layer.borderColor = UIColor.white.cgColor
        Btn_Login.layer.masksToBounds = false
        Btn_Login.layer.shadowOffset = CGSize.zero
        Btn_Login.layer.shadowRadius = 7
        Btn_Login.layer.shadowOpacity = 0.5
        
        Txt_Email.attributedPlaceholder = NSAttributedString(string: "Email",
                                                             attributes: [NSAttributedStringKey.foregroundColor: UIColor.init(red: 145/255, green: 158/255, blue: 181/255, alpha: 1)])
        Txt_Email.validateOnResign = true
        Txt_Password.attributedPlaceholder = NSAttributedString(string: "Password",
                                                                attributes: [NSAttributedStringKey.foregroundColor: UIColor.init(red: 145/255, green: 158/255, blue: 181/255, alpha: 1)])
        
        extractedFunc()
    }
    
    
//MARK: - Actions👊🏻😠
    
    //LoginButtonPressed
    @IBAction func LoginButtonPressed(_ sender: Any) {
        
        if Txt_Email.validate() && Txt_Password.validate(){
            
            AppDelegate.shared().ShowHUD()
            self.Btn_Login.transform = CGAffineTransform(scaleX: 0.9, y: 0.9)
            self.Btn_Login.isUserInteractionEnabled = false
            UIView.animate(withDuration: 2.0,
                           delay: 0,
                           usingSpringWithDamping: 0.2,
                           initialSpringVelocity: 6.0,
                           options: .allowUserInteraction,
                           animations: { [weak self] in
                            self?.Btn_Login.transform = .identity
                            
                            Timer.scheduledTimer(timeInterval: 0.7,
                                                 target: self!,
                                                 selector: #selector(HomeViewController.update),
                                                 userInfo: nil,
                                                 repeats: false)
                },
                           completion: nil)
            
            
        }
    }
    
    @IBAction func ForgotButtonPressed(_ sender: Any) {
        
        let storyboard = UIStoryboard(name:"Main", bundle:nil)
        
        let home = storyboard.instantiateViewController(withIdentifier: "ForgotPasswordViewController")as! ForgotPasswordViewController
        
        self.navigationController?.pushViewController(home, animated: true)
    }
    
    
//MARK: - Methods(Functions)☝🏻😳
    
    // @objc selector expected for Timer
    @objc func update() {
        // do what should happen when timer triggers an event
        
        if NetworkReachabilityManager()!.isReachable {
            CallMyMethod()
        } else {
           
         AppDelegate.shared().HideHUD()
            AppDelegate.shared().ShowAlert(title: Constants.ERROR_NO_INTERNET, msg: "Unable to connect Internet, Please try after some time.")
        }
        
    }
    
    
//MARK: - API🤞🏻🙄
    
    func CallMyMethod(){
        
      //  let deviceToken:String = (UserDefaults.standard.string(forKey: "deviceToken") as String?)!
        var deviceToken : String = "N/A"
        
        if (UserDefaults.standard.value(forKey: "deviceToken") != nil) {
            deviceToken = UserDefaults.standard.string(forKey: "deviceToken")!
        }
      
       
        let params : Parameters = ["login":Txt_Email.text!,
                                   "password":Txt_Password.text!,
                                   "token_id": deviceToken,
                                   "device_id":"121"]
       
        print("param is \(params)")
        let url = URL(string: "\(Constants.API)\(Constants.LOGIN)")!
        print("Url printed \(url)")
        
        let manager = Alamofire.SessionManager.default
        manager.session.configuration.timeoutIntervalForRequest = 120

        let headers = ["Authorization": "Basic YWRtaW46QVBJQEZFREVSQUwhIyRXRUIk",
                       "X-FEDERAL-API-KEY": "kkcoswggwgwkkc8w4ok808o48kswc40c0www4wss"]
        
        
        
        manager.request(url, method: .post, parameters: params, headers: headers).responseJSON() { response in
            switch (response.result) {
            case .success:
                //do json stuff
                let jsonResponse = response.result.value as! NSDictionary
                print("JsonResponse printed \(jsonResponse)")
                
                AppDelegate.shared().HideHUD()
                let myInt: Bool = jsonResponse["status"]! as! Bool
                
                if (myInt == true)
                {
                    self.Btn_Login.isUserInteractionEnabled = true
                    UserDefaults.standard.set(jsonResponse.value(forKeyPath: "data.login_token"), forKey: "LoginToken")
                    UserDefaults.standard.set(jsonResponse.value(forKeyPath: "data.user_id"), forKey: "UserID")
                    UserDefaults.standard.set(jsonResponse.value(forKeyPath: "data.user.first_name"), forKey: "FirstName")
                    UserDefaults.standard.set(jsonResponse.value(forKeyPath: "data.user.last_name"), forKey: "LastName")
                    UserDefaults.standard.set(jsonResponse.value(forKeyPath: "data.user.RoleName"), forKey: "RoleName")
                    UserDefaults.standard.set(jsonResponse.value(forKeyPath: "data.user.email"), forKey: "Email")
                    UserDefaults.standard.set(jsonResponse.value(forKeyPath: "data.user.role_id"), forKey: "RoalID")
                    UserDefaults.standard.synchronize() //Optional
                    
                    let storyboard = UIStoryboard(name:"Main", bundle:nil)
                    
                    let home = storyboard.instantiateViewController(withIdentifier: "HomeViewController")as! HomeViewController
                    
                    self.navigationController?.pushViewController(home, animated: true)
                }
                else{
                    self.Btn_Login.isUserInteractionEnabled = true
                    AppDelegate.shared().ShowAlert(title: Constants.ERROR_HEADER, msg: jsonResponse.value(forKeyPath: "message") as! String)
                }
                
                break
                
            case .failure(let error):
                AppDelegate.shared().HideHUD()
                if error._code == NSURLErrorTimedOut {
                    //HANDLE TIMEOUT HERE
                }
                self.Btn_Login.isUserInteractionEnabled = true
                print("\n\nAuth request failed with error:\n \(error)")
                break
            }
        }
    }
    
    func setupAlerts(){
        Txt_Email.addRegx(Constants.REGEX_EMAIL, withMsg: "Enter valid email.")
        Txt_Password.addRegx(Constants.REGEX_PASSWORD_LIMIT, withMsg: "Password characters limit should be come between 6-20")
    }
    
}
