//
//  MyCertificatesViewController.swift
//  Appraisal
//
//  Created by ADMIN on 4/17/18.
//  Copyright © 2018 empiereTech. All rights reserved.
//

import UIKit
import Alamofire
import SwiftyJSON
import SWRevealViewController
import SDWebImage

class MyCertificatesViewController: UIViewController,SWRevealViewControllerDelegate,UITableViewDelegate,UITableViewDataSource,URLSessionDelegate,UIDocumentInteractionControllerDelegate,UIActionSheetDelegate,UISearchBarDelegate {
    
    

//MARK: - Outlets🤔
    
    @IBOutlet weak var Vw_Tabl: UITableView!
    @IBOutlet weak var Vw_Search: UISearchBar!
    
//MARK: - Declare Variables👇🏻😬
    var ArrList = [[String:AnyObject]]() //Array of dictionary
    var ArrSectionList = [[String:Any]]() //Array of dictionary
    var TitleName = NSString()
    var weburl:  NSURL!
        var cardUrl:  NSURL!
    var urlLink: URL!
    var defaultSession: URLSession!
    var downloadTask: URLSessionDownloadTask!
    
    var GETNewData = [String: String]()
    var filterd: [String] = []
    var GetData = NSArray()
    var GetNameData: [String] = []
    var SearchActivated : Bool = false
   
//MARK: - Happy Coding😊
    override func viewDidLoad() {
        super.viewDidLoad()
      Vw_Search.delegate = self
        
        ViewLayout()
          if NetworkReachabilityManager()!.isReachable {
             self.CallMyMethod()
        } else {
          AppDelegate.shared().HideHUD()
      
            AppDelegate.shared().ShowAlert(title: Constants.ERROR_NO_INTERNET, msg: "Unable to connect Internet, Please try after some time.")
        }
        
    }
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(true)
        
        //NavigationBar
        self.navigationController?.navigationBar.isHidden = false;
        self.title = "My Certificates"
        
        navigationController?.navigationBar.barTintColor = UIColor.white
        if (UIScreen.main.bounds.size.height == 568.0)
        {
            print("iphone5")
            navigationController?.navigationBar.titleTextAttributes = [NSAttributedStringKey.foregroundColor: UIColor(red: 18/255, green: 64/255, blue: 118/255, alpha: 1),NSAttributedStringKey.font: UIFont(name: "Montserrat-Medium", size: 15)!]
        }
        else{
            navigationController?.navigationBar.titleTextAttributes = [NSAttributedStringKey.foregroundColor: UIColor(red: 18/255, green: 64/255, blue: 118/255, alpha: 1),NSAttributedStringKey.font: UIFont(name: "Montserrat-Medium", size: 17)!]
        }

        navigationController?.navigationBar.tintColor = UIColor(red: 18/255, green: 64/255, blue: 118/255, alpha: 1)
        self.navigationItem.setHidesBackButton(true, animated:true);
        
        let menuButton = UIButton(type: .system)
        menuButton.setImage(#imageLiteral(resourceName: "Menu").withRenderingMode(.alwaysOriginal), for: .normal)
        menuButton.frame = CGRect(x: 0, y: 0, width: 34, height: 34)
        navigationItem.leftBarButtonItem = UIBarButtonItem(customView: menuButton)
        
        //SideBar
        if revealViewController() != nil
        {
            self.revealViewController().delegate = self
            menuButton.addTarget(revealViewController(), action: "revealToggle:", for: .touchUpInside)
            self.navigationController?.navigationBar.addGestureRecognizer(self.revealViewController().panGestureRecognizer())
            view.addGestureRecognizer(self.revealViewController().panGestureRecognizer())
        }
        
        let backgroundSessionConfiguration = URLSessionConfiguration.background(withIdentifier: "backgroundSession")
        defaultSession = Foundation.URLSession(configuration: backgroundSessionConfiguration, delegate: self, delegateQueue: OperationQueue.main)
        // progress.setProgress(0.0, animated: false)
        
        
        
        Vw_Tabl.allowsSelection = false
    }
    
//MARK: - Actions👊🏻😠
    
    
//MARK: - Methods(Functions)☝🏻😳
    
    func revealController(_ revealController: SWRevealViewController, didMoveTo position: FrontViewPosition) {
        if case .right = position {
            
            revealController.frontViewController.view.alpha = 0.5
            self.view.isUserInteractionEnabled = false
            self.revealViewController().frontViewController.view.addGestureRecognizer(self.revealViewController().panGestureRecognizer())
            self.revealViewController().frontViewController.view.addGestureRecognizer(self.revealViewController().tapGestureRecognizer())
        } else {
            
            revealController.frontViewController.view.alpha = 1
            self.revealViewController().tapGestureRecognizer()
            self.view.isUserInteractionEnabled = true
        }
    }
    
    func ViewLayout()
    {
       /* Vw_Search.autoresizingMask = UIViewAutoresizing(rawValue: 0)
        Vw_Search.delegate = self
        Vw_Search.placeholder = "Search Certification# or Customer name"
        Vw_Search.barTintColor = UIColor.blue
        Vw_Search.backgroundColor = UIColor.clear
        Vw_Search.isTranslucent = true
        Vw_Search.setBackgroundImage(UIImage(), for: .any, barMetrics: .default)
        
        Vw_Search.layer.cornerRadius = 2.0
        Vw_Search.isUserInteractionEnabled = true
        Vw_Search.layer.masksToBounds = false
        Vw_Search.layer.shadowOffset = CGSize.zero
        Vw_Search.layer.shadowRadius = 3
        Vw_Search.layer.shadowOpacity = 0.2
        
        let searchField: UITextField = Vw_Search.value(forKey: "searchField") as! UITextField
        // Set the font and text color of the search field.
        searchField.font = UIFont(name: "Montserrat-Medium", size: 12)!
        searchField.textColor = UIColor.black
        searchField.autocorrectionType = .default*/
    }
 
    
    @objc func EditCertificate(sender: UIButton) {
        
        let buttonPosition = sender.convert(CGPoint.zero, to: Vw_Tabl)
        var indexPath: IndexPath? = Vw_Tabl.indexPathForRow(at: buttonPosition)
        
        let buttonTag = indexPath?.section
        let buttonTagIndex = indexPath?.row
        
        ArrSectionList = self.ArrList[buttonTag!]["data"] as! [[String : AnyObject]]
        
        let new = self.ArrSectionList[buttonTagIndex!]["Orderid"]!
        GetCommentsMethod(OrderID: new as! String)
    }
    
    @objc func DownloadCertificate(sender: UIButton){
        
        let buttonPosition = sender.convert(CGPoint.zero, to: Vw_Tabl)
        var indexPath: IndexPath? = Vw_Tabl.indexPathForRow(at: buttonPosition)
        
        let buttonTag = indexPath?.section
        let buttonTagIndex = indexPath?.row
        
        ArrSectionList = self.ArrList[buttonTag!]["data"] as! [[String : AnyObject]]
        
        weburl = NSURL(string: self.ArrSectionList[buttonTagIndex!]["certificate_link"] as! String)
        cardUrl = NSURL(string: self.ArrSectionList[buttonTagIndex!]["card_link"] as! String)
        title = self.ArrSectionList[buttonTagIndex!]["CustomerName"] as! NSString as String
       
        if ("\(weburl!)" != "" && "\(cardUrl!)" != "" )
        {
            
//            let actionSheet = UIActionSheet(title: "Choose Option to open", delegate: self, cancelButtonTitle: "Cancel", destructiveButtonTitle: nil, otherButtonTitles: "Certificate", "Card")
//
//            actionSheet.show(in: self.view)
            
            let alert = UIAlertController(title: "Title", message: "Please Select an Option", preferredStyle: .actionSheet)
            
            alert.addAction(UIAlertAction(title: "Certificate", style: .default , handler:{ (UIAlertAction)in
                print("User click Approve button")
            /*    let storyboard = UIStoryboard(name:"Main", bundle:nil)
                let webView = storyboard.instantiateViewController(withIdentifier: "WebViewController")as! WebViewController
                webView.url = self.weburl
                webView.TitleName = (self.ArrSectionList[buttonTagIndex!]["CustomerName"] as! NSString) as String as String as NSString
                self.navigationController?.pushViewController(webView, animated: true)
 */
                let url = self.weburl  as URL
                UIApplication.shared.open(url)
                
            }))
            
            alert.addAction(UIAlertAction(title: "Card", style: .default , handler:{ (UIAlertAction)in
                print("User click Edit button")
               /* let storyboard = UIStoryboard(name:"Main", bundle:nil)
                let webView = storyboard.instantiateViewController(withIdentifier: "WebViewController")as! WebViewController
                webView.url = self.cardUrl
                webView.TitleName = (self.ArrSectionList[buttonTagIndex!]["CustomerName"] as! NSString) as String as String as NSString
                self.navigationController?.pushViewController(webView, animated: true)*/
                
                   let url = self.cardUrl  as URL
                UIApplication.shared.open(url)
            }))
            
           
            
            self.present(alert, animated: true, completion: {
                print("completion block")
            })
           
        } else   if ("\(cardUrl!)" != "")
        {
           /* let storyboard = UIStoryboard(name:"Main", bundle:nil)
            let webView = storyboard.instantiateViewController(withIdentifier: "WebViewController")as! WebViewController
            webView.url = cardUrl
            webView.TitleName = self.ArrSectionList[buttonTagIndex!]["CustomerName"] as! NSString
            self.navigationController?.pushViewController(webView, animated: true)

            */
             let url = self.cardUrl  as URL
            UIApplication.shared.open(url)
           
        }else   if ("\(weburl!)" != "")
        {
            
            /*
            let storyboard = UIStoryboard(name:"Main", bundle:nil)
            let webView = storyboard.instantiateViewController(withIdentifier: "WebViewController")as! WebViewController
            webView.url = weburl
            webView.TitleName = self.ArrSectionList[buttonTagIndex!]["CustomerName"] as! NSString
            self.navigationController?.pushViewController(webView, animated: true) */
            
           let url = self.weburl  as URL
            UIApplication.shared.open(url)
        }
        else{
         AppDelegate.shared().ShowAlert(title: Constants.ERROR_HEADER, msg: "No Certificates available!")
         }
        
        
    }
    
    func removeDuplicates(from items: [NSArray]) -> [NSArray] {
        let uniqueItems = NSOrderedSet(array: items)
        return (uniqueItems.array as? [NSArray]) ?? []
    }
    
    private func actionSheet(actionSheet: UIActionSheet, clickedButtonAtIndex buttonIndex: Int)
    {
        print("\(buttonIndex)")
        switch (buttonIndex){
            
        case 0:
            print("Cancel")
        case 1:
            print("certificate")
            
           
        case 2:
            print("card")
           let url = self.cardUrl  as URL
            UIApplication.shared.open(url)
            
           /* let storyboard = UIStoryboard(name:"Main", bundle:nil)
            let webView = storyboard.instantiateViewController(withIdentifier: "WebViewController")as! WebViewController
            webView.url = cardUrl
            webView.TitleName = title as! NSString
            self.navigationController?.pushViewController(webView, animated: true)*/
        default:
            print("Default")
            //Some code here..
            
        }
    }
//MARK: - Delegates✋🏻😇
    
    //searchBarShouldBeginEditing
//    func searchBarShouldBeginEditing(_ searchBar: UISearchBar) -> Bool
//    {
//
//    }
//
    func searchBarTextDidBeginEditing(_ searchBar: UISearchBar)
    {
       // SearchActivated = true
    }
    
    /*func searchBarTextDidEndEditing(_ searchBar: UISearchBar) {
        SearchActivated = false
    }*/
    
    func searchBar(_ searchBar: UISearchBar, textDidChange searchText: String) {
        

      //  filterd = [""]
        //filterd = GetNameData.filter({$0.lowercased().prefix(searchText.count) == searchText.lowercased()})
        // SearchActivated = true
        //Vw_Tabl.reloadData()
        filterd = GetNameData.filter({ (text) -> Bool in
        print("text \(text)")
        let temp : NSString = text as NSString
        let range = temp.range(of: searchText, options: .caseInsensitive)
           return range.location != NSNotFound
        })
        if  filterd.count == 0 {
            SearchActivated = false
        }
        else{
            SearchActivated = true
        }
        Vw_Tabl.reloadData()
      }
    //titleForHeaderInSection
    public func tableView(_ tableView: UITableView, titleForHeaderInSection section: Int) -> String? {
        return "Items"
    }
    
    //viewForHeaderInSection
    public func tableView(_ tableView: UITableView, viewForHeaderInSection section: Int) -> UIView? {
        let headerView = UIView()
        headerView.backgroundColor = UIColor.clear
        
        let headerLabel2 = UILabel(frame: CGRect(x: 0, y: 0, width:
            tableView.bounds.size.width, height: 30))
        headerLabel2.backgroundColor = UIColor.init(red: 124/255.0, green: 146/255.0, blue: 173/255.0, alpha: 1)
        headerView.addSubview(headerLabel2)

        if SearchActivated {
           /* var array = [String]()
            for var i in (0..<ArrSectionList.count)
            {
                array.append("\(ArrSectionList[i]["CustomerName"]!)")
            }
            
            print("get my array \(array)")
           // print("\(dictNew.value(forKey: "MaritalStatusId")!)")
            if let index = array.index(of: filterd[section]) {
                //array.remove(at: index)
                print("GetIndex\(index)")
                
                if ArrList.count > 0 {
                    let headerLabel = UILabel(frame: CGRect(x: 15, y: 8, width:
                        headerLabel2.bounds.size.width, height: 30))
                    headerLabel.font = UIFont(name: "Montserrat-Medium", size: 12)
                    headerLabel.textColor = UIColor.white
                    
                    
                    //  Converted to Swift 4 by Swiftify v4.1.6680 - https://objectivec2swift.com/
                    let dateString = self.ArrList[index]["date"]!
                    let dateFormatter = DateFormatter()
                    dateFormatter.dateFormat = "yyyy/MM/dd"
                    var date = Date()
                    if let aString = dateFormatter.date(from: dateString as! String) {
                        date = aString
                    }
                    print("\(dateFormatter.string(from: date))")
                    
                    dateFormatter.dateFormat = "MMMM-dd-yyyy"
                    let resultString = dateFormatter.string(from: date)
                    print(resultString)
                    headerLabel.text = resultString
                    headerLabel.sizeToFit()
                    headerView.addSubview(headerLabel)
                }
            }
            else{
                
            }*/
        }
        else{
            if ArrList.count > 0 {
                let headerLabel = UILabel(frame: CGRect(x: 15, y: 8, width:
                    headerLabel2.bounds.size.width, height: 30))
                headerLabel.font = UIFont(name: "Montserrat-Medium", size: 12)
                headerLabel.textColor = UIColor.white
                
                
                //  Converted to Swift 4 by Swiftify v4.1.6680 - https://objectivec2swift.com/
                let dateString = self.ArrList[section]["date"]!
                let dateFormatter = DateFormatter()
                dateFormatter.dateFormat = "yyyy/MM/dd"
                var date = Date()
                if let aString = dateFormatter.date(from: dateString as! String) {
                    date = aString
                }
                print("\(dateFormatter.string(from: date))")
                
                dateFormatter.dateFormat = "MMMM-dd-yyyy"
                let resultString = dateFormatter.string(from: date)
                print(resultString)
                headerLabel.text = resultString
                headerLabel.sizeToFit()
                headerView.addSubview(headerLabel)
            }
        }
       
        
        return headerView
    }
    
    //heightForHeaderInSection
    public func tableView(_ tableView: UITableView, heightForHeaderInSection section: Int) -> CGFloat {
        
        return 30
    }
    
    //heightForRowAt
    public func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat
    {
        return 75
    }
    
    //numberOfSections
    func numberOfSections(in tableView: UITableView) -> Int {
        if SearchActivated{
            return 1
        }
        else{
        return self.ArrList.count
        }
    }
    
    //tableView numberOfRowsInSection
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int
    {
        if SearchActivated{
           var array = [String]()
            for var i in (0..<ArrSectionList.count)
           {
               array.append("\(ArrSectionList[i]["CustomerName"]!)")
            return filterd.count
            }
             print("get my array \(array)")
           // print("\(dictNew.value(forKey: "MaritalStatusId")!)")
         if let index = array.index(of: filterd[section]) {
             //array.remove(at: index)
                print("GetIndex\(index)")

                return self.ArrList[index]["data"]!.count
            }
            else{
                return 0
            }
            return array.count
        }
        else{
            return self.ArrList[section]["data"]!.count
           

        }
    }
    
    //tableView cellForRowAt
    public func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell
    {
        var cell = tableView.dequeueReusableCell(withIdentifier: "cell")
        
        cell = nil
        if(cell == nil)
        {
            let cellAuditDetails:CertificateTableViewCell = Bundle.main.loadNibNamed("CertificateTableViewCell", owner: self, options: nil)? [0] as! CertificateTableViewCell

            if SearchActivated {
                if ArrSectionList.count > indexPath.row {
                cellAuditDetails.Lbl_Customer.text = filterd[indexPath.row] as? String
                var array = [String]()
                array.append("\(ArrSectionList[indexPath.row]["CustomerName"]!)")
                 print("get my array \(array)")
                // print("\(dictNew.value(forKey: "MaritalStatusId")!)")
                if let index = array.index(of: filterd[indexPath.row]) {
                    //array.remove(at: index)
                    print("GetIndex\(index)")
                    
                    
                    ArrSectionList = self.ArrList[index]["data"] as! [[String : AnyObject]]
                    
                    print("GetAll \(ArrSectionList[index]["CustomerName"]! as? String)")
                    cellAuditDetails.Lbl_Customer.text = ArrSectionList[indexPath.row]["CustomerName"]! as? String
                    
                    
                    let null = NSNull()
                    if (ArrSectionList[index]["OrderStatus"] as? String == "Pending")
                    {
                        cellAuditDetails.Img_OrderStatus.image = UIImage(named: "CloseButton")
                    }
                    else{
                        cellAuditDetails.Img_OrderStatus.image = UIImage(named: "RightButton")
                    }
                    
                    
                    
                    cellAuditDetails.Lbl_CertificateNumber.text = "Certificate Number : \(ArrSectionList[index]["CertificateNo"] as? String ?? "N/A")"
                    
                    cellAuditDetails.Lbl_TrackingNumber.text = "Tracking : \(ArrSectionList[index]["TrackingNO"]  as? String ?? "N/A")"
                }
                }
            }
            else{
                
                ArrSectionList = self.ArrList[indexPath.section]["data"] as! [[String : Any]]
                if ArrSectionList.count > indexPath.row{
                    
                cellAuditDetails.Lbl_Customer.text = ArrSectionList[indexPath.row]["CustomerName"] as? String
                
                GetNameData.insert((ArrSectionList[indexPath.row]["CustomerName"] as? String)!, at: indexPath.row)
//                /let dic : [String: Any] = ["CustomerName":ArrSectionList[indexPath.row]["CustomerName"] as? String as Any,
//                                          "OrderStatus":ArrSectionList[indexPath.row]["OrderStatus"] as! String,
//                                          "OrderStatus":ArrSectionList[indexPath.row]["OrderStatus"] as! String,
//                                          "CertificateNo":ArrSectionList[indexPath.row]["CertificateNo"]  as? String ?? "N/A",
//                                          "TrackingNO":ArrSectionList[indexPath.row]["TrackingNO"]  as? String ?? "N/A"]
//                print("Dict Value \(dic)")
 
                GetData = self.ArrList[indexPath.section]["data"]! as! NSArray
                print("Jsonresponse \(GetData)")
               
                
                let null = NSNull()
                if (ArrSectionList[indexPath.row]["OrderStatus"] as? String == "Pending")
                {
                    cellAuditDetails.Img_OrderStatus.image = UIImage(named: "CloseButton")
                    cellAuditDetails.Btn_Download.alpha = 0.5
                     cellAuditDetails.Btn_Download.isUserInteractionEnabled = false
                }
                else{
                    cellAuditDetails.Img_OrderStatus.image = UIImage(named: "RightButton")
                     cellAuditDetails.Btn_Download.alpha = 1
                    cellAuditDetails.Btn_Download.isUserInteractionEnabled = true
                }

               

                    cellAuditDetails.Lbl_CertificateNumber.text = "Certificate Number : \(ArrSectionList[indexPath.row]["CertificateNo"] as? String ?? "N/A")"

                    cellAuditDetails.Lbl_TrackingNumber.text = "Tracking : \(ArrSectionList[indexPath.row]["TrackingNO"]  as? String ?? "N/A")"

                }}
            
            
            
            
           
            
            
            cellAuditDetails.Btn_Edit.tag = indexPath.row
            cellAuditDetails.Btn_Edit.addTarget(self, action: #selector(MyCertificatesViewController.EditCertificate), for: .touchUpInside)
            
            
            cellAuditDetails.Btn_Download.tag = indexPath.row
            cellAuditDetails.Btn_Download.addTarget(self, action: #selector(MyCertificatesViewController.DownloadCertificate), for: .touchUpInside)
            
            cell = cellAuditDetails
            
        }

        return cell!
    }
    
    public func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        tableView.deselectRow(at: indexPath, animated: true)
    }

    
//MARK: - API🤞🏻🙄
    func  CallMyMethod()
    {
        AppDelegate.shared().ShowHUD()
        let url = URL(string: "\(Constants.API)\(Constants.GetAppraisalList)")!
        print("Url printed \(url)")
        
        let manager = Alamofire.SessionManager.default
        manager.session.configuration.timeoutIntervalForRequest = 120
        
        let login = UserDefaults.standard.string(forKey: "LoginToken")
        let UserID = UserDefaults.standard.string(forKey: "UserID")
        
        let headers = ["Authorization": "Basic YWRtaW46QVBJQEZFREVSQUwhIyRXRUIk",
                       "X-FEDERAL-API-KEY": "kkcoswggwgwkkc8w4ok808o48kswc40c0www4wss",
                       "X-FEDERAL-LOGIN-TOKEN": login!,
                       "USER-ID": UserID!]
        
        manager.request(url, method: .get, parameters: nil, headers: headers).responseJSON() { response in
            switch (response.result) {
            case .success:
                //do json stuff
                let jsonResponse = response.result.value as! NSDictionary
                print("JsonResponse printed \(jsonResponse)")
                
                AppDelegate.shared().HideHUD()
                let myInt: Bool = jsonResponse["status"]! as! Bool
                
                if(myInt == true)
                {
                    let dict = jsonResponse
                    self.ArrList = dict["data"]! as! [[String:AnyObject]]
                    self.Vw_Tabl.reloadData()
                }
                
                break
                
            case .failure(let error):
                AppDelegate.shared().HideHUD()
                if error._code == NSURLErrorTimedOut {
                    //HANDLE TIMEOUT HERE
                }
                print("\n\nAuth request failed with error:\n \(error)")
                break
            }
        }
    }
    
    func GetCommentsMethod(OrderID : String)
    {
        AppDelegate.shared().ShowHUD()
        let url = URL(string: "\(Constants.API)\(Constants.GetrderComment)\(OrderID)")!
        print("Url printed \(url)")
        
        let manager = Alamofire.SessionManager.default
        manager.session.configuration.timeoutIntervalForRequest = 120
        
        let login = UserDefaults.standard.string(forKey: "LoginToken")
        let UserID = UserDefaults.standard.string(forKey: "UserID")
        
        let headers = ["Authorization": "Basic YWRtaW46QVBJQEZFREVSQUwhIyRXRUIk",
                       "X-FEDERAL-API-KEY": "kkcoswggwgwkkc8w4ok808o48kswc40c0www4wss",
                       "X-FEDERAL-LOGIN-TOKEN": login!,
                       "USER-ID": UserID!]
        
        manager.request(url, method: .get, parameters: nil, headers: headers).responseJSON() { response in
            switch (response.result) {
            case .success:
                //do json stuff
                let jsonResponse = response.result.value as! NSDictionary
                print("JsonResponse printed \(jsonResponse)")
                
                AppDelegate.shared().HideHUD()
                let myInt: Bool = jsonResponse["status"]! as! Bool
                
                if(myInt == true)
                {
                    
                    var Chatarray = [[String:AnyObject]]() //Array of dictionary
                    let dict = jsonResponse
                    Chatarray = dict["data"]! as! [[String:AnyObject]]
                    
                    let storyboard = UIStoryboard(name:"Main", bundle:nil)
                               let home = storyboard.instantiateViewController(withIdentifier: "ChatDetailViewController")as! ChatDetailViewController
                              
                    home.messagesArray = (dict["data"] as! NSArray).mutableCopy() as! NSMutableArray
                    home.OrderID = OrderID
                    home.hidesBottomBarWhenPushed = true
                  //  home.SenderID = UserID
                    self.navigationController?.pushViewController(home, animated: true)
                    
//                    let chatView = ChatViewController()
//                    let chatNavigationController = UINavigationController(rootViewController: chatView)
                    
//                    for var i in (0..<Chatarray.count)
//                    {
//                        let dateString = Chatarray[i]["CommentedDate"]!
//                        let dateFormatter = DateFormatter()
//                        dateFormatter.dateFormat = "yyyy-MM-dd hh:mm:ss"
//                        var date = Date()
//                        if let aString = dateFormatter.date(from: dateString as! String) {
//                            date = aString
//                        }
//                        dateFormatter.dateFormat = "MM-dd-yyyy"
//
//                        let TestString = Chatarray[i]["Comment"]!
//                        let userid = Chatarray[i]["LoginID"]!
//
//
//                        if ((Chatarray[i]["ImageURL"]! as? NSNull) == nil)
//                        {
//                            /*let url = Chatarray[i]["ImageURL"]! as! String
//                            let imageURL : NSURL = NSURL.fileURL(withPath: url) as NSURL
//                            let photoItem = JSQPhotoMediaItem()
//                           // let key = MD5("\(Chatarray[i]["ImageURL"]! as! String)")
//                            let key = url.md5Hash()
//                            //NSString *key = [url MD5Hash];
//                            let data = FTWCache.object(forKey: key)
//
//                            if data != nil {
//                                var image = UIImage(data: data! as! Data)
//                                if image == nil {
//                                    image = UIImage(named: "upload_image")
//                                }
//                                photoItem.image = image
//                            }else{
//                                DispatchQueue.main.async {
//                                    let data = NSData.init(contentsOf: imageURL as URL)
//                                    FTWCache.setObject(data! as Data, forKey: key)
//                                    var image = UIImage(data: data! as Data)
//
//                                    if image == nil {
//                                        image = UIImage(named: "upload_image")
//                                    }
//                                    photoItem.image = image
//                                }
//
//
//                            }*/
//
//                            let photoItem = JSQPhotoMediaItem()
//                            let url: String? = Chatarray[i]["ImageURL"]! as? String
//                            let imageURL = URL(string: url ?? "")
//                            let key = url?.md5Hash()
//                            let data = FTWCache.object(forKey: key)
//                            if data != nil {
//                                var myimage: UIImage? = nil
//                                if let aData = data {
//                                    myimage = UIImage(data: aData as! Data)?.resizedImage(byMagick: "160x160#")
//                                }
//                                photoItem.image = myimage
//                            } else {
//                                let queue = DispatchQueue.global(qos: .default)
//                                queue.async(execute: {() -> Void in
//                                    var data: Data? = nil
//                                    if let anURL = imageURL {
//                                        data = try! Data(contentsOf: anURL)
//                                    }
//                                    FTWCache.setObject(data, forKey: key)
//                                    DispatchQueue.main.sync(execute: {() -> Void in
//                                        var myimage: UIImage? = nil
//                                        if let aData = data {
//                                            myimage = UIImage(data: aData)?.resizedImage(byMagick: "160x160#")
//                                        }
//                                        photoItem.image = myimage
//                                    })
//                                })
//                            }
//
//                            let photoMessage = JSQMessage(senderId: userid as! String, displayName: "Admin", media: photoItem)
//
//                            chatView.messages.append(photoMessage)
//                        }
//                        else{
//                             let message = JSQMessage(senderId: userid as! String, senderDisplayName: "Admin", date: date, text: TestString as! String)
//                           // let msg = JSQMessage(senderId: userid as! String, displayName: "Admin", text: TestString as! String)
//
//                             chatView.messages.append(message)
//                        }
//
//                        //let mediaItem = JSQPhotoMediaItem(maskAsOutgoing: userid.boolValue ? true : false)
//                        //var message2 = JSQMessage(senderId: userid as! String, senderDisplayName: "Admin", date: date, media: mediaItem)
//                       // JSQMessage *message = [JSQMessage messageWithSenderId:sID displayName:sName date:date media:mediaItem];
//                       // if(message)
//                       // [self.messages addObject:message];
//
//                        /*JSQPhotoMediaItem *mediaItem = [[JSQPhotoMediaItem alloc] initWithMaskAsOutgoing:[sID isEqualToString:[NSString stringWithFormat:@"%@",self.senderId]]?TRUE:FALSE];
//                        [self addPhotoMessage:sID Name:snapshot.key MediaItem:mediaItem Date:[messageData objectForKey:@"date"]];
//                        NSString* photoURL = [messageData objectForKey:@"photoURL"];
//                        NSLog(@"photoURL %@",photoURL);
//                        if([photoURL hasPrefix:@"gs://"]) {
//                            [self fetchImageDataAtURL:photoURL MediaItem:mediaItem clearsPhotoMessageMapOnSuccessForKey:nil];
//                        }*/
//                    }
//
//                    chatView.OrderID = OrderID as NSString
//
//                   /* let date = Date()
//                    let formatter = DateFormatter()
//
//                    formatter.dateFormat = "dd.MM.yyyy"
//                    chatView.OrderID = OrderID as NSString
//                    let UserID = UserDefaults.standard.string(forKey: "UserID")*/
//
//                    //chatView.messages.append(message)
//                    self.present(chatNavigationController, animated: true, completion: nil)
                }
                
                break
                
            case .failure(let error):
                AppDelegate.shared().HideHUD()
                if error._code == NSURLErrorTimedOut {
                    //HANDLE TIMEOUT HERE
                }
                print("\n\nAuth request failed with error:\n \(error)")
                break
            }
        }
    }
    
}

