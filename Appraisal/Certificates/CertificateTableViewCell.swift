//
//  CertificateTableViewCell.swift
//  Appraisal
//
//  Created by ADMIN on 4/17/18.
//  Copyright © 2018 empiereTech. All rights reserved.
//

import UIKit

class CertificateTableViewCell: UITableViewCell {

    //Label
    @IBOutlet weak var Lbl_Customer: UILabel!
    @IBOutlet weak var Lbl_CertificateNumber: UILabel!
    @IBOutlet weak var Lbl_TrackingNumber: UILabel!
    
    //Image
    @IBOutlet weak var Img_OrderStatus: UIImageView!
    
    //Button
    @IBOutlet weak var Btn_Edit: UIButton!
    @IBOutlet weak var Btn_Download: UIButton!
    
    
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }
    
}
