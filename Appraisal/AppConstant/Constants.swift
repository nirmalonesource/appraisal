//
//  Constants.swift
//  JsonParsingWithSwift
//
//  Created by developer on 9/15/17.
//  Copyright © 2017 developer. All rights reserved.
//

import Foundation

extension UIView
{
    func setRadius(radius: CGFloat) {
        self.layoutIfNeeded()
        self.layer.cornerRadius = radius
        self.layer.masksToBounds = true
    }
    
    func setRadiusBorder(color: UIColor , weight : CGFloat = 2) {
        self.layoutIfNeeded()
        self.layer.borderColor = color.cgColor
        self.layer.borderWidth = weight
        self.layer.masksToBounds = true
    }
    
    func setDottedRadiusBorder(color: UIColor) {
        self.layoutIfNeeded()
        let border = CAShapeLayer()
        border.strokeColor = color.cgColor
        border.fillColor = nil
        border.lineDashPattern = [4, 4]
        border.path = UIBezierPath(rect: self.bounds).cgPath
        border.frame = self.bounds
        self.layer.addSublayer(border)
        self.layoutIfNeeded()
    }
    
    func roundCorners(corners: UIRectCorner, radius: CGFloat) {
        self.layoutIfNeeded()
        let path = UIBezierPath(roundedRect: bounds, byRoundingCorners: corners, cornerRadii: CGSize(width: radius, height: radius))
        let mask = CAShapeLayer()
        mask.path = path.cgPath
        layer.mask = mask
        self.layoutIfNeeded()
    }
    
    func setGradientBackground() {
        let colorTop =  UIColor.black.withAlphaComponent(1).cgColor
        let colorBottom = UIColor.white.withAlphaComponent(0.1).cgColor
        
        let gradientLayer = CAGradientLayer()
        gradientLayer.colors = [colorTop , colorTop, colorBottom]
        gradientLayer.locations = [0.0 , 0.05  , 1.0]
        gradientLayer.frame = self.bounds
        self.layer.mask = gradientLayer
    }
    
    func setGredientBorder()
    {
        let gradient = CAGradientLayer()
        gradient.frame =  CGRect(origin: CGPoint.zero, size: self.frame.size)
        gradient.colors = [UIColor.black.withAlphaComponent(0.7).cgColor,
                           UIColor.lightGray.withAlphaComponent(0.5).cgColor]
        
        let shape = CAShapeLayer()
        shape.lineWidth = 2
        shape.path = UIBezierPath(rect: self.bounds).cgPath
        shape.strokeColor = UIColor.black.cgColor
        shape.fillColor = UIColor.clear.cgColor
        gradient.mask = shape
        
        self.layer.addSublayer(gradient)
    }
    
    func setGredientDark()
    {
        self.layoutIfNeeded()
        let gradientLayer = CAGradientLayer()
        gradientLayer.frame.size = self.frame.size
        gradientLayer.colors =  [UIColor.lightGray.cgColor,
                                 UIColor.white.withAlphaComponent(1).cgColor].map{$0}
        self.layer.addSublayer(gradientLayer)
        self.layoutIfNeeded()
    }
    
  
    
    //Table Animation
    func loadAnimation( ) {
        self.transform = CGAffineTransform(translationX: 0, y: 50)
        self.alpha = 0
        
        UIView.animate(withDuration: 0.6, delay: 0.05, options: [.curveEaseInOut], animations: {
            self.transform = CGAffineTransform(translationX: 0, y: 0)
            self.alpha = 1
        }, completion: nil)
    }
    
    func loadAnimationForCollection( ) {
        self.transform = CGAffineTransform(translationX:50, y: 0)
        self.alpha = 0
        
        UIView.animate(withDuration: 0.6, delay: 0.05, options: [.curveEaseInOut], animations: {
            self.transform = CGAffineTransform(translationX: 0, y: 0)
            self.alpha = 1
        }, completion: nil)
    }
    
    func dampAnimation()
    {
        self.transform = CGAffineTransform(scaleX: 0.1, y: 0.1)
        UIView.animate(withDuration: 1.0,
                       delay: 0,
                       usingSpringWithDamping: 0.4,
                       initialSpringVelocity: 8.0,
                       options: .allowUserInteraction,
                       animations: {
                        self.transform = .identity
                        self.transform = CGAffineTransform(scaleX: 1.0, y: 1.0)
                        
        },
                       completion: nil)
    }
    
    func fadeIn(duration: TimeInterval = 0.5, delay: TimeInterval = 0.0, completion: @escaping ((Bool) -> Void) = {(finished: Bool) -> Void in }) {
        self.alpha = 0.0
        
        UIView.animate(withDuration: duration, delay: delay, options: UIView.AnimationOptions.curveEaseIn, animations: {
            self.isHidden = false
            self.alpha = 1.0
        }, completion: completion)
    }
    
    func fadeOut(duration: TimeInterval = 0.5, delay: TimeInterval = 0.0, completion: @escaping (Bool) -> Void = {(finished: Bool) -> Void in }) {
        self.alpha = 1.0
        
        UIView.animate(withDuration: duration, delay: delay, options: UIView.AnimationOptions.curveEaseIn, animations: {
            self.alpha = 0.0
        }) { (completed) in
            self.isHidden = true
            completion(true)
        }
    }
}

//MARK:- TextField
extension UITextField
{
    @IBInspectable var placeHolderColor: UIColor? {
        get {
            return self.placeHolderColor
        }
        set {
            self.attributedPlaceholder = NSAttributedString(string:self.placeholder != nil ? self.placeholder! : "", attributes:[NSAttributedString.Key.foregroundColor: newValue!])
        }
    }
    
    func setLeftPaddingPoints(_ amount:CGFloat , strImage : String){
        
        let paddingView = UIImageView(frame: CGRect(x: 15, y: 20, width: amount, height: 20))
        paddingView.image = UIImage(named : strImage)
       
        paddingView.contentMode = .scaleAspectFit
        self.leftView = paddingView
        self.leftViewMode = .always
    }
    
    func setRightPaddingPoints(_ amount:CGFloat , strImage : String){
        let paddingView = UIImageView(frame: CGRect(x: 15, y: 20, width: amount, height: 20))
        paddingView.image = UIImage(named : strImage)
        paddingView.contentMode = .scaleAspectFit
        self.rightView = paddingView
        self.rightViewMode = .always
    }
    
    func setBottomBorder() {
        self.borderStyle = .none
        self.layer.backgroundColor = UIColor.white.cgColor
        
        self.layer.masksToBounds = false
        self.layer.shadowColor = UIColor.black.cgColor
        self.layer.shadowOffset = CGSize(width: 0.0, height: 1.0)
        self.layer.shadowOpacity = 1.0
        self.layer.shadowRadius = 0.0
    }
    
    func isValidEmail() -> Bool {
        let emailRegEx = "[A-Z0-9a-z._%+-]+@[A-Za-z0-9.-]+\\.[A-Za-z]{2,64}"
        
        let emailTest = NSPredicate(format:"SELF MATCHES %@", emailRegEx)
        return emailTest.evaluate(with: self.text!)
    }
}

class Constants {
    
// MARK: List of Constants
    
    //API
  
        static let API = "https://faaausa.com/api/v3/web_services/"
    // static let API = "https://federal.blenzabi.com/api/v1/web_services/"
    
    //static let ImagePath = "http://faaausa.com/"
    
    //URL Sec
    static let LOGIN = "login"
    static let LOGOUT = "logout"
    static let ForgotPassword = "forgot_password"
    static let Support = "support"
    static let Profile = "profile"
    static let GetAllType = "type"
    static let AddOrder = "order"
    static let GetAppraisalList = "appraisal_list_iso"
    static let GetShippingMethod = "shipping_method"
    static let GetrderComment = "comment/"
    static let AddOrderComment = "comment/"
    static let AddOrderPayment = "order_payment"
    static let UserCardList = "user_card_list"
    static let AddUserCard = "add_user_card"
    static let RemoveCard = "delete_user_card"

    static let DefultCurrency = "USD"
    
    static let StripeChargeURL = "https://api.stripe.com/v1/charges"
    
    static let KEY_LIVE = "Bearer sk_live_hwRAqWmkEMEdk2F73HfUJK3c"
    static let KEY_TEST = "Bearer sk_test_8tWp3SdPGDrj3cNSnU0Ysefe"
    
    static let PUBLISHKEY_LIVE = "pk_live_n9dZgqsWUMjSJjx6SB5GEl4Q"
    static let PUBLISHKEY_TEST = "pk_test_d1VHmq3QUwqKIKyrTVkOGozA"
    
    //Alerts
    static let ERROR_HEADER = "Hey there!"
    static let ERROR_NO_INTERNET = "Connection Error"
    
    //Validation
    static let REGEX_USER_NAME_LIMIT = "^.{1,22}$"
    static let REGEX_USER_NAME = "[A-Za-z0-9 ]{1,22}"
    
    static let REGEX_Note_USER_NAME = "[A-Za-z0-9_ ]{1,24}"
    static let REGEX_SERIALNO = "^[A-Za-z0-9@.%!?/#*()@|\\- $]{1,20}"
// . / ? ! @ # $ %
   // /^[ A-Za-z0-9_@./#&+-]*$/
    static let REGEX_Note_USER_LIMIT = "^.{1,22}$"
    static let REGEX_EMAIL = "[A-Z0-9a-z._%+-]{3,}+@[A-Za-z0-9.-]+\\.[A-Za-z]{2,4}"
    static let REGEX_PASSWORD_LIMIT = "^.{5,20}$"
    static let REGEX_PASSWORD = "[A-Za-z0-9]{5,20}"
    static let REGEX_PHONE_DEFAULT = "[0-9]{10}"
    static let REGEX_OTP_DEFAULT = "[0-9]{6}"
    static let REGEX_AGE_DEFAULT = "[0-9]{1,2}"
    static let REGEX_WEIGHT_DEFAULT = "^.{1,20}$"
    
    
    
}
